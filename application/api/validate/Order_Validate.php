<?php
/**
 * Created by muniao.
 * User: muniao
 * Date: 2018/8/2
 * Time: 13:20
 */

namespace app\api\validate;


use think\Validate;

class Order_Validate extends Validate
{
    protected $rule =   [
        'addres'  => 'require',
        'contacts'   => 'require',
        'pro_id'   => 'require',
        'channel_id'   => 'require',

    ];
    protected $message  =   [
        'addres.require' => '请输入详细地址',
        'contacts.require'     => '请输入联系人姓名',
        'pro_id.require'     => '不能为空',
        'channel_id.require'     => '不能为空',
    ];



}