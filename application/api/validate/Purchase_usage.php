<?php
/**
 * Created by muniao.
 * User: muniao
 * Date: 2018/8/2
 * Time: 13:20
 */

namespace app\api\validate;


use think\Validate;

class Purchase_usage extends Validate
{
    protected $rule =   [
        'stock_name'  => 'require',
        'company'   => 'require',

    ];
    protected $message  =   [
        'stock_name.require' => '材料名称必须',
        'company.require'     => '单位必须',
    ];



}