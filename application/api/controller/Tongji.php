<?php
/**
 * Created by muniao.
 * User: muniao
 * Date: 2018/8/1
 * Time: 10:40
 */

namespace app\api\controller;


use app\api\model\AuditMaterials;
use app\api\model\Capital;
use app\api\model\PollingModel;
use think\Controller;
use think\Db;
use think\Exception;
use app\index\model\Jpush;
use think\Request;
use app\api\model\Common;
use phpcvs\Csv;
use app\index\model\Pdf;
use app\api\model\OrderModel;
use function GuzzleHttp\Psr7\str;

class Tongji extends Controller
{
    //$description  = \db('user_config', config('database.zong'))->where('key', 'action_check')->value('value');
    //        $action_check = json_decode($description, true);
    //        foreach ($action_check as $k=>$l) {
    //            if ($l["type"] == 1) {
    //                $action_check[$k]['content'] = "跟单备注：接单15分钟之内响应，不管是否联系上客户，都需要点击“待接单”操作反馈。只要没有标注上门时间，订单就会跳转为“待处理”状态；标注了上门时间，订单就会跳转为“待上门”状态。“待处理”的订单需要呈现每次的沟通记录。“待上门”的订单如果有时间变动要及时修改。";
    //            } elseif ($l["type"] == 2) {
    //                $action_check[$k]['content'] = "打卡：到达客户家点击“扫码打卡”，客户扫描关注公众号及小程序，即为打卡成功，不能用自己手机操作打卡，系统是能够识别出来的，违规操作会停单处罚。";
    //            } elseif ($l["type"] == 3) {
    //                $action_check[$k]['content'] = "
    //                               （1）先仔细询问了解客户家的房屋情况，添加房屋标签。根据现场掌握的客户信息，添加客户标签。
    //                               （2）报价遵循逻辑，优先标准产品、其次清单报价、最后非标报价。
    //                               （3）赠送项目也必须清单报价，标注区域、施工内容、产品名称及规格，赠送的金额不能超过400元。";
    //            } elseif ($l["type"] == 4) {
    //                $action_check[$k]['content'] = "（1）质保与清单和施工范围关联，需和业主现场确认留底。
    //                               （2）质保项目必须添加质保卡，选择对应质保项目，特殊约定质保项目可以自定义添加，但是尽量避免使用。";
    //            } elseif ($l["type"] == 5) {
    //                $action_check[$k]['content'] = "
    //                                （1）再次核对项目地址，信息填写完整。
    //                                （2）根据项目选择工程内容，可以多选。
    //                                （3）开完工时间确认：提前跟店长或后端确认，如果现场不确定，可以选择“不能确定开工时间”。
    //                                （4）收款标准：合同金额超过2万元的，必须分三次收款，中期款根据实际情况项目清单节点；2万以下视情况一次性或两次收款。收款日期可以选择签合同当日或者开工当日。
    //                                （5）如跟客户有合同外的合理约定，比如对漆的品牌要求，施工细节要求，可以添加说明。
    //                                （6）代购合同选择一次性或两次收款，如果客户没有付款，会影响整体的时间交付。
    //                                （7）如果有项目信息修改可以选择“重签”，按照之前标准操作。如确认无误，直接“分享至小程序签约”，签约必须由客户本人签字确认，不能签单店长代签，否则视为违反公司红线标准。注意签字分为主合同签字和代购合同签字。
    //                                （8）发起收款，选择支付方式，如果是平台订单，选择对应的平台支付方式（嵌入支付流程），如果不按规范操作平台支付，将会影响个人在平台的接单。如果是现金收款，由收款人24小时之内通过收款码支付到公司账户，再在“现金支付”上传付款凭证。严禁私人二维码收款，否则视为违反公司红线标准。";
    //            } elseif ($l["type"] == 6) {
    //                $action_check[$k]['content'] =  "签合同当天建企业微信群，当天发施工计划初期安排，选择开工日期及完工日期，不然师傅没有办法添加师傅施工。";
    //                foreach ($l["choice"] as $o=>$v) {
    //                    if ($v["type"] == 1) {
    //                        $action_check[$k]["choice"][$o]['content'] = "签约当天要把施工区域图文、视频上传App及企业微信群。视频要求：从入户到施工区域完整空间展示。图片要求：施工区域细节拍照并文字标注，标记赠送项目。备注内容：特殊情况、注意事项、客户的喜好等。查看代购合同，跟进材料进场时间。";
    //                    } elseif ($v["type"] == 2) {
    //                        $action_check[$k]["choice"][$o]['content'] = "检核师傅是否每个工地都上工打卡";
    //                    } elseif ($v["type"] == 3) {
    //                        $action_check[$k]["choice"][$o]['content'] = "提前跟师傅确定时间安排，添加师傅，如果不添加师傅，师傅有权拒绝施工。如果不及时添加师傅，将会影响自己的考核及后续的接单。";
    //                    } elseif ($v["type"] == 4) {
    //                        $action_check[$k]["choice"][$o]['content'] = "检核师傅是否每个工地都上工打卡";
    //                    } elseif ($v["type"] == 5) {
    //                        $action_check[$k]["choice"][$o]['content'] = "检核师傅是否按标准上传节点照片、每日完工视频，严格审核";
    //                    } elseif ($v["type"] == 6) {
    //                        $action_check[$k]["choice"][$o]['content'] = "完工自检，对项目核心工序结果进行验收、项目完工交付给客户前先自检。验收和自检结果上传致App，同时结果同步企业微信施工群";
    //                    }
    //                }
    //
    //            } elseif ($l["type"] == 7) {
    //                $action_check[$k]['content'] = "验收合格后操作“验收”，否则会影响代购分红结算。";
    //            } elseif ($l["type"] == 8) {
    //                $action_check[$k]['content'] = "选择报销类型：
    //                                1）材料报销
    //                                公司已有集采的渠道必须走公司集采，临时200元以下的材料采购需要按照以下标准报销（请款）
    //                                ＊材料类报销必须开具发票，实在不能取得发票，需出具商家手写的正规带编号的收据
    //                                ＊收据上写明商品名称、规格、数量、单价、金额、摘要等所有应填写的要素应填写齐全；
    //                                ＊收据上必须写上商家联系电话、项目地址，金额不得涂改缺省；
    //                                ＊提交APP报销时必须填写上收据编号；
    //                                ＊如存在业务真实但因编号重复会被app驳回情况，需要单独走流程跟财务申请编号补充。
    //                                2）协作人工报销
    //                                要核算外部协作人员的人工费，需要外部协作人员开具正规的收据，收据包含的要素：项目名称，清单明细，方量，单价，金额。收款人名字，电话 ，身份证号码。以上要素必须齐全完整，否则不予审批。
    //                                3）建渣打拆费
    //                                主要核算项目的建渣打拆费用，需要商家开具收据，收据包含的要素：项目名称，清单明细，方量，单价，金额。收款人名字，电话 ，身份证号码。另需附施工图片。
    //                                4）运费
    //                                 主要核算项目材料的运费，需要商家手写的正规带编号的收据或第三方运输公司具体行程单（清单需包含：起始地，目的地，金额，日期）";
    //            } elseif ($l["type"] == 9) {
    //                $action_check[$k]['content'] = "选择项目明细，手动填写报销金额，收据编号，选择供应商，如果没有对应对应供应商信息，联系材料部后台添加。上传收据凭证，核实无误，确认提交。如果发现弄虚作假，视为违反公司红线标准。";
    //            } elseif ($l["type"] == 10) {
    //                $action_check[$k]['content'] = "要核算外部协作人员的人工费，需要外部协作人员开具正规的收据，收据包含的要素：项目名称，清单明细，方量，单价，金额。收款人名字，电话 ，身份证号码。以上要素必须齐全完整，否则不予审批";
    //            } elseif ($l["type"] == 11) {
    //                $action_check[$k]['content'] = "1、核对人工总价，核对师傅是否操作标记工时，如果师傅没有添加工时，师傅无法结算提成。如果是协作师傅施工，可以在人工预算里面标记“协作标记”。
    //                                2、结算前先检查确认所有的材料领用、费用报销、人工结算标记都无误后再点击确认。一旦结算后不能再操作费用报销和人工结算";
    //            } elseif ($l["type"] == 12) {
    //                $action_check[$k]['content'] = "1、核对人工总价，核对师傅是否操作标记工时，如果师傅没有添加工时，师傅无法结算提成。如果是协作师傅施工，可以在人工预算里面标记“协作标记”。
    //                                2、结算前先检查确认所有的材料领用、费用报销、人工结算标记都无误后再点击确认。一旦结算后不能再操作费用报销和人工结算";
    //            }
    //        }
    //       echo  json_encode($action_check);die;
    /**
     * 取消原因数据统计
     */
    public function cancel()
    {
        $p = db('user')->where(['status' => 0, 'ce' => 1])->field('user_id,username')->select();//店长
        foreach ($p as $item) {
            $o     = ['对益鸟不满意', '价格比其他家高', '价格比预期高', '客户嫌麻烦', '超出业务范围', '没有时间', '其它'];
            $count = db('order')->where(['state' => 8, 'assignor' => $item['user_id']])->field('reason')->select();//取消量
            
            $qux_count  = [];
            $qux_count1 = [];
            $qux_count2 = [];
            $qux_count3 = [];
            $qux_count4 = [];
            $qux_count5 = [];
            $qux_count6 = [];
            foreach ($count as $value) {
                
                if (stristr($value['reason'], $o[0])) {
                    $qux_count [] = $value;
                }
                if (stristr($value['reason'], $o[1])) {
                    
                    $qux_count1 [] = $value;
                }
                if (stristr($value['reason'], $o[2])) {
                    
                    $qux_count2 [] = $value;
                }
                if (stristr($value['reason'], $o[3])) {
                    $qux_count3 [] = $value;
                }
                if (stristr($value['reason'], $o[4])) {
                    $qux_count4 [] = $value;
                }
                if (stristr($value['reason'], $o[5])) {
                    $qux_count5 [] = $value;
                }
                if (stristr($value['reason'], $o[6])) {
                    $qux_count6 [] = $value;
                }
                $sku = [
                    'user_id' => $item['user_id'],//店长id
                    'user_name' => $item['username'],//店长姓名
                    'count' => count($count),//取消订单量
                    'qux_count' => !empty($qux_count) ? count($qux_count) : 0, //取消占比
                    'qux_count1' => !empty($qux_count1) ? count($qux_count1) : 0, //取消占比
                    'qux_count2' => !empty($qux_count2) ? count($qux_count2) : 0, //取消占比
                    'qux_count3' => !empty($qux_count3) ? count($qux_count3) : 0, //取消占比
                    'qux_count4' => !empty($qux_count4) ? count($qux_count4) : 0, //取消占比
                    'qux_count5' => !empty($qux_count5) ? count($qux_count5) : 0, //取消占比
                    'qux_count6' => !empty($qux_count6) ? count($qux_count6) : 0, //取消占比
                    'time' => time() //取消占比
                
                
                ];
                
                $data["star"] = strtotime(date("Y-m-d", time()) . " 0:0:0");
                $data["end"]  = strtotime(date("Y-m-d", time()) . " 24:00:00");
                
                $sky = db('cancel')->where(['user_id' => $item['user_id'], 'time' => ['between', [$data["star"], $data["end"]]]])->find();
                if ($sky) {
                    db('cancel')->where(['user_id' => $item['user_id'], 'time' => ['between', [$data["star"], $data["end"]]]])->update($sku);
                    
                } else {
                    db('cancel')->insertGetId($sku);
                }
                
            }
            
            
        }
        
    }
    
    public function weixin()
    {
        $xlsCell = ['姓名', '加微信', '未加微信'];
        $entry   = db('order', config('database.zong'))->field('entry')->group('entry')->column('entry');
        foreach ($entry as $k => $imt) {
            $state                      = db('order', config('database.zong'))->where('entry', $imt)->where('wechat', 0)->count();
            $states                     = db('order', config('database.zong'))->where('entry', $imt)->where('wechat', 1)->count();
            $ues[$k]['entry']           = $imt;
            $ues[$k]['channel_details'] = $states;
            $ues[$k]['channel_id']      = $state;
        }
        vendor('phpcvs.Csv');
        $csv = new Csv();
        $csv->export($ues, $xlsCell);
        return res_date($ues);
    }
    
    public function manager()
    {
        $p = db('user')->where(['status' => 0, 'ce' => 1])->field('user.user_id,user.username,st.store_name')
            ->join('store st', 'st.store_id=user.store_id', 'left')
            ->select();//店长
        foreach ($p as $k) {
            $BeginDate   = date('Y-m-01', strtotime(date("Y-m-d", time())));
            $starttime   = strtotime($BeginDate);
            $enttime     = strtotime(date('Y-m-d', strtotime("$BeginDate +1 month -1 day")));
            $count       = db('order')
                ->field('planned,state,through_id,order_id')
                ->where(['order.created_time' => ['between', [$starttime, $enttime]], 'order.assignor' => $k['user_id']])
                ->select();//接单量
            $s           = '';
            $you_count   = [];
            $shang_count = [];
            foreach ($count as $value) {
//                $planned = db('planned')->field('planned as planneds')
//                    ->where(['order_id' => $value['order_id']])
//                    ->order('id desc')
//                    ->find();//接单量
                if (!empty($value['planned'])) {
                    $shang_count[] = $value;//上门量
                }
                
                
                if ($value['state'] != 10) {
                    $you_count[] = $value;//有效订单
                }
                $s .= $value["order_id"] . ',';
            }
            
            $s    = substr($s, 0, -1);
            $thro = db('through')->where(['order_ids' => ['in', $s], 'capital_id' => ['<>', ' ']])->field('baocun')->select();//远程沟通
            
            $g1 = db('contract')->where(['con_time' => ['between', [$starttime, $enttime]]])->field('orders_id')->select();
            $s1 = '';
            foreach ($g1 as $key => $val) {
                $s1 .= $val["orders_id"] . ',';
            }
            $s1    = substr($s1, 0, -1);
            $deal1 = db('order')->where(['order_id' => ['in', $s1], 'assignor' => $k['user_id']])->field('order_id')->select();
            $deal  = count($deal1);//成交量
            
            if ($thro) {
                $remotely = [];
                foreach ($thro as $value) {
                    if ($value['baocun'] == 1) {
                        $remotely[] = $value;
                    }
                }
            } else {
                $remotely = [];
            }
            
            if (!empty($you_count) && !empty($count)) {
                $effective = (count($you_count) / count($count)) * 100; //有效率
            } else {
                $effective = 0;
            }
            //上门率
            if (!empty($shang_count) && !empty($you_count)) {
                $doorto = (count($shang_count) / count($you_count)) * 100;
            } else {
                $doorto = 0;
            }
            //上门成交率
            if (!empty($deal) && !empty($shang_count)) {
                $turnovers = ($deal / count($shang_count)) * 100;
            } else {
                $turnovers = 0;
            }
            //订单成交率
            if (!empty($deal) && !empty($you_count)) {
                $orderturnover = ($deal / count($you_count)) * 100;
            } else {
                $orderturnover = 0;
            }
            
            
            if (!empty($remotely) && !empty($thro)) {
                
                $Remote_communication = (count($remotely) / count($thro)) * 100;
            } else {
                
                $Remote_communication = 0;
            }
            $dian         = [
                'user_id' => $k['user_id'],//接单店长
                'store_name' => $k['store_name'],//
                'username' => $k['username'],//接单店长
                'count' => count($count), //接单量
                'you_count' => count($you_count), //有效订单
                'you_rate' => round($effective, 3), //有效率
                'shang_count' => count($shang_count), //上门量
                'thro' => count($thro), //远程沟通报价
                'deal' => $deal,//成交量
                'doorto' => round($doorto, 3),//上门率
                'turnovers' => round($turnovers, 3),//上门成交率
                'orderturnover' => round($orderturnover, 3),//订单成交率
                'thro_count' => count($remotely),//远程沟通成交量
                'remote_communication' => round($Remote_communication, 3),//远程沟通成交量
                'date' => time(),//远程沟通成交量
            ];
            $data["star"] = strtotime(date("Y-m-d", time()) . " 0:0:0");
            $data["end"]  = strtotime(date("Y-m-d", time()) . " 24:00:00");
            $sky          = db('conversion')->where(['date' => ['between', [$data["star"], $data["end"]]], 'user_id' => $k['user_id']])->find();
            
            if ($sky) {
                db('conversion')->where(['date' => ['between', [$data["star"], $data["end"]]], 'user_id' => $k['user_id']])->update($dian);
                
            } else {
                db('conversion')->insertGetId($dian);
            }
            
        }
        
        
    }
    
    public function xin()
    {
        
        $p = db('user')->where(['status' => 0, 'ce' => 1])->field('user_id')->select();//店长
        foreach ($p as $k) {
            
            $BeginDate = date('Y-m-01', strtotime(date("Y-m-d", time())));
            $starttime = strtotime($BeginDate);
            $enttime   = strtotime(date('Y-m-d', strtotime("$BeginDate +1 month -1 day")));
            //接单店长
            $argv = db('response')->where(['user_id' => $k['user_id'], 'operating' => ['between', [$starttime, $enttime]], 'assign' => ['<>', 0]])->select();
            
            if (count($argv) == 0) {
                $response = 0;
            } else {
                $p = 0;
                foreach ($argv as $value) {
                    if (!empty($value['assign'])) {
                        $p += $value['operating'] - $value['assign'];
                    }
                }
                $response = ($p / count($argv)) / 60;
            }
            //上门及时性
            $timeliness = db('timeliness')->where(['user_id' => $k['user_id'], 'time' => ['between', [$starttime, $enttime]]])->select();
            if (count($timeliness) == 0) {
                $home = 0;
            } else {
                $p1 = 0;
                foreach ($timeliness as $value) {
                    $p1 += $value['startTime'] - $value['planned'];
                }
                $home = ($p1 / count($timeliness)) / 60;
            }

//            //报价及时性
//            $quo = db('quote')->where(['user_id' => $k['user_id'], 'time' => ['between', [$starttime, $enttime]]])->select();
//            if (count($quo) == 0) {
//                $quote = 0;
//            } else {
//                $p2 = 0;
//                foreach ($quo as $value) {
//                    $p2 += $value['time'] - $value['startTime'];
//                }
//
//                $quote = ($p2 / count($quo)) / 60;
//            }

//            //反馈及时性
//            $feed = db('message')->where(['user_id' => $k['user_id'], 'time' => ['between', [$starttime, $enttime]], 'type' => 6])->select();
//            if (count($feed) == 0) {
//                $feedback = 0;
//            } else {
//                $p3 = 0;
//                foreach ($feed as $value) {
//                    if (!empty($value['have'])) {
//                        $p3 += $value['have'] - $value['time'];
//                    }
//
//                }
//                $feedback = ($p3 / count($feed)) / 60;
//            }
//            //下次跟进及时性
//            $count = db('message')->where(['user_id' => $k['user_id'], 'time' => ['between', [$starttime, $enttime]], 'type' => 3])->select();
//            if (count($count) == 0) {
//                $next = 0;
//            } else {
//                $p4 = 0;
//                foreach ($count as $value) {
//                    if (!empty($value['have'])) {
//                        $p4 += $value['have'] - $value['time'];
//                    }
//
//                }
//
//                $next = ($p4 / count($count)) / 60;
//            }
            
            //平均签单时间
            $trans = db('order')->field('order.order_id,order.assignor,cn.con_time')->join('contract cn', 'cn.orders_id=order.order_id', 'left')->where(['cn.con_time' => ['between', [$starttime, $enttime]], 'order.assignor' => $k['user_id']])->select();
            
            if (count($trans) == 0) {
                $average = 0;
            } else {
                $p5 = 0;
                foreach ($trans as $value) {
                    $me = db('message')->where(['type' => 6, 'order_id' => $value['order_id'], 'user_id' => $k['user_id']])->field('have')->find();
                    if (!empty($me['have'])) {
                        $p5 += $value['con_time'] - $me['have'];
                    }
                    
                }
                
                $average = ($p5 / count($trans));
                
                $value["hours"]   = floor(abs($average) / 3600);
                $time             = (abs($average) % 3600);
                $value["minutes"] = floor($time / 60);
                
                $average = $value["hours"] . "小时" . $value["minutes"] . "分";
            }
            
            //远程跟进频次
            $or = db('order')->where(['assignor' => $k['user_id'], 'created_time' => ['between', [$starttime, $enttime]]])->field('order_id')->select();
            if (count($or) == 0) {
                $remotely = 0;
            } else {
                $s = '';
                foreach ($or as $value) {
                    $s .= $value['order_id'] . ',';
                }
                $s        = substr($s, 0, -1);
                $p6       = db('through')->where(['order_ids' => ['in', $s], 'role' => 2])->count();
                $remotely = $p6 / count($or);
                
            }
            
            //消息使用及时性
            $me = db('message')->where(['user_id' => $k['user_id'], 'time' => ['between', [$starttime, $enttime]]])->select();
            if (count($me) == 0) {
                $message = 0;
            } else {
                $p7 = 0;
                foreach ($me as $value) {
                    if (!empty($value['have'])) {
                        $p7 += $value['have'] - $value['time'];
                    }
                }
                $message = ($p7 / count($me)) / 60;
            }
            //页面停留时长
            $stay = db('stay')->where(['user_id' => $k['user_id'], 'time' => ['between', [$starttime, $enttime]]])->select();
            if (count($stay) == 0) {
                $page = 0;
            } else {
                $p8 = 0;
                foreach ($stay as $value) {
                    if (!empty($value['endTime'])) {
                        $p8 += $value['endTime'] - $value['startTime'];
                    }
                    
                }
                $page = ($p8 / count($stay)) / 60;
            }
            //店长活跃位置统计
            $sql  = "select `address` , count(*) AS count from `active` where user_id={$k['user_id']} AND `time` BETWEEN {$starttime} AND {$enttime} group by address order by count DESC LIMIT 1";
            $stay = db()->query($sql);
            if (count($stay) == 0) {
                $active = '';
            } else {
                $active = $stay[0]['address'];
            }
            $data["star"] = strtotime(date("Y-m-d", time()) . " 0:0:0");
            $data["end"]  = strtotime(date("Y-m-d", time()) . " 24:00:00");
            //店长在线时长统计
            $online = db('online')->where(['user_id' => $k['user_id'], 'time' => ['between', [$starttime, $enttime]]])->order('id asc')->select();
            $p9     = 0;
            if ($online) {
                for ($i = 0; $i < count($online); $i++) {
                    if ($online[$i]['type'] == 0 && $i == 0) {
                        echo 1;
                        $p9 += abs($online[$i]['startTime'] - $starttime);
                    }
                    
                    if ($online[$i]['type'] == 0 && $i != 0) {
//                        if(!empty($online[$i-1]['startTime']) &&  $online[$i-1]['type']==1){
//                            echo 2;
//                            $p9 += abs($online[$i]['startTime'] - $online[$i-1]['startTime']);
//
//                        }
                    
                    } elseif ($online[$i]['type'] == 1) {
                        
                        if (!empty($online[$i + 1]['startTime']) && $online[$i + 1]['type'] == 0) {
                            echo 3;
                            
                            $p9 += abs($online[$i]['startTime'] - $online[$i + 1]['startTime']);
                            
                        } elseif (empty($online[$i + 1]['startTime'])) {
                            echo 4;
                            $p9 += abs($online[$i]['startTime'] - time());
                            
                        }
                    }
                    
                }
            } else {
                $p9 = time() - $starttime;
            }
//
            $time   = $p9;
            $d      = floor($time / (3600 * 24));
            $h      = floor(($time % (3600 * 24)) / 3600);
            $m      = floor((($time % (3600 * 24)) % 3600) / 60);
            $online = $d . '天' . $h . '小时' . $m;
            
            //app使用时长统计
            $usageTime = db('usagetime')->where(['user_id' => $k['user_id'], 'time' => ['between', [$starttime, $enttime]]])->select();
            if (count($usageTime) == 0) {
                $app = 0;
            } else {
                $p11 = 0;
                foreach ($usageTime as $value) {
                    if (!empty($value['endTime'])) {
                        $p11 += $value['endTime'] - $value['startTime'];
                    }
                    
                }
                
                $app = ($p11) / 3600;
            }
            //店长上门距离统计
            $act = db('active')->where(['user_id' => $k['user_id'], 'time' => ['between', [$starttime, $enttime]]])->select();
            
            $distance = 0;
            for ($i = 0; $i < count($act) - 1; $i++) {
                $distance += $this->get_distance([$act[$i]['lng'], $act[$i]['lat']], [$act[$i + 1]['lng'], $act[$i + 1]['lat']]);  // 73.734589823361
                
            }
            $dian = [
                'user_id' => $k['user_id'],//接单店长
                'response' => round($response, 3),//接单响应时长
                'home' => round(abs($home), 3),//上门及时性
                'quote' => 0,//报价及时性
                'feedback' => 0, //反馈及时性
                'next' => 0, //下次跟进及时性
                'average' => $average, //平均签单时间
                'remotely' => round($remotely, 3), //远程跟进频次月
                'message' => round($message, 3), //消息使用及时性
                'page' => round($page, 3), //页面停留时长
                'active' => $active,//店长活跃位置统计
                'online' => $online,//店长在线时长统计
                'app' => round($app, 3),//app使用时长统计
                'distance' => round($distance, 3),//店长上门距离统计
            ];
            
            $sky = db('store_manager')->where(['user_id' => $k['user_id'], 'time' => ['between', [$data["star"], $data["end"]]]])->find();
            if ($sky) {
                db('store_manager')->where(['user_id' => $k['user_id'], 'time' => ['between', [$data["star"], $data["end"]]]])->update($dian);
                
            } else {
                $dian['time'] = time();//时间
                db('store_manager')->insertGetId($dian);
                
            }
            
        }
        
        
    }
    
    function get_distance($from, $to, $km = true, $decimal = 2)
    {
        sort($from);
        sort($to);
        $EARTH_RADIUS = 6370.996; // 地球半径系数
        
        $distance = $EARTH_RADIUS * 2 * asin(sqrt(pow(sin(($from[0] * pi() / 180 - $to[0] * pi() / 180) / 2), 2) + cos($from[0] * pi() / 180) * cos($to[0] * pi() / 180) * pow(sin(($from[1] * pi() / 180 - $to[1] * pi() / 180) / 2), 2))) * 1000;
        
        if ($km) {
            $distance = $distance / 1000;
        }
        
        return round($distance, $decimal);
    }
    
    /*
     * 品类成交率
     */
    public function category()
    {
        
        $now       = time();
        $time      = strtotime('-3 month', $now);
        $times     = strtotime('-1 month', $now);
        $beginTime = date('Y-m-d 00:00:00', mktime(0, 0, 0, date('m', $time), 1, date('Y', $time)));
        $endTime   = date('Y-m-d 23:39:59', mktime(0, 0, 0, date('m', $times), date('t', $times), date('Y', $times)));
        $start     = strtotime($beginTime);//获取指定月份的第一天
        $end       = strtotime($endTime); //获取指定月份的最后一天
        $g1        = db('goods_category')->where('parent_id', 0)->where('type', 1)->where('pro_type', 1)->field('id')->select();
        foreach ($g1 as $value) {
            
            $user      = db('user')->where('status', 0)->where('store_id', '<>', 4)->where('get_order', 1)->field('user_id')->select();
            $userStore = [];
            foreach ($user as $k => $item) {
                $jie      = 0;
                $cheng    = 0;
                $fraction = 0;
                //成交量
                $contract  = db('contract')->Join('order', 'order.order_id=contract.orders_id', 'left')->whereBetween('order.state', [4, 7])->whereBetween('contract.con_time', [$start, $end])->where('contract.type', 1)->where('order.assignor', $item['user_id'])->where('order.pro_id', $value['id'])->field('contract.orders_id,order.created_time,order.channel_details')->select();
                $order_ids = [];
                $order     = [];
                foreach ($contract as $key => $val) {
                    $order_ids[] = $val['orders_id'];
                    if ($val['created_time'] < $start) {
                        $order[] = $val['orders_id'];
                    }
                }
                $cheng += count($contract);
                //接单量
                $jie += db('order')->whereBetween('created_time', [$start, $end])->where('assignor', $item['user_id'])->where('order.pro_id', $value['id'])->count();
                //无效订单
                $jie -= db('order')->whereBetween('created_time', [$start, $end])->where('assignor', $item['user_id'])->where('order.pro_id', $value['id'])->where('state', 10)->count();
                //退单
                $cheng -= db('order')->whereBetween('created_time', [$start, $end])->where('assignor', $item['user_id'])->where('order.pro_id', $value['id'])->whereBetween('tui_time', [$start, $end])->where('state', 9)->count();
                //活动订单
                $jie -= db('order')->whereBetween('created_time', [$start, $end])->where('assignor', $item['user_id'])->where('order.pro_id', $value['id'])->where('state', '<>', 10)->where('channel_details', 96)->count();
                //历史活动订单
                $jie += count($order);
                //成交的活动订单
                if (!empty($order_ids)) {
                    $jie += db('order')->whereIn('order_id', $order_ids)->where('channel_details', 96)->count();
                }
                if ($cheng > 0) {
                    $turnovers = empty($jie) ? 0 : sprintf('%.2f', ($cheng / $jie) * 100);//上门成交率
                } else {
                    $turnovers = 0;
                }
                
                $userStore[$k]['turnovers']         = $turnovers;
                $userStore[$k]['goods_category_id'] = $value['id'];
                $userStore[$k]['order_quantity']    = $jie;
                $userStore[$k]['clincha_deal']      = $cheng;
                $userStore[$k]['user_id']           = $item['user_id'];
            }
            $last_names = array_column($userStore, 'turnovers');
            array_multisort($last_names, SORT_DESC, $userStore);
            $count = count($userStore);
            $count = ceil($count / 10);
            
            foreach ($userStore as $key => $it) {
                $key = $key + 1;
                if ($key > 0 && $key <= $count) {
                    $fraction = 10;
                } elseif ($key > $count && $key <= $count * 2) {
                    $fraction = 9;
                } elseif ($key > $count && $key <= $count * 3) {
                    $fraction = 8;
                } elseif ($key > $count && $key <= $count * 4) {
                    $fraction = 7;
                } elseif ($key > $count && $key <= $count * 5) {
                    $fraction = 6;
                } elseif ($key > $count && $key <= $count * 6) {
                    $fraction = 5;
                } elseif ($key > $count && $key <= $count * 7) {
                    $fraction = 4;
                } elseif ($key > $count && $key <= $count * 8) {
                    $fraction = 3;
                } elseif ($key > $count && $key <= $count * 9) {
                    $fraction = 2;
                } elseif ($key > $count && $key <= $count * 10) {
                    $fraction = 1;
                }
                $po = [
                    'goods_category_id' => $it['goods_category_id'],
                    'user_id' => $it['user_id'],
                    'turnovers' => $it['turnovers'],
                    'fraction' => $fraction,
                    'order_quantity' => $it['order_quantity'],
                    'clincha_deal' => $it['clincha_deal'],
                    'creation_time' => time(),
                    'pai' => $key,
                    'city' => 241,
                ];
                
                $category = Db::connect(config('database.cc'))->table('category')->where('goods_category_id', $it['goods_category_id'])->where('city', 241)->where('user_id', $it['user_id'])->find();
                if ($category) {
                    Db::connect(config('database.cc'))->table('category')->where('goods_category_id', $it['goods_category_id'])->where('city', 241)->where('user_id', $it['user_id'])->update($po);
                } else {
                    Db::connect(config('database.cc'))->table('category')->insertGetId($po);
                }
            }
            
            
        }
        
    }
    
    public function tui()
    {
        
        $param     = Request::instance()->get();
        $xlsCell   = ['时间', '城市', '推客类别', '物业', '社区名称', '推单量', '有效单量(上门)', '成交量', '成交额', '上门奖金', '成交奖金'];
        $capital   = db('capital')
            ->field('sum(to_price) as to_price,ordesr_id')
            ->where(['capital.types' => 1, 'capital.enable' => 1, 'capital.agency' => 0])
            ->group('ordesr_id')
            ->buildSql();
        $envelopes = db('envelopes')
            ->field('sum(envelopes.give_money) as give_money,sum(envelopes.expense) as expense,ordesr_id')
            ->where(['envelopes.type' => 1])
            ->group('ordesr_id')
            ->buildSql();
        $income    = db('income')
            ->field('sum(income.commission) as commission,sum(income.deal) as deal,order_id')
            ->where(['income.state' => 1])
            ->group('order_id')
            ->buildSql();
        $clock_in  = db('clock_in')
            ->field('count(id) as count,order_id')
            ->group('order_id')
            ->buildSql();
        $list      = db('personal')
            ->join('order', 'order.tui_jian=personal.personal_id', 'left')
            ->join('personal personals', 'personals.personal_id=personal.num', 'left')
            ->join('group_community group_community', 'personals.group_id=group_community.group_id', 'left')
            ->join('group_community group_community1', 'personal.residential_quarters=group_community1.group_id', 'left')
            ->join([$income => 'income'], 'order.order_id=income.order_id ', 'left')
            ->join([$clock_in => 'clock_in'], 'order.order_id=clock_in.order_id', 'left')
            ->join([$envelopes => 'envelopes'], 'envelopes.ordesr_id=order.order_id', 'left')
            ->join([$capital => 'capital'], 'capital.ordesr_id=order.order_id', 'left')
            ->join('user', 'user.user_id = order.assignor', 'left')
            ->join('city', 'city.city_id = order.city_id', 'left');;
        
        $data = $list->where('user.ce', 'neq', 2)->where('order.created_time', '>', 1609430400)->field('FROM_UNIXTIME(order.created_time,"%Y年%m月%d %H:%i:%S") as created_time,city.city,if(personal.num !=0,"物业推客","个人推客") as tui,group_community.username,group_community1.username as username1,count(distinct order.order_id) as quan,clock_in.count as you,count(distinct CASE WHEN order.state<=7 and order.state>3  THEN order.order_id ELSE NULL END) as count,concat(capital.to_price + envelopes.expense-envelopes.give_money) as sum,income.commission,income.deal')->group('FROM_UNIXTIME(order.created_time,"%Y-%m"),city.city,group_community.username,group_community1.username')->order('created_time desc')->select();
        vendor('phpcvs.Csv');
        $csv = new Csv();
        $csv->export($data, $xlsCell);
        
        
    }
    
    public function tuige()
    {
        $xlsCell = ['推客类别', '推客姓名', '推单量', '有效单量(上门)', '成交量', '成交额', '上门奖金', '成交奖金'];
        
        $list = db('personal')
            ->join('order', 'order.tui_jian=personal.personal_id', 'left')
            ->join('clock_in', 'order.order_id=clock_in.order_id', 'left')
            ->join('user', 'user.user_id = order.assignor', 'left');
        
        $data = $list->where('user.ce', 'neq', 2)->where('order.created_time', '>', 1609430400)->field('if(personal.num !=0,"物业推客","个人推客") as tui,personal.username,count(distinct order.order_id) as quan,count(distinct clock_in.order_id) as you,count(distinct CASE WHEN order.state<=7 and order.state>3  THEN order.order_id ELSE NULL END) as count,personal.personal_id')->group('personal.personal_id')->order('quan desc')->select();
        
        foreach ($data as $k => $item) {
            $order_id = db('order')->where('tui_jian', $item['personal_id'])->column('order_id');
            $sum      = db('capital')
                ->where('capital.types', 1)->where('capital.enable', 1)->where('capital.agency', 0)
                ->whereIn('capital.ordesr_id', $order_id)->sum('to_price');
            
            $envelopes = db('envelopes')->whereIn('envelopes.ordesr_id', $order_id)->where('type', 1)->field('ifnull(sum(envelopes.give_money),0) as give_money, ifnull(sum(envelopes.expense),0) as expense')->select();
            
            $income                 = db('income')->whereIn('income.order_id', $order_id)->where('state', 1)->field('sum(commission) as commission,sum(deal) as deal')->select();
            $data[$k]['sum']        = $sum - $envelopes[0]['give_money'] + $envelopes[0]['expense'];
            $data[$k]['commission'] = $income[0]['commission'];
            $data[$k]['deal']       = $income[0]['deal'];
            unset($data[$k]['personal_id']);
        }
//        $last_names = array_column($list, 'count');
//        array_multisort($last_names, SORT_DESC, $list);
        vendor('phpcvs.Csv');
        $csv = new Csv();
        $csv->export($data, $xlsCell);
        
        
    }
    
    
    function mFristAndLast($y = "", $m = "")
    {
        if ($y == "") $y = date("Y");
        if ($m == "") $m = date("m");
        $m = sprintf("%02d", intval($m));
        $y = str_pad(intval($y), 4, "0", STR_PAD_RIGHT);
        
        $m > 12 || $m < 1 ? $m = 1 : $m = $m;
        $firstday    = strtotime($y . $m . "01000000");
        $firstdaystr = date("Y-m-01", $firstday);
        $lastday     = strtotime(date('Y-m-d 23:59:59', strtotime("$firstdaystr +1 month -1 day")));
        
        return array(
            "firstday" => $firstday,
            "lastday" => $lastday
        );
    }
    
    public function tui1()
    {
        
        $param     = Request::instance()->get();
        $xlsCell   = ['时间', '城市', '物业', '社区名称', '推单量', '预约上门', '打卡上门', '远程沟通量', '成交量', '成交额'];
        $id        = db('community')->column('id');
        $order_id  = db('order')->where('tui_jian', '<>', 0)->column('order_id');
        $order_id  = array_merge($id, $order_id);
        $capital   = db('capital')
            ->field('sum(to_price) as to_price,ordesr_id')
            ->where(['capital.types' => 1, 'capital.enable' => 1, 'capital.agency' => 0])
            ->group('ordesr_id')
            ->buildSql();
        $envelopes = db('envelopes')
            ->field('sum(envelopes.give_money) as give_money,sum(envelopes.expense) as expense,ordesr_id')
            ->where(['envelopes.type' => 1])
            ->group('ordesr_id')
            ->buildSql();
        
        $clock_in = db('clock_in')
            ->field('count(id) as count,order_id')
            ->group('order_id')
            ->buildSql();
        $through  = db('through')
            ->field('count(through_id) as count,order_ids')
            ->where('role', 2)
            ->group('order_ids')
            ->buildSql();
        $list     = db('order')
            ->join('personal', 'order.tui_jian=personal.personal_id', 'left')
            ->join('community', 'order.order_id=community.id', 'left')
            ->join('personal personals', 'personals.personal_id=personal.num', 'left')
            ->join('group_community group_community', 'personals.group_id=group_community.group_id', 'left')
            ->join('group_community group_community1', 'personal.residential_quarters=group_community1.group_id', 'left')
            ->join([$clock_in => 'clock_in'], 'order.order_id=clock_in.order_id', 'left')
            ->join([$envelopes => 'envelopes'], 'envelopes.ordesr_id=order.order_id', 'left')
            ->join([$through => 'through'], 'through.order_ids=order.order_id', 'left')
            ->join([$capital => 'capital'], 'capital.ordesr_id=order.order_id', 'left')
            ->join('user', 'user.user_id = order.assignor', 'left')
            ->join('residential', 'residential.residential_id=community.residential_id', 'left')
            ->join('city', 'city.city_id = order.city_id', 'left');;
        
        $data = $list->where('user.ce', 'neq', 2)->whereIn('order.order_id', $order_id)->where('order.created_time', '>', 1609430400)->field('FROM_UNIXTIME(order.created_time,"%Y年%m月%d %H:%i:%S") as created_time,city.city,if(order.tui_jian !=0,group_community.username,residential.content) as content,if(order.tui_jian !=0,group_community1.username,order.residential_quarters) as residential_quarters,count(distinct order.order_id) as quan,count(distinct CASE WHEN order.planned !=0 THEN order.order_id ELSE NULL END) as shang,clock_in.count as you,through.count as yuan,count(distinct CASE WHEN order.state<=7 and order.state>3  THEN order.order_id ELSE NULL END) as cheng,concat(capital.to_price + envelopes.expense-envelopes.give_money) as sum')->group('FROM_UNIXTIME(order.created_time,"%Y-%m"),city.city,group_community.username,group_community1.username')->order('created_time desc')->select();
        
        vendor('phpcvs.Csv');
        $csv = new Csv();
        $csv->export($data, $xlsCell);
        
        
    }
    
    /*
     * 推荐列表
     */
    public function recommendation()
    {
        
        $capital   = db('capital')
            ->field('sum(to_price) as to_price,ordesr_id')
            ->where(['capital.types' => 1, 'capital.enable' => 1, 'capital.agency' => 0])
            ->group('ordesr_id')
            ->buildSql();
        $envelopes = db('envelopes')
            ->field('sum(envelopes.give_money) as give_money,sum(envelopes.expense) as expense,ordesr_id')
            ->where(['envelopes.type' => 1])
            ->group('ordesr_id')
            ->buildSql();
        $clock_in  = db('clock_in')
            ->field('count(id) as count,order_id')
            ->group('order_id')
            ->buildSql();
        $payment   = db('payment')
            ->field('count(payment_id) as count,orders_id')
            ->where(function ($quer) {
                $quer->where(
                    function ($quer) {
                        $quer->where(['weixin' => ['<>', 2], 'success' => 1]);
                    })->whereOr(function ($quer) {
                    $quer->where(['weixin' => 2, 'success' => 2]);
                });
            })
            ->group('orders_id')
            ->buildSql();
        
        $personal_share = \db('personal_share')
            ->join('personal', 'personal_share.references=personal.personal_id', 'left')
            ->join('personal personal1', 'personal_share.register=personal1.personal_id', 'left')
            ->join('order', 'order.tui_jian=personal1.personal_id', 'left')
            ->join('city', 'city.city_id = order.city_id', 'left')
            ->join([$clock_in => 'clock_in'], 'order.order_id=clock_in.order_id', 'left')
            ->join([$envelopes => 'envelopes'], 'envelopes.ordesr_id=order.order_id', 'left')
            ->join([$capital => 'capital'], 'capital.ordesr_id=order.order_id', 'left')
            ->join([$payment => 'payment'], 'payment.orders_id=order.order_id', 'left')
            ->field('city.city,personal.username,personal.mobile,personal1.username as username1,personal1.mobile as mobile1,order.order_no,if(order.state !=10,"是","否") as you,if(clock_in.count !=0,"是","否") as shang,if(payment.count !=0,"是","否") as fu,concat(capital.to_price + envelopes.expense-envelopes.give_money) as jin,(round((capital.to_price + envelopes.expense-envelopes.give_money)*0.01)) as ti')
            ->select();
        $xlsCell        = ['邀请人姓名', '邀请人手机号', '被邀请人手机号', '推荐订单号', '是否有效订单', '是否上门', '打卡上门', '是否付款', '签约金额', '提成金额'];
        vendor('phpcvs.Csv');
        $csv = new Csv();
        $csv->export($personal_share, $xlsCell);
        
    }
    
    public function Ranking()
    {
        $date            = \request()->get();
        $startTime       = strtotime(date('Y-m-01 H:i:s', strtotime(date('Y-m-d'))));
        $endTime         = strtotime(date('Y-m-t H:i:s', strtotime(date('Y-m-d') . "23:59:59")));
        $group_id        = db('group_community')->where('group_id', '=', $date['group_id'])->value('group_id');
        $group_community = db('group_community')->where('pid', '=', $group_id)->column('group_id');
        
        $list    = db('personal')
            ->Join('income', 'income.personal_id=personal.personal_id and `income`.`uptime` BETWEEN ' . $startTime . ' AND ' . $endTime, 'left')
            ->whereIn('personal.residential_quarters', $group_community)
            ->where('personal.num', '<>', 0)
            ->Join(config('database.connections.zong')['database'] . '.personal personals', 'personals.personal_id=personal.num', 'left')
            ->Join(config('database.connections.zong')['database'] . '.group_community', 'group_community.group_id=personals.group_id', 'left')
            ->Join(config('database.connections.zong')['database'] . '.group_community group_communitys', 'group_communitys.group_id=personal.residential_quarters', 'left')
            ->field('personal.username,sum(income.profit) as sum,group_community.username as username1,group_communitys.username as user')
            ->group('personal.personal_id')
            ->order('sum desc,personal.personal_id desc')
            ->select();
        $xlsCell = ['推客宝姓名', '收益', '物业', '所属小区'];
        vendor('phpcvs.Csv');
        $csv = new Csv();
        $csv->export($list, $xlsCell);
    }
    
    public function product()
    {
        
        $startTime = strtotime(date('Y-m-01 H:i:s', strtotime('2021-11-01')));
        $endTime   = strtotime(date('Y-m-t H:i:s', strtotime("2021-11-30 23:59:59")));
        
        $cd = db('product')
            ->Join(config('database.connections.zong')['database'] . '.order_info', 'order_info.order_id=product.orders_id', 'left')
            ->Join(config('database.connections.zong')['database'] . '.order', 'order.order_id=product.orders_id', 'left')
            ->whereBetween('product.uptime', [$startTime, $endTime])
            ->field('order_info.store_name,order_info.assignor,FROM_UNIXTIME(product.uptime,"%Y年%m月%d %H:%i:%S") as uptime,product.score,product.full,product.content,product.people,product.dissatisfied,product.best,product.connect,product.shopowner_score,product.master_score,product.recommend,order.attributions')
            ->select();
        $cq = db('product', config('database.cq'))
            ->Join(config('database.connections.zong')['database'] . '.order_info', 'order_info.order_id=product.orders_id', 'left')
            ->Join(config('database.connections.zong')['database'] . '.order', 'order.order_id=product.orders_id', 'left')
            ->whereBetween('product.uptime', [$startTime, $endTime])
            ->field('order_info.store_name,order_info.assignor,FROM_UNIXTIME(product.uptime,"%Y年%m月%d %H:%i:%S") as uptime,product.score,product.full,product.content,product.people,product.dissatisfied,product.best,product.connect,product.shopowner_score,product.master_score,product.recommend,order.attributions')
            ->select();
        $gy = db('product', config('database.gy'))
            ->Join(config('database.connections.zong')['database'] . '.order_info', 'order_info.order_id=product.orders_id', 'left')
            ->Join(config('database.connections.zong')['database'] . '.order', 'order.order_id=product.orders_id', 'left')
            ->whereBetween('product.uptime', [$startTime, $endTime])
            ->field('order_info.store_name,order_info.assignor,FROM_UNIXTIME(product.uptime,"%Y年%m月%d %H:%i:%S") as uptime,product.score,product.full,product.content,product.people,product.dissatisfied,product.best,product.connect,product.shopowner_score,product.master_score,product.recommend,
            CASE
            WHEN order.attributions=1 THEN "线上"
             WHEN order.attributions=2 THEN  "KA"
             WHEN order.attributions=3 THEN  "线下"
             
           ELSE  "其它" END
             as attributions
            ')
            ->select();
        
        $wh = db('product', config('database.wh'))
            ->Join(config('database.connections.zong')['database'] . '.order_info', 'order_info.order_id=product.orders_id', 'left')
            ->Join(config('database.connections.zong')['database'] . '.order', 'order.order_id=product.orders_id', 'left')
            ->whereBetween('product.uptime', [$startTime, $endTime])
            ->field('order_info.store_name,order_info.assignor,FROM_UNIXTIME(product.uptime,"%Y年%m月%d %H:%i:%S") as uptime,product.score,product.full,product.content,product.people,product.dissatisfied,product.best,product.connect,product.shopowner_score,product.master_score,product.recommend,
              CASE
            WHEN order.attributions=1 THEN "线上"
             WHEN order.attributions=2 THEN  "KA"
             WHEN order.attributions=3 THEN  "线下"
             
           ELSE  "其它" END
             as attributions
            ')
            ->select();
        $sz = db('product', config('database.sz'))
            ->Join(config('database.connections.zong')['database'] . '.order_info', 'order_info.order_id=product.orders_id', 'left')
            ->Join(config('database.connections.zong')['database'] . '.order', 'order.order_id=product.orders_id', 'left')
            ->whereBetween('product.uptime', [$startTime, $endTime])
            ->field('order_info.store_name,order_info.assignor,FROM_UNIXTIME(product.uptime,"%Y年%m月%d %H:%i:%S%d H:i:s") as uptime,product.score,product.full,product.content,product.people,product.dissatisfied,product.best,product.connect,product.shopowner_score,product.master_score,product.recommend,
              CASE
            WHEN order.attributions=1 THEN "线上"
             WHEN order.attributions=2 THEN  "KA"
             WHEN order.attributions=3 THEN  "线下"
             
           ELSE  "其它" END
             as attributions
            ')
            ->select();
        $sh = db('product', config('database.sh'))
            ->Join(config('database.connections.zong')['database'] . '.order_info', 'order_info.order_id=product.orders_id', 'left')
            ->Join(config('database.connections.zong')['database'] . '.order', 'order.order_id=product.orders_id', 'left')
            ->whereBetween('product.uptime', [$startTime, $endTime])
            ->field('order_info.store_name,order_info.assignor,FROM_UNIXTIME(product.uptime,"%Y年%m月%d %H:%i:%S") as uptime,product.score,product.full,product.content,product.people,product.dissatisfied,product.best,product.connect,product.shopowner_score,product.master_score,product.recommend,
              CASE
            WHEN order.attributions=1 THEN "线上"
             WHEN order.attributions=2 THEN  "KA"
             WHEN order.attributions=3 THEN  "线下"
             
           ELSE  "其它" END
             as attributions
            ')
            ->select();
        $bj = db('product', config('database.bj'))
            ->Join(config('database.connections.zong')['database'] . '.order_info', 'order_info.order_id=product.orders_id', 'left')
            ->Join(config('database.connections.zong')['database'] . '.order', 'order.order_id=product.orders_id', 'left')
            ->whereBetween('product.uptime', [$startTime, $endTime])
            ->field('order_info.store_name,order_info.assignor,FROM_UNIXTIME(product.uptime,"%Y年%m月%d %H:%i:%S") as uptime,product.score,product.full,product.content,product.people,product.dissatisfied,product.best,product.connect,product.shopowner_score,product.master_score,product.recommend,
              CASE
            WHEN order.attributions=1 THEN "线上"
             WHEN order.attributions=2 THEN  "KA"
             WHEN order.attributions=3 THEN  "线下"
             
           ELSE  "其它" END
             as attributions
            ')
            ->select();
        $gz = db('product', config('database.gz'))
            ->Join(config('database.connections.zong')['database'] . '.order_info', 'order_info.order_id=product.orders_id', 'left')
            ->Join(config('database.connections.zong')['database'] . '.order', 'order.order_id=product.orders_id', 'left')
            ->whereBetween('product.uptime', [$startTime, $endTime])
            ->field('order_info.store_name,order_info.assignor,FROM_UNIXTIME(product.uptime,"%Y年%m月%d %H:%i:%S") as uptime,product.score,product.full,product.content,product.people,product.dissatisfied,product.best,product.connect,product.shopowner_score,product.master_score,product.recommend,
              CASE
            WHEN order.attributions=1 THEN "线上"
             WHEN order.attributions=2 THEN  "KA"
             WHEN order.attributions=3 THEN  "线下"
             
           ELSE  "其它" END
             as attributions
            ')
            ->select();
        
        $fs   = db('product', config('database.fs'))
            ->Join(config('database.connections.zong')['database'] . '.order_info', 'order_info.order_id=product.orders_id', 'left')
            ->Join(config('database.connections.zong')['database'] . '.order', 'order.order_id=product.orders_id', 'left')
            ->whereBetween('product.uptime', [$startTime, $endTime])
            ->field('order_info.store_name,order_info.assignor,FROM_UNIXTIME(product.uptime,"%Y年%m月%d %H:%i:%S") as uptime,product.score,product.full,product.content,product.people,product.dissatisfied,product.best,product.connect,product.shopowner_score,product.master_score,product.recommend,
              CASE
            WHEN order.attributions=1 THEN "线上"
             WHEN order.attributions=2 THEN  "KA"
             WHEN order.attributions=3 THEN  "线下"
             
           ELSE  "其它" END
             as attributions
            ')
            ->select();
        $list = array_merge($cd, $cq, $wh, $gy, $sh, $bj, $sz, $fs, $gz);
        
        $xlsCell = ['店铺', '店长', '回访时间', '打分', '满意度', '内容', '回访人员', '不满意', '最好的地方', '是否接通', '店长评分', '师傅评分', '推荐指数', '业绩分组'];
        vendor('phpcvs.Csv');
        $csv = new Csv();
        $csv->export($list, $xlsCell);
    }
    
    public function direct_order()
    {
        
        
        $bj = db('direct_order', config('database.zong'))
            ->Join('order_info', 'order_info.order_id=direct_order.order_id', 'left')
            ->Join('order', 'order.order_id=direct_order.order_id', 'left')
            ->Join('direct_payment_record', 'direct_payment_record.serial_number=direct_order.payNo', 'left')
            ->field('order_info.store_name,order_info.assignor,order.order_no,FROM_UNIXTIME(direct_order.distribute_time,"%Y年%m月%d %H:%i:%S") as distribute_time,FROM_UNIXTIME(direct_order.complete_time,"%Y年%m月%d %H:%i:%S") as complete_time,direct_order.type,direct_order.user_id,direct_order.payNo,direct_order.order_id,direct_payment_record.price,direct_payment_record.mode')
            ->select();
        foreach ($bj as $k => $item) {
            if (substr($item['order_id'], 0, 3) == 241) {
                
                $content['city']      = 'db2';
                $content['shopowner'] = 'cd';
            } elseif (substr($item['order_id'], 0, 3) == 239) {
                $content['shopowner'] = 'cq';
                $content['city']      = 'cq';
            } elseif (substr($item['order_id'], 0, 3) == 172) {
                $content['shopowner'] = 'wh';
                $content['city']      = 'wh';
            } elseif (substr($item['order_id'], 0, 3) == 262) {
                $content['shopowner'] = 'gy';
                $content['city']      = 'gy_mast';
            } elseif (substr($item['order_id'], 0, 3) == 375) {
                $content['shopowner'] = 'sh';
                $content['city']      = 'sh';
            } elseif (substr($item['order_id'], 0, 3) == 202) {
                $content['shopowner'] = 'sz';
                $content['city']      = 'sz';
            }
            if ($item['type'] == 1) {
                if ($content['shopowner'] == 'cd') {
                    $username = db('user')->where('user_id', $item['user_id'])->value('username');
                } else {
                    $username = db('user', config('database.' . $content['shopowner'] . ''))->where('user_id', $item['user_id'])->value('username');
                }
                
            } else {
                $username = db('app_user', config('database.' . $content['city'] . ''))->where('id', $item['user_id'])->value('username');
                
            }
            if ($item['type'] == 1) {
                $type = "店长";
            } else {
                $type = "师傅";
            }
            if ($item['mode'] == 1) {
                $mode = "支付宝";
            } else {
                $mode = "微信";
            }
            $data[$k]['store_name']      = $item['store_name'];
            $data[$k]['assignor']        = $item['assignor'];
            $data[$k]['order_no']        = $item['order_no'] . "\t";
            $data[$k]['payNo']           = $item['payNo'];
            $data[$k]['distribute_time'] = $item['distribute_time'];
            $data[$k]['complete_time']   = $item['complete_time'];
            $data[$k]['type']            = $type;
            $data[$k]['username']        = $username;
            $data[$k]['price']           = $item['price'];
            $data[$k]['mode']            = $mode;
        }
        
        $xlsCell = ['店铺', '店长', '订单号', '流水号', '派单时间', '完成时间', '店长/师傅', '操作人', '金额', '支付方式'];
        vendor('phpcvs.Csv');
        $csv = new Csv();
        $csv->export($data, $xlsCell);
    }
    
    public function newProduct()
    {
        $date = \request()->get();
        
        
        if (!isset($date['name'])) {
            return '请填写下载人名字';
        }
        if (!isset($date['time'])) {
            return '请填写开始时间time';
        }
        if (!isset($date['endTime'])) {
            return '请填写截止时间endTime';
        }
        if ($date['name'] != '王艳') {
            return '没有权限下载';
        }
        $user = ['admin_id' => 0, 'created_at' => time(), 'content' => $date['name'] . '下载了订单回访记录IP：' . $_SERVER['REMOTE_ADDR'], 'ord_id' => '',];
        db('log')->insertGetId($user);
        $timestamp = strtotime($date['time']);
        $startTime = strtotime(date('Y-m-1 00:00:00', $timestamp));
        $endTime   = strtotime($date['endTime']);
        
        $endTime = strtotime(date('Y-m-d 23:59:59', $endTime));
        
        $cd = db('kup')
            ->Join(config('database.connections.zong')['database'] . '.order_info', 'order_info.order_id=kup.orders_id', 'left')
            ->Join(config('database.connections.zong')['database'] . '.order', 'order.order_id=kup.orders_id', 'left')
            ->whereBetween('kup.uptime', [$startTime, $endTime])
            ->field('order_info.store_name,order.order_no,FROM_UNIXTIME(order.finish_time,"%Y年%m月%d日 %H:%i:%S") as finish_time,order.contacts,order.telephone,concat(order_info.county,order.addres,",",order.street,if(order.building !="",concat(order.building,"-"),""),if(order.unit !="",concat(order.unit,"-"),""),order.room) as addres,order_info.assignor,FROM_UNIXTIME(kup.uptime,"%Y年%m月%d日 %H:%i:%S") as uptime,kup.score,kup.score1,kup.score2,kup.score3,kup.content,if(kup.type=0,"客户评价","客服回访") as type,kup.orders_id,
             CASE
            WHEN order.attributions=1 THEN "线上"
             WHEN order.attributions=2 THEN  "KA"
             WHEN order.attributions=3 THEN  "线下"
             
           ELSE  "其它" END
             as attributions,order_info.channel_title,order_info.channel_details_title
            ')
            ->select();
        $cq = db('kup', config('database.cq'))
            ->Join(config('database.connections.zong')['database'] . '.order_info', 'order_info.order_id=kup.orders_id', 'left')
            ->Join(config('database.connections.zong')['database'] . '.order', 'order.order_id=kup.orders_id', 'left')
            ->whereBetween('kup.uptime', [$startTime, $endTime])
            ->field('order_info.store_name,order.order_no,FROM_UNIXTIME(order.finish_time,"%Y年%m月%d日 %H:%i:%S") as finish_time,order.contacts,order.telephone,concat(order_info.county,order.addres,",",order.street,if(order.building !="",concat(order.building,"-"),""),if(order.unit !="",concat(order.unit,"-"),""),order.room) as addres,order_info.assignor,FROM_UNIXTIME(kup.uptime,"%Y年%m月%d日 %H:%i:%S") as uptime,kup.score,kup.score1,kup.score2,kup.score3,kup.content,if(kup.type=0,"客户评价","客服回访") as type,kup.orders_id, CASE
            WHEN order.attributions=1 THEN "线上"
             WHEN order.attributions=2 THEN  "KA"
             WHEN order.attributions=3 THEN  "线下"
             
           ELSE  "其它" END
             as attributions,order_info.channel_title,order_info.channel_details_title
            ')
            ->select();
        $gy = db('kup', config('database.gy'))
            ->Join(config('database.connections.zong')['database'] . '.order_info', 'order_info.order_id=kup.orders_id', 'left')
            ->Join(config('database.connections.zong')['database'] . '.order', 'order.order_id=kup.orders_id', 'left')
            ->whereBetween('kup.uptime', [$startTime, $endTime])
            ->field('order_info.store_name,order.order_no,FROM_UNIXTIME(order.finish_time,"%Y年%m月%d日 %H:%i:%S") as finish_time,order.contacts,order.telephone,concat(order_info.county,order.addres,",",order.street,if(order.building !="",concat(order.building,"-"),""),if(order.unit !="",concat(order.unit,"-"),""),order.room) as addres,order_info.assignor,FROM_UNIXTIME(kup.uptime,"%Y年%m月%d日 %H:%i:%S") as uptime,kup.score,kup.score1,kup.score2,kup.score3,kup.content,if(kup.type=0,"客户评价","客服回访") as type,kup.orders_id, CASE
            WHEN order.attributions=1 THEN "线上"
             WHEN order.attributions=2 THEN  "KA"
             WHEN order.attributions=3 THEN  "线下"
             
           ELSE  "其它" END
             as attributions,order_info.channel_title,order_info.channel_details_title
            ')
            ->select();
        
        $wh   = db('kup', config('database.wh'))
            ->Join(config('database.connections.zong')['database'] . '.order_info', 'order_info.order_id=kup.orders_id', 'left')
            ->Join(config('database.connections.zong')['database'] . '.order', 'order.order_id=kup.orders_id', 'left')
            ->whereBetween('kup.uptime', [$startTime, $endTime])
            ->field('order_info.store_name,order.order_no,FROM_UNIXTIME(order.finish_time,"%Y年%m月%d日 %H:%i:%S") as finish_time,order.contacts,order.telephone,concat(order_info.county,order.addres,",",order.street,if(order.building !="",concat(order.building,"-"),""),if(order.unit !="",concat(order.unit,"-"),""),order.room) as addres,order_info.assignor,FROM_UNIXTIME(kup.uptime,"%Y年%m月%d日 %H:%i:%S") as uptime,kup.score,kup.score1,kup.score2,kup.score3,kup.content,if(kup.type=0,"客户评价","客服回访") as type,kup.orders_id, CASE
            WHEN order.attributions=1 THEN "线上"
             WHEN order.attributions=2 THEN  "KA"
             WHEN order.attributions=3 THEN  "线下"
             
           ELSE  "其它" END
             as attributions,order_info.channel_title,order_info.channel_details_title
            ')
            ->select();
        $sz   = db('kup', config('database.sz'))
            ->Join(config('database.connections.zong')['database'] . '.order_info', 'order_info.order_id=kup.orders_id', 'left')
            ->Join(config('database.connections.zong')['database'] . '.order', 'order.order_id=kup.orders_id', 'left')
            ->whereBetween('kup.uptime', [$startTime, $endTime])
            ->field('order_info.store_name,order.order_no,FROM_UNIXTIME(order.finish_time,"%Y年%m月%d日 %H:%i:%S") as finish_time,order.contacts,order.telephone,concat(order_info.county,order.addres,",",order.street,if(order.building !="",concat(order.building,"-"),""),if(order.unit !="",concat(order.unit,"-"),""),order.room) as addres,order_info.assignor,FROM_UNIXTIME(kup.uptime,"%Y年%m月%d日 %H:%i:%S") as uptime,kup.score,kup.score1,kup.score2,kup.score3,kup.content,if(kup.type=0,"客户评价","客服回访") as type,kup.orders_id, CASE
            WHEN order.attributions=1 THEN "线上"
             WHEN order.attributions=2 THEN  "KA"
             WHEN order.attributions=3 THEN  "线下"
             
           ELSE  "其它" END
             as attributions,order_info.channel_title,order_info.channel_details_title
            ')
            ->select();
        $sh   = db('kup', config('database.sh'))
            ->Join(config('database.connections.zong')['database'] . '.order_info', 'order_info.order_id=kup.orders_id', 'left')
            ->Join(config('database.connections.zong')['database'] . '.order', 'order.order_id=kup.orders_id', 'left')
            ->whereBetween('kup.uptime', [$startTime, $endTime])
            ->field('order_info.store_name,order.order_no,FROM_UNIXTIME(order.finish_time,"%Y年%m月%d日 %H:%i:%S") as finish_time,order.contacts,order.telephone,concat(order_info.county,order.addres,",",order.street,if(order.building !="",concat(order.building,"-"),""),if(order.unit !="",concat(order.unit,"-"),""),order.room) as addres,order_info.assignor,FROM_UNIXTIME(kup.uptime,"%Y年%m月%d日 %H:%i:%S") as uptime,kup.score,kup.score1,kup.score2,kup.score3,kup.content,if(kup.type=0,"客户评价","客服回访") as type,kup.orders_id, CASE
            WHEN order.attributions=1 THEN "线上"
             WHEN order.attributions=2 THEN  "KA"
             WHEN order.attributions=3 THEN  "线下"
             
           ELSE  "其它" END
             as attributions,order_info.channel_title,order_info.channel_details_title
            ')
            ->select();
        $bj   = db('kup', config('database.bj'))
            ->Join(config('database.connections.zong')['database'] . '.order_info', 'order_info.order_id=kup.orders_id', 'left')
            ->Join(config('database.connections.zong')['database'] . '.order', 'order.order_id=kup.orders_id', 'left')
            ->whereBetween('kup.uptime', [$startTime, $endTime])
            ->field('order_info.store_name,order.order_no,FROM_UNIXTIME(order.finish_time,"%Y年%m月%d日 %H:%i:%S") as finish_time,order.contacts,order.telephone,concat(order_info.county,order.addres,",",order.street,if(order.building !="",concat(order.building,"-"),""),if(order.unit !="",concat(order.unit,"-"),""),order.room) as addres,order_info.assignor,FROM_UNIXTIME(kup.uptime,"%Y年%m月%d日 %H:%i:%S") as uptime,kup.score,kup.score1,kup.score2,kup.score3,kup.content,if(kup.type=0,"客户评价","客服回访") as type,kup.orders_id, CASE
            WHEN order.attributions=1 THEN "线上"
             WHEN order.attributions=2 THEN  "KA"
             WHEN order.attributions=3 THEN  "线下"
             
           ELSE  "其它" END
             as attributions,order_info.channel_title,order_info.channel_details_title
            ')
            ->select();
        $fs   = db('kup', config('database.fs'))
            ->Join(config('database.connections.zong')['database'] . '.order_info', 'order_info.order_id=kup.orders_id', 'left')
            ->Join(config('database.connections.zong')['database'] . '.order', 'order.order_id=kup.orders_id', 'left')
            ->whereBetween('kup.uptime', [$startTime, $endTime])
            ->field('order_info.store_name,order.order_no,FROM_UNIXTIME(order.finish_time,"%Y年%m月%d日 %H:%i:%S") as finish_time,order.contacts,order.telephone,concat(order_info.county,order.addres,",",order.street,if(order.building !="",concat(order.building,"-"),""),if(order.unit !="",concat(order.unit,"-"),""),order.room) as addres,order_info.assignor,FROM_UNIXTIME(kup.uptime,"%Y年%m月%d日 %H:%i:%S") as uptime,kup.score,kup.score1,kup.score2,kup.score3,kup.content,if(kup.type=0,"客户评价","客服回访") as type,kup.orders_id, CASE
            WHEN order.attributions=1 THEN "线上"
             WHEN order.attributions=2 THEN  "KA"
             WHEN order.attributions=3 THEN  "线下"
             
           ELSE  "其它" END
             as attributions,order_info.channel_title,order_info.channel_details_title
            ')
            ->select();
        $gz   = db('kup', config('database.gz'))
            ->Join(config('database.connections.zong')['database'] . '.order_info', 'order_info.order_id=kup.orders_id', 'left')
            ->Join(config('database.connections.zong')['database'] . '.order', 'order.order_id=kup.orders_id', 'left')
            ->whereBetween('kup.uptime', [$startTime, $endTime])
            ->field('order_info.store_name,order.order_no,FROM_UNIXTIME(order.finish_time,"%Y年%m月%d日 %H:%i:%S") as finish_time,order.contacts,order.telephone,concat(order_info.county,order.addres,",",order.street,if(order.building !="",concat(order.building,"-"),""),if(order.unit !="",concat(order.unit,"-"),""),order.room) as addres,order_info.assignor,FROM_UNIXTIME(kup.uptime,"%Y年%m月%d日 %H:%i:%S") as uptime,kup.score,kup.score1,kup.score2,kup.score3,kup.content,if(kup.type=0,"客户评价","客服回访") as type,kup.orders_id, CASE
            WHEN order.attributions=1 THEN "线上"
             WHEN order.attributions=2 THEN  "KA"
             WHEN order.attributions=3 THEN  "线下"
             
           ELSE  "其它" END
             as attributions,order_info.channel_title,order_info.channel_details_title
            ')
            ->select();
        $dg   = db('kup', config('database.dg'))
            ->Join(config('database.connections.zong')['database'] . '.order_info', 'order_info.order_id=kup.orders_id', 'left')
            ->Join(config('database.connections.zong')['database'] . '.order', 'order.order_id=kup.orders_id', 'left')
            ->whereBetween('kup.uptime', [$startTime, $endTime])
            ->field('order_info.store_name,order.order_no,FROM_UNIXTIME(order.finish_time,"%Y年%m月%d日 %H:%i:%S") as finish_time,order.contacts,order.telephone,concat(order_info.county,order.addres,",",order.street,if(order.building !="",concat(order.building,"-"),""),if(order.unit !="",concat(order.unit,"-"),""),order.room) as addres,order_info.assignor,FROM_UNIXTIME(kup.uptime,"%Y年%m月%d日 %H:%i:%S") as uptime,kup.score,kup.score1,kup.score2,kup.score3,kup.content,if(kup.type=0,"客户评价","客服回访") as type,kup.orders_id, CASE
            WHEN order.attributions=1 THEN "线上"
             WHEN order.attributions=2 THEN  "KA"
             WHEN order.attributions=3 THEN  "线下"
             
           ELSE  "其它" END
             as attributions,order_info.channel_title,order_info.channel_details_title
            ')
            ->select();
        $list = array_merge($cd, $cq, $wh, $gy, $sh, $bj, $sz, $fs, $gz,$dg);
        foreach ($list as $k => $value) {
            $admin_id = db('crm_cc_task', config('database.zong'))->where('order_id', $value['orders_id'])->where('type', 4)->where('status', 1)->find();
            $list[$k]['order_no'] = $value['order_no']."\n";
            if (!empty($admin_id)) {
                $list[$k]['username'] = db('admin', config('database.zong'))->where('admin_id', $admin_id['admin_id'])->value('username');
            } else {
                $list[$k]['username'] = '';
            }
        }
        
        $xlsCell = ['店铺', '订单编号','完工时间','客户姓名', '电话', '地址', '店长', '回访时间', '施工项目评价', '项目满意度', '店长服务态度', '客服服务态度', '内容', '回访人员', '订单ID', '姓名', '业绩分组', '渠道', '渠道二级'];
        vendor('phpcvs.Csv');
        $csv = new Csv();
        $csv->export($list, $xlsCell);
    }
    
    public function orderThrough()
    {
        $dataList  = [];
        $envelopes = db('envelopes', config('database.zong'))
            ->field('count(envelopes.envelopes_id) as envelopesCount,envelopes.ordesr_id')
            ->group('ordesr_id')
            ->buildSql();
        $through   = db('through', config('database.zong'))
            ->field('sum(if(role=1,1,0)) as customer,sum(if(role=2,1,0)) as user,through.order_ids')
            ->group('order_ids')
            ->buildSql();
        $order     = db('order', config('database.zong'))
            ->join('order_info', 'order_info.order_id=order.order_id', 'left')
            ->join([$envelopes => 'envelopes'], 'envelopes.ordesr_id=order.order_id', 'left')
            ->join([$through => 'through'], 'through.order_ids=order.order_id', 'left')
            ->field('order.order_id,concat(order_info.city,order_info.county,order.addres) as addres,order.contacts,order.telephone,case order.visiting_time when 1 then "立即" when 2 then "三天内" when 3 then "七天内" else "不着急" end as visiting,order_info.pro_id_title,FROM_UNIXTIME(order.created_time,"%Y年%m月%d %H:%i:%S") as created_time,order_info.assignor,order_info.channel_title,envelopes.envelopesCount,through.customer,through.user,if(order.state>3 and order.state<=7,"已签约","未签约") as qian')
            ->whereIn('order.channel_id', [33, 24, 40, 41, 11])
            ->group('order.order_id')
            ->select();
        
        $xlsCell = ['订单id', '维修地址', '客户名称', '联系电话', '期望上门时间', '房屋问题', '创建时间', '店长', '渠道', '报价次数', '店长跟单次数', '客户跟单次数', '是否签约'];
        vendor('phpcvs.Csv');
        $csv = new Csv();
        $csv->export($order, $xlsCell);
    }
    
    public function visPdf()
    {
        $data            = Request::instance()->get();
        $order_id        = $data['order_id'];
        $moneyS          = $data['money'];//合计
        $mainMoney       = $data['mainMoney'];//主合同管理费
        $money           = $data['AngeExpense'];//$money
        $capital         = isset($data['capital']) ? $data['capital'] : [];//$money
        $give_money      = $data['give_money'];//主合同优惠
        $AngeMoney       = $data['AngeMoney'];//代购优惠
        $io              = db('order')
            ->join('province p', 'order.province_id=p.province_id', 'left')
            ->join('city c', 'order.city_id=c.city_id', 'left')
            ->join('county y', 'order.county_id=y.county_id', 'left')
            ->join('user us', 'order.assignor=us.user_id', 'left')
            ->field('concat(p.province,c.city,y.county,order.addres) as addres,assignor,us.username,us.mobile,us.store_id,order.telephone,order.contacts')
            ->where('order_id', $order_id)->find();
        $company_chapter = db('company_config', config('database.zong'))->where('find_in_set(:id,store_list)', ['id' => $io['store_id']])->where('status', 1)->where('city_code', config('cityId'))->field('company_chapter,company_name')->find();
        if (empty($company_chapter['company_name'])) {
            $company_chapter['company_name'] = "成都益鸟科技有限公司";
        }
        $company_name = $company_chapter['company_name'];
        $s            = '';
        $capital      = db('capital')
            ->whereIn('capital_id', $capital)
            ->select();
        foreach ($capital as $k => $o) {
            $capital[$k]['money'] = 0;
            
            $capital = db('visa_form')->where('find_in_set(:id,capital_id)', ['id' => $o['capital_id']])->whereNotNull('signing_time')->select();
            
            if ($o['increment'] == 1 && count($capital) == 0) {
                
                $capital[$k]['money'] = 1;
            }
            if ($o['up_products'] == 1 && count($capital) == 0) {
                
                $capital[$k]['money'] = 2;
            }
            if ($o['types'] == 2 && count($capital) == 0) {
                $capital[$k]['money'] = 2;
            }
            if ($o['types'] == 2 && $o['increment'] != 1 && count($capital) == 0) {
                $capital[$k]['money'] = 2;
            }
            if ($o['types'] == 1 && $o['increment'] == 1 && count($capital) == 0) {
                $capital[$k]['money'] = 1;
            }
            
            if ($o['types'] == 2 && $o['increment'] == 1 && count($capital) == 0) {
                $capital[$k]['money'] = 3;
            }
            
            if (count($capital) != 0 && $capital[0]['signing_time'] < $o['untime']) {
                $capital[$k]['money'] = 2;
            }
            if (count($capital) != 0 && $capital[0]['signing_time'] > $o['untime']) {
                $capital[$k]['money'] = 1;
            }
            
            if ($capital[$k]['money'] == 1 && $o['give'] == 0) {
                $o['zhi'] = "增项";
            } elseif ($capital[$k]['money'] == 2 && $o['give'] == 0) {
                $o['zhi'] = "减项";
            } elseif ($capital[$k]['money'] == 1 && $o['give'] == 2) {
                $o['zhi'] = "增项(赠送项目)";
            } elseif ($capital[$k]['money'] == 2 && $o['give'] == 2) {
                $o['zhi'] = "减项(赠送项目)";
            } else {
                $o['zhi'] = "取消";
            }
            $s .= '
<div></div>
        <p style=" font-size: 18px;">' . $o['zhi'] . ':</p>
        <p style=" font-size: 18px;">项目名称: <span>' . $o['class_b'] . '</span></p>
    
            <span style=" font-size: 18px;">单位: <span style=" font-size: 18px;">' . $o["company"] . '</span> </span>
            <span  style=" font-size: 18px;">单价: <span style=" font-size: 18px;">' . $o["un_Price"] . '</span> </span>
            <span  style=" font-size: 18px;">单方量: <span style=" font-size: 18px;">' . $o["square"] . '</span> </span>
     
       <p style=" font-size: 18px;">总价: <span style=" font-size: 18px;">' . $o["to_price"] . '</span></p>
        <p style=" font-size: 18px;">备注: <span style=" font-size: 18px;">' . $o["projectRemark"] . '</span></p>
        <div style="border-top-style:dashed; height:1px"></div>
   ';
        }
//        $visaFormData = db('visa_form')->where('order_id', $order_id)->where('capital_id', implode(',', array_column($io, 'capital_id')))->whereNull('signing_time')->order('id desc')->find();
        
        
        $sign = db('sign')
            ->where(['order_id' => $order_id])
            ->find();
        
        $user      = empty($sign['username']) ? $io['contacts'] : $sign['username'];
        $addres    = $io['addres'];
        $username  = $io['username'];
        $phone     = $io['mobile'];
        $telephone = $io['telephone'];
        // $time      = date('Y-m-d H:i',time());
        $time = '2023-07-10 23:33:40';
        $html =
            <<<EOF
<html>
<div style="width: 30%">

    <h3 style="text-align: center;display: block;margin-top: 0;font-size: 20px">增项减项签证单</h3>
   <div style=" font-size: 18px;">委托方(简称甲方)：$user</div>
 
 <div style=" font-size: 18px;">施工方(简称乙方)：$company_name</div>
 <div style=" font-size: 18px; ">工程地址：$addres</div>
 <div style=" font-size: 18px; ">签约时间：$time</div>
           $s
           <p style=" font-size: 18px;">主合同管理费：$mainMoney 元</p>
           <p style=" font-size: 18px;">代购管理费：$money 元</p>
           <p style=" font-size: 18px;">主合同优惠：$give_money 元</p>
           <p style=" font-size: 18px;">代购优惠：$AngeMoney 元</p>
           
 <p style=" font-size: 18px;">合计：$moneyS 元</p>
             <div style="border-top: 3px solid #0000"></div>
             
      
       
     
       <span style=" font-size: 18px;">甲方代表：      &ensp;&ensp;&ensp; &nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;乙方代表：$username</span>
      
      <div></div>
       <span style=" font-size: 18px;">电话号码：$telephone&nbsp;&nbsp;&nbsp;&nbsp;电话号码:$phone</span>
       
       
      
       





       
      
</div>


</html>
EOF;
        
        
        require_once ROOT_PATH . 'extend/TCPDF/tcpdf.php';
        $pdf = new \TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        $pdf->SetCreator('益鸟');
        
        
        $pdf->SetAuthor('益鸟 维修');
        $pdf->SetTitle('增项减项签证单');
        
        
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
        $pdf->setFontSubsetting(true);
        $pdf->SetPrintHeader(false);
        $pdf->SetPrintFooter(false);
        $pdf->SetFont('stsongstdlight', '', 20, true);
        $pdf->setCellPaddings(5, 7, 5, 20);
        $pdf->AddPage();
        
        $pdf->writeHTMLCell(0, 90, '', '', $html, 0, 0, 0, true, '', true);
        $pdf->Line(55, 10, 200, 10, ['width' => 0.1, 'cap' => 'butt', 'join' => 'miter', 'dash' => '0', 'phase' => 10, 'color' => [255, 255, 255]]);
        $pdf->Line(60, 47, 200, 47, ['width' => 0.1, 'cap' => 'butt', 'join' => 'miter', 'dash' => '0', 'phase' => 10, 'color' => [100, 100, 100]]);
        $pdf->Line(60, 62, 200, 62, ['width' => 0.1, 'cap' => 'butt', 'join' => 'miter', 'dash' => '0', 'phase' => 10, 'color' => [100, 100, 100]]);
        $pdf->Line(40, 77, 200, 77, ['width' => 0.1, 'cap' => 'butt', 'join' => 'miter', 'dash' => '0', 'phase' => 10, 'color' => [100, 100, 100]]);
        $pdf->Line(40, 95, 200, 95, ['width' => 0.1, 'cap' => 'butt', 'join' => 'miter', 'dash' => '0', 'phase' => 10, 'color' => [100, 100, 100]]);
        $pdf->Line(15, 105, 200, 105, ['width' => 1, 'cap' => 'butt', 'join' => 'miter', 'dash' => '0', 'phase' => 10, 'color' => [100, 100, 100]]);
        $pdf->SetAlpha(1);
        
        if (empty($company_chapter['company_chapter'])) {
            $company_chapter['company_chapter'] = 'http://yiniao.oss-cn-shenzhen.aliyuncs.com/public/chapter/chapter_yn.png';
        }
        $pdf->Image($company_chapter['company_chapter'], 140, 35, 50, 50, '', '', '', false, 300, '', false, false, 0);
        
        
        $pas = ROOT_PATHS . '/uploads/PDF/' . $order_id . '.pdf';
        $pdf->Output($pas, 'F');
        
        $o = $this->pdf2png($pas, ROOT_PATHS . '/uploads/PDF/', $order_id);
        var_dump($o);
        die;
    }
    
    public function pdf2png($pdf, $path, $order_id)
    {
        try {
            $im = new \Imagick();
            $im->setCompressionQuality(100);
            $im->setResolution(120, 120);//设置分辨率 值越大分辨率越高
            
            $im->readImage($pdf);
            
            $canvas = new \Imagick();
            $imgNum = $im->getNumberImages();
            //$canvas->setResolution(120, 120);
            foreach ($im as $k => $sub) {
                $sub->setImageFormat('png');
                //$sub->setResolution(120, 120);
                $sub->stripImage();
                $sub->trimImage(0);
                $width  = $sub->getImageWidth() + 10;
                $height = $sub->getImageHeight() + 10;
                if ($k + 1 == $imgNum) {
                    $height += 10;
                } //最后添加10的height
                $canvas->newImage($width, $height, new \ImagickPixel('white'));
                $canvas->compositeImage($sub, \Imagick::COMPOSITE_DEFAULT, 5, 5);
            }
            
            $canvas->resetIterator();
            $canvas->appendImages(true)->writeImage($path . $order_id . '.png');
            return request()->domain() . '/uploads/PDF/' . $order_id . '.png?t=' . time();
//            return request()->domain() .config('city'). '/uploads/PDF/' . $order_id . '.png?t=' . time();
        } catch (Exception $e) {
            throw $e;
        }
    }
    
    public function ss()
    {
        $date       = \request()->get();
        $Common     = new Common();
        $orderModel = new OrderModel();
        
        $Common->award_config_rule($date['order_id'], $orderModel, 3);
    }
    
    public function node()
    {
        $startTimestamp = strtotime(date('Y-m-01 00:00:00', strtotime('-5 months')));
        $endTimestamp   = time();
        
        $list = [];
        // 循环输出每一天的日期
        $j = 0;
        for ($i = $startTimestamp; $i <= $endTimestamp; $i += 86400) {
            $date  = date('Y-m-d', $i);
            $stare = date('Y-m-d 00:00:00', $i);
            $end   = date('Y-m-d 23:59:59', $i);
            $stare = strtotime($stare);
            $end   = strtotime($end);
            $sql   = "SELECT
	COUNT( auxiliary_delivery_node.adn_id ) AS count
FROM
	`auxiliary_delivery_node`
	LEFT JOIN `auxiliary_delivery_schedule` ON `auxiliary_delivery_schedule`.`id` = `auxiliary_delivery_node`.`auxiliary_delivery_schedul_id`
	LEFT JOIN `auxiliary_project_list` ON `auxiliary_project_list`.`id` = `auxiliary_delivery_schedule`.`auxiliary_project_list_id`
WHERE
	`auxiliary_project_list`.`delete_time` IS NULL
	AND `auxiliary_delivery_schedule`.`delete_time` IS NULL
	AND  auxiliary_delivery_node.upload_time BETWEEN " . $stare . " and " . $end;
//            echo $sql . "\n";;
            //原生sql
            $reality_artificial = db('auxiliary_delivery_node');
            $sum                = $reality_artificial->query($sql);
            
            $sql1 = "SELECT
	COUNT( auxiliary_delivery_node.audit_time ) AS count
FROM
	`auxiliary_delivery_node`
	LEFT JOIN `auxiliary_delivery_schedule` ON `auxiliary_delivery_schedule`.`id` = `auxiliary_delivery_node`.`auxiliary_delivery_schedul_id`
	LEFT JOIN `auxiliary_project_list` ON `auxiliary_project_list`.`id` = `auxiliary_delivery_schedule`.`auxiliary_project_list_id`
WHERE
	`auxiliary_project_list`.`delete_time` IS NULL
	AND `auxiliary_delivery_schedule`.`delete_time` IS NULL
	AND  auxiliary_delivery_node.audit_time BETWEEN " . $stare . " and " . $end;
//            echo $sql . "\n";;
            //原生sql
            $reality_artificial = db('auxiliary_delivery_node');
            $sum1               = $reality_artificial->query($sql1);
            
            
            $stare1 = $stare - (3 * 24 * 60 * 60);
            $sql2   = "SELECT
COUNT( auxiliary_delivery_node.audit_time ) AS count
FROM
 `auxiliary_delivery_node`
 LEFT JOIN `auxiliary_delivery_schedule` ON `auxiliary_delivery_schedule`.`id` = `auxiliary_delivery_node`.`auxiliary_delivery_schedul_id`
 LEFT JOIN `auxiliary_project_list` ON `auxiliary_project_list`.`id` = `auxiliary_delivery_schedule`.`auxiliary_project_list_id`
WHERE
 `auxiliary_delivery_schedule`.`delete_time` IS NULL
 AND `auxiliary_project_list`.`delete_time` IS NULL
 AND `auxiliary_delivery_node`.`audit_time` BETWEEN " . $stare . " and " . $end . "
	AND  auxiliary_delivery_node.upload_time BETWEEN " . $stare1 . " and " . $end;
//            echo $sql . "\n";;
            //原生sql
            $reality_artificial = db('auxiliary_delivery_node');
            $sum2               = $reality_artificial->query($sql2);
            $list[$j]['date']   = $date;
            $list[$j]['count']  = $sum[0]['count'];
            $list[$j]['count1'] = $sum1[0]['count'];
            $list[$j]['count2'] = $sum2[0]['count'];
            $j++;
            
        }
        $list    = array_merge($list);
        $xlsCell = ['日期', '当月每天提交巡检节点的数量', '当月每天验收通过巡检节点的数量', '当月每天验收通过巡检节点的数量(通过的节点最后提交日期为通过当日前3日内)'];
        vendor('phpcvs.Csv');
        $csv = new Csv();
        $csv->export($list, $xlsCell);
    }
    
    public function auditMaterialsList()
    {
        $date = \request()->get();
        if (empty($date['time'])) {
            echo "请输入时间";
            die;
        }
        $timestamp          = strtotime($date['time']);
        $startTime          = strtotime(date('Y-m-1 00:00:00', $timestamp));
        $endTime            = strtotime($date['endTime']);
        $endTime            = strtotime(date('Y-m-d 23:59:59', $endTime));
        $auditMaterialsList = \db('audit_materials')
            ->field('order.order_no,capital.class_b,FROM_UNIXTIME(order.settlement_time,"%Y-%m-%d %H:%i:%s")as settlementTime,store.store_name,user.username,FROM_UNIXTIME(audit_materials.creationtime,"%Y-%m-%d %H:%i:%s")as createdTime,audit_materials.capital_id,materials_id,order.city_id')
            ->join('capital', 'capital.capital_id=audit_materials.capital_id', 'left')
            ->join('order', 'capital.ordesr_id=order.order_id', 'left')
            ->join('user', 'user.user_id=audit_materials.user_id', 'left')
            ->join('store', 'user.store_id=audit_materials.store_id', 'left')
            ->where('capital.types', 1)
            ->where('capital.enable', 1)
            ->where('order.settlement_time', '<>', 0)
            ->whereNotNull('order.settlement_time')
            ->group('capital.capital_id')
            ->order('audit_materials.materials_id desc')
            ->whereBetween('audit_materials.creationtime', [$startTime, $endTime])
            ->select();
        
        $description     = \db('user_config', config('database.zong'))->where('key', 'audit_materials_evaluate')->value('value');
        $description     = json_decode($description, true);
        $descriptionList = $description;
        foreach ($auditMaterialsList as $s => $v1) {
            $list         = \db('audit_materials_evaluate')->where('materials_id', $v1['materials_id'])->select();
            $descriptions = $descriptionList;
            $lo           = [];
            foreach ($descriptions as $k => $item) {
                $l = '';
                foreach ($item['choice'] as $k1 => $value) {
                    $descriptions[$k]['choice'][$k1]['select'] = 0;
                    foreach ($list as $v) {
                        if ($item['type'] == $v['type'] && $value['id'] == $v['evaluate']) {
                            $l = $value['title'];
                        }
                    }
                }
                $lo[] = $l;
            }
            
            $auditMaterialsList[$s]['agency_name'] = \db('reimbursement')->join('reimbursement_relation', 'reimbursement_relation.reimbursement_id=reimbursement.id', 'left')->join('agency', 'agency.agency_id=reimbursement_relation.bank_id', 'left')->where('find_in_set(:id,reimbursement.capital_id)', ['id' => $v1['capital_id']])->value('agency.agency_name');
            
            $auditMaterialsList[$s]['description']  = empty($lo) ? '' : $lo[0];
            $auditMaterialsList[$s]['description1'] = empty($lo) ? '' : $lo[1];
            $auditMaterialsList[$s]['description2'] = empty($lo) ? '' : $lo[2];
            $auditMaterialsList[$s]['description3'] = empty($lo) ? '' : $lo[3];
            $auditMaterialsList[$s]['description4'] = empty($lo) ? '' : $lo[4];
            unset($auditMaterialsList[$s]['capital_id'], $auditMaterialsList[$s]['materials_id']);
        }
        $xlsCell = ['订单号', '主材名称', '订单结算时间', '店铺', '店长名称', '主材验收完成时间', '城市', '供应商名称', '质量评价', '时效评价', '供应商服务评价', '客户满意度评价', '内部满意度评价'];
        vendor('phpcvs.Csv');
        $csv = new Csv();
        $csv->export($auditMaterialsList, $xlsCell);
    }
    
    /*
    * 成都所有店铺订单
    */
    public function beExpectedTo()
    {
        $param      = Request::instance()->get();
        $Capital    = new Capital();
        $arr        = date_parse_from_format('Y年m月', $param['startTime']);
        $time       = mktime(0, 0, 0, $arr['month'], 01, $arr['year']);
        $start      = strtotime(date('Y-m-01 00:00:00', $time));//获取指定月份的第一天
        $end        = strtotime(date('Y-m-t 23:59:59', $time)); //获取指定月份的最后一天
        $order      = \db('order')
            ->field('order.city_id,envelopes.give_money,envelopes.expense,envelopes.purchasing_expense,envelopes.purchasing_discount,
             order_aggregate.main_price-order_setting.order_fee- order_aggregate.material_price-order_aggregate.reimbursement_price-order_aggregate.master_price as lirun, order_aggregate.main_price,
           order.order_id as orderId,order.state,order.created_time,user.store_id,envelopes.envelopes_id,order_aggregate.total_price,order_aggregate.main_price,order_aggregate.agent_price,order.order_no')
            ->join('user', 'user.user_id=order.assignor', 'left')
            ->join('envelopes', 'envelopes.ordesr_id=order.order_id and envelopes.type=1', 'left')
            ->join('order_aggregate', 'order.order_id=order_aggregate.order_id', 'left')
            ->join('order_setting', 'order.order_id=order_setting.order_id', 'left');
        $orderList  = $order->whereBetween('order.created_time', [$start, $end])
//            $order->where(function ($quer) use ($start, $end) {
//            $quer->whereBetween('order.cleared_time', [$start, $end])->whereOr('contract.con_time', 'between', [$start, $end]);
//        })
            
            ->where('order_aggregate.total_price', '>',0)->where('order.state', '<',4)->where('user.ce', '<>', 2)->group('order.order_id')->select();
        $orderArray = [];
        foreach ($orderList as $k => $item) {
            $orderArray[$k]['id']               = $item['orderId'];
            $orderArray[$k]['order_no']               = $item['order_no'];
            $orderArray[$k]['total_price']            = $item['total_price'];
            $orderArray[$k]['main_price']            = $item['main_price'];
            $orderArray[$k]['agent_price']            = $item['agent_price'];
            $orderArray[$k]['give_money']            = $item['give_money'];
            $orderArray[$k]['expense']            = $item['expense'];
            $orderArray[$k]['purchasing_expense']            = $item['purchasing_expense'];
            $orderArray[$k]['purchasing_discount']            = $item['purchasing_discount'];
            $orderArray[$k]['state']            = $item['state'];
            $orderArray[$k]['created_time']            = date('Y-m-d H:i:s',$item['created_time']);
            $orderArray[$k]['clearedOrde']      = 0;
            $orderArray[$k]['beExpectedToOrde'] = 0;
            if (!empty($item['cleared_time'])) {
                $orderArray[$k]['clearedOrde'] = round($item['lirun'] / $item['main_price'], 4);
            }
            $l                 = $Capital->capitalCheck($item['orderId'], $item['envelopes_id']);
            $u8c_invprice      = db('u8c_invprice', config('database.zong'))->where('store_id', $item['store_id'])->select();
            $labor_cost_config = db('labor_cost_config', config('database.zong'))->where('city_id', $item['city_id'])->find();
            foreach ($l as $k1 => $v) {
                $zhuMaterialCost = 0;
                if (!empty($v['content'])) {
                    $content = json_decode($v['content'], true);
                    foreach ($content as $k1 => $item) {
                        foreach ($u8c_invprice as $value) {
                            if ($item['invcode'] == $value['invcode'] && $item['sum'] == 0) {
                                $content[$k]['sum'] = bcmul(bcmul($item['recommended_quantity'], $value['nabprice'], 2), $v['square'], 2);
                                $zhuMaterialCost    += $content[$k1]['sum'];
                                
                            }
                        }
                        
                    }
                    
                    
                    $v['content'] = json_encode($content);
                }
                
                $zhu           = array_sum(array_column($l, 'main_price')) - $l[0]['give_money'] + $l[0]['expense'];//祝主合同清单
                $zhuLaborCosts = array_sum(array_column($l, 'zhuLaborCost'));
                
                $zhuLaborCost  = $zhuLaborCosts + $zhu * ($labor_cost_config['fixed_labor_cost_ratio'] / 100);//人工费
                $zhuMeasurefee = bcmul($zhu, ($labor_cost_config['main_contract_measure_ratio'] / 100), 2);//措施费
                
                $zhuOrderFee = $l[0]['order_fee'];//订单费
                $zhuProfit   = 0;
                if ($zhu != 0) {
                    $zhuProfit = bcsub(bcsub(bcsub(bcsub($zhu, $zhuLaborCosts, 4), $zhuMeasurefee, 4), $zhuMaterialCost, 4), $zhuOrderFee, 4);
                    $zhuProfit = bcsub(bcmul(sprintf('%.4f', $zhuProfit / $zhu), 100, 4), $labor_cost_config['fixed_labor_cost_ratio'], 3);//利润
                }
                $dai             = array_sum(array_column($l, 'agent_price')) - $l[0]['purchasing_discount'] + $l[0]['purchasing_expense'];
                $daiMeasurefee   = bcmul($dai, ($labor_cost_config['agency_contract_measure_ratio'] / 100), 2);
                $daiLaborCost    = array_sum(array_column($l, 'daiLaborCost'));
                $daiOrderFee     = $l[0]['order_fee'];
                $daiMaterialCost = array_sum(array_column($l, 'daiMaterialCost'));//材料费
                
                if ($zhu != 0) {
                    $daiOrderFee = 0;
                }
                $daiProfit = 0;
                
                if ($dai != 0) {
                    $estimate=round(($daiLaborCost+$daiMeasurefee+$daiMaterialCost)/$dai,4)*100;
                    if($estimate<60){
                        $daiMaterialCost=array_sum(array_column($l, 'agent_price'))*0.6;
                    }
                    $daiProfit = bcsub(bcsub(bcsub(bcsub($dai, $daiLaborCost, 4), $daiMeasurefee, 4), $daiMaterialCost, 4), $daiOrderFee, 4);
                    $daiProfit = bcmul(sprintf('%.4f', $daiProfit / $dai), 100, 4);//利润
                }
//                $orderArray[$k]['beExpectedToOrde'] = (string)$zhu;//利润
                $orderArray[$k]['beExpectedToOrde'] = $zhuProfit;//利润
                $orderArray[$k]['beExpectedToOrde1'] = $daiProfit;//利润
            }
            
            
        }
        cache('主合同利润', $orderArray);
        $xlsCell = ['订单ID','订单编号','订单金额', '主合同金额','代购合同金额', '主合同优惠金额', '代购合同优惠金额','主合同管理费','代购合同管理费', '订单状态','创建时间', '订单实际主合同利润', '订单预估主合同利润','订单预估代购合同利润'];
        vendor('phpcvs.Csv');
        $csv = new Csv();
        $csv->export($orderArray, $xlsCell);
    }
    
    public function app_user_order_capital_labor_costs()
    {
        $order   = \db('app_user_order_capital_labor_costs')
            ->field('FROM_UNIXTIME(adopt_time,"%Y-%m-%d")as adopt_time,sum(if((state =1 || state =0) and examine_user_id !=0,1,0)) as "第三人审核通过数",sum(if(state=2 and examine_user_id !=0,1,0)) as "第三人审核不通过数"')
            ->group('app_user_order_capital_labor_costs.adopt_time')->select();
        $xlsCell = ['审核时间', '第三人审核通过数', '不通过数'];
        vendor('phpcvs.Csv');
        $csv = new Csv();
        $csv->export($order, $xlsCell);
    }
    
    public function masterDuan()
    {
        $param = Request::instance()->get();
        $id = base64_decode(strtr($param['i'], '-_', '+/'));
        $key128 = '0123456789abcdef';
        $iv = 'abcdef0123456789';
        $param['id'] = openssl_decrypt($id, 'aes-128-cbc', $key128, OPENSSL_RAW_DATA, $iv);
        $user  = [];
        //你入职益鸟已经X天了（注解：按照系统内入职时间计算，部分人可能和实际有差异）
        //在过去的2023年，你在X店铺，与众多师傅店长一起努力，感谢你，祝2024，工作顺利！
        $days_diff         = \db('app_user')
            ->field('TIMESTAMPDIFF(DAY, FROM_UNIXTIME(created_at,"%Y-%m-%d %H:%i:%s"),NOW()) AS days_diff,store.store_name')
            ->join('store', 'app_user.store_id=store.store_id', 'left')
            ->where('app_user.id', $param['id'])->find();
        if(empty($days_diff)){
            res_date(null,300,'分享链接参数错误，无法查看，祝您新年新气象');
        }
        $user['entry']     = empty($days_diff) ? 0 : $days_diff['days_diff'];
        $user['storeName'] = empty($days_diff) ? '' : $days_diff['store_name'];
        //过去一年，你参与施工了X个订单，其中【局部改造】最多，达到X个（注解：数据中排除了0工费的工地）
        $app_user_order_capital = db('app_user_order_capital')
            ->field('order_id,user_id')
            ->distinct(true)
            ->where('app_user_order_capital.work_time', '<>', 0)
            ->whereNull('app_user_order_capital.deleted_at')
            ->group('order_id')
            ->buildSql();
        $orderProId             = db('order')
            ->field('count(order.pro_id) as value,goods_category.title as name')
            ->join('app_user_order', 'order.order_id=app_user_order.order_id', 'left')
            ->join('goods_category', 'order.pro_id=goods_category.id', 'left')
//            ->join('app_user_order_capital', 'app_user_order_capital.user_id=app_user_order.user_id and app_user_order_capital.order_id=app_user_order.order_id and app_user_order_capital.deleted_at is null', 'left')
            ->join([$app_user_order_capital => 'app_user_order_capital'], 'app_user_order_capital.order_id=app_user_order.order_id and app_user_order_capital.user_id=' . $param['id'], 'right')
            ->where('app_user_order.status', '<>', 10)
            ->where('app_user_order.created_at', '>=', 1672502400)
            ->whereNull('app_user_order.deleted_at')
            ->where('app_user_order.user_id', $param['id'])
            ->group('order.pro_id')
            ->select();

//        $user['participateInConstruction'] = empty($orderProId) ? '' : $orderProId[array_search(max(array_column($orderProId, 'count')), array_column($orderProId, 'count'))];
        $user['participateInConstruction'] = ['participateInConstruction'=> empty($orderProId) ? 0 :array_sum(array_column($orderProId, 'value')),'max'=> empty($orderProId) ? ["value"=>0, "name"=>""] :$orderProId[array_search(max(array_column($orderProId, 'value')), array_column($orderProId, 'count'))],'list'=> empty($orderProId) ? [] :$orderProId];
        //3、过去一年，你完工结算了X个订单，
        //和X店长配合的最多（注解：数据中排除了0工费的工地）
        $orderCoordinate = db('order')
            ->field('count(order.assignor) count,user.username')
            ->join('app_user_order', 'order.order_id=app_user_order.order_id', 'left')
            ->join('user', 'order.assignor=user.user_id', 'left')
            ->join([$app_user_order_capital => 'app_user_order_capital'], 'app_user_order_capital.order_id=app_user_order.order_id and app_user_order_capital.user_id=' . $param['id'], 'right')
            ->where('app_user_order.status', '<>', 10)
            ->where('app_user_order.created_at', '>=', 1672502400)
            ->whereNull('app_user_order.deleted_at')
            ->where('app_user_order.user_id', $param['id'])
            ->group('order.assignor')
            ->select();
        
        //6、还记得吗，在X工地，你获得了X工费，是今年最大的一笔单工地收入
        
        $app_user_order_capitals = db('app_user_order_capital')
            ->field('order_id,user_id,sum(personal_price) as personal_price')
            ->distinct(true)
            ->where('app_user_order_capital.work_time', '<>', 0)
            ->whereNull('app_user_order_capital.deleted_at')
            ->group('order_id,user_id')
            ->buildSql();
        $orderPrice              = db('app_user_order')
            ->field('sum(app_user_order_capital.personal_price) as master_price,app_user_order.order_id,order.addres')
            ->join([$app_user_order_capitals => 'app_user_order_capital'], 'app_user_order_capital.order_id=app_user_order.order_id and app_user_order_capital.user_id=' . $param['id'], 'right')
            ->join('order', 'order.order_id=app_user_order.order_id', 'left')
            ->where('app_user_order.status', 5)
            ->where('app_user_order.cleared_time', '>=', 1672502400)
            ->whereNull('app_user_order.deleted_at')
            ->group('app_user_order.order_id')
            ->where('app_user_order.user_id', $param['id'])
            ->select();
        $user['orderCoordinate'] =['count'=>0,'coordinate'=>''];
        
        if (!empty($orderCoordinate)) {
            $user['orderCoordinate'] = ['count' => count($orderPrice), 'coordinate' => $orderCoordinate[array_search(max(array_column($orderCoordinate, 'count')), array_column($orderCoordinate, 'count'))]['username']];
        }
        $user['orderPrice'] = ['price'=>0,'orderPriceAddres'=>''];
        if (!empty($orderPrice)) {
            $maxArray           = $orderPrice[array_search(max(array_column($orderPrice, 'master_price')), array_column($orderPrice, 'master_price'))];
            $user['orderPrice'] = ['price' => $maxArray['master_price'], 'orderPriceAddres' => $maxArray['addres']];
        }
        //4、还记得吗，你最早的一次工地打卡，是X点，位于【地址】，早起的鸟儿有虫吃，对吧
        $earliest         = db('app_user_order_clock_in')
            ->field('clock_in_at-UNIX_TIMESTAMP(FROM_UNIXTIME(clock_in_at,"%Y-%m-%d 00:00:00")) as l,FROM_UNIXTIME(clock_in_at,"%Y-%m-%d %H:%i:%s") as clock_in_at,address')
            ->where('app_user_order_clock_in.created_at', '>=', 1672502400)
            ->where('app_user_order_clock_in.user_id', $param['id'])
            ->where('app_user_order_clock_in.type', 11)
            ->order('l asc')
            ->select();
        $Latest           = db('app_user_order_clock_in')
            ->field('clock_in_at-UNIX_TIMESTAMP(FROM_UNIXTIME(clock_in_at,"%Y-%m-%d 00:00:00")) as l,FROM_UNIXTIME(clock_in_at,"%Y-%m-%d %H:%i:%s") as clock_in_at,address')
            ->where('app_user_order_clock_in.created_at', '>=', 1672502400)
            ->where('app_user_order_clock_in.user_id', $param['id'])
            ->where('app_user_order_clock_in.type', 21)
            ->order('l desc')
            ->select();
        $user['earliest'] = ["l"=>0, "clock_in_at"=>"", "address"=>""];
        $user['Latest']   = ["l"=>0, "clock_in_at"=>"", "address"=>""];
        if (!empty($earliest)) {
            $user['earliest'] = $earliest[0];
            
        }
        if (!empty($Latest)) {
            $user['Latest'] = $Latest[0];
        }
        $user['url'] = 'http://myear.yiniao.cc/#/?type=1&i='.$param['i'].'&city='.config('city');
        res_date($user);
    }
    
    public function StoreManager()
    {
        $param = Request::instance()->get();
        $id = base64_decode(strtr($param['i'], '-_', '+/'));
        $key128 = '0123456789abcdef';
        $iv = 'abcdef0123456789';
        $param['id'] = openssl_decrypt($id, 'aes-128-cbc', $key128, OPENSSL_RAW_DATA, $iv);
        $user  = [];
        //你入职益鸟已经X天了（注解：按照系统内入职时间计算，部分人可能和实际有差异）
        $days_diff     = \db('user')
            ->field('TIMESTAMPDIFF(DAY, FROM_UNIXTIME(created_time,"%Y-%m-%d %H:%i:%s"),NOW()) AS days_diff')
            ->where('user.user_id', $param['id'])->find();
        if(empty($days_diff)){
            res_date(null,300,'分享链接参数错误，无法查看，祝您新年新气象');
        }
        $user['entry'] = empty($days_diff) ? '' : $days_diff['days_diff'];
        //2、过去一年，你签约了X个订单，其中【局部改造】最多，达到X个
        $orderProId = db('order')
            ->field('count(order.pro_id) value,goods_category.title as name')
            ->join('order_times', 'order_times.order_id=order.order_id', 'left')
            ->join('goods_category', 'order.pro_id=goods_category.id', 'left')
            ->where('order.assignor', $param['id'])
            ->where('order_times.signing_time', '>=', 1672502400)
            ->group('order.pro_id')
            ->select();
        
        $user['participateInConstruction'] = empty($orderProId) ? ['count' => 0, 'max' =>['value'=>0,'name'=>''],'list'=>[]] : ['count' => array_sum(array_column($orderProId, 'value')), 'max' => $orderProId[array_search(max(array_column($orderProId, 'value')), array_column($orderProId, 'count'))],'list'=>$orderProId];
        //过去一年，你完工结算了X个订单，最长的一个订单，经历了N天
        $orderPrice         = db('order')
            ->field('TIMESTAMPDIFF(DAY, FROM_UNIXTIME(order.start_time,"%Y-%m-%d %H:%i:%s"),FROM_UNIXTIME(order.finish_time,"%Y-%m-%d %H:%i:%s")) AS gong,order.order_id')
            ->where('order.start_time', '>=', 1672502400)
            ->where('order.finish_time', '>', 0)
            ->where('order.cleared_time', ">", 0)
            ->where('order.assignor', $param['id'])
            ->select();
        $orderConut         = db('order')
            ->field('sum(if(order.state=7 AND ( CASE  WHEN order_aggregate.main_price=0 and order_aggregate.agent_price !=0 and order.settlement_time is Not null THEN 1 WHEN order_aggregate.main_price !=0 and order_aggregate.agent_price =0 and order.cleared_time is not null THEN  1 WHEN order_aggregate.main_price !=0 and order_aggregate.agent_price !=0 and order.cleared_time is not null and order.settlement_time is not null THEN 1  ELSE 0 END)=1,1,0)) as closed')
            ->join('order_times', 'order.order_id=order_times.order_id', 'left')
            ->join('order_aggregate', 'order.order_id=order_aggregate.order_id', 'left')
            ->where('order.assignor', $param['id'])
            ->select();
        $user['orderConut'] = ['count' => empty($orderConut[0]['closed']) ? 0 : $orderConut[0]['closed'], 'maxArray' => empty($orderPrice) ?0 : $orderPrice[array_search(max(array_column($orderPrice, 'gong')), array_column($orderPrice, 'gong'))]['gong']];
        //还记得吗，你最早的一次上门打卡，是X点，位于【地址】，早起的鸟儿有虫吃，对吧
        // 还记得吗，你最晚的一次上门打卡，是X点，位于【地址】，致敬自己，奋斗者
        $earliest            = db('clock_in')
            ->field('punch_clock-UNIX_TIMESTAMP(FROM_UNIXTIME(punch_clock,"%Y-%m-%d 00:00:00")) as l,FROM_UNIXTIME(punch_clock,"%Y-%m-%d %H:%i:%s") as clock_in_at,address')
            ->join('order', 'order.order_id=clock_in.order_id', 'left')
            ->where('clock_in.punch_clock', '>=', 1672502400)
            ->where('order.assignor', $param['id'])
            ->order('l asc')
            ->select();
        $Latest              = db('clock_in')
            ->field('punch_clock-UNIX_TIMESTAMP(FROM_UNIXTIME(punch_clock,"%Y-%m-%d 00:00:00")) as l,FROM_UNIXTIME(punch_clock,"%Y-%m-%d %H:%i:%s") as clock_in_at,address')
            ->join('order', 'order.order_id=clock_in.order_id', 'left')
            ->where('clock_in.punch_clock', '>=', 1672502400)
            ->where('order.assignor', $param['id'])
            ->order('l desc')
            ->select();
        $user['clock_in_at'] = ['earliest' => empty($earliest) ? ["l"=>0, "clock_in_at"=>'', "address"=>""] : $earliest[0], 'Latest' => empty($Latest) ? ["l"=>0, "clock_in_at"=>'', "address"=>""] : $Latest[0]];
        //还记得吗，在X时间，你和客户达成最终协议，完成了合同签署并且收取首笔款，时间很晚了，但你依然斗志昂扬
        $contractLatest = db('contract')
            ->field('con_time,order_id')
            ->join('order', 'order.order_id=contract.orders_id', 'left')
            ->where('contract.con_time', '>=', 1672502400)
            ->order('con_time asc')
            ->where('order.assignor', $param['id'])
            ->select();
        
        $al  = [];
        $al1 = [];
        foreach ($contractLatest as $k => $item) {
            $data   = date('Y-m-d', $item['con_time'] - 86400);
            $datas  = date('Y-m-d 00:00:00', $item['con_time'] - 86400);
            $date   = strtotime(date('Y-m-d 23:59:59', strtotime($data))) + 21600;
            $start  = strtotime(date('Y-m-d 06:00:00', strtotime($data)));
            $data1  = date('Y-m-d', $item['con_time']);
            $date1  = strtotime(date('Y-m-d 23:59:59', strtotime($data1))) + 21600;
            $start1 = strtotime(date('Y-m-d 06:00:00', strtotime($data1)));
            if ($date > $item['con_time'] && $start < $item['con_time']) {
                $contractLatest[$k]['data'] = $data;
            }
            if ($date1 > $item['con_time'] && $start1 < $item['con_time']) {
                $contractLatest[$k]['data'] = $data1;
            }
            $contractLatest[$k]['s']         = abs(strtotime($contractLatest[$k]['data']) - $item['con_time']);
            $contractLatest[$k]['con_times'] = date('Y-m-d H:i:s', $item['con_time']);
        }
        $user['contractLatest'] = empty($contractLatest) ? ["con_time"=>'', "order_id"=>'', "data"=> "", "s"=> 0, "con_times"=>""] : $contractLatest[array_search(max(array_column($contractLatest, 's')), array_column($contractLatest, 's'))];
        //还记得吗，在X时间，你完成了今年最大的一笔签约，金额合计X元
        $orderSigning         = db('order')
            ->field('sum(order_achievement.assert_money) as assert_money,FROM_UNIXTIME(order_achievement.assert_time,"%Y-%m-%d %H:%i:%s") as assert_time')
            ->join('order_achievement', 'order_achievement.order_id=order.order_id', 'left')
            ->where('order.assignor', $param['id'])
            ->where('order_achievement.assert_time', '>=', 1672502400)
            ->where('order_achievement.delete_time', 0)
            ->group('order.order_id')
            ->whereIn('order_achievement.achievement_type', [1, 2])
            ->select();
        $user['orderSigning'] = empty($orderSigning) ? ["assert_money"=> "0", "assert_time"=>""] : $orderSigning[array_search(max(array_column($orderSigning, 'assert_money')), array_column($orderSigning, 'assert_money'))];
        //在过去的2023年，你共计获得销冠X次，所在店铺也获得了X次最高业绩店铺，你的贡献大家都看在眼里（如果没有销冠数据则销冠话术变为：你还没有获得过销冠；如果店铺没有过全国最高业绩则话术变为：你所在的店铺还没有问鼎月度最高业绩店铺，要一起加油哦）
        
        $userFind = \db('store', config('database.zong'))->join('user','user.store_id=store.store_id')->where('user_id',$param['id'])->field('user_id,username,mobile,user_id,store_name,user.store_id')->find();
        vendor('phpcvs.Csv');
        $csv = new Csv();
        $so  = $csv->inputCsv("/www/wwwroot/api.shopowner.yiniao.cc/2023.1-2023.11销冠个人及销冠店铺.csv", '奖项名称,月份,城市,姓名,业绩,备注,电话', '');
        $s1  = $csv->inputCsv("/www/wwwroot/api.shopowner.yiniao.cc/2023.1-2023.11销冠店铺.csv", '奖项名称,月份,城市,姓名,业绩', '');
        $userList=[];
        $storeList=[];
        
        foreach ($so as $k=>$value) {
            if ($userFind['mobile'] == $value["电话"]) {
                $userList[$k]['ye'][] = $value;
                $userList[$k]['user_id'] = $userFind['user_id'];
                $userList[$k]['username'] = $userFind['username'];
            }
        }
        
        
        foreach ($s1 as $k=>$value) {
            if ($userFind['store_name'] == $value["城市"]) {
                $storeList[$k]['ye'][] = $value;
                $storeList[$k]['store_id'] = $userFind['store_id'];
                $storeList[$k]['store_name'] = $userFind['store_name'];
            }
        }
        $userList=array_merge($userList);
        $storeList=array_merge($storeList);
        $user['sellingCrown']=['personaSellingCrown'=>empty($userList)?"0":count($userList),'storeSellingCrown'=>empty($storeList)?0:count($storeList),'type'=>empty($userList)?0:1];
        //经过2023的努力，你总共完成业绩X元，如果非要排名，可能属于【摘星星的人】【只差一点，你就摘到了星星】【没挂科，61分选手】【不多不少，59分】【再努力努力，你又不差】。只是按照全年业绩做的参考，入职时间较短的同学不要灰心哦
        $orderPerformance = db('order', config('database.zong'))
            ->field('sum(order_achievement.assert_money) as total_price,order.assignor,group_concat(order.order_id)')
            ->join('order_achievement', 'order_achievement.order_id=order.order_id', 'left')
            ->join('user', 'user.user_id=order.assignor', 'left')
            ->where('order_achievement.assert_time', '>=', 1672502400)
            ->where('order_achievement.delete_time', 0)
            ->where('user.ce', 1)
            ->group('order.assignor')
            ->order('total_price desc')
            ->select();
        $userList         = [];
        foreach ($orderPerformance as $k => $value) {
            if ($value['assignor'] == $param['id']) {
                $zan   = (string)round(($k + 1) / count($orderPerformance), 3) * 100;
                $title = '';
                if ($zan <= 10) {
                    $title = "【摘星星的人】";
                } elseif (10 <= $zan && 20 >= $zan) {
                    $title = "【只差一点，你就摘到了星星】";
                } elseif (20 <= $zan && 40 >= $zan) {
                    $title = "【没挂科，61分选手】";
                } elseif (40 <= $zan && 50 >= $zan) {
                    $title = "【不多不少，59分】";
                } else {
                    $title = "【再努力努力，你又不差】";
                }
                $userList = [
                    'yeji' => $value['total_price'],
                    'xu' => $k + 1,
                    'zan' => $zan,
                    'title' => $title,
                ];
                break;
            }
        }
        $user['ye'] = empty($userList)?["yeji"=>0, "xu"=> 0, "zan"=>0, "title"=>'【再努力努力，你又不差】']:$userList;
        $user['url'] = 'http://syear.yiniao.cc/#/?type=1&i='.$param['i'].'&city='.config('city');
        res_date($user);
    }
    public function kl(){
        $list=db('shopwner_profit_margin_limitation', config('database.statistics'))->where('id',63)->find();
        $store_id=db('order', config('database.zong'))->join('user','user.user_id=order.assignor','left')->join('store','store.store_id=user.store_id','left')->where('order_id',$list['order_id'])->field('store.store_id,store.city')->find();
        $city = $store_id['city'];
        $give_money=0;
        $expense=0;
        $purchasing_discount=0;
        $purchasing_expense=0;
        if($list['type']==1){
            $give_money=$list['give_money'];
            $expense=$list['expense'];
        }
        if($list['type']==2){
            $purchasing_discount=$list['give_money'];
            $purchasing_expense=$list['expense'];
        }
        $capitalList=json_decode($list['param'],true);
        $l=[];
        foreach ($capitalList as $k => $item) {
            $capitalList[$k]['give_money']          = $give_money;
            $capitalList[$k]['expense']             = $expense;
            $capitalList[$k]['purchasing_discount'] = $purchasing_discount;
            $capitalList[$k]['purchasing_expense']  = $purchasing_expense;
            $capitalList[$k]['created_time']        = time();
            $capitalList[$k]['main_price']          = 0;
            $capitalList[$k]['agent_price']         = 0;
            $option_value_id                        = 0;
            if (isset($item['specsList']) && !empty($item['specsList'])) {
                $option_value_id = array_column($item['specsList'], 'option_value_id');
            }
            $specsList                  = \db('detailed_option_value_material')
                ->field("group_concat(detailed_option_value_material.invcode) as invcode,
                  CONCAT('[', GROUP_CONCAT(JSON_OBJECT('invcode',detailed_option_value_material.invcode,'option_value_id',detailed_option_value_material.option_value_id,'recommended_quantity',detailed_option_value_material.recommended_quantity,'sum',0) SEPARATOR ','), ']') as  content")
                ->whereIn('detailed_option_value_material.option_value_id', $option_value_id)
                ->find();
            $capitalList[$k]['invcode'] = empty($specsList) ? '' : $specsList['invcode'];//三
            $capitalList[$k]['content'] = empty($specsList) ? '' : $specsList['content'];//三
            if (($item['agency'] == 1 && $item['products'] != 1) || ($item['products'] == 1 && $item['unique_status'] && $item['agency'] == 1)) {
                $capitalList[$k]['agent_price']     = $item['toPrice'];
                $capitalList[$k]['daiMaterialCost'] = round($item['base_cost'] * $item['fang'], 2);
                $capitalList[$k]['daiLaborCost']    = $item['labor_cost_price'];
            }
            if (($item['agency'] == 0 && $item['products'] != 1) || ($item['products'] == 1 && $item['unique_status'] && $item['agency'] == 0)) {
                $capitalList[$k]['main_price']   = $item['toPrice'];
                $capitalList[$k]['zhuLaborCost'] = (string)round($item['sumMast'] * $item['fang'], 2);//三
            }
            
            $capitalList[$k]['order_fee'] = 0;
        }
        
        $u8c_invprice      = db('u8c_virtual_material', config('database.zong'))->where(function ($query) use ($store_id) {
            $query->where('store_id', $store_id['store_id'])
                ->whereOr(function ($query) use ($store_id) {
                    $query->where('city_id', $store_id['city'])
                        ->where('store_id', 0);
                });
        })->select();
        $labor_cost_config = db('labor_cost_config', config('database.zong'))->where('city_id', $city)->find();
        $interes_rate      = Db::connect(config('database.zong'))->table('sys_config')->where('keyss', 'interes_rate')->value('valuess');
        $interes_rate      = json_decode($interes_rate, true);
        
        $l                 = $capitalList;
        $s=[];
        foreach ($l as $k => $v) {
            $zhuMaterialCost = 0;
            if (!empty($v['content'])) {
                $content = json_decode($v['content'], true);
                foreach ($content as $item) {
                    foreach ($u8c_invprice as $value) {
                        if ($item['invcode'] == $value['invcode'] && $item['sum'] == 0) {
                            $content[$k]['sum'] = bcmul(bcmul($item['recommended_quantity'], $value['nabprice'], 2), $v['fang'], 2);
                            $zhuMaterialCost    += $content[$k]['sum'];
                            
                        }
                    }
                    
                }
                $v['content'] = json_encode($content);
            }
            if($list['type']==1 && $v['agency'] == 0){
                $s[$k]['order_id']     = $list['order_id'];
                $s[$k]['type']     = $list['type'];
                $s[$k]['order_fee']     = $v['order_fee'];
                $s[$k]['projectTitle']=$v['projectTitle'];
                $s[$k]['categoryTitle']=$v['categoryTitle'];
                $s[$k]['agencyName']='主材';
                if($v['agency'] == 1){
                    $s[$k]['agencyName']='代购';
                }
                $s[$k]['price']     = $v['toPrice'];
                if (($v['agency'] == 1 && $v['products'] != 1) || ($v['products'] == 1 && $v['unique_status'] && $v['agency'] == 1)) {
                    $s[$k]['MaterialCost'] = round($v['base_cost'] * $v['fang'], 2);
                    $s[$k]['LaborCost']    = $v['labor_cost_price'];
                }
                if (($v['agency'] == 0 && $v['products'] != 1) || ($v['products'] == 1 && $v['unique_status'] && $v['agency'] == 0)) {
                    $s[$k]['MaterialCost']=$zhuMaterialCost;
                    $s[$k]['LaborCost'] = (string)round($v['sumMast'] * $v['fang'], 2);//三
                }
                $s[$k]['small_order_fee']=$v['small_order_fee'];
                $s[$k]['give_money']=$list['give_money'];
                $s[$k]['expense']=$list['expense'];
                $s[$k]['order_rate']=$list['order_rate'];
                $s[$k]['exceeding_rate']=$list['exceeding_rate'];
            }
            if($list['type']==2 && $v['agency'] == 1){
                $s[$k]['order_id']     = $list['order_id'];
                $s[$k]['type']     = $list['type'];
                $s[$k]['order_fee']     = $v['order_fee'];
                $s[$k]['projectTitle']=$v['projectTitle'];
                $s[$k]['categoryTitle']=$v['categoryTitle'];
                $s[$k]['agencyName']='主材';
                if($v['agency'] == 1){
                    $s[$k]['agencyName']='代购';
                }
                $s[$k]['price']     = $v['toPrice'];
                if (($v['agency'] == 1 && $v['products'] != 1) || ($v['products'] == 1 && $v['unique_status'] && $v['agency'] == 1)) {
                    $s[$k]['MaterialCost'] = round($v['base_cost'] * $v['fang'], 2);
                    $s[$k]['LaborCost']    = $v['labor_cost_price'];
                }
                if (($v['agency'] == 0 && $v['products'] != 1) || ($v['products'] == 1 && $v['unique_status'] && $v['agency'] == 0)) {
                    $s[$k]['MaterialCost']=$zhuMaterialCost;
                    $s[$k]['LaborCost'] = (string)round($v['sumMast'] * $v['fang'], 2);//三
                }
                $s[$k]['small_order_fee']=$v['small_order_fee'];
                $s[$k]['give_money']=$list['give_money'];
                $s[$k]['expense']=$list['expense'];
                $s[$k]['order_rate']=$list['order_rate'];
                $s[$k]['exceeding_rate']=$list['exceeding_rate'];
            }
            if($list['type']==3){
                $s[$k]['order_id']     = $list['order_id'];
                $s[$k]['type']     = $list['type'];
                $s[$k]['order_fee']     = $v['order_fee'];
                $s[$k]['projectTitle']=$v['projectTitle'];
                $s[$k]['categoryTitle']=$v['categoryTitle'];
                $s[$k]['agencyName']='主材';
                if($v['agency'] == 1){
                    $s[$k]['agencyName']='代购';
                }
                $s[$k]['price']     = $v['toPrice'];
                if (($v['agency'] == 1 && $v['products'] != 1) || ($v['products'] == 1 && $v['unique_status'] && $v['agency'] == 1)) {
                    $s[$k]['MaterialCost'] = round($v['base_cost'] * $v['fang'], 2);
                    $s[$k]['LaborCost']    = $v['labor_cost_price'];
                }
                if (($v['agency'] == 0 && $v['products'] != 1) || ($v['products'] == 1 && $v['unique_status'] && $v['agency'] == 0)) {
                    $s[$k]['MaterialCost']=$zhuMaterialCost;
                    $s[$k]['LaborCost'] = (string)round($v['sumMast'] * $v['fang'], 2);//三
                }
                $s[$k]['small_order_fee']=$v['small_order_fee'];
                $s[$k]['give_money']=$list['give_money'];
                $s[$k]['expense']=$list['expense'];
                $s[$k]['order_rate']=$list['order_rate'];
                $s[$k]['exceeding_rate']=$list['exceeding_rate'];
            }
            
        }
        $xlsCell = ['订单ID', '1主合同2代购3综合', '订单费','项目名称','分组名称','主材/代购','价格','材料费','人工费','小单费','优惠','管理费','订单利润','利润率超出率'];
        vendor('phpcvs.Csv');
        $csv = new Csv();
        $csv->export($s, $xlsCell);
        res_date($s);
    }
    public function ki(){
        $store_id=db('capital')->join('order','capital.ordesr_id=order.order_id','left')->join('order_info','order_info.order_id=order.order_id','left')->join('detailed','detailed.detailed_id=capital.projectId','left')->where('capital.types',1)->where('capital.enable',1)->field('order_info.city,order_info.assignor,order.order_no,detailed.detailed_title,capital.projectId,capital.class_b,capital.un_Price,capital.square,capital.to_price,capital.labor_cost')->whereBetween('capital.crtime',[1672502400,1719763199])
            ->where('capital.class_b <> detailed.detailed_title')->where('order.state','<>',10)->select();
        $xlsCell = ['城市', '店长', '订单号','清单ID','分组名称','修改后清单名称','单价','方量','总价','清单结算工费'];
        vendor('phpcvs.Csv');
        $csv = new Csv();
        $csv->export($store_id, $xlsCell);
        
    }
    public function rework_end(){
        $rework_end=db('rework_end')->join('order','rework_end.order_id=order.order_id','left')->field('order.assignor,rework_end.update_time,order.deliverer,rework_end.id')->select();
        foreach ($rework_end as $k=>$i){
            $list['user_id']=$i['assignor'];
            if($i['deliverer'] !=0){
                $list['user_id']=$i['deliverer'];
            }
            db('rework_end')->where('id',$i['id'])->update($list);
        }
    }
    
    public function SignUp()
    {
        $order   = \db('agency_task')
        ->field('agency.agency_id,agency.agency_name,capital.capital_id,capital.class_b,capital.projectId,agency_task.agency_order_id')
        ->join('capital','capital.capital_id=agency_task.capital_id','left')
        ->join('agency','agency.agency_id=agency_task.agency_id','left')
        ->where('agency.user_id','>',0)
        ->where('agency_task.delete_time',0)
        ->select();
        $lk=[];
        foreach ($order as $k=>$i){
            $db=Common::city($i['capital_id']);
            $detailed_option_value_material = Db::connect(config('database.' . $db['db']))->table('detailed')
           ->where('detailed_id',$i['projectId'])
           ->where('user_id','>',0)
            ->find();
            if(!empty($detailed_option_value_material)){
                $lk[$k]['agency_id']=$i['agency_id'];
                $lk[$k]['agency_name']=$i['agency_name'];
                $lk[$k]['capital_id']=$i['capital_id'];
                $lk[$k]['class_b']=$i['class_b'];
                $lk[$k]['projectId']=$i['projectId'];
                $lk[$k]['agency_order_id']=$i['agency_order_id'];
                $lk[$k]['title']=$detailed_option_value_material['detailed_title'];
            }
           
        }
    $xlsCell = ['供应商ID', '供应商名称', '基建ID','基建名称','清单ID','主表ID','清单名称'];
    vendor('phpcvs.Csv');
    $lk=array_merge($lk);
    $csv = new Csv();
    $csv->export($lk, $xlsCell);
    }
    public function SignUp1()
    {
        $order   = \db('order_collection',config('database.zong'))
        ->field('order.order_no,order_info.assignor,order_times.signing_time,order.start_time,order.state,order.finish_time,content_id,result_id')
        ->join('order','order.order_id=order_collection.order_id','left')
        ->join('order_info','order_info.order_id=order_collection.order_id','left')
        ->join('order_times','order_times.order_id=order_collection.order_id','left')
       
        ->select();
        $sys_config  = Db::connect(config('database.zong'))->table('sys_config')->where('keyss', 'order_collection')->value('valuess');
        $sys_config  = json_decode($sys_config, true);
        $lk=[];
        foreach ($order as $k=>$i){
            $lk[$k]['order_no']=$i['order_no'];  
            $lk[$k]['assignor']=$i['assignor'];  
            $lk[$k]['signing_time']=date("Y-m-d H:i:s",$i['signing_time']);  
            $lk[$k]['start_time']=date("Y-m-d H:i:s",$i['start_time']); 
            foreach ($sys_config as $k2=>$i2){
                if($i2['id'] == $i['content_id']){
                    $lk[$k]['title']=$i2['title'];  
                }
                foreach ($i2['select'] as $k3=>$i3){
                    if($i3['id'] == $i['result_id']){
                        $lk[$k]['title1']=$i3['title'];  
                    }
                }
            }
            $lk[$k]['state']=$i['state']; 
            $lk[$k]['finish_time']=date("Y-m-d H:i:s",$i['finish_time']);  
           
        }
    $xlsCell = ['订单号', '店长名称', '签约时间','开工时间','状态','完工时间','选择','选择1','选择','选择1','选择','选择1','选择','选择1'];
    vendor('phpcvs.Csv');
    $lk=array_merge($lk);
    $csv = new Csv();
    $csv->export($lk, $xlsCell);
    }
    public function startupNewandover()
    {
        $cd = db('startup_new_handover')
            ->Join(config('database.connections.zong')['database'] . '.order_info', 'order_info.order_id=startup_new_handover.order_id', 'left')
            ->Join(config('database.connections.zong')['database'] . '.order', 'order.order_id=startup_new_handover.order_id', 'left')
            ->Join(config('database.connections.zong')['database'] . '.order_times', 'order_times.order_id=startup_new_handover.order_id', 'left')
          ->field('order.order_no,order_info.assignor,FROM_UNIXTIME(order_times.signing_time,"%Y年%m月%d %H:%i:%S") as uptime,order.state')
            ->select();
        $cq = db('startup_new_handover', config('database.cq'))
        ->Join(config('database.connections.zong')['database'] . '.order_info', 'order_info.order_id=startup_new_handover.order_id', 'left')
        ->Join(config('database.connections.zong')['database'] . '.order', 'order.order_id=startup_new_handover.order_id', 'left')
        ->Join(config('database.connections.zong')['database'] . '.order_times', 'order_times.order_id=startup_new_handover.order_id', 'left')
        ->field('order.order_no,order_info.assignor,FROM_UNIXTIME(order_times.signing_time,"%Y年%m月%d %H:%i:%S") as uptime,order.state')
        ->select();
        $gy = db('startup_new_handover', config('database.gy'))
        ->Join(config('database.connections.zong')['database'] . '.order_info', 'order_info.order_id=startup_new_handover.order_id', 'left')
        ->Join(config('database.connections.zong')['database'] . '.order', 'order.order_id=startup_new_handover.order_id', 'left')
        ->Join(config('database.connections.zong')['database'] . '.order_times', 'order_times.order_id=startup_new_handover.order_id', 'left')
      ->field('order.order_no,order_info.assignor,FROM_UNIXTIME(order_times.signing_time,"%Y年%m月%d %H:%i:%S") as uptime,order.state')
        ->select();
        
        $wh = db('startup_new_handover', config('database.wh'))
        ->Join(config('database.connections.zong')['database'] . '.order_info', 'order_info.order_id=startup_new_handover.order_id', 'left')
        ->Join(config('database.connections.zong')['database'] . '.order', 'order.order_id=startup_new_handover.order_id', 'left')
        ->Join(config('database.connections.zong')['database'] . '.order_times', 'order_times.order_id=startup_new_handover.order_id', 'left')
      ->field('order.order_no,order_info.assignor,FROM_UNIXTIME(order_times.signing_time,"%Y年%m月%d %H:%i:%S") as uptime,order.state')
        ->select();
        $sz = db('startup_new_handover', config('database.sz'))
        ->Join(config('database.connections.zong')['database'] . '.order_info', 'order_info.order_id=startup_new_handover.order_id', 'left')
        ->Join(config('database.connections.zong')['database'] . '.order', 'order.order_id=startup_new_handover.order_id', 'left')
        ->Join(config('database.connections.zong')['database'] . '.order_times', 'order_times.order_id=startup_new_handover.order_id', 'left')
      ->field('order.order_no,order_info.assignor,FROM_UNIXTIME(order_times.signing_time,"%Y年%m月%d %H:%i:%S") as uptime,order.state')
        ->select();
        $sh = db('startup_new_handover', config('database.sh'))
        ->Join(config('database.connections.zong')['database'] . '.order_info', 'order_info.order_id=startup_new_handover.order_id', 'left')
        ->Join(config('database.connections.zong')['database'] . '.order', 'order.order_id=startup_new_handover.order_id', 'left')
        ->Join(config('database.connections.zong')['database'] . '.order_times', 'order_times.order_id=startup_new_handover.order_id', 'left')
      ->field('order.order_no,order_info.assignor,FROM_UNIXTIME(order_times.signing_time,"%Y年%m月%d %H:%i:%S") as uptime,order.state')
        ->select();
        $bj = db('startup_new_handover', config('database.bj'))
        ->Join(config('database.connections.zong')['database'] . '.order_info', 'order_info.order_id=startup_new_handover.order_id', 'left')
        ->Join(config('database.connections.zong')['database'] . '.order', 'order.order_id=startup_new_handover.order_id', 'left')
        ->Join(config('database.connections.zong')['database'] . '.order_times', 'order_times.order_id=startup_new_handover.order_id', 'left')
      ->field('order.order_no,order_info.assignor,FROM_UNIXTIME(order_times.signing_time,"%Y年%m月%d %H:%i:%S") as uptime,order.state')
        ->select();
        $gz = db('startup_new_handover', config('database.gz'))
        ->Join(config('database.connections.zong')['database'] . '.order_info', 'order_info.order_id=startup_new_handover.order_id', 'left')
        ->Join(config('database.connections.zong')['database'] . '.order', 'order.order_id=startup_new_handover.order_id', 'left')
        ->Join(config('database.connections.zong')['database'] . '.order_times', 'order_times.order_id=startup_new_handover.order_id', 'left')
      ->field('order.order_no,order_info.assignor,FROM_UNIXTIME(order_times.signing_time,"%Y年%m月%d %H:%i:%S") as uptime,order.state')
        ->select();
        
        $fs   = db('startup_new_handover', config('database.fs'))
        ->Join(config('database.connections.zong')['database'] . '.order_info', 'order_info.order_id=startup_new_handover.order_id', 'left')
        ->Join(config('database.connections.zong')['database'] . '.order', 'order.order_id=startup_new_handover.order_id', 'left')
        ->Join(config('database.connections.zong')['database'] . '.order_times', 'order_times.order_id=startup_new_handover.order_id', 'left')
      ->field('order.order_no,order_info.assignor,FROM_UNIXTIME(order_times.signing_time,"%Y年%m月%d %H:%i:%S") as uptime,order.state')
        ->select();
        $list = array_merge($cd, $cq, $wh, $gy, $sh, $bj, $sz, $fs, $gz);
        
        $xlsCell = ['订单号', '店长', '签约时间', '状态'];
        vendor('phpcvs.Csv');
        $csv = new Csv();
        $csv->export($list, $xlsCell);
    }
}
