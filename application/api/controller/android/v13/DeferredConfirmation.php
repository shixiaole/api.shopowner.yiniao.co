<?php
/**
 * Created by PhpStorm.
 * User: Raytine
 * Date: 2019/11/29
 * Time: 15:23
 */

namespace app\api\controller\android\v13;





use app\api\model\Common;
use shortLink\WxService;
use think\Controller;
use app\api\model\Authority;
use think\Db;


class DeferredConfirmation extends Controller
{
    protected $us;

    public function _initialize()
    {
        $this->us = Authority::check(1);
    }

  
    public function list(){
        $data         = Authority::param(['orderId']);
        $order_defer=Db::connect(config('database.zong'))->table('order_defer')->where('order_id',$data['orderId'])->where('deleted_at',0)->field('days,concat(if(order_defer.start_time !=0,FROM_UNIXTIME(order_defer.start_time,"%Y-%m-%d "),"--"),"至",if(order_defer.start_time !=0,FROM_UNIXTIME(order_defer.end_time,"%Y-%m-%d "),"--")) as delayTime,FROM_UNIXTIME(order_defer.create_time,"%Y-%m-%d %H:%i:%s") as createTime,CASE
            WHEN order_defer.status=1  THEN   "未确认"
             WHEN order_defer.status=2  THEN  FROM_UNIXTIME(order_defer.confirm_time,"%Y-%m-%d %H:%i:%s")
             WHEN order_defer.status=3 THEN  "已拒绝" END as statusTitle,order_defer.status,order_defer.id,order_defer.description,title as reasonForDelay')->select();
        foreach ($order_defer as $l=>$value){
            $order_defer[$l]['title']="项目延期确认单";
        }
        r_date($order_defer, 200);
    }
    public function orderDeferCount(){
        $data         = Authority::param(['orderId']);
        $order_defer_count=\db('order_setting')->where('order_id',$data['orderId'])->value('order_defer_count');
        r_date($order_defer_count, 200);
    }
    
    public function indexOption(){
       
        $data         = Authority::param(['orderId']);
        $startup=db('startup')->where('orders_id',$data['orderId'])->find();
        r_date(['toBeFinished'=>empty($startup['up_time'])?0:date('Y-m-d',$startup['up_time']),'type'=>[['title'=>'我方原因','id'=>1],['title'=>'客户原因','id'=>2],['title'=>'第三方原因','id'=>3],['title'=>'双方协商一致','id'=>4]]], 200);
    }
    
    public function add(Common $common){
       
        $data         = Authority::param(['orderId','type','days','startTime','endTime','description']);
        $common->isStart($data['orderId']);
        $startup=db('startup')->where('orders_id',$data['orderId'])->find();
        if(empty($startup)){
            r_date(null, 300,"请检查是否开工或进行开工设置，完成设置后才可以签延期单");
        }
        $order_defer_count=db('order_setting')->where('order_id',$data['orderId'])->value('order_defer_count');
        if($order_defer_count<=0 || $data['days']>40){
            r_date(null, 300,"一个订单，只能添加一次延期单，且最长不超过40天");
        }
        $title                                = array_search($data['type'], array_column($this->type(), 'id'));
        $id=Db::connect(config('database.zong'))->table('order_defer')->insert(['order_id'=>$data['orderId'],'days'=>$data['days'],'start_time'=>empty($data['startTime'])?0:strtotime($data['startTime']),'end_time'=>empty($data['endTime'])?0:strtotime($data['endTime'].' 23:59:59'),'title'=>$this->type()[$title]['title'],'create_time'=>time(),'description'=>$data['description']]);
        if($id){
            db('order_setting')->where('order_id',$data['orderId'])->setDec('order_defer_count',1);
            $post_data = [
            'path' => 'pages/order/DelaySheet/DelaySheet',
                'query' => "order_id={$data['orderId']}",
                'env_version' => 'release',
                'expire_type' => 1,
                'expire_interval' => 179
            ];
            $WxService=new WxService();
            $url=$WxService->getWxUrlLink($post_data);
            r_date('https://yiniao.co/'.$url, 200);
        }
        r_date(null, 300,"添加错误");
     
    }
    
    public function deleted(){
       
        $data         = Authority::param(['id']);
        Db::connect(config('database.zong'))->table('order_defer')->where('id',$data['id'])->update(['deleted_at'=>time()]);
        $order_id=Db::connect(config('database.zong'))->table('order_defer')->where('id',$data['id'])->value('order_id');
        db('order_setting')->where('order_id',$order_id)->setInc('order_defer_count',1);
        r_date(null, 200);
    }
    
    public function share(){
       
        $data         = Authority::param(['orderId']);
        $post_data = [
            'path' => 'pages/order/DelaySheet/DelaySheet',
            'query' => "order_id={$data['orderId']}",
            'env_version' => 'release',
            'expire_type' => 1,
            'expire_interval' => 179
        ];
        $WxService=new WxService();
        $url=$WxService->getWxUrlLink($post_data);
        r_date('https://yiniao.co/'.$url, 200);
    }
   public function type(){
        return [['title'=>'我方原因','id'=>1],['title'=>'客户原因','id'=>2],['title'=>'第三方原因','id'=>3],['title'=>'双方协商一致','id'=>4]];
   }

}