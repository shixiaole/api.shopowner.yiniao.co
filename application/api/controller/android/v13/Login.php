<?php
/**
 * Created by muniao.
 * User: muniao
 * Date: 2018/7/23
 * Time: 16:32
 */

namespace app\api\controller\android\v13;


use jwt\Token;
use think\Cache;
use think\Controller;
use think\Exception;
use think\Request;
use redis\RedisPackage;
use app\api\model\Authority;


class Login extends Controller
{
    /**
     * @param array $data
     * @return array
     * $data=['id','name'=>'123']  id必填，name选填，默认值为123（有默认值时key不能为int）
     */
    public function post(array $data)
    {
        if (empty($data)) {
            r_date([], 300, '提交数据不正确');
        }
        $return = [];
        
        foreach ($data as $key => $item) {//key course_id item 1
            
            if (!is_int($key)) {
                $val = Request::instance()->post($key);
                
                if ($val != '') {
                    $return[$key] = trim($val);
                } else {
                    $return[$key] = trim($item);
                }
            } else {
                $val = Request::instance()->post($item);
                if (!isset($val)) {
                    r_date([], 300, "缺少 $item 数据");
                }
                $return[$item] = trim($val);
            }
        }
        return $return;
    }
    
    /**
     * 用户注册
     */
    public function register()
    {
        $data           = $this->post(['mobile', 'username', 'code', 'password', 'sex', 'age']);
        $data['avatar'] = "https://images.yiniao.co/static/images/avatar.jpg";
        if (!is_mobile($data['mobile'])) {
            r_date([], 300, '电话号码不符合规则');
        }
        // if (!is_password($data['password'])) {
        //     r_date([], 300, '请输入6~12位数字或字母为密码');
        // }
        $redis    = new RedisPackage();
        $s_verify = $redis::get(md5($data['mobile']));
        if (empty($data['code']) || $data['code'] != $s_verify) {
            r_date([], 300, '验证码错误');
            
        }
        $redis::del(md5($data['mobile']));
//        $user=db("user")->where(['mobile'=>$data['mobile']])->find();
//        if ($user) {
//            if ($user['status'] != 3) {
//                r_date([], 300, '该手机号已经注册');
//            }
//        }
        $data['password']       = encrypt($data['password']);
        $data['created_time']   = time();
        $data['status']         = 1;
        $data['originalmobile'] = $data['mobile'];
        $data['originalname']   = $data['username'];
        unset($data['code']);
        \db()->startTrans();
        try {
            $user_id = \db("user")->insertGetId($data);
            \db()->commit();
            r_date(['user_id' => $user_id], 200, '注册成功');
        } catch (Exception $e) {
            \db()->rollback();
            r_date([], 300, $e->getMessage());
        }
    }
    
    /**
     * 完工
     */
    public function finished()
    {
        $data = Request::instance()->post('order_id');
        $res  = db('order')->where(['order_id' => $data['order_id']])->update(['state' => 6, 'update_time' => time()]);
        if ($res) {
            res_date([], 200, '新增成功');
        } else {
            res_date([], 300, '操作失败');
        }
    }
    
    /**
     * 重置密码
     */
    public function reset_password()
    {
        $data = $this->post(['mobile', 'code', 'password']);
        // if (!is_password($data['password'])) {
        //     r_date([], 300, '请输入6~12位数字或字母为密码');
        // }
        $redis    = new RedisPackage();
        $s_verify = $redis::get(md5($data['mobile']));
        if (empty($data['code']) || $data['code'] != $s_verify) {
            r_date(null, 300, '验证码错误');
        }
        $redis::del(md5($data['mobile']));
        $user = db('user')
            ->where(['mobile' => $data['mobile']])
            ->find();
        if (!$user) {
            r_date(null, 300, '该手机号未注册');
        }
        $res = db('user')->where(['mobile' => $data['mobile']])->update(['password' => encrypt($data['password'])]);
        if ($res !== false) {
            r_date();
        }
        r_date(null, 300, '修改失败，请刷新后重试');
    }
    
    /*
     * 用户选择
     */
    public function UserSelect()
    {
        $data     = Request::instance()->post();
        if(!isset($data['mobile']) || empty($data['mobile'])){
            r_date(null, 300, '请输入手机号');
        }
        $redis    = new RedisPackage();
        $userList = [
            'mobile|username' => trim($data['mobile']),
        ];
        if (isset($data['type']) && $data['type'] == 2) {
            $s_verify = $redis::get(md5($data['mobile']));
            if (empty($data['code']) || $data['code'] != $s_verify) {
                r_date(null, 300, '验证码错误');
            }

            $redis::del(md5($data['mobile']));
            
        } else {
            $userList['password'] = encrypt($data['password']);
        }
        if ($data['mobile'] == 17313193200) {
            $user = [
                ['universal' => 1],
            ];
        } else {
            $user = db('user')
                ->field('user.user_id,user.username,user.mobile,st.store_name,user.universal,user.sales_or_deliverer as deliverer')
                ->join('store st', 'user.store_id=st.store_id', 'left')
                ->order('user.user_id desc')
                ->where($userList)
                ->select();
            if (!$user) {
                r_date(null, 300, '账号或密码错误');
            }
        }
        if ($user[0]['universal'] == 1) {
            $userArray = db('user')
                ->field('user.user_id,if(user.sales_or_deliverer=1,concat(user.username,"(销售)"),concat(user.username,"(交付)")) as username,user.mobile,st.store_name,user.universal,user.sales_or_deliverer as deliverer')
                ->join('store st', 'user.store_id=st.store_id', 'left')
                ->where([
                    'user.status' => 0])
                ->select();
            foreach ($userArray as $k => $item) {
                $userArray[$k]['universal'] = $user[0]['universal'];
            }
            $user = array_merge($user, $userArray);
        }
        r_date($user, 200, '操作成功');
    }
    
    /**
     * 登录
     */
    public function login()
    {
        $data  = $this->post(['user_id','phone']);
        $datas = Request::instance()->post();
        $user  = db('user')->where(['user_id' => $data['user_id']])->find();
        $universal  = db('user')->where(['mobile' => $data['phone']])->where('universal',1)->find();
        if ($user['status'] == 1) {
            r_date(null, 300, '请等待审核');
        }
        if ($user['status'] == 2) {
            r_date(null, 300, '该用户已冻结');
        }
        $user_name=$user['username'];
        $userInfo=['last_login_time' => time(), 'access_token' => md5(uniqid('check', true))];
        if (isset($datas['universal']) && $datas['universal'] == 0) {
            $userInfo['registrationId']=isset($datas['registrationId'])?$datas['registrationId']:'';
            $res = db('user')->where(['user_id' => $user['user_id']])->update($userInfo);
        } else {
            if(!empty($universal)){
                $user_name=$universal['username'].'用万能账号登录了'.$user['username'].'的账号';
            }
            if($user['mobile']==15608216710 || $user['mobile']==17313193200){
                $user_name=$user['username'];
            }
            if (empty($user['access_token'])) {
                $res = db('user')->where(['user_id' => $user['user_id']])->update($userInfo);
            } else {
                $res = db('user')->where(['user_id' => $user['user_id']])->update(['last_login_time' => time()]);
            }
            
        }
        
        if ($res !== false) {
            $retrun              = db('user')->field('user.user_id,user.mobile,user.username,user.avatar,user.sex,user.access_token,st.store_name,user.reserve,user.sales_or_deliverer as deliverer')->join('store st', 'user.store_id=st.store_id', 'left')->where(['user.user_id' => $user['user_id']])->find();
            $access_token = request()->header("accesstoken");
            if(!empty($access_token) && !empty($datas['oldToken']) && $access_token !=$retrun['access_token'] && empty($universal)){
                $beforeRes = db('user')->where(['access_token' => $access_token])->find(); 
                $user_name=$beforeRes['username'].'切换了'.$retrun['username'].'的账号';
            }
            $retrun['universal'] = isset($datas['universal']) ? $datas['universal'] : 0;
            $retrun['mobile'] = trim($retrun['mobile']);
            $retrun['phone'] = trim($data['phone']);
            $retrun['cityId'] = config('cityId');
            $objToken=new Token;
            $token_data = [
                'user_id'   => $user['user_id'],
            ];
            $token_res = $objToken->createToken($token_data);
            if($token_res['status']==200){
                $retrun['access_token']=$token_res['token'];
            }
             db('user_login_log', config('database.zong'))->insert(['role'=>6,'user_id'=>$data['user_id'],'user_name'=>$user_name,'login_time'=>time()]);
            r_date($retrun, 200);
        }
        r_date(null, 300, '请刷新后登录');
    }
    
    /**
     * 发送验证码
     */
    public function send_code()
    {
        $data = $this->post(['mobile']);
        $user = db("user")->where(['mobile' => $data['mobile']])->find();
//        if ($user) {
//            if ($user['status'] != 3) {
//                r_date([], 300, '该手机号已经注册');
//            }
//        }
        
        
        $code     = trim(mt_rand(100000, 999999));
        $redis    = new RedisPackage();
        $s_verify = $redis::set(md5($data['mobile']), $code);
        try {
            sendMsg($data['mobile'], 1, [$code]);
            r_date('[]', 200, '发送成功');
        } catch (Exception $e) {
            r_date([], 300, $e->getMessage());
        }
    }
    
    /**
     * 发送验证码
     */
    public function send_code1()
    {
        $data = $this->post(['mobile']);
        $user = db("user")->where(['mobile' => $data['mobile']])->find();
        if (!$user) {
            r_date([], 300, '该手机号未注册，无法登录');
        }
        
        $code  = trim(mt_rand(100000, 999999));
        $redis = new RedisPackage();
        $redis::set(md5($data['mobile']), $code);
        
        try {
            sendMsg($data['mobile'], 1, [$code]);
            r_date([], 200, '发送成功');
        } catch (Exception $e) {
            r_date([], 300, $e->getMessage());
        }
    }
    
    
    /***
     *
     * 退出
     *
     */
    public function logout()
    {
        $model     = new Authority();
        $user      = $model->check(1);
        $datas     = Request::instance()->post();
        $universal = isset($datas['universal']) ? $datas['universal'] : 0;
        if ($universal == 0) {
            $info = [
                'access_token' => '',
                'lat' => '',
                'lng' => '',
            ];
            $res  = db('user')
                ->where(['user_id' => $user['user_id']])
                ->update($info);
            if (!$res) {
                r_date(null, 300, '退出失败');
            }
        }
        
        r_date(null, 200, '退出成功');
        
    }
    
    
}