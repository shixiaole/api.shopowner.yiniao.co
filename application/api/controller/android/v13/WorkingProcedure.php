<?php
/**
 * Created by PhpStorm.
 * User: Raytine
 * Date: 2021/11/18
 * Time: 10:01
 */

namespace app\api\controller\android\v13;


use app\api\model\Aliyunoss;
use app\api\model\Capital;
use app\api\model\Common;
use app\api\model\QuotationListOperation;
use app\api\model\WorkProcess;
use think\Request;
use think\Controller;
use app\api\model\Authority;
use Think\Db;


class WorkingProcedure extends Controller
{
    protected $us;
    
    public function _initialize()
    {
        $this->us = Authority::check(1);
        
    }
    
    /*
     * 工序模版
     */
    public function ProcessTemplate(WorkProcess $workProcess, Capital $capital)
    {
        $data                   = Authority::param(['orderId']);
        $template               = null;
        $QuotationListOperation = new QuotationListOperation();
        $list                   = $QuotationListOperation->ProcessTemplateList($data['orderId']);
        if (!empty($list['list'])) {
            $template = Db::connect('database.zong')->table('work_template')->whereIn('id', $list['list'])->where('status', 1)->field('title,id')->select();
        }
        $work_capital_type = \db('work_capital_type')->where('order_id', $data['orderId'])->select();
        $isId              = 1;
        if (!empty($work_capital_type)) {
            $isId = 1;
        }
        //        if (empty($template)) {
//            $list = $capital->workingProcedure($data['orderId']);
//            if (!empty($list)) {
//                $workProcessId = array_merge(array_unique(array_column($list, 'workProcessId')));
//
//                $workProcessIdString = null;
//                foreach ($workProcessId as $item) {
//                    if (!empty($item)) {
//                        $workProcessIdString .= $item . ',';
//                    }
//                }
//            }
//            if (!empty($workProcessIdString)) {
//                $WorkTemplateDetailed = $workProcess->WorkTemplateDetailed($workProcessIdString)->toArray();
//            }
//            if (!empty($WorkTemplateDetailed)) {
//                $isId = 1;
//            }
//        }
        r_date(['data' => $template, 'isId' => $isId], 200);
    }
    
    /*
     *
     * 施工进度
     */
    public function ConstructionProgress(Capital $capital, WorkProcess $workProcess, QuotationListOperation $listOperation)
    {
        $data = Authority::param(['id', 'orderId', 'isSave']);
        $list = $listOperation->ConstructionProgressModel($capital, $workProcess, $data['orderId'], $data['id']);
        if ($data['isSave'] == 1 || $list['Additions'] == 1) {
            $listArray = $workProcess->saveList($list['result'], $data['orderId'], 1, $this->us['user_id']);
            r_date($list['result'], $listArray['code'], $listArray['message']);
        } else {
            r_date($list['result'], 200);
        }
        
        
    }
    
    /*
     * 根据工种获取工序
     */
    public function getOperation(Capital $capital, WorkProcess $workProcess, QuotationListOperation $listOperation)
    {
        $data = Authority::param(['id', 'capitalList']);
        if ($data['id'] == 0) {
            r_date(null, 300, '缺少参数');
        }
        $capitalList = json_decode($data['capitalList'], true);
        if (empty($capitalList)) {
            r_date(null, 300, '缺少参数');
        }
        $result = $listOperation->getOperationModel($capital, $workProcess, $data['id'], $data['capitalList']);
        
        r_date($result, 200);
    }
    
    /*
     * 保存工序
     */
    public function workCapital(WorkProcess $workProcess)
    {
        $data      = Authority::param(['list', 'orderId']);
        $list      = json_decode($data['list'], true);
        $listArray = $workProcess->saveList($list, $data['orderId'], 1, $this->us['user_id']);
        r_date(null, $listArray['code'], $listArray['message']);
        
    }
    
    /*
     * 主材进度
     */
    public function mainMaterialProgress(QuotationListOperation $listOperation)
    {
        $data   = Authority::param(['orderId']);
        $result = $listOperation->mainMaterialProgressModel($data['orderId']);
        r_date($result, 200);
    }
    
    /*
     * 展示
     */
    public function ShowMaterials(Capital $capital)
    {
        $data         = Authority::param(['orderId']);
        $config       = Db::connect(config('database.zong'))->table('sys_config')->where('keyss', 'agent_detailed_code')->find();
        $capitalCount = 0;
        if ($config) {
            $capitalCount = $capital->ShowMaterialsCspital($data['orderId'], $config['valuess']);
        }
        r_date($capitalCount, 200);
    }
    
    /*
     * 完工节点必要检查动作
     */
    public function CompletionAcceptanceData(QuotationListOperation $listOperation)
    {
        $data = Authority::param(['orderId']);
        $list = $listOperation->listProcess($data['orderId']);
        r_date($list, 200);
    }
    
    /*
     * 后端负责人节点完工验收数据上传
     */
    public function CompletionAcceptanceDataAdd(QuotationListOperation $listOperation,Common $common)
    {
        $params  = Authority::param(['orderId', 'id']);
        $post    = Request::instance()->post();
        $list    = $listOperation->listProcess($params['orderId']);
        $state   = isset($post['state']) ? $post['state'] : 1;
        $order=Db::connect(config('database.db2'))->table('order')->where('order_id', $params['orderId'])->find();
        if(empty($order['start_time'])){
            r_date(null, 300,"订单还未开工，无法验收");
        }
        $picture = isset($post['picture']) ? json_decode($post['picture'], true) : null;
        $far=[];
        foreach ($list as $item) {
            if ($item['id'] == $params['id']) {
                $data['work_title']    = $item['title'];
                $data['work_content']  = $item['content'];
                $data['work_title_id'] = $item['id'];
                $data['creation_time'] = time();
                $data['order_id']      = $params['orderId'];
                $data['type']          = 1;
                $data['user_id']       = $this->us['user_id'];
                $data['img']           = '';
                if($item['id']==5 && (empty($picture) || !is_array($picture))){
                    r_date(null, 300,"请上传图片");
                }
                if (!empty($picture)) { 
                    $aLi=new Aliyunoss();
                    $pathArray=[];
                    $pathOldArray=[];
                    foreach ($picture as $value) {
                        $pathArray[]=$value['path'];
                        if($value['lat'] !=0 && $value['lng'] !=0 && $post['model'] ==1){
                            $Difference= getDistance($order['lat'], $order['lng'], $value['lat'], $value['lng']);
                            if($Difference>3000){
                                $far[]=$Difference;
                            }
                        }
//                        $data['model']=$post['model'];
//                        $data['is_audit']=1;
//                        if($post['model'] ==1){
//                            $data['is_audit']=2;
//                        }
                        if(isset($value['pathOld']) && $value['pathOld'] !=''){
                            $path=$value['path'];
                            $filename=pathinfo($path)['dirname'].'/'.pathinfo($path)['filename'].'_OLD.'.pathinfo($path)['extension'];
                            $filename = parse_url($filename);
                            $parts = parse_url($value['pathOld']);
                            $l=$aLi->copyObject(ltrim($parts['path'],'/'),ltrim($filename['path'],'/'));
                            if ($l['info']['http_code'] != 200) {
                                r_date(null, 300, '图片上传错误');
                            }
                            $pathOldArray[]=$filename['scheme'].'://'.$filename['host'].$filename['path'];
                        }
                    }
                    $data['img'] = empty($item['picture']) ? json_encode($pathArray) : json_encode(array_merge($item['picture'], $pathArray));
                    $data['no_watermark_image'] = '';
                    if (!empty($item['no_watermark_image']) && !empty($pathOldArray)){
                        $data['no_watermark_image'] = json_encode(array_merge($item['no_watermark_image'], $pathOldArray));
                    }elseif (empty($item['no_watermark_image']) && !empty($pathOldArray)){
                        $data['no_watermark_image'] = json_encode($pathOldArray);
                    }elseif (!empty($item['no_watermark_image']) && empty($pathOldArray)){
                        $data['no_watermark_image'] = json_encode($item['no_watermark_image']);
                    }
                    $common->MultimediaResources($data['order_id'],$this->us['user_id'],empty(json_decode($data['img'], true))?[]:json_decode($data['img'], true),14);
                }
                $data['state'] = $state;
            }
        }
        $audit_status     = db('work_check')->where(['work_check.type' => 1, 'order_id' => $data['order_id'],'work_check.work_title_id' => 5])->order('id desc')->where('delete_time', 0)->find();
        db('work_check')->where(['order_id' => $data['order_id'], 'work_title_id' => $data['work_title_id']])->where('delete_time', 0)->update(['delete_time' => time()]);
        if ($state != 0) {
           $data['is_audit'] = 1;
            if ($audit_status['audit_status']==1 || $audit_status['is_audit']==2) {
                $data['is_audit'] = 2;
            }
            $data['admin_image']=!empty($audit_status['admin_image'])?$audit_status['admin_image']:'';
            $id=db('work_check')->insertGetId($data);
            if($data['work_title_id']==5){
                Db::connect(config('database.zong'))->table('order_completion')->insert(['work_check_id'=>$id,'order_id'=>$data['order_id']]);
            }
        }
        if(empty($far)){
            r_date(null, 200);
        }else{
            r_date(null, 301,'上传的照片（拍摄地）不在工地附近拍摄，地点异常，8月中会严格限制，请知晓');
        }
        
    }
    
    /*
     * 获取工种
     */
    public function workType()
    {
        $work_type = Db::connect(config('database.zong'))->table('work_type')->where('work_type.status', 1)->field('work_type.id,work_type.title')->order('sort aes')->select();
        
        $increase[] = [
            
            "id" => 99,
            "title" => "措施费"
        ];
        r_date(array_merge($work_type, $increase), 200);
    }
    
    /*
     * 根据工种获取清单
     */
    public function detailedWorkProcess(WorkProcess $workProcess)
    {
        
        $params = Authority::param(['id']);
        if ($params['id'] != 99) {
            $work_process_id = $workProcess->JobAcquisitionList($params['id']);
        }
        
        $detailed = \db('detailed')
            ->where('detailed.groupss', 1)
            ->where('detailed.is_compose', 1)
            ->where('detailed.groupss', 1)
            ->where('detailed.display', 1)
            ->whereNull('deleted_at');
        if ($params['id'] == 99) {
            $detailed->where('detailed.detailed_category_id', 8);
        } else {
            $detailed ->join('detailed_work_process', 'detailed_work_process.detailed_id=detailed.detailed_id')
                ->whereIn('detailed_work_process.work_process_id', $work_process_id);
        }
        
        $detailed= $detailed->group('detailed.detailed_id')
            ->field('detailed.detailed_id as id,detailed.detailed_title as title')
            ->order('usage_count desc')
            ->select();
      
        foreach ($detailed as $k => $value) {
            $detailed[$k]['tips'] = '';
        }
        r_date($detailed, 200);
    }
    
    /*
     * 根据清单匹配成基建数据
     */
    public function ListMatchingData()
    {
        $data     = Authority::param(['processJSON', 'orderId']);
        $list     = null;
        $tageList = null;
        $result   = array();
        if (!empty(json_decode($data['processJSON'], true))) {
            $projectJson = json_decode($data['processJSON'], true);
            foreach ($projectJson as $k => $l) {
                if (isset($l['process'])) {
                    foreach ($l['process'] as $v) {
                        if ($v['choose']) {
                            foreach ($v['manifestList'] as $value) {
                                $scheme_id[]           = $value['id'];
                                $detailed              = db('detailed')->where('detailed.detailed_id', $value['id'])->where('display', 1)->whereNull('deleted_at')->join('unit', 'unit.id=detailed.un_id', 'left')->field('warranty_years as warrantYears,detaileds_id,artificial,agency,unit.title,detailed.detailed_title,warranty_text_1 as warrantyText1,warranty_text_2 as exoneration,display')->find();
                                $value["projectId"]    = $value['id'];
                                $value["capital_id"]   = "0";
                                $value["class_a"]      = db('detailed')->where('detailed.detailed_id', $detailed['detaileds_id'])->value("detailed_title");
                                $value["projectTitle"] = $detailed["detailed_title"];
                                $value["company"]      = $detailed["title"];
                                $value["square"]       = 0;
                                $value["projectMoney"] = $detailed["artificial"];
                                $value["allMoney"]     = $detailed["artificial"];
                                $value["types"]        = 4;
                                $value["agency"]       = $detailed["agency"];
                                $value["remarks"]      = "";
                                $value["categoryName"] = $v["title"];
                                $value["sectionTitle"] = "";
                                if (!empty($detailed['warrantYears']) && $detailed['warrantYears'] != '0.00') {
                                    $value['isWarranty'] = 1;
                                    $value['warranty']   = [
                                        'warrantYears' => $detailed['warrantYears'],
                                        'warrantyText1' => $detailed['warrantyText1'],
                                        'exoneration' => $detailed['exoneration'],
                                    ];
                                } else {
                                    $value['warranty']   = null;
                                    $value['isWarranty'] = 0;
                                }
                                $value['artificialTag'] = 0;
                                $v['specsList']         = null;
                                unset($value['choose'], $value['id']);
                                $result[$l['title']][] = $value;
                                
                            }
                        }
                    }
                }
                
                
            }
            
            foreach ($result as $k => $list) {
                $schemeTag['title'] = $k;
                $schemeTag['data']  = $result[$k];
                $tageList[]         = $schemeTag;
            }
        }
        
        
        r_date($tageList, 200);
    }
    
    /*
     * 添加音频文件
     */
    public function OrderAudioTranslation()
    {
        $data = Authority::param(['voiceJSON', 'orderId']);
        db('order_voice_memo')->where(['order_id' => $data['orderId']])->update(['deleted_at' => time()]);
        $voiceJSON = json_decode($data['voiceJSON'], true);
        if (!empty($voiceJSON)) {
            foreach ($voiceJSON as $value) {
                if (isset($value['id'])) {
                    db('order_voice_memo')->where(['id' => $value['id']])->update(['content' => $value['content'], 'deleted_at' => 0, 'voice_path' => $value['voicePath']]);
                } else {
                    db('order_voice_memo')->insert(['content' => $value['content'], 'create_time' => time(), 'voice_path' => $value['voicePath'], 'order_id' => $data['orderId'], 'user_id' => $this->us['user_id']]);
                }
                
            }
        }
        r_date(null, 200);
    }
    
    /*
     * 获取音频文件
     */
    public function OrderAudioTranslationList()
    {
        $data = Authority::param(['orderId']);
        $list = db('order_voice_memo')->where(['order_id' => $data['orderId'], 'deleted_at' => 0])->field('content,voice_path as voicePath,FROM_UNIXTIME(create_time ,"%Y-%m-%d %H:%i:%s") as createTime,id')->select();
        r_date($list, 200);
    }
    
    
    /*
     * 贝壳验收
     */
    public function shellAcceptance()
    {
        $data               = Authority::param(['orderId']);
        $orderList          = Db::connect('database.zong')->table('order_setting')
            ->join('partnership_config','partnership_config.id=order_setting.partnership_config_id','left')
            ->field('order_setting.work_check,partnership_config.receipt_image')
            ->where('order_setting.order_id', $data['orderId'])
            ->find();
        if ($orderList['work_check'] == 1) {
            $picture = db('order_acceptance')->where('deleted_at', 0)->where(['order_id' => $data['orderId']])->column('picture');
            r_date(['img' => empty($orderList['receipt_image'])?'https://images.yiniao.co/public/yanshou.png':$orderList['receipt_image'], 'data' => $picture, 'title' => '当前订单为贝壳订单，需要上传验收单'], 200);
        }
        r_date(null, 200);
    }
    
    /*
     * 贝壳验收图片上传
     */
    public function shellAcceptanceAdd()
    {
        $data  = Authority::param(['orderId', 'picture']);
        $state = db('order')->where(['order_id' => $data['orderId']])->value('state');
        if ($state >= 7) {
            r_date(null, 300, '订单已完工不可编辑');
        }
        $picture = !empty($data['picture']) ? json_decode($data['picture'], true) : [];
        foreach ($picture as $item) {
            db('order_acceptance')->insert(['order_id' => $data['orderId'], 'picture' => $item, 'created_time' => time(), 'update_time' => time()]);
        }
        r_date(null, 200);
    }
    
    /*
    * 贝壳验收图片删除
    */
    public function shellAcceptanceDel()
    {
        $data  = Authority::param(['orderId', 'picture']);
        $state = db('order')->where(['order_id' => $data['orderId']])->value('state');
        if ($state >= 7) {
            r_date(null, 300, '订单已完工不可编辑');
        }
        
        db('order_acceptance')->where(['order_id' => $data['orderId'], 'picture' => $data['picture']])->update(['deleted_at' => time()]);
        r_date(null, 200);
    }
    
    /*
   * 贝壳验收图片删除
   */
    public function workCheckDel()
    {
        $data              = Authority::param(['orderId', 'id', 'picture']);
        $work_check        = db('work_check')->where(['order_id' => $data['orderId'], 'work_title_id' => $data['id']])->where('delete_time', 0)->find();
        $work_check['img'] = json_decode($work_check['img'], true);
        $work_check['no_watermark_image'] = json_decode($work_check['no_watermark_image'], true);
        $img               = [];
        $imgs               = [];
        foreach ($work_check['img'] as $v) {
            if ($v != $data['picture']) {
                $img[] = $v;
            }
        }
        IF(!empty($work_check['no_watermark_image'])){
            foreach ($work_check['no_watermark_image'] as $v) {
                $filename=pathinfo($data['picture'])['filename'].'_OLD.'.pathinfo($data['picture'])['extension'];
            if ($v != pathinfo($data['picture'])['dirname'].'/'.$filename) {
                $imgs[] = $v;
            }
        }
        }
       
            $list['img'] = empty($img) ? '' : json_encode($img);
            $list['no_watermark_image'] = empty($imgs) ? '' : json_encode($imgs);
        if (empty($img)) {
            $list['state'] = 0;
            $list['delete_time'] = time();
        } else {
            $list['state'] = 1;
        }
        db('work_check')->where(['order_id' => $data['orderId'], 'work_title_id' => $data['id']])->where('delete_time',0)->update($list);
        r_date(null, 200);
        
    }
}