<?php
/**
 * Created by muniao.
 * User: muniao
 * Date: 2018/5/31
 * Time: 15:20
 */

namespace app\api\controller\android\v12;


use app\api\model\Aliyunoss;
use think\Controller;
use think\Request;


class Upload extends Controller
{
    /**
     * 单张图片上传
     */
    public function upload_img()
    {
        $file = Request::instance()->file('img');

        if ($file == null) {
            res_date([], 300, '未获取到文件');
        }
        $info = $file->validate(['ext' => 'gif,jpg,jpeg,bmp,png'])->move(ROOT_PATH . 'public' . DS . 'uploads/img');
        if ($info) {
            $path = ROOT_PATH . 'public' . '/uploads/img/' . date("Ymd") . '/' . $info->getFilename();
            $ali =new Aliyunoss();
          
            $oss_result  =$ali->upload(config('city').'/' .$info->getFilename(), $path);
                if ($oss_result['info']['http_code'] == 200) {
                    $paths=parse_url($oss_result['info']['url'])['path'];
                    if (file_exists($path)) {
                        unlink($path);
                    }
            
                }
        
                res_date(['url' =>'https://images.yiniao.co'.$paths]);
        }
        res_date([], 300, $file->getError());
    }

    /**
     * 多张图片上传
     */
    public function uploads()
    {
        $files = Request::instance()->file('ims');

        if (!is_array($files) || $files == null) {
            res_date([], 300, '未获取到文件');
        }
        $img = [];
        foreach ($files as $file) {
            $info = $file->validate(['size' => 15678, 'ext' => 'gif,jpg,jpeg,bmp,png'])->rule('uniqid')->move(ROOT_PATH . 'public' . DS . 'uploads/imgs/' . date("Ymd"));
            if ($info) {
                $img_src = DOMAIN_SRC . '/uploads/imgs/' . date("Ymd") . '/' . $info->getFilename();
                $img[]   = $img_src;
            } else {
                res_date([], 300, $file->getError());
            }
        }
        res_date($img);
    }

    /**
     * 单个音频上传
     */
    public function upload_audio()
    {
        $file = Request::instance()->file('audio');
        if ($file == null) {
            r_date([], 300, '未获取到文件');
        }
        $info = $file->validate(['ext' => 'wav,aac,mp3'])->move(ROOT_PATH . 'public' . DS . 'uploads/audio');
        if ($info) {
            $path = DOMAIN_SRC . '/uploads/audio/' . date("Ymd") . '/' . $info->getFilename();
            r_date(['url' => $path]);
        }
        r_date([], 300, $file->getError());
    }

    /**
     * 单个视频上传
     */
    public function upload_video()
    {
        $file = Request::instance()->file('video');
        if ($file == null) {
            r_date([], 300, '未获取到文件');
        }
        $info = $file->validate(['ext' => 'avi,FLV,mp4,mov,f4v,mpg,mkv'])->move(ROOT_PATH . 'public' . DS . 'uploads/video');
        if ($info) {
            $path = DOMAIN_SRC . '/uploads/video/' . date("Ymd") . '/' . $info->getFilename();
            r_date(['url' => $path]);
        }
        r_date([], 300, $file->getError());
    }

    /**
     * 师傅排期
     */
    public function support()
    {
        $data = Request::instance()->post(['master_id','date']);
        $er   = send_post(UIP_SRC . '/support-v1/scheduling/list?master_id='.$data['master_id'].'&date='.$data['date'], [],2);
        $er   = json_decode($er, true);
        if ($er['code'] == 200) {
            r_date($er['data'], 200);
        }
        r_date([], 300, $er['message']);
    }
    public function month()
    {
        $data = Request::instance()->post(['master_id','date']);
        $er   = send_post(UIP_SRC . '/support-v1/scheduling/month?master_id='.$data['master_id'].'&date='.$data['date'], [],2);
        $er   = json_decode($er, true);
        if ($er['code'] == 200) {
            r_date($er['data'], 200);
        }
        r_date([], 300, $er['message']);
    }



    /*
     * 计算成交价格
     */
    public function ll()
    {
        $res = db('order')->where(['state' => ['between', '6,7']])->select();
        $s   = '';
        foreach ($res as $key => $val) {
            $s .= $val["order_id"] . ',';
        }
        $s = substr($s, 0, -1);

        $p    = db('capital')->where(['ordesr_id' => ['in', $s], 'types' => 1, 'enable' => 1])->sum('to_price');
        $p1   = db('capital')->where(['ordesr_id' => ['in', $s], 'types' => 1, 'enable' => 1])->sum('give_money');
        $thro = db('through')->where(['order_ids' => ['in', $s], 'baocun' => 1])->find();
        $p5   = (int)$p - (int)$p1 + (int)$thro;;

    }
    /*
       * 获取省,市,区
       */
    public function province()
    {
        $parent=Request::instance()->post();
        if ($parent['structures'] == 1) {
            $province=send_post( 'http://order.yiniaoweb.com/api/v1/getAllProvince?', [], true);
            $province=json_decode($province,true);
        } else{
            $data    =['pid'=>$parent['city_id'], 'level'=>$parent['structures']];
            $province=send_post('http://order.yiniaoweb.com/api/v1/getAreaList', $data, true);
            $province=json_decode($province,true);
            $last_names = array_column($province['data'],'old_id');
            array_multisort($last_names,SORT_ASC,$province['data']);
        }
        r_date($province, 300);
    }

}