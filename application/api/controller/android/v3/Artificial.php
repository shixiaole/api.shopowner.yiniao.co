<?php
/**
 * Created by PhpStorm.
 * User: Raytine
 * Date: 2019/11/29
 * Time: 15:23
 */

namespace app\api\controller\android\v3;

use app\api\model\OrderModel;
use think\Controller;
use think\Request;
use app\api\model\Authority;

class Artificial extends Controller
{
    protected $us;

    public function _initialize()
    {
        $this->us = Authority::check(1);
    }

    /**
     * 日报参数
     */
    protected function day_param($max = 3)
    {
        $param = Request::instance()->post();
        if (empty($param['start_date'])) {
            r_date(null, 300, "开始时间不能为空");
        }
        if (empty($param['end_date'])) {
            r_date(null, 300, "结束时间不能为空");
        }
        $start = date("Y-m-d", strtotime($param['start_date']));
        $end = date("Y-m-d", strtotime($param['end_date']));
        //两个时间差的天数
        $day = day_value($start, $end);
        if ($day < 0) {
            r_date(null, 300, "开始时间不能小于结束时间");
        }
        //两个时间差的月数
        $month = month_value($start, $end);
        if ($month > $max) {
            r_date(null, 300, "时间跨度不能超过" . $max . "月");
        }

        return array(
            'start_date' => $start,
            'end_date' => $end,
            'start_timestamp' => strtotime($start . ' 00:00:00'),
            'end_timestamp' => strtotime($end . ' 23:59:59'),
            'type' => isset($param['type']) ? $param['type'] : 1,
            'Difference' => isset($param['Difference']) ? $param['Difference'] : 1,
            'day' => $day,
            'month' => $month,
            'user_id' => isset($param['user_id']) ? $param['user_id'] : $this->us['user_id']
        );
    }

    public function user_list()
    {
        $cap = db('user')->where('store_id', $this->us['store_id'])->field('reserve,user_id,username')->select();
        r_date($cap, 200);
    }

    /*
     * 材料计算
     */

    public function Cost(OrderModel $orderModel)
    {
        $order_id = Request::instance()->get('order_id');
        $cap = $orderModel->ManualSettlement($order_id);

        r_date($cap, 200);
    }

    /*
     * 提交人工报价
     */
    public function category()
    {
        $data = Request::instance()->post();
        $op = json_decode($data['company'], true);
        foreach ($op as $item) {
            db('capital')->where(['capital_id' => $item['projectId']])->update(['labor_cost' => $item['pri'],'material'=>$item['material']]);
        }
        r_date([], 200);
    }

    //店铺成交量
    public function turnover_day(OrderModel $orderModel)
    {
        $timedata = $this->day_param(3);

        $start_timestamp = $timedata['start_timestamp'];
        // $end_timestamp = $timedata['end_timestamp'];
        $day = $timedata['day'];
        $order_num = array();
        $order_turnover = array();
        $order_cost = array();
        $material_usage = array();
        $reimbursement = array();
        $profit = array();
        $order_unit_price = array();
        for ($i = $day; $i > -1; $i--) {
            $_begin = $start_timestamp + $i * 86400;
            $_end = $start_timestamp + 86399 + $i * 86400;
            $order = $orderModel->order_list($timedata['type'], $_begin, $_end, empty($timedata['user_id']) ? $this->us['user_id'] : $timedata['user_id'], $this->us['store_id']);

            //成交额
            $turnover = order_for_turnover($order);
            $order_num[] = array(
                'time' => date('Y/m/d', $_begin),
                'data' => count($order),
            );
            $order_turnover[] = array(
                'time' => date('Y/m/d', $_begin),
                'data' => $turnover,
            );

            if ($timedata['Difference'] == 4) {
                if ($turnover > 0) {
                    $unit_price = sprintf("%.2f", ($turnover / count($order)));
                } else {
                    $unit_price = 0;
                }
                $order_unit_price[] = array(
                    'time' => date('Y/m/d', $_begin),
                    'data' => $unit_price,
                );
            } elseif ($timedata['Difference'] == 5) {

                //预计人工费
                $order_cost[] = array(
                    'time' => date('Y/m/d', $_begin),
                    'data' => order_for_artificial($order),
                );
                //预计材料费
                $material_usage[] = array(
                    'time' => date('Y/m/d', $_begin),
                    'data' => order_for_Finance($order),
                );
                //报销费(只要实际报销)
                $reimbursement[] = array(
                    'time' => date('Y/m/d', $_begin),
                    'data' => order_for_outlay($order),
                );
                $profit[] = array(
                    'time' => date('Y/m/d', $_begin),
                    'data' => sprintf("%.2f", ($turnover - order_for_artificial($order) - order_for_Finance($order) - order_for_outlay($order))),
                );

            } else if ($timedata['Difference'] == 6) {

                //实际人工费
                $order_cost[] = array(
                    'time' => date('Y/m/d', $_begin),
                    'data' => order_for_reality_artificial($order),
                );
                //实际材料费
                $material_usage[] = array(
                    'time' => date('Y/m/d', $_begin),
                    'data' => order_for_reality_material($order),
                );
                //报销费(只要实际报销)
                $reimbursement[] = array(
                    'time' => date('Y/m/d', $_begin),
                    'data' => order_for_outlay($order),
                );
                $profit[] = array(
                    'time' => date('Y/m/d', $_begin),
                    'data' => sprintf("%.2f", ($turnover - order_for_reality_artificial($order) - order_for_reality_material($order) - order_for_outlay($order))),
                );
            }


        }
        r_date(array('order_num' => $order_num, 'order_turnover' => $order_turnover, 'order_unit_price' => $order_unit_price, 'order_cost' => $order_cost, 'material_usage' => $material_usage, 'reimbursement' => $reimbursement, 'profit' => $profit), 200, '操作成功');

    }


}