<?php
/**
 * Created by muniao.
 * User: muniao
 * Date: 2018/5/31
 * Time: 15:20
 */

namespace app\api\controller\android\v3;


use think\Controller;
use think\Request;
use foxyZeng\huyi\HuYiSMS;

class Upload extends Controller
{
    /**
     * 单张图片上传
     */
    public function upload_img()
    {
        $file = Request::instance()->file('img');

        if ($file == null) {
            res_date([], 300, '未获取到文件');
        }
        $info = $file->validate(['ext' => 'gif,jpg,jpeg,bmp,png'])->move(ROOT_PATH . 'public' . DS . 'uploads/img');
        if ($info) {
            $path = DOMAIN_SRC . '/uploads/img/' . date("Ymd") . '/' . $info->getFilename();
            res_date(['url' => $path]);
        }
        res_date([], 300, $file->getError());
    }

    /**
     * 多张图片上传
     */
    public function uploads()
    {
        $files = Request::instance()->file('ims');

        if (!is_array($files) || $files == null) {
            res_date([], 300, '未获取到文件');
        }
        $img = [];
        foreach ($files as $file) {
            $info = $file->validate(['size' => 15678, 'ext' => 'gif,jpg,jpeg,bmp,png'])->rule('uniqid')->move(ROOT_PATH . 'public' . DS . 'uploads/imgs/' . date("Ymd"));
            if ($info) {
                $img_src = DOMAIN_SRC . '/uploads/imgs/' . date("Ymd") . '/' . $info->getFilename();
                $img[]   = $img_src;
            } else {
                res_date([], 300, $file->getError());
            }
        }
        res_date($img);
    }

    /**
     * 单个音频上传
     */
    public function upload_audio()
    {
        $file = Request::instance()->file('audio');
        if ($file == null) {
            r_date([], 300, '未获取到文件');
        }
        $info = $file->validate(['ext' => 'wav,aac,mp3'])->move(ROOT_PATH . 'public' . DS . 'uploads/audio');
        if ($info) {
            $path = DOMAIN_SRC . '/uploads/audio/' . date("Ymd") . '/' . $info->getFilename();
            r_date(['url' => $path]);
        }
        r_date([], 300, $file->getError());
    }

    /**
     * 单个视频上传
     */
    public function upload_video()
    {
        $file = Request::instance()->file('video');
        if ($file == null) {
            r_date([], 300, '未获取到文件');
        }
        $info = $file->validate(['ext' => 'avi,FLV,mp4,mov,f4v,mpg,mkv'])->move(ROOT_PATH . 'public' . DS . 'uploads/video');
        if ($info) {
            $path = DOMAIN_SRC . '/uploads/video/' . date("Ymd") . '/' . $info->getFilename();
            r_date(['url' => $path]);
        }
        r_date([], 300, $file->getError());
    }

    /**
     * 师傅排期
     */
    public function support()
    {
        $data = Request::instance()->post(['master_id','date']);
        $er   = send_post(UIP_SRC . 'support-v1/scheduling/list?master_id='.$data['master_id'].'&date='.$data['date'], [],2);
        $er   = json_decode($er, true);
        if ($er['code'] == 200) {
            r_date($er['data'], 200);
        }
        r_date([], 300, $er['message']);
    }
    public function month()
    {
        $data = Request::instance()->post(['master_id','date']);
        $er   = send_post(UIP_SRC . 'support-v1/scheduling/month?master_id='.$data['master_id'].'&date='.$data['date'], [],2);
        $er   = json_decode($er, true);
        if ($er['code'] == 200) {
            r_date($er['data'], 200);
        }
        r_date([], 300, $er['message']);
    }

    /*
     * 祝福短信
     */
    public function zhu()
    {
        vendor('foxyzeng.huyi.HuYiSMS');
        $sms = new HuYiSMS();

        $us        = db('order')->where(['state' => ['between', [2, 8]]])->field('telephone')->select();
        $is_mobile = db('personal')->field('mobile')->select();
        foreach ($is_mobile as $k => $item) {
            $is_mobile[$k]['telephone'] = $item['mobile'];
        }
        $o = array_merge($us, $is_mobile);
        $s = '';
        foreach ($o as $key => $val) {
            $s .= $val["telephone"] . ',';
        }


        $content = "亲爱的益鸟用户，感谢您对益鸟维修的信赖和支持！逢此佳节，益鸟全体员工祝您新春快乐！阖家幸福！~～回T退订【益鸟维修】";
        $sms->duan($s, $content);

    }

    /*
     * 计算成交价格
     */
    public function ll()
    {
        $res = db('order')->where(['state' => ['between', '6,7']])->select();
        $s   = '';
        foreach ($res as $key => $val) {
            $s .= $val["order_id"] . ',';
        }
        $s = substr($s, 0, -1);

        $p    = db('capital')->where(['ordesr_id' => ['in', $s], 'types' => 1, 'enable' => 1])->sum('to_price');
        $p1   = db('capital')->where(['ordesr_id' => ['in', $s], 'types' => 1, 'enable' => 1])->sum('give_money');
        $thro = db('through')->where(['order_ids' => ['in', $s], 'baocun' => 1])->find();
        $p5   = (int)$p - (int)$p1 + (int)$thro;;

    }

}