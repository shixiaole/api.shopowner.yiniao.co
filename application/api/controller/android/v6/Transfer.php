<?php
/**
 * Created by muniao.
 * User: muniao
 * Date: 2018/7/31
 * Time: 16:00
 */

namespace app\api\controller\android\v6;


use think\Cache;
use think\Controller;
use app\api\model\Authority;
use think\db;

use think\Request;
use  app\api\model\OrderModel;

class Transfer extends Controller
{
    
    protected $us;
    
    public function _initialize()
    {
        $model   =new Authority();
        $this->us=$model->check(1);
    }
    
    /*
     * 转派记录添加
     */
    public function record()
    {
       
        $data=\request()->post();
        $op  =\db('transfer_record')->insertGetId([
            'user_id'  =>$data['user_id'],
            'time'     =>time(),
            'order_id' =>$data['order_id'],
            'operation'=>$this->us['user_id'],
        ]);
        if ($op) {
            \db('order')->where('order_id',$data['order_id'])->update(['assignor'=>$data['user_id']]);
            r_date('', 200);
        }
        r_date('', 300, '添加失败');
    }
   
}