<?php
/**
 * Created by muniao.
 * User: muniao
 * Date: 2018/8/2
 * Time: 13:20
 */

namespace app\api\controller\android\v6;


use think\Controller;

use app\api\model\Authority;
use app\api\model\Common;
use  app\api\model\OrderModel;
use think\Request;

class CostCalculation extends Controller
{
    protected $model;
    protected $us;
    protected $newTime;
    protected $endTime;
    protected $overtime;
    protected $pdf;
    
    public function _initialize()
    {
        $this->model=new Authority();
        
        $this->us=Authority::check(1);
        
    }
    
    public function share(OrderModel $orderModel)
    {
        $data=Request::instance()->post();
        if (empty($data['id'])) {
            r_date(null, 300, '合同ID不能为空');
        }
     
        $op          =db('sign')->where(['sign.sign_id'=>$data['id']])->join('order or', 'or.order_id=sign.order_id', 'left')->join('goods_category go', 'or.pro_id=go.id', 'left')->field('sign.*,go.title,or.telephone,or.province_id')->find();
        $offer       =$orderModel::offer($data['order_id']);
        $do=[
            'data'  =>[]
        ];
        if(!empty($offer['amount'])){
            if($offer['amount']-$offer['agency']){
                $imageDefault=[
                    'left'   =>260,
                    'top'    =>6360,
                    'right'  =>0,
                    'bottom' =>0,
                    'width'  =>100,
                    'height' =>100,
                    'opacity'=>50,
                ];
        
                if ($op['title'] == '墙面翻新') {
                    $p[]=[
                        'text'     =>'√',
                        'left'     =>270,
                        'top'      =>2264,
                        'fontSize' =>30,       //�ֺ�
                        'fontColor'=>'36, 36, 36, 1', //������ɫ
                        'fontPath' =>ROOT_PATH . 'public/simsun.ttc', //������ɫ
                        'angle'    =>0,
                    ];
                } elseif ($op['title'] == '室内渗漏水'){
                    $p[]=[
                        'text'     =>'√',
                        'left'     =>450,
                        'top'      =>2264,
                        'fontSize' =>30,       //�ֺ�
                        'fontColor'=>'36, 36, 36, 1', //������ɫ
                        'fontPath' =>ROOT_PATH . 'public/simsun.ttc', //������ɫ
                        'angle'    =>0,
                    ];
                } elseif ($op['title'] == '建筑渗漏水'){
                    $p[]=[
                        'text'     =>'√',
                        'left'     =>657,
                        'top'      =>2264,
                        'fontSize' =>30,       //�ֺ�
                        'fontColor'=>'36, 36, 36, 1', //������ɫ
                        'fontPath' =>ROOT_PATH . 'public/simsun.ttc', //������ɫ
                        'angle'    =>0,
                    ];
                } elseif ($op['title'] == '局部改造'){
                    $p[]=[
                        'text'     =>'√',
                        'left'     =>850,
                        'top'      =>2264,
                        'fontSize' =>30,       //�ֺ�
                        'fontColor'=>'36, 36, 36, 1', //������ɫ
                        'fontPath' =>ROOT_PATH . 'public/simsun.ttc', //������ɫ
                        'angle'    =>0,
                    ];
                } else{
            
                    $p[]=[
                        'text'     =>'√',
                        'left'     =>1060,
                        'top'      =>2264,
                        'fontSize' =>30,       //�ֺ�
                        'fontColor'=>'36, 36, 36, 1', //������ɫ
                        'fontPath' =>ROOT_PATH . 'public/simsun.ttc', //������ɫ
                        'angle'    =>0,
                    ];
                }
        
                $textDefault=[
                    [
                        'text'     =>$op['contract'],
                        'left'     =>945,
                        'top'      =>95,
                        'fontSize' =>20,       //�ֺ�
                        'fontColor'=>'36, 36, 36, 1', //������ɫ
                        'fontPath' =>ROOT_PATH . 'public/simsun.ttc', //������ɫ
                        'angle'    =>0,
                    ],
                    [
                        'text'     =>$this->us['username'],
                        'left'     =>400,
                        'top'      =>1600,
                        'fontSize' =>20,       //�ֺ�
                        'fontColor'=>'36, 36, 36, 1', //������ɫ
                        'fontPath' =>ROOT_PATH . 'public/simsun.ttc', //������ɫ
                        'angle'    =>0,
                    ],
                    [
                        'text'     =>!empty($op['con_time'])? date('Y年m月d日H:i', $op['con_time']) : '',
                        'left'     =>820,
                        'top'      =>1600,
                        'fontSize' =>20,       //�ֺ�
                        'fontColor'=>'36, 36, 36, 1', //������ɫ
                        'fontPath' =>ROOT_PATH . 'public/simsun.ttc', //������ɫ
                        'angle'    =>0,
                    ],
                    [
                        'text'     =>$op['username'],
                        'left'     =>290,
                        'top'      =>1790,
                        'fontSize' =>20,       //�ֺ�
                        'fontColor'=>'36, 36, 36, 1', //������ɫ
                        'fontPath' =>ROOT_PATH . 'public/simsun.ttc', //������ɫ
                        'angle'    =>0,
                    ],
                    [
                        'text'     =>$op['idnumber'],
                        'left'     =>280,
                        'top'      =>1845,
                        'fontSize' =>20,       //�ֺ�
                        'fontColor'=>'36, 36, 36, 1', //������ɫ
                        'fontPath' =>ROOT_PATH . 'public/simsun.ttc', //������ɫ
                        'angle'    =>0,
                    ],
                    [
                        'text'     =>$op['addres'],
                        'left'     =>267,
                        'top'      =>2204,
                        'fontSize' =>20,       //�ֺ�
                        'fontColor'=>'36, 36, 36, 1', //������ɫ
                        'fontPath' =>ROOT_PATH . 'public/simsun.ttc', //������ɫ
                        'angle'    =>0,
                    ],
                    [
                        'text'     =>!empty($offer['gong'])?$offer['gong']:'——————',
                        'left'     =>900,
                        'top'      =>2555,
                        'fontSize' =>20,       //�ֺ�
                        'fontColor'=>'36, 36, 36, 1', //������ɫ
                        'fontPath' =>ROOT_PATH . 'public/simsun.ttc', //������ɫ
                        'angle'    =>0,
                    ],
                    [
                        'text'     =>!empty($op['addtime'])? date('Y年m月d日H:i', $op['addtime']) :'——————',
                        'left'     =>180,
                        'top'      =>2550,
                        'fontSize' =>20,       //�ֺ�
                        'fontColor'=>'36, 36, 36, 1', //������ɫ
                        'fontPath' =>ROOT_PATH . 'public/simsun.ttc', //������ɫ
                        'angle'    =>0,
                    ],
                    [
                        'text'     =>!empty($op['uptime'])? date('Y年m月d日H:i', $op['uptime']):'———————',
                        'left'     =>500,
                        'top'      =>2550,
                        'fontSize' =>20,       //�ֺ�
                        'fontColor'=>'36, 36, 36, 1', //������ɫ
                        'fontPath' =>ROOT_PATH . 'public/simsun.ttc', //������ɫ
                        'angle'    =>0,
                    ],
            
                    [
                        'text'     =>$offer['amount']-$offer['agency'],
                        'left'     =>380,
                        'top'      =>3245,
                        'fontSize' =>20,       //�ֺ�
                        'fontColor'=>'36, 36, 36, 1', //������ɫ
                        'fontPath' =>ROOT_PATH . 'public/simsun.ttc', //������ɫ
                        'angle'    =>0,
                    ],
                    [
                        'text'     =>Common::NumToCNMoney($offer['amount']-$offer['agency']),
                        'left'     =>720,
                        'top'      =>3245,
                        'fontSize' =>20,       //�ֺ�
                        'fontColor'=>'36, 36, 36, 1', //������ɫ
                        'fontPath' =>ROOT_PATH . 'public/simsun.ttc', //������ɫ
                        'angle'    =>0,
                    ],
                    [
                        'text'     =>$op['b1'].'%',
                        'left'     =>690,
                        'top'      =>3358,
                        'fontSize' =>20,       //�ֺ�
                        'fontColor'=>'36, 36, 36, 1', //������ɫ
                        'fontPath' =>ROOT_PATH . 'public/simsun.ttc', //������ɫ
                        'angle'    =>0,
                    ],
                    [
                        'text'     =>$op['ding'],
                        'left'     =>260,
                        'top'      =>3390,
                        'fontSize' =>20,       //�ֺ�
                        'fontColor'=>'36, 36, 36, 1', //������ɫ
                        'fontPath' =>ROOT_PATH . 'public/simsun.ttc', //������ɫ
                        'angle'    =>0,
                    ],
                    //天数
                    [
                        'text'     =>$op['explain'],
                        'left'     =>200,
                        'top'      =>3440,
                        'fontSize' =>18,       //�ֺ�
                        'fontColor'=>'36, 36, 36, 1', //������ɫ
                        'fontPath' =>ROOT_PATH . 'public/simsun.ttc', //������ɫ
                        'angle'    =>0,
                    ],
                    [
                        'text'     =>$op['b2'].'%',
                        'left'     =>675,
                        'top'      =>3445,
                        'fontSize' =>20,       //�ֺ�
                        'fontColor'=>'36, 36, 36, 1', //������ɫ
                        'fontPath' =>ROOT_PATH . 'public/simsun.ttc', //������ɫ
                        'angle'    =>0,
                    ],
                    [
                        'text'     =>$op['zhong'],
                        'left'     =>1030,
                        'top'      =>3440,
                        'fontSize' =>20,       //�ֺ�
                        'fontColor'=>'36, 36, 36, 1', //������ɫ
                        'fontPath' =>ROOT_PATH . 'public/simsun.ttc', //������ɫ
                        'angle'    =>0,
                    ],
                    [
                        'text'     =>$op['wei'],
                        'left'     =>180,
                        'top'      =>3550,
                        'fontSize' =>20,       //�ֺ�
                        'fontColor'=>'36, 36, 36, 1', //������ɫ
                        'fontPath' =>ROOT_PATH . 'public/simsun.ttc', //������ɫ
                        'angle'    =>0,
                    ],
                    [
                        'text'     =>$op['telephone'],
                        'left'     =>260,
                        'top'      =>6515,
                        'fontSize' =>20,       //�ֺ�
                        'fontColor'=>'36, 36, 36, 1', //������ɫ
                        'fontPath' =>ROOT_PATH . 'public/simsun.ttc', //������ɫ
                        'angle'    =>0,
                    ],
                    [
                        'text'     =>!empty($op['con_time'])? date('Y年m月d日H:i', $op['con_time']) : '',
                        'left'     =>260,
                        'top'      =>6590,
                        'fontSize' =>20,       //�ֺ�
                        'fontColor'=>'36, 36, 36, 1', //������ɫ
                        'fontPath' =>ROOT_PATH . 'public/simsun.ttc', //������ɫ
                        'angle'    =>0,
                    ],
                    [
                        'text'     =>$this->us['username'],
                        'left'     =>780,
                        'top'      =>6440,
                        'fontSize' =>20,       //�ֺ�
                        'fontColor'=>'36, 36, 36, 1', //������ɫ
                        'fontPath' =>ROOT_PATH . 'public/simsun.ttc', //������ɫ
                        'angle'    =>0,
                    ],
                    [
                        'text'     =>$this->us['mobile'],
                        'left'     =>780,
                        'top'      =>6515,
                        'fontSize' =>20,       //�ֺ�
                        'fontColor'=>'36, 36, 36, 1', //������ɫ
                        'fontPath' =>ROOT_PATH . 'public/simsun.ttc', //������ɫ
                        'angle'    =>0,
                    ],
                    [
                        'text'     =>!empty($op['con_time'])? date('Y年m月d日H:i', $op['con_time']) : '',
                        'left'     =>780,
                        'top'      =>6590,
                        'fontSize' =>20,       //�ֺ�
                        'fontColor'=>'36, 36, 36, 1', //������ɫ
                        'fontPath' =>ROOT_PATH . 'public/simsun.ttc', //������ɫ
                        'angle'    =>0,
                    ],
        
                ];
                $textDefault=array_merge($textDefault, $p);
                if($op['province_id']!=1 && $op['province_id']!=22){
                    $op1=1;
                }else{
                    $op1=$op['province_id'];
                }
                $background=ROOT_PATHS . '/contract/'.$op1.'.png';//背景
                $path      =parse_url($op['autograph'], PHP_URL_PATH);
        
                $config['image'][]['url']=ROOT_PATHS . $path;
                $filename                =ROOT_PATHS . '/ContractNumber/' . $data['id'] . '.jpg';
                Common::getbgqrcode($imageDefault, $textDefault, $background, $filename, $config);
                $u['url'] =request()->domain() . '/ContractNumber/' . $data['id'] . '.jpg';
                $u['type']=!empty($op['contractpath'])? 1 : 2;
                $do       =[
                    'agency'=>0,
                    'data'  =>[
                        $u,
                    ],
                ];
            }
        }
       
       
       
        if ($offer['agency']) {
            $img=CostCalculation::agency($op, $offer['agency']);
            $do['agency']       =1;
            array_push($do['data'], $img);
        }
       
        if(!empty($offer['agency']) && !empty(($offer['amount']-$offer['agency']))){
            $do['state']       =3;
        }elseif(!empty($offer['agency'])){
            $do['state']       =2;
        }elseif (!empty(($offer['amount']-$offer['agency']))){
            $do['state']       =1;
        }
        r_date($do, 200);
    }
    
    /*
     * 代购合同
     */
    
    public function agency($op, $offer)
    {
        $imageDefault            =[
            'left'   =>560,
            'top'    =>2550,
            'right'  =>0,
            'bottom' =>0,
            'width'  =>100,
            'height' =>100,
            'opacity'=>50,
        ];
        $textDefault             =[
            [
                'text'     =>$op['username'],
                'left'     =>320,
                'top'      =>260,
                'fontSize' =>20,       //�ֺ�
                'fontColor'=>'36, 36, 36, 1', //������ɫ
                'fontPath' =>ROOT_PATH . 'public/simsun.ttc', //������ɫ
                'angle'    =>0,
            ],
            [
                'text'     =>$op['addres'],
                'left'     =>730,
                'top'      =>260,
                'fontSize' =>20,       //�ֺ�
                'fontColor'=>'36, 36, 36, 1', //������ɫ
                'fontPath' =>ROOT_PATH . 'public/simsun.ttc', //������ɫ
                'angle'    =>0,
            ],
            [
                'text'     =>$offer,
                'left'     =>250,
                'top'      =>345,
                'fontSize' =>20,       //�ֺ�
                'fontColor'=>'36, 36, 36, 1', //������ɫ
                'fontPath' =>ROOT_PATH . 'public/simsun.ttc', //������ɫ
                'angle'    =>0,
            ],
            [
                'text'     =>Common::NumToCNMoney($offer),
                'left'     =>620,
                'top'      =>345,
                'fontSize' =>20,       //�ֺ�
                'fontColor'=>'36, 36, 36, 1', //������ɫ
                'fontPath' =>ROOT_PATH . 'public/simsun.ttc', //������ɫ
                'angle'    =>0,
            ],
            [
                'text'     =>$this->us['username'],
                'left'     =>940,
                'top'      =>2620,
                'fontSize' =>20,       //�ֺ�
                'fontColor'=>'36, 36, 36, 1', //������ɫ
                'fontPath' =>ROOT_PATH . 'public/simsun.ttc', //������ɫ
                'angle'    =>0,
            ],
            
            [
                'text'     =>!empty($op['con_time'])? date('Y年m月d日H:i', $op['con_time']) : '',
                'left'     =>110,
                'top'      =>2670,
                'fontSize' =>20,       //�ֺ�
                'fontColor'=>'36, 36, 36, 1', //������ɫ
                'fontPath' =>ROOT_PATH . 'public/simsun.ttc', //������ɫ
                'angle'    =>0,
            ],
            [
                'text'     =>!empty($op['con_time'])? date('Y年m月d日H:i', $op['con_time']) : '',
                'left'     =>730,
                'top'      =>2670,
                'fontSize' =>20,       //�ֺ�
                'fontColor'=>'36, 36, 36, 1', //������ɫ
                'fontPath' =>ROOT_PATH . 'public/simsun.ttc', //������ɫ
                'angle'    =>0,
            ],
        
        ];
        $background              =ROOT_PATHS . '/contract/20200608150647.png';//背景
        $path                    =parse_url($op['autograph'], PHP_URL_PATH);
        $config['image'][]['url']=ROOT_PATHS . $path;
        $filename                =ROOT_PATHS . '/agency/' . $op['sign_id'] . '.jpg';
        Common::getbgqrcode($imageDefault, $textDefault, $background, $filename, $config);
        $u['url'] =request()->domain() . '/agency/' . $op['sign_id'] . '.jpg';
        $u['type']=!empty($op['contractpath'])? 1 : 2;
        
        return $u;
    }
    
}