<?php
/**
 * Created by muniao.
 * User: muniao
 * Date: 2018/7/25
 * Time: 9:48
 */

namespace app\api\controller\android\v7;


use app\api\model\Aliyunoss;
use app\api\model\Authority;

use app\api\model\OrderModel;
use think\Controller;

use think\Request;
use think\Exception;
use app\api\validate;
use  app\api\model\Approval;
use Think\Db;


class MaterialScience extends Controller
{
    protected $model;
    protected $us;
    protected $newTime;
    protected $endTime;
    protected $overtime;
    protected $pdf;
    
    
    public function _initialize()
    {
        $this->model=new Authority();
        
        $this->us=Authority::check(1);
        
    }
    
    
    /**
     * 获取库存列表
     */
    public function Stock_list()
    {
        $data=Request::instance()->post(['type', 'ok']);
        if ($data['type'] == 0) {
            $custom=db('custom_material')
                ->field('id,company,stock_name,latest_cost,number,created_time,number')
                ->where(['store_id'=>$this->us['store_id'], 'status'=>0])
                ->where('number', 'neq', 0)
                ->select();
            foreach ($custom as $k=>$l) {
                $custom[$k]['type']=0;
            }
        } else if ($data['type'] == 99) {
            $cust=db('purchase_usage')
                ->field('id,company,stock_name,latest_cost,number,created_time,content,remake')
                ->where('type', 1)
                ->where('number', 'neq', 0);
            if (isset($data['ok']) && $data['ok'] != '') {
                $cust->where(['status'=>3]);
            }
            $custom=$cust->where('store_id', $this->us['store_id'])
                ->order('created_time desc')
                ->select();
            foreach ($custom as $k=>$l) {
                $custom[$k]['type']=99;
            }
        } else if ($data['type'] == 98) {
            $cooperation_usage=db('cooperation_usage')
                ->join('stock', 'stock.id=cooperation_usage.stock_id', 'left')
                ->field('cooperation_usage.id,cooperation_usage.company,cooperation_usage.stock_name,cooperation_usage.stock_id,cooperation_usage.latest_cost,sum(cooperation_usage.number) as number,cooperation_usage.created_time,cooperation_usage.remake,stock.type')
                ->where('stock.type', $data['type'])
                ->group('cooperation_usage.latest_cost,cooperation_usage.stock_id');
            
            if (isset($data['ok']) && $data['ok'] != '') {
                $cooperation_usage->where(['cooperation_usage.status'=>3]);
            }
            $custom=$cooperation_usage->order('cooperation_usage.created_time desc')
                ->select();
        } else{
            $custom=db('routine_usage')
                ->join('stock', 'stock.id=routine_usage.stock_id', 'left')
                ->field('routine_usage.id,routine_usage.company,routine_usage.stock_name,routine_usage.stock_id,routine_usage.latest_cost,sum(routine_usage.number) as number,routine_usage.created_time,routine_usage.remake,stock.type')
                ->where('routine_usage.store_id', $this->us['store_id'])
                ->where('stock.type', $data['type'])
                ->group('routine_usage.latest_cost,routine_usage.stock_id')
                ->where('routine_usage.status', 3)
                ->select();
            
        }
        
        r_date($custom, 200);
    }
    
    /**
     * 获取库存列表
     */
    public function Stock_list_search()
    {
        $data           =Request::instance()->post(['title']);
        $custom_material=[];
        $purchase_usage =[];
        $routine_usage  =[];
        $custom_material=db('custom_material')
            ->field('id,company,stock_name,latest_cost,number,created_time,number')
            ->where(['store_id'=>$this->us['store_id'], 'status'=>0])
            ->where(['stock_name'=>['like', "%{$data['title']}%"]])
            ->where('number', 'neq', 0)
            ->select();
        foreach ($custom_material as $k=>$l) {
            $custom_material[$k]['type']=0;
        }
        
        $purchase_usage=db('purchase_usage')
            ->field('id,company,stock_name,latest_cost,number,created_time,status,content,remake')
            ->where('type', 1)
            ->where('number', 'neq', 0)
            ->where('status', 3)
            ->where(['stock_name'=>['like', "%{$data['title']}%"]])
            ->where('store_id', $this->us['store_id'])
            ->select();
        foreach ($purchase_usage as $k=>$l) {
            $purchase_usage[$k]['type']=99;
        }
        
        $routine_usage=db('routine_usage')
            ->join('stock', 'stock.id=routine_usage.stock_id', 'left')
            ->field('routine_usage.id,routine_usage.company,routine_usage.stock_name,routine_usage.stock_id,routine_usage.latest_cost,sum(routine_usage.number) as number,routine_usage.created_time,routine_usage.remake,stock.type')
            ->where('routine_usage.store_id', $this->us['store_id'])
            ->where(['routine_usage.stock_name'=>['like', "%{$data['title']}%"]])
            ->group('routine_usage.latest_cost,routine_usage.stock_id')
            ->where('routine_usage.status', 3)
            ->select();
        
        $cooperation_usage=db('cooperation_usage')
            ->join('stock', 'stock.id=cooperation_usage.stock_id', 'left')
            ->field('cooperation_usage.id,cooperation_usage.company,cooperation_usage.stock_name,cooperation_usage.stock_id,cooperation_usage.latest_cost,sum(cooperation_usage.number) as number,cooperation_usage.created_time,cooperation_usage.remake,stock.type')
            ->group('cooperation_usage.latest_cost,cooperation_usage.stock_id')
            ->where(['cooperation_usage.stock_name'=>['like', "%{$data['title']}%"]])
            ->where('cooperation_usage.status', 3)
            ->order('cooperation_usage.created_time desc')
            ->select();
        
        
        r_date(array_merge(array_merge(array_merge($custom_material, $purchase_usage), $routine_usage), $cooperation_usage), 200);
    }
    
    /**
     * 获取库存列表
     */
    public function CollectingStock_list()
    {
        $data=Request::instance()->post(['type', 'ok']);
        if ($data['type'] == 0) {
            $custom=db('custom_material')
                ->field('id,company,stock_name,status,latest_cost,number,created_time,number')
                ->where(['store_id'=>$this->us['store_id'], 'status'=>0])
                ->where('number', 'neq', 0)
                ->select();
            foreach ($custom as $k=>$l) {
                $custom[$k]['type']=0;
            }
        } else if ($data['type'] == 99) {
            $cust=db('purchase_usage')
                ->field('id,company,stock_name,status,latest_cost,number,created_time,content,remake')
                ->where('type', 1)
                ->where('number', 'neq', 0);
            if (isset($data['ok']) && $data['ok'] != '') {
                $cust->where(['status'=>3]);
            }
            $custom=$cust->where('store_id', $this->us['store_id'])
                ->order('created_time desc')
                ->select();
            foreach ($custom as $k=>$l) {
                $custom[$k]['type']=99;
            }
        } else if ($data['type'] == 98) {
            $custom=db('stock')
                ->field('id,company,stock_name,latest_cost,type,class_table')
                ->where('type', 98)
                ->whereNull('deleted_at')
                ->select();
            foreach ($custom as $k=>$value) {
                $custom[$k]['latest_cost']=!empty($value['latest_cost'])? $value['latest_cost'] : 0;
                $custom[$k]['number']     =db('cooperation_usage')
                    ->where('stock_id', $value['id'])
                    ->where('status', 3)
                    ->sum('number');
                $custom[$k]['state']      =db('cooperation_usage')
                    ->where('stock_id', $value['id'])
                    ->where('status', 3)
                    ->value('status');
            }
        } else{
            $custom=db('stock')
                ->field('id,company,stock_name,latest_cost,type,class_table')
                ->where('type', $data['type'])
                ->whereNull('deleted_at')
                ->select();
            
            foreach ($custom as $k=>$value) {
                $custom[$k]['latest_cost']=!empty($value['latest_cost'])? $value['latest_cost'] : 0;
                $custom[$k]['number']     =db('routine_usage')
                    ->where('store_id', $this->us['store_id'])
                    ->where('stock_id', $value['id'])
                    ->where('status', 3)
                    ->sum('number');
                $custom[$k]['state']      =db('routine_usage')
                    ->where('store_id', $this->us['store_id'])
                    ->where('stock_id', $value['id'])
                    ->value('status');
            }
        }
        
        r_date($custom, 200);
    }
    
    /**
     * 获取库存列表
     */
    public function CollectingStock_list_search()
    {
        $data           =Request::instance()->post(['title']);
        $custom_material=[];
        $purchase_usage =[];
        $routine_usage  =[];
        $custom_material=db('custom_material')
            ->field('id,company,stock_name,latest_cost,number,created_time,number')
            ->where(['store_id'=>$this->us['store_id'], 'status'=>0])
            ->where(['stock_name'=>['like', "%{$data['title']}%"]])
            ->where('number', 'neq', 0)
            ->select();
        foreach ($custom_material as $k=>$l) {
            $custom_material[$k]['type']=0;
        }
        
        $purchase_usage=db('purchase_usage')
            ->field('id,company,stock_name,latest_cost,number,created_time,status,content,remake')
            ->where('type', 1)
            ->where('number', 'neq', 0)
            ->where('status', 3)
            ->where(['stock_name'=>['like', "%{$data['title']}%"]])
            ->where('store_id', $this->us['store_id'])
            ->select();
        foreach ($purchase_usage as $k=>$l) {
            $purchase_usage[$k]['type']=99;
        }
        
        $routine_usage=db('stock')
            ->field('id,company,stock_name,latest_cost,type,class_table')
            ->whereNull('deleted_at')
            ->where(['stock_name'=>['like', "%{$data['title']}%"]])
            ->select();
        foreach ($routine_usage as $k=>$value) {
            $routine_usage[$k]['number']=db('routine_usage')
                ->where('store_id', $this->us['store_id'])
                ->where('stock_id', $value['id'])
                ->where('status', 3)
                ->sum('number');
            
        }
        
        
        r_date(array_merge(array_merge($custom_material, $purchase_usage), $routine_usage), 200);
    }
    
    
    /**
     * 获取已选列表
     */
    public function get_Material_list()
    {
        $data=Request::instance()->post();
        //材料领用
        $material_usage=db('material_usage')
            ->where(['order_id'=>$data['order_id'], 'types'=>$data['type']])
            ->field('Material_id,material_name,type,square_quantity,total_price,company,unit_price,created_time,id,status,remake')
            ->order('status asc')
            ->select();
        r_date($material_usage, 200);
    }
    
    /**
     * 入库详情列表
     */
    public function get_routine_usage()
    {
        $params        =Request::instance()->get();
        $stock_id      =$params['stock_id'];
        $material_usage=db('routine_usage')
            ->where(['stock_id'=>$stock_id])
            ->where('store_id', $this->us['store_id'])
            ->whereNotNull('sp_no')
            ->where('secondment', 0)
            ->order('id desc')
            ->select();
        $stock         =db('stock')
            ->whereNull('deleted_at')
            ->where(['id'=>$stock_id])
            ->find();
        
        if ($material_usage) {
            $material_usages=db('routine_usage')
                ->where(['stock_id'=>$stock_id])
                ->where('store_id', $this->us['store_id'])
                ->order('id desc')
                ->select();
            $num            =0;
            $pr             =0;
            foreach ($material_usages as $k=>$value) {
                if ($value['status'] == 3) {
                    $num+=$value['number'];
                    $pr +=$value['latest_cost'] * $value['number'];
                }
                
            }
            $data=['stock_name'=>$stock['stock_name'], 'latest_cost'=>$stock['latest_cost'], 'number'=>sprintf('%.2f', $num), 'total_price'=>sprintf('%.2f', $pr), 'data'=>$material_usage];
        } else{
            $material_usages=db('routine_usage')
                ->where(['stock_id'=>$stock_id])
                ->where('store_id', $this->us['store_id'])
                ->order('id desc')
                ->select();
            $num            =0;
            $pr             =0;
            foreach ($material_usages as $k=>$value) {
                if ($value['status'] == 3) {
                    $num+=$value['number'];
                    $pr +=$value['latest_cost'] * $value['number'];
                }
                
            }
            $data=['stock_name'=>$stock['stock_name'], 'latest_cost'=>$stock['latest_cost'], 'number'=>sprintf('%.2f', $num), 'total_price'=>sprintf('%.2f', $pr), 'data'=>$material_usage,];
        }
        if (isset($params['Test']) && $params['Test'] == 1) {
            res_date($data, 200);
            
        } else{
            r_date($data, 200);
        }
        
    }
    
    /*
     * 领用详情
     */
    public function get_Collect()
    {
        $params=Request::instance()->get();
        if ($params['type'] == 1) {
            $material_usage=db('material_usage')
                ->field('material_usage.created_time,material_usage.id,material_usage.types,material_usage.user_id,material_usage.square_quantity,material_usage.status')
                ->join('purchase_usage', 'purchase_usage.id=material_usage.Material_id', 'left')
                ->where(['purchase_usage.id'=>$params['stock_id']])
                ->where(['material_usage.type'=>99])
                ->where('purchase_usage.store_id', $this->us['store_id'])
                ->order('purchase_usage.id desc')
                ->select();
//            echo db('material_usage')->getLastSql();die;
        } elseif ($params['type'] == 2){
            $material_usage=db('material_usage')
                ->field('material_usage.created_time,material_usage.types,material_usage.user_id,material_usage.square_quantity,material_usage.status')
                ->join('custom_material', 'custom_material.id=material_usage.Material_id', 'left')
                ->where(['custom_material.id'=>$params['stock_id']])
                ->where('custom_material.store_id', $this->us['store_id'])
                ->where(['material_usage.type'=>0])
                ->order('custom_material.id desc')
                ->select();
        } else{
            $material_usage=db('material_usage')
                ->field('material_usage.created_time,material_usage.types,material_usage.user_id,material_usage.square_quantity,material_usage.status')
                ->join('usage_record', 'usage_record.id=material_usage.id', 'left')
                ->join('routine_usage', 'routine_usage.id=usage_record.unified', 'left')
                ->where(['routine_usage.stock_id'=>$params['stock_id']])
                ->whereNull('routine_usage.sp_no')
                ->where('routine_usage.store_id', $this->us['store_id'])
                ->order('routine_usage.id desc')
                ->select();
        }
        
        foreach ($material_usage as $k=>$value) {
            if ($value['types'] == 2) {
                $material_usage[$k]['username']=Db::connect(config('database.db2'))->table('app_user')->where('id', $value['user_id'])->value('username');
            } elseif ($value['types'] == 1){
                $material_usage[$k]['username']=db('user')->where('user_id', $value['user_id'])->value('username');
            }
        }
        r_date($material_usage, 200);
    }
    
    /*
     * 店长采购详情
     */
    public function purchaseUsage()
    {
        $params=Request::instance()->get();
        //type1采购2领用
        $purchase_usage=db('purchase_usage')
            ->where(['id'=>$params['id']])
            ->where('store_id', $this->us['store_id'])
            ->order('id desc')
            ->find();
        $num           =0;
        $pr            =0;
        
        if ($purchase_usage['status'] == 3) {
            $num+=$purchase_usage['number'];
            $pr +=$purchase_usage['latest_cost'] * $purchase_usage['number'];
        }
        $purchase_usage['number']=$purchase_usage['number'] + db('material_usage')->where('Material_id', $purchase_usage['id'])->where('type', 99)->whereIn('status', [0, 1])->sum('square_quantity');
        
        $purchase_usage['classification']=99;
        $data[]                          =$purchase_usage;
        $data                            =['stock_name'=>$purchase_usage['stock_name'], 'latest_cost'=>$purchase_usage['latest_cost'], 'number'=>$num, 'total_price'=>sprintf('%.2f', $pr), 'data'=>$data];
        r_date($data, 200);
    }
    
    /*
     * 自购采购详情
     */
    public function CollaborativeWarehouse()
    {
        $params=Request::instance()->get();
        
        //type1采购2领用
        $custom_material=db('custom_material')
            ->where(['id'=>$params['id']])
            ->where('store_id', $this->us['store_id'])
            ->order('id desc')
            ->find();
        $num            =0;
        $pr             =0;
        
        
        $num+=$custom_material['number'];
        
        $pr                       +=$custom_material['latest_cost'] * $custom_material['number'];
        $custom_material['status']=3;
        
        $custom_material['number']=$custom_material['number'] + db('material_usage')->where('Material_id', $custom_material['id'])->where('type', 0)->whereIn('status', [0, 1])->sum('square_quantity');
        
        $data[]=$custom_material;
        $data  =['stock_name'=>$custom_material['stock_name'], 'latest_cost'=>$custom_material['latest_cost'], 'number'=>sprintf('%.2f', $num), 'total_price'=>sprintf('%.2f', $pr), 'data'=>$data];
        r_date($data, 200);
        
        
    }
    
    /**
     * 师傅材料审核
     */
    public function get_Material_To_examine()
    {
        $data=Request::instance()->post();
        db()->startTrans();
        try {
            $material_usage=db('material_usage')->where(['id'=>$data['id']])->find();
            $status        =$data['status'];
            //材料领用
            db('material_usage')
                ->where(['id'=>$data['id']])
                ->update(['status'=>$status, 'remake'=>$data['remake'], 'adopt'=>time()]);
            
            if ($data['status'] == 2) {
                if ($material_usage['type'] == 0) {
                    $routine_usage=db('custom_material')->where('id', $material_usage['Material_id'])->setInc('number', $material_usage['square_quantity']);
                    
                } elseif ($material_usage['type'] == 99){
                    $routine_usage=db('purchase_usage')->where('id', $material_usage['Material_id'])->setInc('number', $material_usage['square_quantity']);
                } elseif ($material_usage['type'] == 98){
                    $usage_record =db('usage_record')->where(['id'=>$data['id']])->find();
                    $routine_usage=db('cooperation_usage')->where(['id'=>$usage_record['unified']])->update(['status'=>2]);
                } else{
                    $routine_usage=db('usage_record')->where(['id'=>$data['id']])->find();
                    db('routine_usage')->where(['id'=>$routine_usage['unified']])->update(['status'=>2]);
                }
                if (!$routine_usage) {
                    throw new Exception('更改库存失败');
                }
            } else{
                
                if ($material_usage['type'] != 0 && $material_usage['type'] != 99 && $material_usage['type'] != 98) {
                    
                    $routine_usage=db('usage_record')->where(['id'=>$data['id']])->find();
                    $routine      =db('routine_usage')->where(['id'=>$routine_usage['unified']])->find();
                    $numner       =db('routine_usage')
                        ->join('stock', 'stock.id=routine_usage.stock_id', 'left')
                        ->where('routine_usage.store_id', $routine['store_id'])
                        ->where('routine_usage.stock_id', $routine['stock_id'])
                        ->where('routine_usage.latest_cost', $routine['latest_cost'])
                        ->group('routine_usage.latest_cost,routine_usage.stock_id')
                        ->where('routine_usage.status', 3)
                        ->sum('routine_usage.number');
                    if ($numner + $routine['number'] < 0) {
                        throw new Exception('库存不足，请添加库存后再审核');
                    } else{
                        db('routine_usage')->where(['id'=>$routine_usage['unified']])->update(['status'=>3]);
                    }
                    
                } elseif($material_usage['type'] == 98){
                    
                    $square_quantity=db('cooperation_usage')->where(['stock_id'=>$material_usage['Material_id'], 'status'=>3, 'latest_cost'=>$material_usage['unit_price']])->sum('number');
                    $number         =$square_quantity - $material_usage['square_quantity'];
                    if ((int)$number < 0) {
                        throw new Exception('库存不足，请添加库存后再审核');
                    }
                    $usage_record =db('usage_record')->where(['id'=>$data['id']])->find();
                    db('cooperation_usage')->where(['id'=>$usage_record['unified']])->update(['status'=>3]);
                }
            }
            db()->commit();
            r_date(null, 200);
        } catch (Exception $e) {
            db()->rollback();
            r_date(null, 300, $e->getMessage());
        }
        
    }
    
    /**
     * 获取费用已选列表
     */
    public function get_reimbursement_list()
    {
        $data=Request::instance()->post();
        if (!empty($data['user_id'])) {
            $this->us['user_id']=$data['user_id'];
        }
        //费用报销
        if ($data['type'] == 1) {
            $quer=db('reimbursement');
            if (isset($data['isMaterial'])) {
                $quer->where('classification', 4);
            } else{
                $quer->where('classification', '<>', 4);
            }
            $reimbursement=$quer->where(['order_id'=>$data['order_id'], 'type'=>1])->field('reimbursement_name,money,voucher,created_time,id,status,remake, content as remarks,classification,payment_notes,receipt_number')->select();
        } else{
            $exp =new \think\db\Expression('field(status,0,3,1,2)');
            $quer=db('reimbursement');
            if (isset($data['isMaterial'])) {
                $quer->where('classification', 4);
            } else{
                $quer->where('classification', '<>', 4);
            }
            $reimbursement=$quer->where(['order_id'=>$data['order_id'], 'type'=>2])->field('reimbursement_name,money,voucher,created_time,id,status,remake,user_id, content as remarks,classification,payment_notes,receipt_number')->order($exp)->select();
            foreach ($reimbursement as $k=>$item) {
                $reimbursement[$k]['user_id']=Db::connect(config('database.db2'))->table('app_user')
                    ->where('id', $item['user_id'])
                    ->field('username')
                    ->find()['username'];
            }
        }
        foreach ($reimbursement as $k=>$item) {
            $reimbursement[$k]['voucher']=empty(unserialize($item['voucher']))? [] : unserialize($item['voucher']);
        }
        
        r_date($reimbursement, 200);
    }
    
    /**
     * 师傅报销审核
     */
    public function get_Material_To_Reimbursement(Approval $approval)
    {
        $data=Request::instance()->post();
        db()->startTrans();
        try {
            //材料领用
            $reimbursement=db('reimbursement')->where(['id'=>$data['id']])->find();
            if ($data['status'] == 1) {
                $my_string                  =unserialize($reimbursement['voucher']);
                $data['name']               =str_replace(' ', '', $reimbursement['reimbursement_name']);
                $data['money']              =$reimbursement['money'];
                $data['type']               =$reimbursement['classification'];
                $data['content']            =$reimbursement['content'];
                $data['work_wechat_user_id']=$this->us['work_wechat_user_id'];
                $data['reserve']            =$this->us['reserve'];
                $data['payment_notes']      =$reimbursement['payment_notes'];
                
                if (empty($data['work_wechat_user_id'])) {
                    throw new Exception('请联系公司');
                }
                
                $p=$approval->Reimbursement($data, $my_string);
                if ($p['errcode'] != 0) {
                    throw new Exception('企业微信提交失败');
                }
                sendZONG($p['sp_no'],'Bs7uczfFFNujAN7v9f9EX3MfDoM2WoFjdDRPtZ1hx');
                $p=['status'=>3, 'sp_no'=>$p['sp_no'], 'submission_time'=>time()];
            } else{
                $p=['status'=>2, 'remake'=>$data['remake'], 'submission_time'=>time()];
            }
            db('reimbursement')->where(['id'=>$data['id']])->update($p);
            db()->commit();
            r_date(null, 200);
        } catch (Exception $e) {
            db()->rollback();
            r_date(null, 300, $e->getMessage());
        }
        
    }
    
    
    /*
     * 人工结算
     */
    public
    function artificial_list()
    {
        
        $data   =Request::instance()->post();
        $capital=$cap=db('capital')->where(['ordesr_id'=>$data['order_id'], 'types'=>1, 'enable'=>1])->field('class_b,capital_id')->select();
        if (empty($capital)) {
            r_date(null, 300, '数据不存在');
        }
        foreach ($capital as $k=>$kl) {
            $reality_artificial=Db::connect(config('database.db2'))->table('app_user_order_capital')
                ->join('app_user', 'app_user.id=app_user_order_capital.user_id', 'left')
                ->where('app_user_order_capital.capital_id', $kl['capital_id'])
                ->where('app_user_order_capital.deleted_at', 'null')
                ->field('app_user_order_capital.work_time,app_user_order_capital.personal_price,app_user.username,app_user_order_capital.user_id')
                ->select();
//            echo Db::connect(config('database.db2'))->table('app_user_order_capital')->getLastSql();die;
            $capital[$k]['data']=$reality_artificial;;
        }
        r_date($capital, 200);
    }
    
    
    /*
     * 采购添加
     */
    public
    function purchase_usage_material(Approval $approval)
    {
        
        $data=Request::instance()->post();
        db()->startTrans();
        try {
            $validate=new validate\Purchase_usage();
            if (!$validate->check($data)) {
                r_date($validate->getError(), 300);
            }
            if ($data['type'] == 1) {
                $data['work_wechat_user_id']=$this->us['work_wechat_user_id'];
                if (empty($data['work_wechat_user_id'])) {
                    throw new Exception('请联系公司');
                }
                $wechatObj_data=$approval->Purchase($data, json_decode($data['voucher'], true));
                if ($wechatObj_data['errcode'] != 0) {
                    throw new Exception('企业微信提交失败');
                }
                sendZONG($wechatObj_data['sp_no'],'3TmACnEeaNnzBa2rNYCCRH75ZacUaDdfKw3VxnWy');
                $id=db('purchase_usage')->insertGetId([
                    'stock_name'  =>$data['stock_name'],
                    'company'     =>$data['company'],
                    'latest_cost' =>$data['latest_cost'],
                    'voucher'     =>!empty($data['voucher'])? serialize(json_decode($data['voucher'], true)) : '',
                    'user_id'     =>$this->us['user_id'],
                    'store_id'    =>$this->us['store_id'],
                    'created_time'=>time(),
                    'remake'      =>$data['remake'],
                    'type'        =>1,
                    'number'      =>$data['number'],
                    'status'      =>0,
                    'sp_no'       =>$wechatObj_data['sp_no'],
                ]);
                
            } elseif ($data['type'] == 2){
                $id=db('custom_material')->insertGetId([
                    'stock_name'  =>$data['stock_name'],
                    'company'     =>$data['company'],
                    'latest_cost' =>$data['latest_cost'],
                    'voucher'     =>!empty($data['voucher'])? serialize(json_decode($data['voucher'], true)) : '',
                    'user_id'     =>$this->us['user_id'],
                    'store_id'    =>$this->us['store_id'],
                    'remake'      =>$data['remake'],
                    'created_time'=>time(),
                    'status'      =>0,
                    'number'      =>$data['number'],
                ]);
            }
            
            db()->commit();
            r_date(null, 200);
        } catch (Exception $e) {
            db()->rollback();
            r_date(null, 300, $e->getMessage());
        }
        
        
    }
    
    /*
    * 采购收货/入库
    */
    public function purchase_usage_Warehousing()
    {
        $data=Request::instance()->post();
        //入库
        $id=db('purchase_usage')->where('id', $data['id'])->update(['status'=>3, 'notes'=>$data['notes'], 'notes_voucher'=>!empty($data['img'])? serialize(json_decode($data['img'], true)) : '']);
        if ($id) {
            r_date($id, 200);
        } else{
            r_date(null, 300);
        }
        
    }
    
    /*
    * 常规材料/入库
    */
    public function routine_usage_Warehousing()
    {
        $data=Request::instance()->post();
        db::startTrans();
        try {
            $routine_usage=db('routine_usage')->where('id', $data['id'])->find();
            
            if ($routine_usage['secondment'] == 1) {
                $secondment=\db('secondment')->where('debi_id', $routine_usage['id'])->find();
                
                db('routine_usage')->where('created_time', $secondment['created_time'])->where('stock_id', $secondment['stock_id'])->where(['store_id'=>$secondment['lender_store_id']])->update(['storage_time'=>time(), 'status'=>3]);
                db('routine_usage')->where('created_time', $secondment['created_time'])->where('stock_id', $secondment['stock_id'])->where(['store_id'=>$secondment['debi_store_id']])->update(['status'=>3, 'notes'=>$data['notes'], 'storage_time'=>time(), 'voucher'=>!empty($data['img'])? serialize(json_decode($data['img'], true)) : '']);
                
                
                db('secondment')->where('created_time', $secondment['created_time'])->where('stock_id', $secondment['stock_id'])->where(['lender_store_id'=>$secondment['lender_store_id']])->update(['status'=>3]);
                
            } else{
                //入库
                db('routine_usage')->where('id', $data['id'])->update(['status'=>3, 'notes'=>$data['notes'], 'storage_time'=>time(), 'voucher'=>!empty($data['img'])? serialize(json_decode($data['img'], true)) : '']);
            }
            db::commit();
            r_date(null, 200);
        } catch (Exception $exception) {
            db::rollback();
            r_date(null, 300, $exception->getMessage());
        }
    }

//    /*
//         * 常规材料直接入库没有收货
//         */
//    public function routine_usage_Warehousing()
//    {
//        $data = Request::instance()->post();
//        //入库
//        $custom = db('stock')
//            ->where('id', $data['id'])
//            ->find();
//
//        if (empty($custom)) {
//            r_date(null, 300, '数据不存在');
//        }
//        $id = db('routine_usage')->insertGetId([
//            'stock_name'   => $custom['stock_name'],
//            'stock_id'     => $custom['id'],
//            'store_id'     => $this->us['store_id'],
//            'company'      => $custom['company'],
//            'latest_cost'  => $custom['latest_cost'],
//            'user_id'      => $this->us['user_id'],
//            'number'       => $data['number'],
//            'status'       => 0,
//            'created_time' => time(),
//        ]);
//        if ($id) {
//            r_date($id, 200);
//        } else {
//            r_date(null, 400);
//        }
//    }
    /*
      * 常规材料直接入库没有收货
      */
    public function routine_Warehousing(Approval $approval)
    {
        $data=Request::instance()->post();
        //入库
        $custom=db('stock')
            ->where(['id'=>$data['id']])
            ->find();
        if (empty($custom)) {
            r_date(null, 300, '数据不存在');
        }
        db()->startTrans();
        try {
            $custom['work_wechat_user_id']=$this->us['work_wechat_user_id'];
            $custom['number']             =$data['number'];
            $custom['remake']             =$data['remake'];
            if (empty($custom['work_wechat_user_id'])) {
                throw new Exception('请联系公司');
            }
          
            $wechatObj_data=$approval->Purchase($custom, '');
            
            if ($wechatObj_data['errcode'] != 0) {
                throw new Exception('企业微信提交失败');
            }
            sendZONG($wechatObj_data['sp_no'],'3TmACnEeaNnzBa2rNYCCRH75ZacUaDdfKw3VxnWy');
            $id=db('routine_usage')->insertGetId([
                'stock_name'  =>$custom['stock_name'],
                'stock_id'    =>$custom['id'],
                'store_id'    =>$this->us['store_id'],
                'company'     =>$custom['company'],
                'latest_cost' =>$custom['latest_cost'],
                'user_id'     =>$this->us['user_id'],
                'number'      =>$data['number'],
                'remake'      =>$data['remake'],
                'status'      =>0,
                'created_time'=>time(),
                'sp_no'       =>$wechatObj_data['sp_no'],
            ]);
            
            db()->commit();
            r_date(null, 200);
        } catch (Exception $e) {
            db()->rollback();
            r_date(null, 300, $e->getMessage());
        }
    }
    
    public function list_reimbursement_type($type=1)
    {
        
        
        if ($type == 1) {
            $data=[
                [
                    'name'=>'其它費用',
                    'type'=>1,
                ],
                [
                    'name'=>'材料成本',
                    'type'=>2,
                ],
                [
                    'name'=>'人工成本',
                    'type'=>3,
                ],
            ];
            r_date($data, 200);
        } else{
            $data=[
                [
                    'name'=>'其它費用',
                    'type'=>1,
                ],
                [
                    'name'=>'材料成本',
                    'type'=>2,
                ],
                [
                    'name'=>'人工成本',
                    'type'=>3,
                ],
                [
                    'name'=>'代购主材',
                    'type'=>4,
                ],
            ];
            return $data;
        }
        
    }
    
    public function MaterialList(OrderModel $orderModel, $order_startDate, $order_endDate, $type, $page, $limit=20)
    {
        
        $material_usage=db('material_usage');
        $condition     =[];
        if (!empty($order_startDate) && !empty($order_endDate)) {
            
            $start                    =strtotime($order_startDate . ' 00:00:00');
            $end                      =strtotime($order_endDate . ' 23:59:59');
            $condition['created_time']=['between', [$start, $end]];
        }
        
        if (!empty($type) && $type == 3) {
            $condition['status']=0;
            $condition['types'] =2;
        }
        $article=$material_usage->where('shopowner_id', $this->us['user_id'])->distinct('order_id')->where($condition)->field('order_id')->order('created_time desc')->page($page, $limit)->select();
        $data   =[];
        foreach ($article as $k=>$item) {
            $data[$k]['order_id']=$item['order_id'];
            $data[$k]['address'] =$orderModel->addres($item['order_id']);
            $list                =db('material_usage')->where('order_id', $item['order_id'])->where($condition)->field('material_name,square_quantity,unit_price,user_id,types,total_price,status,types,id')->select();
            foreach ($list as $value) {
                if ($value['types'] == 2) {
                    $value['username']=Db::connect(config('database.db2'))->table('app_user')
                        ->where('id', $value['user_id'])->find()['username'];
                } else{
                    $value['username']=$this->us['username'];
                }
                $data[$k]['data'][]=$value;
                
                
            }
        }
        
        r_date($data, 200);
        
    }
    
    public function ReimbursementList(OrderModel $orderModel, $order_startDate, $order_endDate, $type, $page, $limit=1)
    {
        $isMaterial   =Request::instance()->get();
        $reimbursement=db('reimbursement');
        $condition    =[];
        if (!empty($order_startDate) && !empty($order_endDate)) {
            
            $start                    =strtotime($order_startDate . ' 00:00:00');
            $end                      =strtotime($order_endDate . ' 23:59:59');
            $condition['created_time']=['between', [$start, $end]];
        }
        if (!empty($type) && $type == 4) {
            $condition['status']=0;
            $condition['type']  =2;
        }
        $quer=$reimbursement;
        if (isset($isMaterial['isMaterial'])) {
            
            $quer->where('classification', 4);
        } else{
            
            $quer->where('classification', '<>', 4);
        }
        $list=$quer->where('shopowner_id', $this->us['user_id'])->where($condition)->distinct('order_id')->field('order_id ')->order('created_time desc')->page($page, $limit)->select();
        $data=[];
        foreach ($list as $k=>$item) {
            $data[$k]['order_id']=$item['order_id'];
            $data[$k]['address'] =$orderModel->addres($item['order_id']);
            $reimbur             =db('reimbursement');
            
            if (isset($isMaterial['isMaterial'])) {
                $reimbur->where('classification', 4);
            } else{
                $reimbur->where('classification', '<>', 4);
            }
            $lists=$reimbur->where($condition)->where('order_id', $item['order_id'])->field('id,order_id,reimbursement_name,money,user_id,type,status,classification,id')->order('created_time desc')->select();
            foreach ($lists as $value) {
                if ($value['type'] == 2) {
                    $value['username']=Db::connect(config('database.db2'))->table('app_user')
                        ->where('id', $value['user_id'])->find()['username'];
                } else{
                    $value['username']=$this->us['username'];
                }
                if (!empty($value['cleared_time'])) {
                    $value['status']=4;
                }
                $title         =array_search($value['classification'], array_column($this->list_reimbursement_type(2), 'type'));
                $value['title']=$this->list_reimbursement_type(2)[$title]['name'];
                unset($value['classification']);
                $data[$k]['data'][]=$value;
            }
        }
        r_date(array_values($data), 200);
    }
    
    /*
     * 报销详情
     */
    public function ReimbursementInfo($id)
    {
        
        $reimbursement=db('reimbursement')->where('id', $id)->find();
        if ($reimbursement['type'] == 2) {
            $reimbursement['username']=Db::connect(config('database.db2'))->table('app_user')
                ->where('id', $reimbursement['user_id'])->find()['username'];
        } else{
            $reimbursement['username']=$this->us['username'];
        }
        if ($reimbursement['classification'] == 4) {
            $reimbursement['mainMaterialIds']=db('capital')->whereIn('capital_id', $reimbursement['capital_id'])->field('class_b')->select();
        } else{
            $reimbursement['mainMaterialIds']=null;
        }
        $title                        =array_search($reimbursement['classification'], array_column($this->list_reimbursement_type(2), 'type'));
        $reimbursement['title']       =$this->list_reimbursement_type(2)[$title]['name'];
        $reimbursement['created_time']=!empty($reimbursement['created_time'])? date('y-m-d H:i', $reimbursement['created_time']) : '';
        $reimbursement['adopt']       =!empty($reimbursement['adopt'])? date('y-m-d H:i', $reimbursement['adopt']) : '';
        $reimbursement['cleared_time']=!empty($reimbursement['cleared_time'])? date('y-m-d H:i', $reimbursement['cleared_time']) : '';//计算时间
        $reimbursement['voucher']     =!empty($reimbursement['voucher'])? unserialize($reimbursement['voucher']) : null;
        r_date($reimbursement, 200);
        
    }
    
    public function ListSummary()
    {
        
        $data['reimbursementMoney']=sprintf("%.2f", db('reimbursement')->where('shopowner_id', $this->us['user_id'])->where('classification', '<>', 4)->where('status', 1)->whereNull('cleared_time')->sum('money'));//订单费用报销
        $data['materialUsageCount']=db('material_usage')->where('shopowner_id', $this->us['user_id'])->where('status', 0)->count();
        $data['reimbursementCount']=db('reimbursement')->where('shopowner_id', $this->us['user_id'])->where('type', 2)->where('status', 0)->count();
        $data['MaterialCount']     =sprintf("%.2f", db('reimbursement')->where('shopowner_id', $this->us['user_id'])->where('classification', 4)->where('status', 1)->sum('money'));
        $data['routineUusageCount']=db('routine_usage')->where('user_id', $this->us['user_id'])->where('secondment', 1)->where('number', '<', 0)->where('status', 0)->count();
        
        r_date($data, 200);
    }
    
    /*
     * 材料外借
     */
    public function lend()
    {
        $data=Request::instance()->post();
        
        db()->startTrans();
        try {
            $m =\db('user')->where('user_id', $data['user_id'])->field('user_id,store_id')->find();
            $id=explode(',', $data['id']);
            
            if ($data['type'] == 0) {
            
            } elseif ($data['type'] == 99){
            
            } else{
                
                if (count($id) > 1) {
                    $cooperation_usage=db('routine_usage')->whereIn('id', $id)->where(['store_id'=>$m['store_id']])->order('created_time asc')->select();
                    $cooperation_num  =db('routine_usage')->where('status', 3)->where('stock_id', $cooperation_usage[0]['stock_id'])->where(['store_id'=>$m['store_id']])->sum('number');
                    if ($cooperation_num < $data['number']) {
                        throw new Exception('库存不足');
                    }
                    foreach ($cooperation_usage as $item) {
                        $cooperation_usage_num=db('routine_usage')->where('status', 3)->where('stock_id', $item['stock_id'])->where(['store_id'=>$m['store_id']])->where('latest_cost', $item['latest_cost'])->sum('number');
                        if ($data['number'] == 0 || $data['number'] < 0) {
                            break;
                        }
                        if ($data['number'] > 0) {
                            $num=round($data['number'] - $cooperation_usage_num, 2);
                            if ($num > 0) {
                                $num_s=$data['number'] - $num;
                            } else{
                                $num_s=$data['number'];
                            }
                            if ($num_s > 0) {
                                $time=time();
                                $ids =db('routine_usage')->insertGetId(['store_id'=>$m['store_id'], 'stock_name'=>$item['stock_name'], 'latest_cost'=>$item['latest_cost'], 'user_id'=>$m['user_id'], 'number'=>'-' . $num_s, 'stock_id'=>$item['stock_id'], 'status'=>0, 'notes'=>$item['notes'], 'adopt'=>$item['adopt'], 'voucher'=>$item['voucher'], 'created_time'=>$time, 'company'=>$item['company'], 'secondment'=>1]);
                                
                                $id=db('routine_usage')->insertGetId(['store_id'=>$this->us['store_id'], 'stock_name'=>$item['stock_name'], 'latest_cost'=>$item['latest_cost'], 'user_id'=>$this->us['user_id'], 'number'=>$num_s, 'stock_id'=>$item['stock_id'], 'status'=>0, 'notes'=>$item['notes'], 'adopt'=>$item['adopt'], 'voucher'=>$item['voucher'], 'created_time'=>$time, 'company'=>$item['company'], 'secondment'=>1, 'sp_no'=>$time]);
                                
                                db('secondment')->insertGetId(['debi_store_id'=>$this->us['store_id'], 'stock_name'=>$item['stock_name'], 'latest_cost'=>$item['latest_cost'], 'debi'=>$this->us['user_id'], 'number'=>$num_s, 'stock_id'=>$item['stock_id'], 'company'=>$item['company'], 'created_time'=>$time, 'remake'=>$item['remake'], 'lender'=>$m['user_id'], 'lender_store_id'=>$m['store_id'], 'lender_id'=>$ids, 'debi_id'=>$id]);
                                $data['number']=$num;
                                continue;
                            }
                            
                        }
                        
                    }
                    
                } else{
                    $time             =time();
                    $cooperation_usage=db('routine_usage')->whereIn('id', $id)->where(['store_id'=>$m['store_id']])->find();
                    $ids              =db('routine_usage')->insertGetId(['store_id'=>$m['store_id'], 'stock_name'=>$cooperation_usage['stock_name'], 'latest_cost'=>$cooperation_usage['latest_cost'], 'user_id'=>$m['user_id'], 'number'=>'-' . $data['number'], 'stock_id'=>$cooperation_usage['stock_id'], 'status'=>0, 'notes'=>$cooperation_usage['notes'], 'adopt'=>$cooperation_usage['adopt'], 'voucher'=>$cooperation_usage['voucher'], 'created_time'=>$time, 'company'=>$cooperation_usage['company'], 'secondment'=>1]);
                    
                    $id=db('routine_usage')->insertGetId(['store_id'=>$this->us['store_id'], 'stock_name'=>$cooperation_usage['stock_name'], 'latest_cost'=>$cooperation_usage['latest_cost'], 'user_id'=>$this->us['user_id'], 'number'=>$data['number'], 'stock_id'=>$cooperation_usage['stock_id'], 'status'=>0, 'notes'=>$cooperation_usage['notes'], 'adopt'=>$cooperation_usage['adopt'], 'voucher'=>$cooperation_usage['voucher'], 'created_time'=>$time, 'company'=>$cooperation_usage['company'], 'secondment'=>1, 'sp_no'=>$time]);
                    
                    db('secondment')->insertGetId(['debi_store_id'=>$this->us['store_id'], 'stock_name'=>$cooperation_usage['stock_name'], 'latest_cost'=>$cooperation_usage['latest_cost'], 'debi'=>$this->us['user_id'], 'number'=>$data['number'], 'stock_id'=>$cooperation_usage['stock_id'], 'created_time'=>$time, 'remake'=>$cooperation_usage['remake'], 'lender'=>$m['user_id'], 'lender_store_id'=>$m['store_id'], 'company'=>$cooperation_usage['company'], 'lender_id'=>$ids, 'debi_id'=>$id]);
                }
            }
            
            db()->commit();
            r_date(null, 200);
            
        } catch
        (Exception $e) {
            db()->rollback();
            r_date(null, 300, $e->getMessage());
        }
    }
    
    /*
     * 戒掉
     */
    public
    function ItemLending()
    {
        $data =Request::instance()->post();
        $users=\db('user');
        if (isset($data['user_name']) && $data['user_name'] != '') {
            $users->whereLike('user.username', "%{$data['user_name']}%");
        }
        if (isset($data['store_id']) && $data['store_id'] != 0) {
            $users->whereLike('user.store_id', $data['store_id']);
        }
        $user=$users->where('user.reserve', 1)->where('user.store_id', '<>', $this->us['store_id'])->where('user.status', 0)->join('store', 'store.store_id=user.store_id', 'left')->field('user.user_id,user.store_id,user.username,user.mobile,user.work_tag,store.store_name,user.store_id')->select();
        foreach ($user as $k=>$v) {
            $work_tag=empty($v['work_tag'])? [0, 0, 0, 0] : explode(',', $v['work_tag']);
            
            $user[$k]['work_tags']=[];
            foreach ($work_tag as $ks=>$value) {
                switch ($ks) {
                    case 0:
                        $values='建筑渗漏水维修专家';
                        break;
                    case 1:
                        $values='旧房改造类专家';
                        break;
                    case 2:
                        $values='墙面维修专家';
                        break;
                    case 3:
                        $values='室内渗漏水维修专家';
                        break;
                }
                
                if ($value == 1) {
                    $user[$k]['work_tags'][]=$values;
                }
            }
            $routine_usage            =db('routine_usage')->where('stock_id', $data['stock_id'])->where('store_id', $v['store_id'])->where('status', 3)->sum('number');
            $user[$k]['routine_usage']=$routine_usage;
            
        }
        
        $userData=null;
        foreach ($user as $item) {
            if ($item['routine_usage'] > 0) {
                $userData[]=$item;
            }
        }
        
        r_date($userData, 200);
        
        
    }
    
    public function SecondmentDetails()
    {
        $data=Request::instance()->post();
        if ($data['type'] == 0) {
            $date  =db('custom_material')->where(['id'=>$data['stock_id'], 'store_id'=>$data['store_id'], 'status'=>0])->group('latest_cost')->field('id,sum(number) as number,stock_name,latest_cost')->select();
            $number=db('custom_material')->where(['id'=>$data['stock_id'], 'store_id'=>$data['store_id'], 'status'=>3])->sum('number');
        } elseif ($data['type'] == 99){
            $date  =db('purchase_usage')->where(['id'=>$data['stock_id'], 'store_id'=>$data['store_id'], 'status'=>3])->group('latest_cost')->field('id,sum(number) as number,stock_name,latest_cost')->select();
            $number=db('purchase_usage')->where(['id'=>$data['stock_id'], 'store_id'=>$data['store_id'], 'status'=>3])->sum('number');
        } elseif ($data['type'] == 98){
            $date=db('cooperation_usage')->where(['id'=>$data['stock_id'], 'status'=>3])->group('latest_cost')->field('id,stock_name,latest_cost')->select();;
            $number=db('cooperation_usage')->where(['id'=>$data['stock_id'], 'status'=>3])->sum('number');
        } else{
            $date  =db('routine_usage')->where(['stock_id'=>$data['stock_id'], 'store_id'=>$data['store_id'], 'status'=>3])->group('latest_cost')->field("id,stock_name,latest_cost")->select();
            $number=db('routine_usage')->where(['stock_id'=>$data['stock_id'], 'store_id'=>$data['store_id'], 'status'=>3])->sum('number');
        }
        if (count($date) > 1) {
            $id=implode(array_column($date, 'id'), ',');
        } else{
            $id=$date[0]['id'];
        }
        
        
        r_date(['num'=>$number, 'stock_name'=>$date[0]['stock_name'], 'latest_cost'=>$date[0]['latest_cost'], 'id'=>$id], 200);
        
    }
    
    /*
     * 借用记录
     */
    public function QuitTheList()
    {
        $date=Request::instance()->get();
        //Debit1借方2出借方
        $secondment=\db('secondment')->field('secondment.stock_name,secondment.id,secondment.latest_cost,FROM_UNIXTIME(secondment.created_time,"%Y-%m-%d %H:%i:%s") as created_time,sum(secondment.number) as number,user.username,secondment.status,secondment.debi_id');
        if (isset($date['stock_id']) && $date['stock_id'] != '') {
            $secondment->where('secondment.stock_id', $date['stock_id']);
        }
        $data=$secondment->join('user', 'user.user_id=secondment.debi', 'left')
            ->where('secondment.debi', $this->us['user_id'])
            ->group('secondment.stock_id,secondment.created_time,secondment.debi')
            ->select();
        foreach ($data as $k=>$i) {
            $data[$k]['Debit']=1;
            $data[$k]['ids']  =$i['debi_id'];
        }
        $secondments=\db('secondment')->field('secondment.stock_name,secondment.debi_id,secondment.id,secondment.latest_cost,FROM_UNIXTIME(secondment.created_time,"%Y-%m-%d %H:%i:%s") as created_time,sum(secondment.number) as number,user.username,secondment.status');
        if (isset($date['stock_id']) && $date['stock_id'] != '') {
            $secondments->where('secondment.stock_id', $date['stock_id']);
        }
        $secondments=$secondments->join('user', 'user.user_id=secondment.debi', 'left')
            ->where('secondment.lender', $this->us['user_id'])
            ->group('secondment.stock_id,secondment.created_time,secondment.debi')
            ->select();
        foreach ($secondments as $k=>$i) {
            $secondments[$k]['Debit']=2;
            $secondments[$k]['ids']  =$i['debi_id'];
        }
        $userArrayList=array_values(array_merge($data, $secondments));
        $arr_key      =array_column($userArrayList, 'created_time');
        array_multisort($arr_key, SORT_DESC, $userArrayList);
        r_date($userArrayList, 200);
    }
    
    /*
     * 戒掉审批列表
     */
    public function routineUusageApproval()
    {
        $date      =Request::instance()->post();
        $secondment=\db('secondment')->field('secondment.stock_name,secondment.id,secondment.latest_cost,FROM_UNIXTIME(secondment.created_time,"%Y-%m-%d %H:%i:%s") as created_time,sum(secondment.number) as number,user.username,secondment.status');
        if (!empty($date['created_time']) && $date['created_time'] != '' && !empty($date['end_time']) && $date['end_time'] != '') {
            $start=strtotime($date['created_time'] . ' 00:00:00');
            $end  =strtotime($date['end_time'] . ' 23:59:59');
            $secondment->whereBetween('secondment.created_time', [$start, $end]);
        }
        $data=$secondment->join('user', 'user.user_id=secondment.debi', 'left')
            ->where('secondment.lender', $this->us['user_id'])
            ->group('secondment.stock_id,secondment.created_time,secondment.debi')
            ->select();
        
        r_date($data, 200);
    }
    
    /*
     * 戒掉审批详情
     */
    public function routineUusageApprovalInfo()
    {
        $date=Request::instance()->post();
        
        $secondment          =\db('secondment')->field('secondment.stock_name,stock_id,lender_store_id,secondment.latest_cost,FROM_UNIXTIME(secondment.created_time,"%Y-%m-%d %H:%i:%s") as created_time,secondment.number,secondment.remake,user.username,user1.username as username1,secondment.status')
            ->join('user', 'user.user_id=secondment.lender', 'left')
            ->join('user user1', 'user1.user_id=secondment.debi', 'left')
            ->where('secondment.id', $date['id'])
            ->where('secondment.lender', $this->us['user_id'])
            ->find();
        $secondment['number']=\db('secondment')->where('created_time', strtotime($secondment['created_time']))->where('stock_id', $secondment['stock_id'])->where(['lender_store_id'=>$secondment['lender_store_id']])->sum('number');
        r_date($secondment, 200);
    }
    
    /*
     * 戒掉审批审核
     */
    public function toExamine()
    {
        $date=Request::instance()->post();
        
        $secondment=\db('secondment')
            ->where('id', $date['id'])
            ->find();
        
        $cooperation_usage_num=db('routine_usage')->where('status', 3)->where('stock_id', $secondment['stock_id'])->where(['store_id'=>$secondment['lender_store_id']])->sum('number');
        if ($cooperation_usage_num <= 0) {
            r_date(null, 300, '库存不足');
        }
        $cooperation_num=db('routine_usage')->where('created_time', $secondment['created_time'])->where('stock_id', $secondment['stock_id'])->where(['store_id'=>$secondment['lender_store_id']])->sum('number');
        if ($cooperation_usage_num < abs($cooperation_num)) {
            r_date(null, 300, '库存不足');
        }
        
        db('routine_usage')->where('created_time', $secondment['created_time'])->where('stock_id', $secondment['stock_id'])->where(['store_id'=>$secondment['lender_store_id']])->update(['status'=>$date['status'], 'adopt'=>time(), 'remake'=>$date['remake']]);
        db('routine_usage')->where('created_time', $secondment['created_time'])->where('stock_id', $secondment['stock_id'])->where(['store_id'=>$secondment['debi_store_id']])->update(['status'=>$date['status'], 'adopt'=>time(), 'remake'=>$date['remake']]);
        
        db('secondment')->where('created_time', $secondment['created_time'])->where('stock_id', $secondment['stock_id'])->where(['lender_store_id'=>$secondment['lender_store_id']])->update(['status'=>$date['status'], 'adopt'=>time()]);
        r_date(null, 200);
    }
    
    
}