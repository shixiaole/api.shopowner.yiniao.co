<?php
/**
 * Created by muniao.
 * User: muniao
 * Date: 2018/7/25
 * Time: 9:48
 */

namespace app\api\controller\android\v1;


use think\Controller;
use app\index\model\Jpush;


class Remind extends Controller
{
    protected $JPush;
    protected $newTime;
    protected $endTime;
    protected $overtime;


    protected function _initialize()
    {
        $this->JPush = new JPush();
    }

    public function index()
    {
        $list = db('app_version')
            ->field('version_code,path')
            ->order('id desc')
            ->find();
        r_date($list, 200);
    }

    /*
     * 1系统消息2订单提醒3订单跟进提醒4上门提醒5公司通知6新订单
     */

    public function NewOrders()
    {
        $res = db('assignment')
            ->field('assignment.user_id,u.registrationId,assignment.time,assignment.order_id,or.state')
            ->join('user u', 'assignment.user_id=u.user_id ', 'left')
            ->join('order or', 'or.order_id=assignment.order_id ', 'left')
            ->order('time desc')
            ->select();

        foreach ($res as $k => $v) {
            if ($v['state'] == 1 && !empty($v['registrationId'])) {
                $current_time = time();
                $span         = $current_time - $v['time'];
                $m            = floor((($span % (3600 * 24)) % 3600) / 60);
                $content      = '';
                if ($m == 10) {
                    $content = '5分钟后将超时';

                } elseif ($m == 14) {
                    $content = '1分钟后将超时';

                } elseif ($m == 20) {
                    $content = '已超时5分钟';

                } elseif ($m == 25) {
                    $content = '已超时10分钟';
                }
                if (!empty($content)) {
                    $data = array('order_id' => $v['order_id'], 'content' => $content, 'type' => 2, 'user_id' => $v['user_id']);
                    $this->JPush->tui('', $v['registrationId'], $data);
                }


            }


        }
    }

    /*
     * 跟进提醒
     */
    public function FollowupReminders()
    {
        $start_time = strtotime(date('Y-m-d 00:00:00', time())); //2016-11-01 00:00:00
        $end_time   = strtotime(date('Y-m-d 23:59:59', time())); //2016-11-01 23:59:59
        $res        = db('through')
            ->field('through.order_ids,u.registrationId,through.end_time,or.contacts,or.order_id,or.assignor')
            ->join('order or', 'or.order_id=through.order_ids ', 'left')
            ->join('user u', 'or.assignor=u.user_id ', 'left')
            ->where('through.role=2')
            ->select();
        foreach ($res as $k => $v) {
            $i = db('through')->where(['order_ids' => $v['order_ids'], 'role' => 2])->field('end_time')->order('through_id desc')->find();
            if ($start_time < $i['end_time'] && $end_time > $i['end_time']) {
                $content = $v['contacts'] . '订单需要跟进了,赶紧收拾一下，去见客户，把合同签了';
                if (!empty($v['registrationId'])) {
                    $data = array('order_id' => $v['order_id'], 'content' => $content, 'type' => 3, 'user_id' => $v['assignor']);
                    $this->JPush->tui('', $v['registrationId'], $data);
                }
            }
        }


    }

    /*
    * 1系统消息2订单提醒3订单跟进提醒4上门提醒5公司通知6新订单
    */
    public function Newshan()
    {
        //当天结束之间
        $res = db('order')
            ->field('order.created_time,u.registrationId,order.order_id,order.assignor,order.planned')
            ->join('user u', 'order.assignor=u.user_id ', 'left')
            ->where(['order.planned' => ['neq', '<>']])
            ->select();

        foreach ($res as $k => $v) {
            $current_time = time();
            $span         = $v['planned'] - $current_time;
            $m = floor((($span % (3600*24)) % 3600) / 60);
            $h            = floor(($span % (3600 * 24)) / 3600);
            var_dump($h,$m,$v['planned']);
            echo "<br>";
            if ($h == 1 && $m==0) {
                $content = "倒计时1小时";
                if (!empty($v['registrationId'])) {
                    $data = array('order_id' => $v['order_id'], 'content' => $content, 'type' => 4, 'user_id' => $v['assignor']);
                    $this->JPush->tui('', $v['registrationId'], $data);
                }
            } elseif ($h == 2 && $m==0) {
                $content = "倒计时2小时";
                if (!empty($v['registrationId'])) {
                    $data = array('order_id' => $v['order_id'], 'content' => $content, 'type' => 4, 'user_id' => $v['assignor']);
                    $this->JPush->tui('', $v['registrationId'], $data);
                }
            } elseif ($h == 6 && $m==0) {
                $content = "倒计时6小时";
                if (!empty($v['registrationId'])) {
                    $data = array('order_id' => $v['order_id'], 'content' => $content, 'type' => 4, 'user_id' => $v['assignor']);
                    $this->JPush->tui('', $v['registrationId'], $data);
                }
            }


        }
    }



}