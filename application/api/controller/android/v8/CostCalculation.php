<?php
/**
 * Created by muniao.
 * User: muniao
 * Date: 2018/8/2
 * Time: 13:20
 */

namespace app\api\controller\android\v8;


use app\api\model\PollingModel;
use Doctrine\ORM\Version;
use think\Config;
use think\Controller;

use app\api\model\Authority;
use app\api\model\Common;
use  app\api\model\OrderModel;
use think\Request;

class CostCalculation extends Controller
{
    protected $model;
    protected $us;
    protected $newTime;
    protected $endTime;
    protected $overtime;
    protected $pdf;

    public function _initialize()
    {
        $this->model = new Authority();

        $this->us = Authority::check(1);

    }

    public function share(OrderModel $orderModel)
    {

        $data = Request::instance()->post();
        if (empty($data['id'])) {
            r_date(null, 300, '合同ID不能为空');
        }

        $op = db('sign')->where(['sign.sign_id' => $data['id']])->join('order or', 'or.order_id=sign.order_id', 'left')->join('user', 'or.assignor=user.user_id', 'left')->field('sign.*,or.telephone,or.province_id,or.assignor,additional,additional_agent,user.store_id')->find();

        $offer = $orderModel::offer($data['order_id']);
        $do = [
            'data' => [],
        ];

        if (!empty($offer['amount'])) {
            if ($offer['amount'] - $offer['agency']) {
                $imageDefault = [
                    'left' => 260,
                    'top' => 6700,
                    'right' => 0,
                    'bottom' => 0,
                    'width' => 100,
                    'height' => 100,
                    'opacity' => 50,
                ];
                $engineering = explode(',', $op['engineering']);
                $p = [];
                if ($engineering) {
                    foreach ($engineering as $item) {
                        if ($item == 0) {
                            $da = [
                                'text' => '√',
                                'left' => 270,
                                'top' => 2264,
                                'fontSize' => 30,       //�ֺ�
                                'fontColor' => '36, 36, 36, 1', //������ɫ
                                'fontPath' => ROOT_PATH . 'public/simsun.ttc', //������ɫ
                                'angle' => 0,
                            ];
                        } elseif ($item == 1) {
                            $da = [
                                'text' => '√',
                                'left' => 450,
                                'top' => 2264,
                                'fontSize' => 30,       //�ֺ�
                                'fontColor' => '36, 36, 36, 1', //������ɫ
                                'fontPath' => ROOT_PATH . 'public/simsun.ttc', //������ɫ
                                'angle' => 0,
                            ];
                        } elseif ($item == 2) {
                            $da = [
                                'text' => '√',
                                'left' => 657,
                                'top' => 2264,
                                'fontSize' => 30,       //�ֺ�
                                'fontColor' => '36, 36, 36, 1', //������ɫ
                                'fontPath' => ROOT_PATH . 'public/simsun.ttc', //������ɫ
                                'angle' => 0,
                            ];
                        } elseif ($item == 3) {
                            $da = [
                                'text' => '√',
                                'left' => 850,
                                'top' => 2264,
                                'fontSize' => 30,       //�ֺ�
                                'fontColor' => '36, 36, 36, 1', //������ɫ
                                'fontPath' => ROOT_PATH . 'public/simsun.ttc', //������ɫ
                                'angle' => 0,
                            ];
                        } else {
                            $da = [
                                'text' => '√',
                                'left' => 1060,
                                'top' => 2264,
                                'fontSize' => 30,       //�ֺ�
                                'fontColor' => '36, 36, 36, 1', //������ɫ
                                'fontPath' => ROOT_PATH . 'public/simsun.ttc', //������ɫ
                                'angle' => 0,
                            ];
                        }
                        $p[] = $da;
                    }
                }

                $textDefault = [
                    [
                        'text' => $op['contract'],
                        'left' => 945,
                        'top' => 95,
                        'fontSize' => 20,       //�ֺ�
                        'fontColor' => '36, 36, 36, 1', //������ɫ
                        'fontPath' => ROOT_PATH . 'public/simsun.ttc', //������ɫ
                        'angle' => 0,
                    ],
                    [
                        'text' => $this->us['username'],
                        'left' => 400,
                        'top' => 1600,
                        'fontSize' => 20,       //�ֺ�
                        'fontColor' => '36, 36, 36, 1', //������ɫ
                        'fontPath' => ROOT_PATH . 'public/simsun.ttc', //������ɫ
                        'angle' => 0,
                    ],
                    [
                        'text' => !empty($op['con_time']) ? date('Y年m月d日H:i', $op['con_time']) : '',
                        'left' => 820,
                        'top' => 1600,
                        'fontSize' => 20,       //�ֺ�
                        'fontColor' => '36, 36, 36, 1', //������ɫ
                        'fontPath' => ROOT_PATH . 'public/simsun.ttc', //������ɫ
                        'angle' => 0,
                    ],
                    [
                        'text' => $op['username'],
                        'left' => 290,
                        'top' => 1790,
                        'fontSize' => 20,       //�ֺ�
                        'fontColor' => '36, 36, 36, 1', //������ɫ
                        'fontPath' => ROOT_PATH . 'public/simsun.ttc', //������ɫ
                        'angle' => 0,
                    ],
                    [
                        'text' => $op['idnumber'],
                        'left' => 280,
                        'top' => 1845,
                        'fontSize' => 20,       //�ֺ�
                        'fontColor' => '36, 36, 36, 1', //������ɫ
                        'fontPath' => ROOT_PATH . 'public/simsun.ttc', //������ɫ
                        'angle' => 0,
                    ],
                    [
                        'text' => $op['addres'],
                        'left' => 267,
                        'top' => 2204,
                        'fontSize' => 20,       //�ֺ�
                        'fontColor' => '36, 36, 36, 1', //������ɫ
                        'fontPath' => ROOT_PATH . 'public/simsun.ttc', //������ɫ
                        'angle' => 0,
                    ],
                    [
                        'text' => !empty($offer['gong']) ? $offer['gong'] : '——————',
                        'left' => 900,
                        'top' => 2555,
                        'fontSize' => 20,       //�ֺ�
                        'fontColor' => '36, 36, 36, 1', //������ɫ
                        'fontPath' => ROOT_PATH . 'public/simsun.ttc', //������ɫ
                        'angle' => 0,
                    ],
                    [
                        'text' => !empty($op['addtime']) ? date('Y年m月d日H:i', $op['addtime']) : '——————',
                        'left' => 180,
                        'top' => 2550,
                        'fontSize' => 20,       //�ֺ�
                        'fontColor' => '36, 36, 36, 1', //������ɫ
                        'fontPath' => ROOT_PATH . 'public/simsun.ttc', //������ɫ
                        'angle' => 0,
                    ],
                    [
                        'text' => !empty($op['uptime']) ? date('Y年m月d日H:i', $op['uptime']) : '———————',
                        'left' => 500,
                        'top' => 2550,
                        'fontSize' => 20,       //�ֺ�
                        'fontColor' => '36, 36, 36, 1', //������ɫ
                        'fontPath' => ROOT_PATH . 'public/simsun.ttc', //������ɫ
                        'angle' => 0,
                    ],

                    [
                        'text' => $offer['amount'] - $offer['agency'],
                        'left' => 380,
                        'top' => 3245,
                        'fontSize' => 20,       //�ֺ�
                        'fontColor' => '36, 36, 36, 1', //������ɫ
                        'fontPath' => ROOT_PATH . 'public/simsun.ttc', //������ɫ
                        'angle' => 0,
                    ],
                    [
                        'text' => Common::NumToCNMoney($offer['amount'] - $offer['agency']),
                        'left' => 720,
                        'top' => 3245,
                        'fontSize' => 20,       //�ֺ�
                        'fontColor' => '36, 36, 36, 1', //������ɫ
                        'fontPath' => ROOT_PATH . 'public/simsun.ttc', //������ɫ
                        'angle' => 0,
                    ],
                    [
                        'text' => $op['b1'] . '%',
                        'left' => 690,
                        'top' => 3358,
                        'fontSize' => 20,       //�ֺ�
                        'fontColor' => '36, 36, 36, 1', //������ɫ
                        'fontPath' => ROOT_PATH . 'public/simsun.ttc', //������ɫ
                        'angle' => 0,
                    ],
                    [
                        'text' => $op['ding'],
                        'left' => 260,
                        'top' => 3390,
                        'fontSize' => 20,       //�ֺ�
                        'fontColor' => '36, 36, 36, 1', //������ɫ
                        'fontPath' => ROOT_PATH . 'public/simsun.ttc', //������ɫ
                        'angle' => 0,
                    ],
                    //天数
                    [
                        'text' => $op['explain'],
                        'left' => 200,
                        'top' => 3440,
                        'fontSize' => 18,       //�ֺ�
                        'fontColor' => '36, 36, 36, 1', //������ɫ
                        'fontPath' => ROOT_PATH . 'public/simsun.ttc', //������ɫ
                        'angle' => 0,
                    ],
                    [
                        'text' => $op['b2'] . '%',
                        'left' => 675,
                        'top' => 3445,
                        'fontSize' => 20,       //�ֺ�
                        'fontColor' => '36, 36, 36, 1', //������ɫ
                        'fontPath' => ROOT_PATH . 'public/simsun.ttc', //������ɫ
                        'angle' => 0,
                    ],
                    [
                        'text' => $op['zhong'],
                        'left' => 1030,
                        'top' => 3440,
                        'fontSize' => 20,       //�ֺ�
                        'fontColor' => '36, 36, 36, 1', //������ɫ
                        'fontPath' => ROOT_PATH . 'public/simsun.ttc', //������ɫ
                        'angle' => 0,
                    ],
                    [
                        'text' => $op['wei'],
                        'left' => 180,
                        'top' => 3550,
                        'fontSize' => 20,       //�ֺ�
                        'fontColor' => '36, 36, 36, 1', //������ɫ
                        'fontPath' => ROOT_PATH . 'public/simsun.ttc', //������ɫ
                        'angle' => 0,
                    ],
                    [
                        'text' => $op['telephone'],
                        'left' => 260,
                        'top' => 6820,
                        'fontSize' => 20,       //�ֺ�
                        'fontColor' => '36, 36, 36, 1', //������ɫ
                        'fontPath' => ROOT_PATH . 'public/simsun.ttc', //������ɫ
                        'angle' => 0,
                    ],
                    [
                        'text' => !empty($op['con_time']) ? date('Y年m月d日H:i', $op['con_time']) : '',
                        'left' => 260,
                        'top' => 6895,
                        'fontSize' => 20,       //�ֺ�
                        'fontColor' => '36, 36, 36, 1', //������ɫ
                        'fontPath' => ROOT_PATH . 'public/simsun.ttc', //������ɫ
                        'angle' => 0,
                    ],
                    [
                        'text' => $this->us['username'],
                        'left' => 780,
                        'top' => 6740,
                        'fontSize' => 20,       //�ֺ�
                        'fontColor' => '36, 36, 36, 1', //������ɫ
                        'fontPath' => ROOT_PATH . 'public/simsun.ttc', //������ɫ
                        'angle' => 0,
                    ],
                    [
                        'text' => $this->us['mobile'],
                        'left' => 780,
                        'top' => 6820,
                        'fontSize' => 20,       //�ֺ�
                        'fontColor' => '36, 36, 36, 1', //������ɫ
                        'fontPath' => ROOT_PATH . 'public/simsun.ttc', //������ɫ
                        'angle' => 0,
                    ],
                    [
                        'text' => !empty($op['con_time']) ? date('Y年m月d日H:i', $op['con_time']) : '',
                        'left' => 780,
                        'top' => 6895,
                        'fontSize' => 18,       //�ֺ�
                        'fontColor' => '36, 36, 36, 1', //������ɫ
                        'fontPath' => ROOT_PATH . 'public/simsun.ttc', //������ɫ
                        'angle' => 0,
                    ],

                ];
                $st = empty($op['additional']) ? '无' : $op['additional'];
                $lp = ceil(mb_strlen($st) / 45);
                if (mb_strlen($st) > 45) {
                    for ($i = 0; $i <= $lp; $i++) {
                        $lo = mb_substr($st, $i * 45, 45, "utf-8");
                        $textContent[] =
                            [
                                'text' => $lo,
                                'left' => 110,
                                'top' => 6483 + $i * 35,
                                'fontSize' => 18,       //�ֺ�
                                'fontColor' => '36, 36, 36, 1', //������ɫ
                                'fontPath' => ROOT_PATH . 'public/simsun.ttc', //������ɫ
                                'angle' => 0,
                            ];

                    }

                } else {
                    $textContent[] =
                        [
                            'text' => $st,
                            'left' => 110,
                            'top' => 6483,
                            'fontSize' => 20,       //�ֺ�
                            'fontColor' => '36, 36, 36, 1', //������ɫ
                            'fontPath' => ROOT_PATH . 'public/simsun.ttc', //������ɫ
                            'angle' => 0,
                        ];
                }

                $textDefault = array_values(array_merge($textDefault, $textContent));
                $textDefault = array_merge($textDefault, $p);
                if ($op['province_id'] != 1 && $op['province_id'] != 22) {
                    $op1 = 1;
                } else {
                    if ($op['province_id'] == 1) {
                        if ($op['store_id'] == 1 || $op['store_id'] == 17) {
                            //成都益鸟银沙装饰
                            $op1 = 'ys';
                        } elseif ($op['store_id'] == 5 || $op['store_id'] == 12 || $op['store_id'] == 6){
                            //成都益鸟金沙装饰
                            $op1 = 'js';
                        } elseif ($op['store_id'] == 3 || $op['store_id'] == 14 || $op['store_id'] == 10){
                            //成都益鸟紫荆装饰
                            $op1 = 'zj';
                        }elseif ($op['store_id'] == 9 || $op['store_id'] == 13){
                            //成都益鸟美居东区
                            $op1 = 'mj';

                        }else{
                            //公共店
                            $op1 = 'mj';
                        }
                    } else {
                        $op1 = $op['province_id'];
                    }

                }
                $background = ROOT_PATHS . '/contract/' . $op1 . '.png';//背景
                if (!empty($op['autograph']) && (parse_url($op['autograph'])['host'] == 'imgaes.yiniaoweb.com' || parse_url($op['autograph'])['host'] == 'api.yiniaoweb.com')) {

                    $path = '/app/' . (new PollingModel())->file_exists_S3($op['autograph'], ROOT_PATH . 'public/app/');
                    $filename = pathinfo($path, PATHINFO_BASENAME);

                    $contractpaths = ROOT_PATH . 'public/app/' . $filename;

                } else {
                    $path = parse_url($op['autograph'], PHP_URL_PATH);
                    $contractpaths = '';
                }


                $config['image'][]['url'] = ROOT_PATHS . $path;


                $filename = ROOT_PATHS . '/ContractNumber/' . $data['id'] . '.jpg';
                Common::getbgqrcode($imageDefault, $textDefault, $background, $filename, $config);
                $u['url'] = request()->domain() . '/' . config('city') . '/ContractNumber/' . $data['id'] . '.jpg?t=' . time();
                $u['type'] = !empty($op['contractpath']) ? 1 : 2;
                $do = [
                    'agency' => 0,
                    'data' => [
                        $u,
                    ],
                ];
            }
        }
        if ($offer['agency']) {
            $img = CostCalculation::agency($op, $offer['agency']);
            $do['agency'] = 1;
            array_push($do['data'], $img);
            $contractpaths = '';
        }
        if (!empty($offer['agency']) && !empty(($offer['amount'] - $offer['agency']))) {
            $do['state'] = 3;
        } elseif (!empty($offer['agency'])) {
            $do['state'] = 2;
        } elseif (!empty(($offer['amount'] - $offer['agency']))) {
            $do['state'] = 1;
        }

        if (file_exists($contractpaths)) {
            unlink($contractpaths);
        }
        r_date($do, 200);
    }

    /*
     * 代购合同
     */

    public function agency($op, $offer)
    {
        $imageDefault = [
            'left' => 560,
            'top' => 2800,
            'right' => 0,
            'bottom' => 0,
            'width' => 100,
            'height' => 100,
            'opacity' => 50,
        ];
        $textDefault = [
            [
                'text' => $op['username'],
                'left' => 320,
                'top' => 260,
                'fontSize' => 20,       //�ֺ�
                'fontColor' => '36, 36, 36, 1', //������ɫ
                'fontPath' => ROOT_PATH . 'public/simsun.ttc', //������ɫ
                'angle' => 0,
            ],
            [
                'text' => $op['addres'],
                'left' => 730,
                'top' => 260,
                'fontSize' => 20,       //�ֺ�
                'fontColor' => '36, 36, 36, 1', //������ɫ
                'fontPath' => ROOT_PATH . 'public/simsun.ttc', //������ɫ
                'angle' => 0,
            ],
            [
                'text' => $offer,
                'left' => 250,
                'top' => 345,
                'fontSize' => 20,       //�ֺ�
                'fontColor' => '36, 36, 36, 1', //������ɫ
                'fontPath' => ROOT_PATH . 'public/simsun.ttc', //������ɫ
                'angle' => 0,
            ],
            [
                'text' => Common::NumToCNMoney($offer),
                'left' => 620,
                'top' => 345,
                'fontSize' => 20,       //�ֺ�
                'fontColor' => '36, 36, 36, 1', //������ɫ
                'fontPath' => ROOT_PATH . 'public/simsun.ttc', //������ɫ
                'angle' => 0,
            ],
            [
                'text' => $this->us['username'],
                'left' => 940,
                'top' => 2850,
                'fontSize' => 20,       //�ֺ�
                'fontColor' => '36, 36, 36, 1', //������ɫ
                'fontPath' => ROOT_PATH . 'public/simsun.ttc', //������ɫ
                'angle' => 0,
            ],

//            [
//                'text'     =>!empty($op['con_time'])? date('Y年m月d日H:i', $op['con_time']) : '',
//                'left'     =>110,
//                'top'      =>2670,
//                'fontSize' =>20,       //�ֺ�
//                'fontColor'=>'36, 36, 36, 1', //������ɫ
//                'fontPath' =>ROOT_PATH . 'public/simsun.ttc', //������ɫ
//                'angle'    =>0,
//            ],
//            [
//                'text'     =>!empty($op['con_time'])? date('Y年m月d日H:i', $op['con_time']) : '',
//                'left'     =>730,
//                'top'      =>2670,
//                'fontSize' =>20,       //�ֺ�
//                'fontColor'=>'36, 36, 36, 1', //������ɫ
//                'fontPath' =>ROOT_PATH . 'public/simsun.ttc', //������ɫ
//                'angle'    =>0,
//            ],

        ];

        $st = empty($op['additional_agent']) ? '无' : $op['additional_agent'];

        $lp = ceil(mb_strlen($st) / 30);
        if (mb_strlen($st) > 30) {
            for ($i = 0; $i <= $lp; $i++) {
                $lo = mb_substr($st, $i * 30, 45, "utf-8");
                $textContent[] =
                    [
                        'text' => $lo,
                        'left' => 110,
                        'top' => 2510 + $i * 48,
                        'fontSize' => 18,       //�ֺ�
                        'fontColor' => '36, 36, 36, 1', //������ɫ
                        'fontPath' => ROOT_PATH . 'public/simsun.ttc', //������ɫ
                        'angle' => 0,
                    ];

            }
        } else {

            $textContent[] =
                [
                    'text' => $st,
                    'left' => 120,
                    'top' => 2510,
                    'fontSize' => 20,       //�ֺ�
                    'fontColor' => '36, 36, 36, 1', //������ɫ
                    'fontPath' => ROOT_PATH . 'public/simsun.ttc', //������ɫ
                    'angle' => 0,
                ];
        }

        $textDefault = array_values(array_merge($textDefault, $textContent));
        if ($op['store_id'] == 1 || $op['store_id'] == 17) {
            //成都益鸟银沙装饰
            $op1 = config('city').'ys';
        } elseif ($op['store_id'] == 5 || $op['store_id'] == 12|| $op['store_id'] == 6){
            //成都益鸟金沙装饰
            $op1 = config('city').'js';
        } elseif ($op['store_id'] == 3 || $op['store_id'] == 14 || $op['store_id'] == 10){
            //成都益鸟紫荆装饰
            $op1 = config('city').'zj';
        }elseif ($op['store_id'] == 9 || $op['store_id'] == 13){
            //成都益鸟美居东区
            $op1 = config('city').'mj';

        }else{
            //公共店
            $op1 =  config('city').'mj';
        }
        $background = ROOT_PATHS . '/contract/' .$op1 . '.png';//背景
        if (!empty($op['autograph']) && (parse_url($op['autograph'])['host'] == 'imgaes.yiniaoweb.com' || parse_url($op['autograph'])['host'] == 'api.yiniaoweb.com')) {

            $path = '/app/' . (new PollingModel())->file_exists_S3($op['autograph'], ROOT_PATH . 'public/app/');
            $filename = pathinfo($path, PATHINFO_BASENAME);

            $contractpaths = ROOT_PATH . 'public/app/' . $filename;

        } else {
            $path = parse_url($op['autograph'], PHP_URL_PATH);
            $contractpaths = '';
        }

        $config['image'][]['url'] = ROOT_PATHS . $path;
        $filename = ROOT_PATHS . '/agency/' . $op['sign_id'] . '.jpg';

        Common::getbgqrcode($imageDefault, $textDefault, $background, $filename, $config);
        $u['url'] = request()->domain() . '/' . config('city') . '/agency/' . $op['sign_id'] . '.jpg?t=' . time();
        $u['type'] = !empty($op['contractpath']) ? 1 : 2;
        if (file_exists($contractpaths)) {
            unlink($contractpaths);
        }

        return $u;
    }

    /*
    * 合同问题
    */
    public function ContractIssues()
    {
        $data = [
            [
                'title' => '墙面翻新',
                'type' => 0,
            ],
            [
                'title' => '室内渗漏水',
                'type' => 1,
            ],
            [
                'title' => '建筑渗漏水',
                'type' => 2,
            ],
            [
                'title' => '局部改造',
                'type' => 3,
            ],
            [
                'title' => '其它',
                'type' => 4,
            ],
        ];
        r_date($data, 200);
    }

}