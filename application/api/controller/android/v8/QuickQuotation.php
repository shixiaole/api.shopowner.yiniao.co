<?php
/**
 * Created by PhpStorm.
 * User: Raytine
 * Date: 2019/11/29
 * Time: 15:23
 */

namespace app\api\controller\android\v8;


use app\api\model\Common;


use app\api\model\SchemeCollection;
use app\api\model\schemeMaster;
use constant\config;
use think\Cache;
use think\Controller;
use think\Db;
use think\Exception;
use think\Request;
use app\api\model\Authority;


class QuickQuotation extends Controller
{
    protected $us;

    public function _initialize()
    {
        $this->us = Authority::check(1);
    }

    /**
     * @param array $data
     * @return array
     * $data=['id','name'=>'123']  id必填，name选填，默认值为123（有默认值时key不能为int）
     */
    public static function param(array $data)
    {
        if (empty($data)) {
            r_date([], 300, '提交数据不正确');
        }
        $return = [];

        foreach ($data as $key => $item) {

            if (!is_int($key)) {

                $val = Request::instance()->post($key);
                $get = Request::instance()->get($key);
                if (isset($val) || isset($get)) {
                    $return[$key] = isset($val)?trim($val):trim($get);

                } else {
                    $return[$key] = trim($item);
                }
            } else {
                $val = Request::instance()->post($item);
                $get = Request::instance()->get($item);

                if (!isset($val) && !isset($get)) {
                    r_date([], 300, "缺少参数");
                }
                $return[$item] = isset($val)?trim($val):$get;
            }
        }
        return $return;
    }

    /**
     * 新版快速报价页面数据
     */
    public function QuickGetList()
    {
        $data = static ::param(['order_no']);
        $op = db('order')->where(['order.order_no' => $data['order_no']])
            ->join('sign','order.order_id=sign.order_id','left')
            ->field('sign.sign_id,sign.username,sign.idnumber,sign.contract,sign.addtime,sign.uptime,sign.addres,sign.contractpath,sign.agency_img,sign.ding,sign.zhong,sign.wei,sign.b1,sign.b2,sign.explain,order.contacts,order.order_id,order.quotation')
            ->find();
        if($op){
            $op['username'] = $op['contacts'];
            $op['idnumber'] = !empty($op['idnumber']) ? $op['idnumber'] : null;
            $op['contract'] = !empty($op['contract']) ? $op['contract'] : null;
            $op['type'] = !empty($op['contractpath']) ? 1 : 2;
            $op['addtime'] = !empty($op['addtime']) ? date('Y-m-d H:i', $op['addtime']) : null;
            $op['uptime'] = !empty($op['uptime']) ? date('Y-m-d H:i', $op['uptime']) : null;
            $op['addres'] = !empty($op['addres']) ? $op['addres'] : null;
            $op['quotation'] = !empty($op['quotation']) ? $op['quotation'] : '';
            $op['agency_img'] = !empty($op['agency_img']) ? $op['agency_img'] : '';
            $envelopes = db('envelopes')->where(['ordesr_id' => $op['order_id'],'type'=>1])->find();
            if($envelopes){
                $agency=[];
                $sumlist=[];
                $capital = db('capital')->where(['envelopes_id' => $envelopes['envelopes_id'], 'types' => 1])
                    ->field('projectId,capital_id,class_a,class_b as projectTitle,company,square,
            un_Price as projectMoney,to_price,fen as category,agency,projectRemark as remarks')->select();
                foreach ($capital as $item){
                    if($item['agency']==1){
                        $agency[]=$item;
                    }else{
                        $sumlist[]=$item;
                    }

                }
                $envelopes['agencysum']=$s = array_sum(array_column($agency, 'to_price'));
                $envelopes['sumlist']=array_sum(array_column($sumlist, 'to_price'));
                $s = implode(',', array_column($capital, 'capital_id'));
                $yc = empty($envelopes['through_id']) ? 1 : 2;
                $allow = db('allow')->where(['order_id' => $op['order_id'], 'capital_id' => $s, 'envelopes_id' => $envelopes['envelopes_id'], 'yc' => $yc])->value('id');

                $envelopes['allow'] = $allow['id'];

                $envelopes['give_b'] = null;

                if ($envelopes['capitalgo']) {
                    $envelopes['capitalgo'] = unserialize($envelopes['capitalgo']);
                } else {
                    $envelopes['capitalgo'] = null;
                }
                $envelopes['project_title'] = empty($envelopes['project_title']) ? '报价方案' : $envelopes['project_title'];
                $envelopes['yc'] = $yc;

                //质保卡
                $warranty_collection=\db('warranty_collection')->where('warranty_collection.envelopes_id', $envelopes['envelopes_id'])->where('warranty_collection.pro_types', 0)->join('warranty', 'warranty.id=warranty_collection.warranty_id', 'left')->where('warranty_collection.order_id', $op['order_id'])->field('warranty.title,warranty_collection.years,warranty_collection.type,warranty_collection.warranty_id,warranty_collection.id')->select();
                //交互标准
                $capitalList=$capital;
                foreach ($capitalList as $k=>$item) {
                    $capitalList[$k]['class_b'] =$item['projectTitle'];
                    $po           =db('auxiliary_project_list')->where('capital_id', $item['capital_id'])->whereNull('delete_time')->field('auxiliary_id,id')->select();
                    $auxiliary_ids=array_unique(array_column($po, 'id'));
                    if ($po) {
                        foreach ($po as $l=>$key) {

                            $capitalList[$k]['auxiliary_id'] =$item['projectId'];
                            $capitalList[$k]['auxiliary_ids']=implode($auxiliary_ids, ',');
                            $capitalList[$k]['data'][]       =db('auxiliary_interactive')->where('id', $key['auxiliary_id'])->field('id,title')->find();
                            $auxiliary_interactive       =db('auxiliary_interactive')
                                ->Join('auxiliary_delivery_schedule', 'auxiliary_delivery_schedule.auxiliary_interactive_id=auxiliary_interactive.id', 'left')
                                ->where('auxiliary_delivery_schedule.auxiliary_project_list_id', $key['id'])
                                ->whereNull('auxiliary_delivery_schedule.delete_time')
                                ->field('auxiliary_interactive.id, auxiliary_interactive.title, auxiliary_delivery_schedule.id as auxiliary_delivery_schedule_id')->select();
                            foreach ($auxiliary_interactive as $p=>$value) {
                                $auxiliary_delivery_node=db('auxiliary_delivery_node')->where('auxiliary_delivery_schedul_id', $value['auxiliary_delivery_schedule_id'])->field('state,node_id')->select();
                                $auxiliaryNodeId        =array_column($auxiliary_delivery_node, 'state');
                                $auxiliaryNodeNode      =array_column($auxiliary_delivery_node, 'node_id');
                                if ($auxiliaryNodeId) {
                                    if (in_array(2, $auxiliaryNodeId)) {
                                        $auxiliary_interactive[$p]['state']  =2;
                                        $auxiliary_interactive[$p]['node_id']=implode(',', $auxiliaryNodeNode);

                                    } elseif (in_array(0, $auxiliaryNodeId)){
                                        $auxiliary_interactive[$p]['state']  =0;
                                        $auxiliary_interactive[$p]['node_id']=implode(',', $auxiliaryNodeNode);

                                    } else{
                                        $auxiliary_interactive[$p]['state']  =1;
                                        $auxiliary_interactive[$p]['node_id']=implode(',', $auxiliaryNodeNode);
                                    }
                                } else{
                                    $auxiliary_interactive[$p]['state']  =3;
                                    $auxiliary_interactive[$p]['node_id']=0;
                                }


                            }
                            $capitalList[$k]['data'][$l]['data']=array_merge($auxiliary_interactive);
                        }
                    } else{
                        $capitalList[$k]['data']=[];
                    }

                }
                $op = ['gong' => $envelopes, 'company' => $capital,'warranty_collection'=>$warranty_collection,'capitalList'=>$capitalList];
                r_date($op, 200);
            }else{
                r_date(null, 300,'暂未找到报价');
            }





        }else{
            r_date(null, 300,'暂未找到报价');
        }




    }

}