<?php
/**
 * Created by muniao.
 * User: muniao
 * Date: 2018/7/31
 * Time: 16:00
 */

namespace app\api\controller\android\v11;


use think\Cache;
use think\Controller;
use app\api\model\Authority;
use think\db;

use think\Request;
use  app\api\model\OrderModel;

class Transfer extends Controller
{
    
    protected $us;
    
    public function _initialize()
    {
        $model   =new Authority();
        $this->us=$model->check(1);
    }
    
    /*
     * 转派记录添加
     */
    public function record()
    {
        
        $data=\request()->post();
        $op  =\db('transfer_record')->insertGetId([
            'user_id'  =>$data['user_id'],
            'time'     =>time(),
            'order_id' =>$data['order_id'],
            'operation'=>$this->us['user_id'],
        ]);
        if ($op) {
            $store_id=db('user')->where('user_id',$data['user_id'])->value('store_id');
            $company_id = db('company_config', config('database.zong'))->where('find_in_set(:id,store_list)', ['id' => $store_id])->where('status', 1)->value('company_id');
            \db('order')->where('order_id', $data['order_id'])->update(['assignor'=>$data['user_id'],'company_id'=>$company_id]);
            db('message')->where(['order_id'=>$data['order_id']])->update(['already'=>0, 'have'=>time()]);
            db('remind')->where(['order_id'=>$data['order_id']])->update(['tai'=>0]);
            $li=db('order')
                ->field('order.*,st.store_id,st.store_name,us.*,p.province,c.city,y.county')
                ->join('user us', 'order.assignor=us.user_id', 'left')
                ->join('store st', 'us.store_id=st.store_id', 'left')
                ->join('province p', 'order.province_id=p.province_id', 'left')
                ->join('city c', 'order.city_id=c.city_id', 'left')
                ->join('county y', 'order.county_id=y.county_id', 'left')
                ->where(['order.order_id'=>$data['order_id']])
                ->find();
            $s =$li['province'] . $li['city'] . $li['county'] . $li['addres'];

            db('remind')->insertGetId([
                'admin_id'=>$data['user_id'],
                'order_id'=>$data['order_id'],
                'time'    =>time(),
                'stater'  =>1,
                'tai'     =>1,
            ]);
            $content="尊敬的{$li['username']}店长您好！客户:{$li['contacts']}，地址:{$s}，电话:{$li['telephone']}，订单：{$li['order_no']}来了，请尽快联系客户预约上门服务。如有任何问题都可致电4000-987-009,回T退订";
            db('message')->insertGetId([
                'type'    =>6,
                'content' =>$content,
                'order_id'=>$data['order_id'],
                'time'    =>time(),
                'user_id' =>$data['user_id'],
            ]);
            db('through')->where('order_ids' ,$data['order_id'])->update(['handle'=>time()]);
            \db('workbench_read')->where('order_id',$data['order_id'])->update(['have'=>time(),'already'=>0]);
            $orderSMS=json_decode(sendOrder($data['order_id']), true);
            if ($orderSMS['code'] != 200) {
                r_date('', 300, '编辑失败');
            }
            r_date('', 200);
        }
        r_date('', 300, '添加失败');
    }
    
}