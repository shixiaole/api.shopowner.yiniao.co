<?php
/**
 * Created by PhpStorm.
 * User: Raytine
 * Date: 2019/11/29
 * Time: 15:23
 */

namespace app\api\controller\android\v11;


use app\api\model\Capital;
use app\api\model\Common;


use app\api\model\DiagnosticField;

use app\api\model\schemeMaster;
use constant\config;
use think\Cache;
use think\Controller;
use think\Db;
use think\Exception;
use think\Request;
use app\api\model\Authority;


class QuickQuotation extends Controller
{
    protected $us;

    public function _initialize()
    {
        $this->us = Authority::check(1);
    }

    /**
     * @param array $data
     * @return array
     * $data=['id','name'=>'123']  id必填，name选填，默认值为123（有默认值时key不能为int）
     */
    public static function param(array $data)
    {
        if (empty($data)) {
            r_date([], 300, '提交数据不正确');
        }
        $return = [];

        foreach ($data as $key => $item) {

            if (!is_int($key)) {

                $val = Request::instance()->post($key);
                $get = Request::instance()->get($key);
                if (isset($val) || isset($get)) {
                    $return[$key] = isset($val) ? trim($val) : trim($get);

                } else {
                    $return[$key] = trim($item);
                }
            } else {
                $val = Request::instance()->post($item);
                $get = Request::instance()->get($item);

                if (!isset($val) && !isset($get)) {
                    r_date([], 300, "缺少参数");
                }
                $return[$item] = isset($val) ? trim($val) : $get;
            }
        }
        return $return;
    }

    /**
     * 新版快速报价页面数据
     */
    public function QuickGetList(Capital $capitals)
    {

        $data = static::param(['order_no', 'order_id']);
        $op   = db('order')->where(['order.order_no' => $data['order_no']])->find();
        if ($op) {
            $envelopes = db('envelopes')->where(['ordesr_id' => $op['order_id'], 'type' => 1])->find();
            if ($envelopes) {
                $agency      = [];
                $sumlist     = [];
                $result      = array();
                $capital     = $capitals->with('specsList')
                    ->where(['envelopes_id' => $envelopes['envelopes_id'], 'types' => 1])
                    ->field('projectId,capital_id,class_a,class_b as projectTitle,company,square, un_Price as projectMoney,to_price as allMoney,fen as types,agency,projectRemark as remarks,categoryName')->select();
                $capitalList = $capital;

                $envelopes['order_list'] = db('order')->where(['order_id' => $data['order_id']])->field('order_id,state')->find();


                $envelopes['give_b']    = null;
                if ($envelopes['capitalgo']) {
                    $envelopes['capitalgo'] = unserialize($envelopes['capitalgo']);
                } else {
                    $envelopes['capitalgo'] = null;
                }
                $envelopes['project_title'] = empty($envelopes['project_title']) ? '报价方案' : $envelopes['project_title'];


                //质保卡
                $warranty_collection = \db('warranty_collection')->where('warranty_collection.envelopes_id', $envelopes['envelopes_id'])->where('warranty_collection.pro_types', 0)->join('warranty', 'warranty.id=warranty_collection.warranty_id', 'left')->where('warranty_collection.order_id', $op['order_id'])->field('warranty.title,warranty_collection.years,warranty_collection.type,warranty_collection.warranty_id,warranty_collection.id')->select();
                //交互标准

                $capital_id = array_column($capitalList, 'capital_id');

                $po = db('auxiliary_project_list')->whereIn('capital_id', $capital_id)->whereNull('delete_time')->field('auxiliary_id,id,capital_id')->select();

                $auxiliary_id          = array_column($po, 'auxiliary_id');
                $auxiliary_interactive = [];
                if (!empty($auxiliary_id)) {
                    $auxiliary_interactive = db('auxiliary_interactive')->whereIn('id', $auxiliary_id)->field('id,title')->select();
                }
                $id                       = array_column($po, 'id');
                $auxiliaryInteractiveList = db('auxiliary_interactive')
                    ->Join('auxiliary_delivery_schedule', 'auxiliary_delivery_schedule.auxiliary_interactive_id=auxiliary_interactive.id', 'left')
                    ->whereIn('auxiliary_delivery_schedule.auxiliary_project_list_id', $id)
                    ->whereNull('auxiliary_delivery_schedule.delete_time')
                    ->field('auxiliary_interactive.id, auxiliary_interactive.title, auxiliary_delivery_schedule.id as auxiliary_delivery_schedule_id,auxiliary_delivery_schedule.auxiliary_project_list_id')->select();
                if (!empty($auxiliaryInteractiveList)) {
                    $auxiliary_delivery_node = db('auxiliary_delivery_node')->whereIn('auxiliary_delivery_schedul_id', array_column($auxiliaryInteractiveList, 'auxiliary_delivery_schedule_id'))->field('state,node_id,auxiliary_delivery_schedul_id')->select();
                }
                $capitalListArray = [];
                $agency           = [];
                $sumlist          = [];
                foreach ($capitalList as $k => $item) {
                    if ($item['agency'] == 1) {
                        $agency[] = $item;
                    } else {
                        $sumlist[] = $item;
                    }
                    $auxiliary_ids = array_unique(array_column($po, 'id'));
                    if ($po) {
                        foreach ($po as $l => $key) {
                            if ($item['capital_id'] == $key['capital_id']) {
                                $capitalListArray[$k]['class_b']       = $item['projectTitle'];
                                $capitalListArray[$k]['auxiliary_id']  = $item['projectId'];
                                $capitalListArray[$k]['auxiliary_ids'] = implode($auxiliary_ids, ',');;
                                foreach ($auxiliary_interactive as $val) {
                                    if ($key['auxiliary_id'] == $val['id']) {
                                        $auxiliaryList[] = $val;
                                    }

                                }
                                foreach ($auxiliaryList as $ls) {
                                    $auxiliaryInteractiveListNode = [];

                                    foreach ($auxiliaryInteractiveList as $p => $value) {
                                        if ($key['id'] == $value['auxiliary_project_list_id'] && $key['id'] == $value['auxiliary_project_list_id']) {
                                            $state   = 3;
                                            $node_id = 0;
                                            foreach ($auxiliary_delivery_node as $node) {
                                                if ($node['auxiliary_delivery_schedul_id'] == $value['auxiliary_delivery_schedule_id'] && $key['id'] == $value['auxiliary_project_list_id']) {
                                                    $auxiliaryNodeId   = array_column($auxiliary_delivery_node, 'state');
                                                    $auxiliaryNodeNode = array_column($auxiliary_delivery_node, 'node_id');
                                                    if ($auxiliaryNodeId) {
                                                        if (in_array(2, $auxiliaryNodeId)) {
                                                            $state   = 2;
                                                            $node_id = implode(',', $auxiliaryNodeNode);
                                                        } elseif (in_array(0, $auxiliaryNodeId)) {
                                                            $state   = 0;
                                                            $node_id = implode(',', $auxiliaryNodeNode);
                                                        } else {
                                                            $state   = 1;
                                                            $node_id = implode(',', $auxiliaryNodeNode);
                                                        }
                                                    } else {
                                                        $state   = 3;
                                                        $node_id = 0;
                                                    }
                                                }
                                            }
                                            $auxiliaryInteractiveListNode[$p]['state']                          = $state;
                                            $auxiliaryInteractiveListNode[$p]['node_id']                        = $node_id;
                                            $auxiliaryInteractiveListNode[$p]['id']                             = $value['id'];
                                            $auxiliaryInteractiveListNode[$p]['title']                          = $value['title'];
                                            $auxiliaryInteractiveListNode[$p]['auxiliary_delivery_schedule_id'] = $value['auxiliary_delivery_schedule_id'];
                                            $auxiliaryInteractiveListNode[$p]['auxiliary_project_list_id']      = $value['auxiliary_project_list_id'];
                                        }

                                        $ls['data'] = array_merge($auxiliaryInteractiveListNode);
                                    }
                                }
                                $capitalListArray[$k]['data'][] = $ls;
                            }
                        }

                    } else {
                        $capitalListArray[$k]['data'] = [];
                    }

                }
                foreach ($capital as $item) {
                    $item['specsList'] = $item['specs_list'];
                    unset($item['specs_list']);
                    if ($item['agency'] == 1) {
                        $agency[] = $item;
                    } else {
                        $sumlist[] = $item;
                    }
                    $result[$item['categoryName']][] = $item;

                }
                foreach ($result as $k => $list) {
                    $schemeTag['title'] = $k;
                    $schemeTag['data']  = $result[$k];
                    $tageList[]         = $schemeTag;
                }

                $envelopes['capital_list'] = $tageList;
                $envelopes['agencysum'] = $s = array_sum(array_column(array_unique($agency), 'allMoney'));
                $envelopes['sumlist']   = array_sum(array_column(array_unique($sumlist), 'allMoney'));
                r_date(['company' => $envelopes, 'warranty_collection' => $warranty_collection, 'capitalList' => $capitalListArray], 200);
            } else {
                r_date(null, 300, '暂未找到报价');
            }


        } else {
            r_date(null, 300, '暂未找到报价');
        }


    }

    public function QuestionName()
    {

        $data = static::param(['order_id']);
        $re   = db('order')->field('order.pro_id,go.title')
            ->join('goods_category go', 'order.pro_id=go.id', 'left')
            ->where('order.order_id', $data['order_id'])
            ->find();
        r_date($re, 200);
    }

    /*
     * 上门诊断
     *
     */
    public function diagnosticRecord(DiagnosticField $diagnosticField)
    {
        $data = static::param(['problemId']);
        $rows = $diagnosticField
            ->where('difference', $data['problemId']);
        if ($data['problemId'] == 3) {
            $rows->where('type', 18);
        }
        if ($data['problemId'] == 104) {
            $rows->where('type', 32);
        }
        if ($data['problemId'] == 42) {
            $rows->whereIn('type', [7, 8, 9, 10]);
        }
        $rows     = $rows->field('id,title,moreChoice,type,headline,log,click,sort')
            ->order('sort asc')
            ->select();
        $tageList = [];
        if ($rows) {
            $result = array();
            foreach ($rows as $k => $v) {
                if (!empty($v['log'])) {
                    $v['log'] = unserialize($v['log']);
                }

                $result[$v['headline']][] = $v;
            }

            foreach ($result as $k => $list) {
                $schemeTag['title']      = $k;
                $schemeTag['type']       = $list[0]['type'];
                $schemeTag['moreChoice'] = $list[0]['moreChoice'];
                $schemeTag['data']       = $result[$k];
                unset($rows['position']);
                $tageList[] = $schemeTag;
            }

        }


        r_date($tageList, 200);
    }

    public function subordinate(DiagnosticField $diagnosticField)
    {
        $data     = static::param(['problemId', 'id', 'childId']);
        $p        = 0;
        $tageList = [];
        $childId  = json_decode($data['childId'], true);
        if ($data['problemId'] == 42 && (count($childId) == 4 || count($childId) == 6)) {
            $rows = \db('diagnostic_record', config('database.zong'))->where(['goods_category_id' => $data['problemId']])->select();


            foreach ($childId as $item) {
                if ($item['type'] == 7 || $item['type'] == 8) {
                    $diagnosis[] = $item;
                }
                if ($item['type'] == 8 || $item['type'] == 11 || $item['type'] == 12) {
                    $diagnosisArray[] = $item;
                }

            }
            foreach ($rows as $l => $list) {
                $diagn               = [];
                $array               = array_column($diagnosis, 'id');
                $diagnostic_relation = \db('diagnostic_relation', config('database.zong'))->where(function ($quer) {
                    $quer->where('diagnostic_relation.type', 7)->whereOr('diagnostic_relation.type', 8);
                })->join('diagnostic_field', 'diagnostic_field.id=diagnostic_relation.position', 'left')->where(['diagnostic_record_id' => $list['id']])->field('diagnostic_field.title,diagnostic_relation.type,diagnostic_relation.result,diagnostic_relation.position,diagnostic_record_id')->order('diagnostic_field.sort asc')->select();
                $diagnostiId         = implode(',', array_unique(array_column($diagnostic_relation, 'position')));

                if ($diagnostiId == implode(',', $array)) {
                    if ($diagnostic_relation[0]['result'] == 0) {
                        $p = $list['id'];
                        continue;
                    } elseif ($diagnostic_relation[0]['result'] == 2) {

                        if (count(array_column($diagnosisArray, 'id')) == 3) {

                            $array1               = implode(',', array_column($diagnosisArray, 'id'));
                            $diagnostic_relation1 = \db('diagnostic_relation', config('database.zong'))->join('diagnostic_field', 'diagnostic_field.id=diagnostic_relation.position', 'left')->where(['diagnostic_record_id' => $list['id']])->where('diagnostic_relation.type', '<>', 7)->field('diagnostic_field.title,diagnostic_relation.type,diagnostic_relation.result,diagnostic_relation.position,diagnostic_record_id')->order('diagnostic_field.sort asc')->select();
                            $result               = array_merge(array_unique(array_column($diagnostic_relation1, 'result')));
                            $result               = $result[count($result) - 1];

                            foreach ($diagnostic_relation1 as $o) {
                                if ($o['type'] == 8 || $o['type'] == 11 || $o['type'] == 12) {
                                    $diagn[] = $o['position'];
                                }
                            }
                            $diagnostiId2 = implode(',', $diagn);

                            if ($diagnostiId2 == $array1) {
                                if ($result == 0) {
                                    $p = $list['id'];
                                    continue;
                                }
                            }
                        }


                    }
                }
            }
        }
        if ($data['problemId'] == 42 || $data['problemId'] == 3 || $data['problemId'] == 104) {
            $rows = $diagnosticField
                ->where('difference', $data['problemId']);
            if ($data['problemId'] == 3) {
                if ($data['id'] == 57) {
                    $rows->whereNotIn('type', [28, 29]);
                } else {
                    $rows->whereIn('type', [28, 29]);
                }
                $rows->where('type', '<>', 18);
            }
            if ($data['problemId'] == 104) {
                if ($data['id'] == 127) {
                    $rows->where('type', 30);
                } else {
                    $rows->where('type', 31);
                }
                $rows->where('type', '<>', 32);
            }

            if ($data['problemId'] == 42) {
                if (count($childId) > 3) {
                    if (count($childId) > 0 && count($childId) <= 5) {
                        if ($p != 0) {
                            $rows->whereIn('type', [7, 8, 9, 10]);
                        } else {
                            $rows->whereIn('type', [7, 8, 9, 10, 11, 12]);
                        }

                    } elseif (count($childId) >= 6) {
                        if ($p != 0) {
                            $rows->whereIn('type', [7, 8, 9, 10, 11, 12]);
                        } else {
                            $rows->whereIn('type', [7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17]);
                        }

                    }
                } else {
                    $rows->whereIn('type', [7, 8, 9, 10]);
                }
            }

            $rows = $rows->field('id,title,moreChoice,type,headline,log,click,sort')
                ->order('sort asc')
                ->select();
            if ($rows) {
                $result = array();
                foreach ($rows as $k => $v) {
                    if (!empty($v['log'])) {
                        $v['log'] = unserialize($v['log']);
                    }


                    $result[$v['headline']][] = $v;
                }

                foreach ($result as $k => $list) {
                    $schemeTag['title']      = $k;
                    $schemeTag['type']       = $list[0]['type'];
                    $schemeTag['moreChoice'] = $list[0]['moreChoice'];
                    $schemeTag['data']       = $result[$k];
                    unset($rows['position']);
                    $tageList[] = $schemeTag;
                }

            }

        }


        r_date($tageList, 200);
    }

    /*
     * 数据匹配
     */
    public function matching(schemeMaster $schemeMaster)
    {

        $data    = static::param(['childId', 'mainId', 'lat', 'lng', 'order_id']);

        $childId = json_decode($data['childId'], true);
        $row     = null;
        foreach ($childId as $item) {
            db('basic_selection', config('database.zong'))->insert(['position' => $item['id'], 'title' => $item['title'], 'type' => $item['type'], 'order_id' => $data['order_id'], 'cleared_time' => time(), 'goods_category_id' => $data['mainId']]);
        }
        r_date(null, 200);
        $diagnostic_record = \db('diagnostic_record', config('database.zong'))->where(['goods_category_id' => $data['mainId']])->select();
        if ($diagnostic_record) {
            $Present        = '';
            $diagnosis      = [];
            $diagnosisArray = [];
            foreach ($childId as $item) {
                if ($item['type'] == 5) {
                    $Present = $item['id'];
                }
                if ($item['type'] == 7 || $item['type'] == 8 || $item['type'] == 18 || $item['type'] == 19) {
                    $diagnosis[] = $item;
                }
                if ($item['type'] == 8 || $item['type'] == 11 || $item['type'] == 12) {
                    $diagnosisArray[] = $item;
                }
                if ($item['type'] == 13 || $item['type'] == 14 || $item['type'] == 15 || $item['type'] == 16 || $item['type'] == 17) {
                    $diagnosisArrayList[] = $item;
                }
            }

            $size = [];
            foreach ($diagnostic_record as $l => $list) {
                $diagn = [];
                if ($Present) {
                    $diagnostic_relation = \db('diagnostic_relation', config('database.zong'))->join('diagnostic_field', 'diagnostic_field.id=diagnostic_relation.position', 'left')->where(['diagnostic_relation.position' => $Present, 'diagnostic_relation.diagnostic_record_id' => $list['id']])->order('diagnostic_field.sort asc')->field('diagnostic_field.title')->find();
                    if ($diagnostic_relation) {
                        $diagnosticId = \db('diagnostic_relation', config('database.zong'))->join('diagnostic_field', 'diagnostic_field.id=diagnostic_relation.position', 'left')->where(['diagnostic_relation.diagnostic_record_id' => $list['id']])->order('diagnostic_field.sort asc')->field('diagnostic_field.title')->select();
                        $last_names   = array_column($childId, 'sort');
                        array_multisort($last_names, SORT_ASC, $childId);
                        $matches = implode(',', array_column($diagnosticId, 'title'));
                        $mat     = implode(',', array_column($childId, 'title'));

                        $size[$l]['similarity'] = similar_text($matches, $mat);
                        $size[$l]['id']         = $list['id'];
                    } else {
                        $size[$l]['similarity'] = 0;
                        $size[$l]['id']         = $list['id'];
                    }

                } else {

                    $array               = array_column($diagnosis, 'id');
                    $diagnostic_relation = \db('diagnostic_relation', config('database.zong'))->where(function ($quer) {
                        $quer->where('diagnostic_relation.type', 7)->whereOr('diagnostic_relation.type', 8)->whereOr('diagnostic_relation.type', 18)->whereOr('diagnostic_relation.type', 19);
                    })->join('diagnostic_field', 'diagnostic_field.id=diagnostic_relation.position', 'left')->where(['diagnostic_record_id' => $list['id']])->field('diagnostic_field.title,diagnostic_relation.type,diagnostic_relation.result,diagnostic_relation.position,diagnostic_record_id')->order('diagnostic_field.sort asc')->select();

                    $diagnostiId = implode(',', array_unique(array_column($diagnostic_relation, 'position')));

                    if ($diagnostiId == implode(',', $array)) {
                        $size[$l]['similarity'] = 4;
                        if ($diagnostic_relation[0]['result'] == 2) {
                            $array1               = implode(',', array_column($diagnosisArray, 'id'));
                            $diagnostic_relation1 = \db('diagnostic_relation', config('database.zong'))->join('diagnostic_field', 'diagnostic_field.id=diagnostic_relation.position', 'left')->where(['diagnostic_record_id' => $list['id']])->where('diagnostic_relation.type', '<>', 7)->field('diagnostic_field.title,diagnostic_relation.type,diagnostic_relation.result,diagnostic_relation.position,diagnostic_record_id')->order('diagnostic_field.sort asc')->select();
                            $result               = array_merge(array_unique(array_column($diagnostic_relation1, 'result')));
                            $result               = $result[count($result) - 1];

                            if ($result == 2) {

                                $array2               = implode(',', array_column($diagnosisArray, 'id'));
                                $diagnostic_relation2 = \db('diagnostic_relation', config('database.zong'))->join('diagnostic_field', 'diagnostic_field.id=diagnostic_relation.position', 'left')->where(['diagnostic_record_id' => $list['id']])->whereIn('diagnostic_relation.type', [13, 14, 15, 16, 17])->field('diagnostic_field.title,diagnostic_relation.type,diagnostic_relation.result,diagnostic_relation.position,diagnostic_record_id')->order('diagnostic_field.sort asc')->select();
                                if ($diagnostic_relation) {
                                    $diagnostiId1 = implode(',', array_unique(array_column($diagnostic_relation2, 'position')));
                                    if ($diagnostiId1 == $array2) {
                                        $size[$l]['similarity'] = 11;
                                    } else {
                                        $size[$l]['similarity'] = 0;
                                    }
                                } else {
                                    $size[$l]['similarity'] = 0;
                                }
                            } elseif ($diagnostic_relation1 && $result != 1) {


                                foreach ($diagnostic_relation1 as $o) {
                                    if ($o['type'] == 8 || $o['type'] == 11 || $o['type'] == 12) {
                                        $diagn[] = $o['position'];
                                    }
                                }

                                $diagnostiId2 = implode(',', $diagn);

                                if ($diagnostiId2 == $array1) {
                                    $size[$l]['similarity'] = 6;
                                } else {
                                    $size[$l]['similarity'] = 0;
                                }
                            } elseif ($result == 1) {

                                $size[$l]['id']         = 0;
                                $size[$l]['similarity'] = 10;
                                break;
                            }
                        } elseif ($diagnostic_relation[0]['result'] == 1) {

                            $size[$l]['id']         = 0;
                            $size[$l]['similarity'] = 10;
                            break;
                        }

                        $size[$l]['id'] = $diagnostic_relation[0]['diagnostic_record_id'];
                    }


                }


            }

            $last = array_column($size, 'similarity');
            array_multisort($last, SORT_DESC, $size);
            if (!empty($size)) {


                $plan_id           = \db('scheme_relation', config('database.zong'))->where(['diagnostic_record_id' => $size[0]['id']])->column('plan_id');
                $diagnostic_record = \db('diagnostic_record', config('database.zong'))->where(['id' => $size[0]['id']])->field('title,reminders')->find();

                $rows = $schemeMaster
                    ->with(['schemeProject', 'schemeProject.specsList'])
                    ->with('schemeImg')
                    ->with('schemeTag')
                    ->with('schemeCharacteristic')
                    ->whereIn('scheme_master.plan_id', $plan_id)
                    ->where('scheme_master.state', 1)
                    ->field('scheme_master.plan_id,scheme_master.name,scheme_master.info,scheme_master.day,scheme_master.reminde,scheme_master.pricing_rules,scheme_master.service_content,scheme_master.materials_used,scheme_master.service_duration,scheme_master.completion,scheme_master.quality')
                    ->select();

                if ($rows) {
                    foreach ($rows as $item) {
                        $tageList = [];
                        if ($item['scheme_tag']) {
                            $result = array();
                            foreach ($item['scheme_tag'] as $k => $v) {
                                $result[$v['pid']][] = $v;
                            }

                            foreach ($result as $k => $list) {
                                $schemeTag['id'] = $k;

                                $schemeTag['position'] = $result[$k];
                                unset($item['scheme_tag']);
                                $tageList[] = $schemeTag;
                            }
                        }

                        if ($item['scheme_project']) {
                            $title  = array_unique(array_column($item['scheme_project'], 'grouping'));
                            $result = array();
                            foreach ($title as $key => $info) {
                                foreach ($item['scheme_project'] as $it) {
                                    $it['specsList'] = $it['specs_list'];
                                    $it['allMoney']  = $it['to_price'];
                                    $it['types']     = 4;
                                    unset($it['specs_list']);
                                    if ($info == $it['grouping']) {
                                        $result[$key]['title']  = $info;
                                        $result[$key]['id']     = 0;
                                        $result[$key]['data'][] = $it;
                                    }

                                }
                            }

                        }

                        unset($item['scheme_project']);
                        $item['scheme_project'] = array_merge($result);


                    }

                    $array                  = $this->getAround($data['lng'], $data['lat'], 5000);
                    $condition['order.lng'] = array(array('EGT', $array['minLng']), array('ELT', $array['maxLng']), 'and');//(`longitude` >= minLng) AND (`longitude` <= maxLng)
                    $condition['order.lat'] = array(array('EGT', $array['minLat']), array('ELT', $array['maxLat']), 'and');//(`latitude` >= minLat) AND (`latitude` <=maxLat)

                    $row['nearby']    = db('order')
                        ->join('order_info', 'order_info.order_id=order.order_id', 'left')
                        ->field('order.order_id,concat(order_info.province,order_info.city,order_info.county,order.addres) as addres,if(order.logo=10,"https://yiniao.oss-cn-shenzhen.aliyuncs.com/img/1.png","https://yiniao.oss-cn-shenzhen.aliyuncs.com/img/1.png") as log')
                        ->where($condition)->where('order.pro_id', $data['mainId'])->limit(3)->select();
                    $row['data']      = $rows;
                    $row['title']     = $diagnostic_record['title'];
                    $row['reminders'] = $diagnostic_record['reminders'];

                }

            }

        }

        r_date($row, 200);

    }

    public function getAround($longitude, $latitude, $raidus)
    {
        $PI        = 3.14159265;
        $degree    = (24901 * 1609) / 360.0;
        $dpmLat    = 1 / $degree;
        $radiusLat = $dpmLat * $raidus;
        $minLat    = $latitude - $radiusLat;
        $maxLat    = $latitude + $radiusLat;
        $mpdLng    = $degree * cos($latitude * ($PI / 180));
        $dpmLng    = 1 / $mpdLng;
        $radiusLng = $dpmLng * $raidus;
        $minLng    = $longitude - $radiusLng;
        $maxLng    = $longitude + $radiusLng;
        return array('minLng' => $minLng, 'maxLng' => $maxLng, 'minLat' => $minLat, 'maxLat' => $maxLat);
    }

    public static function PosOffset($arr1, $i, $arr2)
    {
        $len2         = count($arr2);
        $arr2_reverse = array_reverse($arr2);
        for ($j = 0; $j < $len2; $j++) {
            $rev_num     = abs($i - $j) - 1;
            $rev_data    = isset($arr2_reverse[$rev_num]) ? $arr2_reverse[$rev_num] : '';
            $notrev_data = isset($arr2[$i - $j]) ? $arr2[$i - $j] : '';
            if ($i + $j >= 0 && $arr1[$i] == ($i - $j >= 0 ? $notrev_data : $rev_data)) {
                return $j;
            }
            if ($i + $j < $len2 && $arr1[$i] == $arr2[$i + $j]) {
                return $j;
            }
        }
        return $len2;
    }

    #计算匹配文字$arr1[$i]对于整体相似度的贡献量
    public static function CC($arr1, $i, $arr2)
    {
        $len2       = count($arr2);
        $len2_float = sprintf("%.2f", $len2);
        $temp       = self::PosOffset($arr1, $i, $arr2);
        $data       = ($len2 - $temp) / $len2_float;
        return $data;
    }

    #计算短语$arr1相对于短语$arr2的相似度sc
    public static function SC($arr1, $arr2)
    {
        $sc   = 0.0;
        $len1 = count($arr1);
        for ($i = 0; $i < $len1; $i++) {
            $sc += self::CC($arr1, $i, $arr2);
        }
        $sc /= $len1;
        return $sc;
    }

    #计算短语$arr1与短语$arr2之间的相似度
    public static function S($arr1, $arr2)
    {
        $temp1 = self::SC($arr1, $arr2);
        $temp2 = self::SC($arr2, $arr1);
        return ($temp1 + $temp2) / 2;
    }


}