<?php
/**
 * Created by muniao.
 * User: muniao
 * Date: 2018/8/2
 * Time: 13:20
 */

namespace app\api\controller\android\v10;


use app\api\model\PollingModel;
use Doctrine\ORM\Version;
use think\Config;
use think\Controller;

use app\api\model\Authority;
use app\api\model\Common;
use  app\api\model\OrderModel;
use think\Request;

class CostCalculation extends Controller
{
    protected $model;
    protected $us;
    protected $newTime;
    protected $endTime;
    protected $overtime;


    public function _initialize()
    {
        $this->model = new Authority();

        $this->us = Authority::check(1);

    }

    public function share(OrderModel $orderModel)
    {

        $data = Request::instance()->post();
        if (empty($data['id'])) {
            r_date(null, 300, '合同ID不能为空');
        }

        $op            = db('sign')->where(['sign.sign_id' => $data['id']])->join('order or', 'or.order_id=sign.order_id', 'left')->join('user', 'or.assignor=user.user_id', 'left')->field('sign.*,or.telephone,or.province_id,or.assignor,additional,additional_agent,user.store_id,or.company_id')->find();

        if (empty($op['company_id'])) {
            $company_id = db('company_config', config('database.zong'))->where('find_in_set(:id,store_list)', ['id' => $this->us['store_id']])->where('status', 1)->value('company_id');
            db('order')->where(['order_id' => $data['order_id']])->update(['company_id' => $company_id]);
        }
        $offer = $orderModel::offer($data['order_id']);

        if ($op['ding'] + $op['zhong'] + $op['wei'] > $offer['amount'] - $offer['agency']) {
            r_date(null, 300, '金额错误');
        }

        if(!empty($offer['mainContract'])){
            $pdf=contractPdf($data['order_id']);
            if($pdf['code']!=200){
                r_date(null, 300, '获取合同异常');
            }
            $signPath['contract_html_path']= $pdf['data']['html_url'];
            $signPath['contract_pdf_path']= $pdf['data']['url'];
            $signPath['contractpath']= 0;
            $u['type'] = !empty($op['contractpath']) ? 1 : 2;
            $u['url'] = $signPath['contract_pdf_path'];
            $do        = [
                'agency' => 0,
                'data' => [
                    $u,
                ],
            ];
        }



        if ($offer['agency']) {
            $agentPdf=agentContractPdf($data['order_id']);
            $img['url']          = $agentPdf['data']['url'];
            $img['type']         =   !empty($op['contractpath']) ? 1 : 2;
            if($pdf['code']!=200){
                r_date(null, 300, '获取合同异常');
            }
            $do['agency'] = 1;
            array_push($do['data'], $img);
            $signPath['agent_contract_pdf_path']= $agentPdf['data']['url'];
            $signPath['agent_contract_html_path']=$agentPdf['data']['html_url'];
            $signPath['agency_img']= 0;

        }
        $signPath['confirm'] = 1;
        if (!empty($offer['agency']) && !empty(($offer['amount'] - $offer['agency']))) {
            $do['state'] = 3;
        } elseif (!empty($offer['agency'])) {
            $do['state'] = 2;
        } elseif (!empty(($offer['amount'] - $offer['agency']))) {
            $do['state'] = 1;
        }
        db('sign')->where(['sign_id' => $data['id']])->update($signPath);
        r_date($do, 200);
    }

    /*
     * 代购合同
     */



    /*
    * 合同问题
    */
    public function ContractIssues()
    {
        $data = [
            [
                'title' => '墙面翻新',
                'type' => 0,
            ],
            [
                'title' => '室内渗漏水',
                'type' => 1,
            ],
            [
                'title' => '建筑渗漏水',
                'type' => 2,
            ],
            [
                'title' => '局部改造',
                'type' => 3,
            ],
            [
                'title' => '其它',
                'type' => 4,
            ],
        ];
        r_date($data, 200);
    }

}