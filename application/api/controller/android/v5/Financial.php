<?php
/**
 * Created by muniao.
 * User: muniao
 * Date: 2018/7/31
 * Time: 16:00
 */

namespace app\api\controller\android\v5;


use think\Cache;
use think\Controller;
use app\api\model\Authority;
use think\db;

use think\Request;
use  app\api\model\OrderModel;

class Financial extends Controller
{
    protected $model;
    protected $us;
    
    public function _initialize()
    {
        $this->model=new Authority();
        $this->us   =$this->model->check(1);
    }
    
    public function state()
    {
        $data=[
            [
                'type' =>1,
                'title'=>'签约时间',
            ],
            [
                'type' =>2,
                'title'=>'完工时间',
            ],
            [
                'type' =>3,
                'title'=>'完款时间',
            ],
            [
                'type' =>4,
                'title'=>'结算时间',
            ],
        ];
        r_date($data, 200);
    }
    
    /*
     * 全部店长
     */
    public function user()
    {
        $user=\db('user')->where('store_id',$this->us['store_id'])->where('status',0)->field('user_id,username')->select();
        r_date($user, 200);
    }
    
    /*
     * 店铺成交量
     * 签约时间，完工时间，完款时间，结算时间
     */
    public function ShopClose(OrderModel $orderModel)
    {
        $params=\request()->get();
        $start =strtotime(date('Y-m-01 00:00:00', strtotime($params['startTime'])));//获取指定月份的第一天
        $end   =strtotime(date('Y-m-t 23:59:59', strtotime($params['startTime']))); //获取指定月份的最后一天
        
        $m=$orderModel->table('order')
            ->alias('a')
            ->field('concat(p.province,c.city,u.county,a.addres) as  addres,a.contacts,co.con_time,a.order_id,a.state,go.title,a.assignor')
            ->join('province p', 'a.province_id=p.province_id', 'left')
            ->join('city c', 'a.city_id=c.city_id', 'left')
            ->join('county u', 'a.county_id=u.county_id', 'left')
            ->join('user us', 'a.assignor=us.user_id', 'left')
            ->join('goods_category go', 'a.pro_id=go.id', 'left')
            ->join('contract co', 'a.contract_id=co.contract_id', 'left');
        if (isset($params['user_id']) && $params['user_id'] != 0) {
            $m->where('a.assignor', $params['user_id']);
        }
        switch ($params['type']) {
            //签约时间
            case 1:
                $m->whereBetween('co.con_time', [$start, $end]);
                $m->order('co.con_time desc');
                break;
            //完工时间
            case 2:
                $m->whereBetween('a.finish_time', [$start, $end]);
                $m->order('a.finish_time desc');
                break;
            //完款时间
            case 3:
                $m->whereBetween('a.received_time', [$start, $end]);
                $m->order('a.received_time desc');
                break;
            //结算时间
            default :
                $m->whereBetween('a.cleared_time', [$start, $end]);
                $m->order('a.cleared_time desc');
                break;
        }
        $list=$m->where('us.store_id', $this->us['store_id'])->select();
        
        foreach ($list as $k=>$key) {
            //签约时间
            $list[$k]['con_time']=!empty($key['con_time'])? date('Y-m-d H:i:s', $key['con_time']) : '';
            $mouth               =$orderModel->offer($key['order_id']);
            //签约金额
            $list[$k]['amount']=sprintf('%.2f', $mouth['amount']);
            //代购主材
            $list[$k]['agency']=sprintf('%.2f', $mouth['agency']);
        }
        r_date($list, 200);
    }
    
    /*
     * 店铺财务
     * 预算/结算
     */
    public function ShopFinance(OrderModel $orderModel)
    {
       
        $params=\request()->get();
        $start =strtotime(date('Y-m-01 00:00:00', strtotime($params['startTime'])));//获取指定月份的第一天
        $end   =strtotime(date('Y-m-t 23:59:59', strtotime($params['startTime']))); //获取指定月份的最后一天
        
        $m=$orderModel->table('order')
            ->alias('a')
            ->field('concat(p.province,c.city,u.county,a.addres) as  addres,a.contacts,co.con_time,a.order_id,a.state,a.assignor,go.title')
            ->join('province p', 'a.province_id=p.province_id', 'left')
            ->join('city c', 'a.city_id=c.city_id', 'left')
            ->join('county u', 'a.county_id=u.county_id', 'left')
            ->join('user us', 'a.assignor=us.user_id', 'left')
            ->join('goods_category go', 'a.pro_id=go.id', 'left')
            ->join('contract co', 'a.contract_id=co.contract_id', 'left');
        if (isset($params['user_id']) && $params['user_id'] != 0) {
            $m->where('a.assignor', $params['user_id']);
        }
        switch ($params['type']) {
            //签约时间
            case 1:
                $m->whereBetween('co.con_time', [$start, $end]);
                $m->order('co.con_time desc');
                break;
            //完工时间
            case 2:
                $m->whereBetween('a.finish_time', [$start, $end]);
                $m->order('a.finish_time desc');
                break;
            //完款时间
            case 3:
                $m->whereBetween('a.received_time', [$start, $end]);
                $m->order('a.received_time desc');
                break;
            //结算时间
            default :
                $m->whereBetween('a.cleared_time', [$start, $end]);
                $m->order('a.cleared_time desc');
                break;
        }
        $list=$m->where('us.store_id', $this->us['store_id'])->select();
        foreach ($list as $k=>$key) {
            $data                =[];
            $list[$k]['con_time']=!empty($key['con_time'])? date('Y-m-d H:i:s', $key['con_time']) : '';
            $mouth               =$orderModel->offer($key['order_id']);
            //签约金额
            $list[$k]['amount']=sprintf('%.2f', $mouth['amount']);
            //代购主材
            $list[$k]['agency']=sprintf('%.2f', $mouth['agency']);
            $da['ification']   =2;
            $da['order_id']    =$key['order_id'];
            $data[]            =$da;
            //报销金额
            $list[$k]['agency_sum']=sprintf('%.2f', db('reimbursement')->where(['order_id'=>$key['order_id'], 'status'=>1])->sum('money'));
            if (isset($params['expected'])) {
                //人工预算
                $list[$k]['estimate']=sprintf('%.2f', db('capital')->where(['ordesr_id'=>$key['order_id'], 'types'=>1, 'enable'=>1,' agency'=>0])->sum('labor_cost'));
                //材料预算
                $list[$k]['MaterialBudget']=sprintf('%.2f', Financial::budget($key['order_id']));
            } else{
                $capital=$cap=db('capital')->where(['ordesr_id'=>$key['order_id'], 'types'=>1, 'enable'=>1])->field('class_b,capital_id')->column('capital_id');
                if (!empty($capital)) {
                    $sql="SELECT SUM(`personal_price`) AS sum FROM app_user_order_capital AS a, (SELECT b.`user_id`, b.`order_id`,b.`capital_id`,MAX(b.created_at) AS `created_at` FROM app_user_order_capital AS b GROUP BY b.`user_id`,b.`order_id`,b.`capital_id`)AS c WHERE a.`user_id`=c.`user_id` AND a.`order_id`=c.`order_id` AND a.`capital_id`=c.`capital_id` AND a.created_at = c.created_at And a.`deleted_at` IS NULL AND a.`capital_id` in(" . implode(',', $capital) . ")";
                    //原生sql
                    $reality_artificial=Db::connect(config('database.db2'))->table('app_user_order_capital');
                    $sum               =$reality_artificial->query($sql);
                    if (isset($sum[0]['sum'])) {
                        $order_for_reality_artificial=round($sum[0]['sum'], 2);
                    } else{
                        $order_for_reality_artificial=0;
                    }
                } else{
                    $order_for_reality_artificial=0;
                }
                //人工实际
                $list[$k]['estimate']=sprintf('%.2f', $order_for_reality_artificial);
                //材料实际
                $list[$k]['MaterialBudget']=sprintf('%.2f', db('material_usage')
                    ->whereIn('order_id', $key['order_id'])
                    ->where('status', 1)
                    ->sum('total_price'));
            }
            //利润
            $list[$k]['profit']=sprintf('%.2f', $list[$k]['amount'] - $list[$k]['estimate'] - $list[$k]['MaterialBudget'] - $list[$k]['agency'] - $list[$k]['agency_sum']);
            
        }
       
        r_date($list, 200);
        
    }
    
    
    /*
     * 店铺费用查看
     */
    public function shopExpenses($inside=1, $startTimes=0)
    {
        
        $params=\request()->get();
        if (isset($params['startTime'])) {
            $startTime=$params['startTime'];
        } else{
            $startTime=$startTimes;
        }
        
        $start =strtotime(date('Y-m-01 00:00:00', strtotime($startTime)));//获取指定月份的第一天
        $end   =strtotime(date('Y-m-t 23:59:59', strtotime($startTime))); //获取指定月份的最后一天
        $subsql=db('store_cost')
            ->where('store_id', $this->us['store_id'])
            ->where('created_time', '<=', $end)
            ->field('store_id,cost_type,max(created_time) as created_time')
            ->group('store_id,cost_type')
            ->buildSql();
        
        $data=db('store_cost')->alias('a')
            ->join([$subsql=>'b'], 'a.store_id=b.store_id and a.cost_type=b.cost_type and a.created_time=b.created_time')
            ->order('a.store_id,a.cost_type')
            ->field('a.cost_type,a.money')
            ->order('a.cost_type ASC')
            ->select();
        $date=[
            [
                'cost_type'=>12,
                'money'    =>'0.00',
            ],
            [
                'cost_type'=>13,
                
                'money'=>'0.00',
            ],
            [
                'cost_type'=>14,
                
                'money'=>'0.00',
            ],
        
        ];
        $d   =array_merge($data, $date);
        foreach ($d as $j=>$datum) {
            switch ($datum['cost_type']) {
                case 1:
                    $data[$j]['cost_name']='办公费';
                    break;
                case 2:
                    $data[$j]['cost_name']='基本工资';
                    break;
                case 3:
                    $data[$j]['cost_name']='运输费';
                    break;
                case 4:
                    $data[$j]['cost_name']='折旧费';
                    break;
                case 5:
                    $data[$j]['cost_name']='房租';
                    break;
                case 6:
                    $data[$j]['cost_name']='福利费';
                    break;
                case 7:
                    $data[$j]['cost_name']='水电物管费';
                    break;
                case 8:
                    $data[$j]['cost_name']='招待费';
                    break;
                case 9:
                    $data[$j]['cost_name']='维修费';
                    break;
                case 10:
                    $data[$j]['cost_name']='脚手架费用';
                    break;
                case 11:
                    $data[$j]['cost_name']='建渣清运费';
                    break;
                case 12:
                    $data[$j]['cost_name']='储备店长提成';
                    $op                   =Financial::Commission(1, 0, $start, $end);
                    $data[$j]['money']    =$op['count'];
                    break;
                case 13:
                    $data[$j]['cost_name']='订单费用';
                    $count                =db('order')
                        ->join('user', 'user.user_id=order.assignor', 'left')
                        ->where('user.store_id', $this->us['store_id'])
                        ->whereNull('order.notcost_time')
                        ->whereBetween('order.created_time', [$start, $end])
                        ->count();
                    $data[$j]['money']    =$count * 120;
                    break;
                default:
                    $data[$j]['cost_name']='师傅人工提成';
                    $order_id             =db('order')
                        ->join('user', 'user.user_id=order.assignor', 'left')
                        ->whereBetween('order.cleared_time', [$start, $end])
                        ->where('user.store_id', $this->us['store_id'])
                        ->column('order_id');
                    $capital              =db('capital')->whereIn('ordesr_id', $order_id)->where(['types'=>1, 'enable'=>1, 'agency'=>0])->column('capital_id');
                    if (!empty($capital)) {
                        $sql="SELECT SUM(`personal_price`) AS sum FROM app_user_order_capital AS a, (SELECT b.`user_id`, b.`order_id`,b.`capital_id`,MAX(b.created_at) AS `created_at` FROM app_user_order_capital AS b GROUP BY b.`user_id`,b.`order_id`,b.`capital_id`)AS c WHERE a.`user_id`=c.`user_id` AND a.`order_id`=c.`order_id` AND a.`capital_id`=c.`capital_id` AND a.created_at = c.created_at And a.`deleted_at` IS NULL AND a.`capital_id` in(" . implode(',', $capital) . ")";
                        //原生sql
                        $reality_artificial=Db::connect(config('database.db2'))->table('app_user_order_capital');
                        $sum               =$reality_artificial->query($sql);
                        if (isset($sum[0]['sum'])) {
                            $order_for_reality_artificial=sprintf('%.2f ',$sum[0]['sum']);
                        } else{
                            $order_for_reality_artificial=0;
                        }
                    } else{
                        $order_for_reality_artificial=0;
                    }
                    $data[$j]['money']=$order_for_reality_artificial;
                    break;
            }
            
        }
        if ($inside == 1) {
            r_date(['data'=>$data,'count'=>sprintf('%.2f ',array_sum(array_column($data, 'money')))], 200);
        } else{
            return array_sum(array_column($data, 'money'));
        }
        
    }
    
    /**
     * 提成
     */
    
    public static function Commission($store_id=0, $user_id=0, $start, $end)
    {
        $quer       =\db('order')
            ->join('user', 'order.assignor=user.user_id', 'left');
        $quer_agency=\db('order')
            ->join('user', 'order.assignor=user.user_id', 'left');
        if ($store_id != 0) {
            $quer->where('user.store_id', $store_id);
            $quer_agency->where('user.store_id', $store_id);
            
        } elseif ($user_id != 0){
            $quer->where('user.user_id', $user_id);
            $quer_agency->where('user.user_id', $user_id);
        }
        $user  =$quer->where('user.reserve', 2)->whereBetween('order.cleared_time', [$start, $end])->whereNotNull('order.received_time')->where('order.state', 7)->where('user.status', 0)->distinct(true)->column('order.order_id');
        $quer_a=$quer_agency->where('user.reserve', 2)->whereBetween('order.settlement_time', [$start, $end])->distinct(true)->column('order.order_id');
        
        //代购主材金额
        $agency=db('capital')->whereIn('ordesr_id', $quer_a)->where(['types'=>1, 'enable'=>1, 'agency'=>1])->sum('to_price');//代购主材;
        //代购报销
        $agency_sum=db('reimbursement')->whereIn('order_id', $quer_a)->where(['status'=>1, 'classification'=>4])->sum('money');
        $data      =[];
        foreach ($user as $key=>$item) {
            $amount                =OrderModel::offer($item, 2);
            $data[$key]['order_id']=$item;
            //完工完款金额除去主材
            $data[$key]['amount']=$amount['amount'] - $amount['agency'];
            //人工费
            $capital=db('capital')->whereIn('ordesr_id', $item)->where(['types'=>1, 'enable'=>1, 'agency'=>0])->field('class_b,capital_id')->column('capital_id');
            if (!empty($capital)) {
                $sql="SELECT SUM(`personal_price`) AS sum FROM app_user_order_capital AS a, (SELECT b.`user_id`, b.`order_id`,b.`capital_id`,MAX(b.created_at) AS `created_at` FROM app_user_order_capital AS b GROUP BY b.`user_id`,b.`order_id`,b.`capital_id`)AS c WHERE a.`user_id`=c.`user_id` AND a.`order_id`=c.`order_id` AND a.`capital_id`=c.`capital_id` AND a.created_at = c.created_at And a.`deleted_at` IS NULL AND a.`capital_id` in(" . implode(',', $capital) . ")";
                //原生sql
                $reality_artificial=Db::connect(config('database.db2'))->table('app_user_order_capital');
                $sum               =$reality_artificial->query($sql);
                if (isset($sum[0]['sum'])) {
                    $order_for_reality_artificial=round($sum[0]['sum'], 2);
                } else{
                    $order_for_reality_artificial=0;
                }
            } else{
                $order_for_reality_artificial=0;
            }
            $data[$key]['money']=$order_for_reality_artificial;
            //材料报销
            $data[$key]['material_usage']=sprintf('%.2f', db('material_usage')->where('status', 1)->where(['order_id'=>$item])->sum('total_price'));
            //报销费用
            $data[$key]['reimbursement']=db('reimbursement')->where(['order_id'=>$item, 'status'=>1, 'classification'=>['<>', 4]])->sum('money');;
            
        }
        
        
        $money['amounts']       =sprintf('%.2f', array_sum(array_column($data, 'amount')));
        $money['agency']        =sprintf('%.2f', $agency);
        $money['material_usage']=sprintf('%.2f', array_sum(array_column($data, 'material_usage')));
        $money['reimbursement'] =sprintf('%.2f', array_sum(array_column($data, 'reimbursement')));
        $money['agency_sum']    =sprintf('%.2f', $agency_sum);
        $money['money']         =sprintf('%.2f', array_sum(array_column($data, 'money')));
        $count                  =($money['amounts'] + $money['agency'] - $money['material_usage'] - $money['reimbursement'] - $money['agency_sum'] - $money['money']) * 0.02;
        
        return ['count'=>sprintf('%.2f', $count), 'date'=>$data, 'money'=>$money];
        
    }
    
    /*
     * 材料预算
     */
    public static function budget($order_id)
    {
        $material  =0;
        $labor_cost=db('capital')
            ->where('ordesr_id', $order_id)
            ->where('types', 1)
            ->where('enable', 1)
            ->where('agency', 0)
            ->field('projectId,square,material')
            ->select();
        foreach ($labor_cost as $k) {
            $material+=$k['material'];
        }
        return $material;
    }
    
    /*
     * 订单费用
     */
    public function OrderFee()
    {
        $params=\request()->get();
        $start =strtotime(date('Y-m-01 00:00:00', strtotime($params['startTime'])));//获取指定月份的第一天
        $end   =strtotime(date('Y-m-t 23:59:59', strtotime($params['startTime']))); //获取指定月份的最后一天
        //计入费用
        $IncludediIn=db('order')
            ->join('user', 'user.user_id=order.assignor', 'left')
            ->join('contract co', 'order.contract_id=co.contract_id', 'left')
            ->where('user.store_id', $this->us['store_id'])
            ->whereNull('order.notcost_time')
            ->whereBetween('order.created_time', [$start, $end])
            ->count();
        //不计入费用
        $IncludediNotIn=db('order')
            ->join('user', 'user.user_id=order.assignor', 'left')
            ->join('contract co', 'order.contract_id=co.contract_id', 'left')
            ->where('user.store_id', $this->us['store_id'])
            ->whereNotNull('order.notcost_time')
            ->whereBetween('order.created_time', [$start, $end])
            ->count();
        r_date(['IncludediIn'=>$IncludediIn . '个/￥' . $IncludediIn * 120, 'IncludediNotIn'=>$IncludediNotIn . '个/￥' . $IncludediNotIn * 120,'count'=>$IncludediIn * 120+$IncludediNotIn * 120], 200);
    }
    
    /*
     *订单费用列表
     */
    
    public function OrderFeeList(OrderModel $orderModel)
    {
        $params=\request()->get();
        
        $m=$orderModel->table('order')
            ->alias('a')
            ->field('concat(p.province,c.city,u.county,a.addres) as  addres,a.contacts,co.con_time,a.order_id,a.state,go.title,a.assignor')
            ->join('province p', 'a.province_id=p.province_id', 'left')
            ->join('city c', 'a.city_id=c.city_id', 'left')
            ->join('county u', 'a.county_id=u.county_id', 'left')
            ->join('user us', 'a.assignor=us.user_id', 'left')
            ->join('goods_category go', 'a.pro_id=go.id', 'left')
            ->join('contract co', 'a.contract_id=co.contract_id', 'left');
        switch ($params['type']) {
            //计入
            case 1:
                $m->whereNull('a.notcost_time');
                break;
            //不计入
            case 2:
                $m->whereNotNull('a.notcost_time');
                break;
        }
        if (isset($params['startTime']) && $params['startTime'] != '') {
            $start=strtotime(date('Y-m-01 00:00:00', strtotime($params['startTime'])));//获取指定月份的第一天
            $end  =strtotime(date('Y-m-t 23:59:59', strtotime($params['startTime']))); //获取指定月份的最后一天
            $m->whereBetween('a.created_time', [$start, $end]);
        }
        $list=$list=$m->where('us.store_id', $this->us['store_id'])->select();
        r_date(['count'=>count($list),'money'=>count($list)*120,'data'=>$list], 200);
        
    }
    
    /*
     * 师傅人工提成
     */
    public function ArtificialCommission(OrderModel $orderModel)
    {
        $params=\request()->get();
        $start =strtotime(date('Y-m-01 00:00:00', strtotime($params['startTime'])));//获取指定月份的第一天
        $end   =strtotime(date('Y-m-t 23:59:59', strtotime($params['startTime']))); //获取指定月份的最后一天
        $limit =isset($params['limit'])? $params['limit'] : 20;
        $m     =$orderModel->table('order')
            ->alias('a')
            ->field('concat(p.province,c.city,u.county,a.addres) as  addres,a.contacts,co.con_time,a.order_id,a.state,go.title,a.assignor')
            ->join('province p', 'a.province_id=p.province_id', 'left')
            ->join('city c', 'a.city_id=c.city_id', 'left')
            ->join('county u', 'a.county_id=u.county_id', 'left')
            ->join('user us', 'a.assignor=us.user_id', 'left')
            ->join('goods_category go', 'a.pro_id=go.id', 'left')
            ->join('contract co', 'a.order_id=co.orders_id', 'left');
        switch ($params['type']) {
            //签约时间
            case 1:
                $m->whereBetween('co.con_time', [$start, $end]);
                $m->order('co.con_time desc');
                break;
            //完工时间
            case 2:
                $m->whereBetween('a.finish_time', [$start, $end]);
                $m->order('a.finish_time desc');
                break;
            //完款时间
            case 3:
                $m->whereBetween('a.received_time', [$start, $end]);
                $m->order('a.received_time desc');
                break;
            //结算时间
            default :
                $m->whereBetween('a.cleared_time', [$start, $end]);
                $m->order('a.cleared_time desc');
                break;
        }
        if ($this->us['reserve'] == 2) {
            $m->where('a.assignor', $this->us['user_id']);
        } else{
            $m->where('us.store_id', $this->us['store_id']);
        }
        $list=$list=$m->select();
        $count=0;
        foreach ($list as $k=>$key) {
            $app_orders=[];
            //签约时间
            $list[$k]['con_time']=!empty($key['con_time'])? date('Y-m-d H:i:s', $key['con_time']) : '';
            
            $app_order=Db::connect(config('database.db2'))->table('app_user_order');
            if (isset($params['user_id']) && $params['user_id'] != 0) {
                $app_order->where('user_id', $params['user_id']);
            }
            $app_user_order=$app_order->where('order_id', $key['order_id'])->whereNull('deleted_at')->select();
            foreach ($app_user_order as $value=>$item) {
                $personal_price=Db::connect(config('database.db2'))->table('app_user_order_capital')->join('app_user', 'app_user.id=app_user_order_capital.user_id', 'left')->where('app_user_order_capital.order_id', $item['order_id'])->join('ynweixiu_2_0.capital','capital.capital_id=app_user_order_capital.capital_id','left')->where(['capital.types'=>1, 'capital.enable'=>1, 'capital.agency'=>0])->where('app_user_order_capital.user_id', $item['user_id'])->whereNull('app_user_order_capital.deleted_at')->field('sum(app_user_order_capital.personal_price) as personal_price,app_user.username')->select();
                if (!empty($personal_price[0]['username'])) {
                    $app_orders[$value]['money']   =sprintf('%.2f', array_sum(array_column($personal_price, 'personal_price')));
                    $app_orders[$value]['username']=!empty($personal_price[0]['username'])? $personal_price[0]['username'] : 0;
                }
            }
            if (empty($app_orders)) {
                unset($list[$k]);
            } else{
                $list[$k]['data']=array_values($app_orders);
                $count+=array_sum(array_column(array_values($app_orders), 'money'));
            }
            
            
        }
       
        r_date(['data'=>array_values($list),'count'=>sprintf('%.2f',$count)], 200);
        
    }
    
    /*
     * 储备店长提成
     */
    public function ReserveManager()
    {
      
        $params=\request()->get();
        $start =strtotime(date('Y-m-01 00:00:00', strtotime($params['startTime'])));//获取指定月份的第一天
        $end   =strtotime(date('Y-m-t 23:59:59', strtotime($params['startTime']))); //获取指定月份的最后一天
        if ($this->us['reserve'] == 2) {
            $data=Financial::Commission(0, $this->us['user_id'], $start, $end)['money'];
        } else{
            if (isset($params['user_id']) && $params['user_id'] != 0) {
                $data=Financial::Commission(0, $params['user_id'], $start, $end)['money'];
            } else{
                $data=Financial::Commission($this->us['store_id'], 0, $start, $end)['money'];
            }
            
        }
        $TotalCommission=sprintf('%.2f',$data['amounts']+$data['agency']-$data['material_usage']-$data['reimbursement']-$data['agency_sum']-$data['money']);
        if($TotalCommission<0){
            $AmountOfCommission=0;
        }else{
            $AmountOfCommission=sprintf('%.2f',$TotalCommission*0.02);
        }
        $TotalProfit=sprintf('%.2f',$TotalCommission-$AmountOfCommission);
        r_date(['datta'=>$data,'TotalCommission'=>$TotalCommission,'proportion'=>'2%','AmountOfCommission'=>$AmountOfCommission,'TotalProfit'=>$TotalProfit], 200);
       
    }
    
    /*
     * 店长分红
     */
    public function StoreManagerBonus(OrderModel $orderModel)
    {
        $params=\request()->get();
        $start =strtotime(date('Y-m-01 00:00:00', strtotime($params['startTime'])));//获取指定月份的第一天
        $end   =strtotime(date('Y-m-t 23:59:59', strtotime($params['startTime']))); //获取指定月份的最后一天
        $pa    =db('payment')->join('order', 'order.order_id=payment.orders_id', 'left')->join('user', 'order.assignor=user.user_id', 'left')->where('user.store_id', $this->us['store_id'])->whereBetween('payment.uptime', [$start, $end])->where('payment.weixin', '<>', 2)->field('sum(payment.material) as material,sum(payment.agency_material) as agency_material')->select();
       
        $pa1=db('payment')->join('order', 'order.order_id=payment.orders_id', 'left')->join('recharge', 'recharge.project=payment.payment_id', 'left')->join('user', 'order.assignor=user.user_id', 'left')->where('user.store_id', $this->us['store_id'])->whereBetween('recharge.last_login_time', [$start, $end])->where('recharge.status', 1)->where('payment.weixin', 2)->field('sum(payment.material) as material,sum(payment.agency_material) as agency_material')->select();
        //人工费用
//        $list          =$orderModel->where('assignor', $this->us['user_id'])->whereBetween('received_time', [$start, $end])->where('state', 7)->distinct(true)->column('order_id');
////        $app_user_order=Db::connect(config('database.db2'))->table('app_user_order')->whereIn('order_id', $list)->whereNull('deleted_at')->column('order_id');
//        $app_orders=Db::connect(config('database.db2'))->table('app_user_order_capital')->join('app_user', 'app_user.id=app_user_order_capital.user_id', 'left')->whereIn('app_user_order_capital.order_id',$list)->whereNull('app_user_order_capital.deleted_at')->sum('app_user_order_capital.personal_price');
        $da['material']            =$pa[0]['material'] + $pa1[0]['material'];
        $da['agency_material']     =$pa[0]['agency_material'] + $pa1[0]['agency_material'];
//        $da['money']               =$app_orders;
        $da['material_usage']      =sprintf('%.2f', db('material_usage')->join('user', 'material_usage.shopowner_id=user.user_id', 'left')->where('user.store_id', $this->us['store_id'])->where('material_usage.status', 1)->whereBetween('material_usage.adopt', [$start, $end])->sum('total_price'));
        $da['reimbursement']       =sprintf('%.2f', db('reimbursement')->join('user', 'reimbursement.shopowner_id=user.user_id', 'left')->where('user.store_id', $this->us['store_id'])->whereBetween('reimbursement.adopt', [$start, $end])->where(['reimbursement.status'=>1, 'reimbursement.classification'=>['<>', 4]])->sum('money'));
        $da['agency_reimbursement']=sprintf('%.2f', db('reimbursement')->join('user', 'reimbursement.shopowner_id=user.user_id', 'left')->where('user.store_id', $this->us['store_id'])->whereBetween('reimbursement.adopt', [$start, $end])->where(['reimbursement.status'=>1, 'reimbursement.classification'=>4])->sum('money'));
        $da['shopExpenses']        =sprintf('%.2f', $this->shopExpenses(2, $params['startTime']));
        $TotalCommission=sprintf('%.2f',$da['material']+$da['agency_material']-$da['material_usage']-$da['reimbursement']-$da['agency_reimbursement']-$da['shopExpenses']);
        if($TotalCommission<0){
            $AmountOfCommission=0;
        }else{
          
            $AmountOfCommission=sprintf('%.2f',$TotalCommission*0.02);
        }
        $TotalProfit=sprintf('%.2f',$TotalCommission-$AmountOfCommission)   ;
        r_date(['datta'=>$da,'TotalCommission'=>$TotalCommission,'proportion'=>'2%','AmountOfCommission'=>$AmountOfCommission,'TotalProfit'=>$TotalProfit], 200);
    }
}