<?php
/**
 * Created by muniao.
 * User: muniao
 * Date: 2018/8/2
 * Time: 13:20
 */

namespace app\api\controller\android\v4;


use think\Controller;

use app\api\model\Authority;
use app\api\model\Common;
use  app\api\model\OrderModel;
use think\Request;

class CostCalculation extends Controller
{
    protected $model;
    protected $us;
    protected $newTime;
    protected $endTime;
    protected $overtime;
    protected $pdf;

    public function _initialize()
    {
        $this->model = new Authority();

        $this->us = Authority::check(1);

    }

    public function share(OrderModel $orderModel)
    {


        $data = Request::instance()->post();
       if(empty($data['id'])){
           r_date(null, 300,'合同ID不能为空');
       }
        $op = db('sign')->where(['sign.sign_id' => $data['id']])->join('order or', 'or.order_id=sign.order_id', 'left')->join('goods_category go', 'or.pro_id=go.id', 'left')->field('sign.*,go.title,or.telephone')->find();

        $offer        = $orderModel::offer($data['order_id']);
        $imageDefault = [
            'left'    => 260,
            'top'     => 6360,
            'right'   => 0,
            'bottom'  => 0,
            'width'   => 100,
            'height'  => 100,
            'opacity' => 50,
        ];

        if ($op['title'] == '墙面翻新') {
            $p[] = [
                'text'      => '√',
                'left'      => 270,
                'top'       => 2264,
                'fontSize'  => 30,       //�ֺ�
                'fontColor' => '36, 36, 36, 1', //������ɫ
                'fontPath'  => ROOT_PATH . 'public/simsun.ttc', //������ɫ
                'angle'     => 0,
            ];
        } elseif ($op['title'] == '室内渗漏水') {
            $p[] = [
                'text'      => '√',
                'left'      => 450,
                'top'       => 2264,
                'fontSize'  => 30,       //�ֺ�
                'fontColor' => '36, 36, 36, 1', //������ɫ
                'fontPath'  => ROOT_PATH . 'public/simsun.ttc', //������ɫ
                'angle'     => 0,
            ];
        } elseif ($op['title'] == '建筑渗漏水') {
            $p[] = [
                'text'      => '√',
                'left'      => 657,
                'top'       => 2264,
                'fontSize'  => 30,       //�ֺ�
                'fontColor' => '36, 36, 36, 1', //������ɫ
                'fontPath'  => ROOT_PATH . 'public/simsun.ttc', //������ɫ
                'angle'     => 0,
            ];
        } elseif ($op['title'] == '局部改造') {
            $p[] = [
                'text'      => '√',
                'left'      => 850,
                'top'       => 2264,
                'fontSize'  => 30,       //�ֺ�
                'fontColor' => '36, 36, 36, 1', //������ɫ
                'fontPath'  => ROOT_PATH . 'public/simsun.ttc', //������ɫ
                'angle'     => 0,
            ];
        } else {

            $p[] = [
                'text'      => '√',
                'left'      => 1060,
                'top'       => 2264,
                'fontSize'  => 30,       //�ֺ�
                'fontColor' => '36, 36, 36, 1', //������ɫ
                'fontPath'  => ROOT_PATH . 'public/simsun.ttc', //������ɫ
                'angle'     => 0,
            ];
        }

        $textDefault = [
            [
                'text'      => $op['contract'],
                'left'      => 945,
                'top'       => 95,
                'fontSize'  => 20,       //�ֺ�
                'fontColor' => '36, 36, 36, 1', //������ɫ
                'fontPath'  => ROOT_PATH . 'public/simsun.ttc', //������ɫ
                'angle'     => 0,
            ],
            [
                'text'      => $this->us['username'],
                'left'      => 400,
                'top'       => 1600,
                'fontSize'  => 20,       //�ֺ�
                'fontColor' => '36, 36, 36, 1', //������ɫ
                'fontPath'  => ROOT_PATH . 'public/simsun.ttc', //������ɫ
                'angle'     => 0,
            ],
            [
                'text'      => !empty($op['con_time']) ? date('Y年m月d日H:i', $op['con_time']) : '',
                'left'      => 820,
                'top'       => 1600,
                'fontSize'  => 20,       //�ֺ�
                'fontColor' => '36, 36, 36, 1', //������ɫ
                'fontPath'  => ROOT_PATH . 'public/simsun.ttc', //������ɫ
                'angle'     => 0,
            ],
            [
                'text'      => $op['username'],
                'left'      => 290,
                'top'       => 1790,
                'fontSize'  => 20,       //�ֺ�
                'fontColor' => '36, 36, 36, 1', //������ɫ
                'fontPath'  => ROOT_PATH . 'public/simsun.ttc', //������ɫ
                'angle'     => 0,
            ],
            [
                'text'      => $op['idnumber'],
                'left'      => 280,
                'top'       => 1845,
                'fontSize'  => 20,       //�ֺ�
                'fontColor' => '36, 36, 36, 1', //������ɫ
                'fontPath'  => ROOT_PATH . 'public/simsun.ttc', //������ɫ
                'angle'     => 0,
            ],
            [
                'text'      => $op['addres'],
                'left'      => 267,
                'top'       => 2204,
                'fontSize'  => 20,       //�ֺ�
                'fontColor' => '36, 36, 36, 1', //������ɫ
                'fontPath'  => ROOT_PATH . 'public/simsun.ttc', //������ɫ
                'angle'     => 0,
            ],
            [
                'text'      => $offer['gong'],
                'left'      => 900,
                'top'       => 2555,
                'fontSize'  => 20,       //�ֺ�
                'fontColor' => '36, 36, 36, 1', //������ɫ
                'fontPath'  => ROOT_PATH . 'public/simsun.ttc', //������ɫ
                'angle'     => 0,
            ],
            [
                'text'      => !empty($op['addtime']) ? date('Y年m月d日H:i', $op['addtime']) : '',
                'left'      => 180,
                'top'       => 2550,
                'fontSize'  => 20,       //�ֺ�
                'fontColor' => '36, 36, 36, 1', //������ɫ
                'fontPath'  => ROOT_PATH . 'public/simsun.ttc', //������ɫ
                'angle'     => 0,
            ],
            [
                'text'      => !empty($op['uptime']) ? date('Y年m月d日H:i', $op['uptime']) : '',
                'left'      => 500,
                'top'       => 2550,
                'fontSize'  => 20,       //�ֺ�
                'fontColor' => '36, 36, 36, 1', //������ɫ
                'fontPath'  => ROOT_PATH . 'public/simsun.ttc', //������ɫ
                'angle'     => 0,
            ],

            [
                'text'      => $offer['amount'],
                'left'      => 380,
                'top'       => 3248,
                'fontSize'  => 20,       //�ֺ�
                'fontColor' => '36, 36, 36, 1', //������ɫ
                'fontPath'  => ROOT_PATH . 'public/simsun.ttc', //������ɫ
                'angle'     => 0,
            ],
            [
                'text'      => $op['telephone'],
                'left'      => 260,
                'top'       => 6515,
                'fontSize'  => 20,       //�ֺ�
                'fontColor' => '36, 36, 36, 1', //������ɫ
                'fontPath'  => ROOT_PATH . 'public/simsun.ttc', //������ɫ
                'angle'     => 0,
            ],
            [
                'text'      => !empty($op['con_time']) ? date('Y年m月d日H:i', $op['con_time']) : '',
                'left'      => 260,
                'top'       => 6590,
                'fontSize'  => 20,       //�ֺ�
                'fontColor' => '36, 36, 36, 1', //������ɫ
                'fontPath'  => ROOT_PATH . 'public/simsun.ttc', //������ɫ
                'angle'     => 0,
            ],
            [
                'text'      => $this->us['username'],
                'left'      => 780,
                'top'       => 6440,
                'fontSize'  => 20,       //�ֺ�
                'fontColor' => '36, 36, 36, 1', //������ɫ
                'fontPath'  => ROOT_PATH . 'public/simsun.ttc', //������ɫ
                'angle'     => 0,
            ],
            [
                'text'      => $this->us['mobile'],
                'left'      => 780,
                'top'       => 6515,
                'fontSize'  => 20,       //�ֺ�
                'fontColor' => '36, 36, 36, 1', //������ɫ
                'fontPath'  => ROOT_PATH . 'public/simsun.ttc', //������ɫ
                'angle'     => 0,
            ],
            [
                'text'      => !empty($op['con_time']) ? date('Y年m月d日H:i', $op['con_time']) : '',
                'left'      => 780,
                'top'       => 6590,
                'fontSize'  => 20,       //�ֺ�
                'fontColor' => '36, 36, 36, 1', //������ɫ
                'fontPath'  => ROOT_PATH . 'public/simsun.ttc', //������ɫ
                'angle'     => 0,
            ],

        ];
        $textDefault = array_merge($textDefault, $p);

        $background = ROOT_PATHS . '/contract/20200415120222.png';//背景
        $path       = parse_url($op['autograph'], PHP_URL_PATH);

        $config['image'][]['url'] = ROOT_PATHS . $path;
        $filename                 = ROOT_PATHS . '/ContractNumber/' . $data['id'] . '.jpg';
        Common::getbgqrcode($imageDefault, $textDefault, $background, $filename, $config);
        $u['url']  = request()->domain() . '/ContractNumber/' . $data['id'] . '.jpg';
        $u['type'] = !empty($op['contractpath']) ? 1 : 2;

        r_date($u, 200);
    }

}