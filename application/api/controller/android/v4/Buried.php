<?php
/**
 * Created by muniao.
 * User: muniao
 * Date: 2018/7/25
 * Time: 9:48
 */

namespace app\api\controller\android\v4;


use think\Controller;
use app\api\model\Authority;


class Buried extends Controller
{
    protected $model;
    protected $us;

    public function _initialize()
    {
        $this->model = new Authority();
        $this->us    = $this->model->check(1);
    }

    /*
      * 接单响应时长
      */
    public function contact()
    {
        $data = $this->model->post(['order_id']);
        $argv = db('response')->where('order_id', $data['order_id'])->find();
        if (!$argv) {
            $m = db('message')->where(['type' => 6, 'order_id' => $data['order_id'], 'user_id' => $this->us['user_id']])->field('time')->find();
            db('response')->insertGetId([
                'user_id' => $this->us['user_id'],
                'order_id' => $data['order_id'],
                'operating' => time(),
                'assign' => $m['time'],
            ]);
        }
        r_date(null,200);
    }
    /*
   * 上门及时性
   */
    public function Timeliness()
    {
        $data = $this->model->post(['startTime','order_id']);
        $argv = db('timeliness')->where('order_id', $data['order_id'])->find();
        $cap = db('order')->where(['order_id' => $data['order_id']])->field('planned')->find();
        if($argv){
            db('timeliness')->where('order_id', $data['order_id'])->update([
                'planned' => $cap['planned'],
                'startTime' => substr($data['startTime'],0,-3),
            ]);
        }else{
            db('timeliness')->insertGetId([
                'user_id' => $this->us['user_id'],
                'order_id' => $data['order_id'],
                'time' => time(),
                'planned' => $cap['planned'],
                'startTime' => substr($data['startTime'],0,-3),
            ]);
        }

        r_date(null,200);
    }


    /*
     * 上门距离统计
     */
    public function Home()
    {
        $data = $this->model->post(['lat','lng','address']);

        db('active')->insertGetId(['lat' => $data['lat'], 'lng' => $data['lng'], 'user_id' => $this->us['user_id'], 'time' => time(), 'address' => $data['address']]);
    }
    /*
    * app使用时间
     */
    public function UsageTime()
    {
        $data = $this->model->post(['id']);
        if(empty($data['id'])){
            $r=db('usagetime')->insertGetId([
                'user_id' => $this->us['user_id'],
                'startTime' => time(),
                'time'=>time()
            ]);
            r_date($r,200);
        }else{
            db('usagetime')->where(['user_id' => $this->us['user_id'],'id'=>$data['id']])->update(['endTime' => time()]);
            r_date(null,200);
        }


    }

}