<?php
/**
 * Created by muniao.
 * User: muniao
 * Date: 2018/7/25
 * Time: 9:48
 */

namespace app\api\controller\android\v4;


use app\api\model\Authority;
use app\index\model\Pdf;
use think\Controller;
use think\Exception;
use think\Request;
use foxyZeng\huyi\HuYiSMS;
use  app\api\model\OrderModel;
use app\api\Order_Validate;
use Think\Db;



class Test extends Controller
{
   public function index(){
         //使用方法
		$corpid = "wwafc9a61f8dd402b1";
		$corpsecret = "f_fskBOm_dz397xPv8HYKnfLMEoz0hvfwEeY13J-Rbc";
		$agentid = "3010040";
		$access_token = null; //从数据库中获取
		$expires_time = 0;//从数据库中获取
		$time=time()-7200;
		$where['data_time']=['>',$time];
		$token = db('enterprise_wechat')->where($where)->find();
		if($token){
			$access_token=$token['access_token'];
			$expires_time=$token['data_time'];
		}
		$wechatObj = new \work\workWechatCallbackApi($corpid, $corpsecret, $agentid, $access_token, $expires_time);
	     //获取全部部门
	     //print_r($wechatObj->get_department_list());die();
	     //获取部门成员
	     $departmentid=37;//来自上一个接口
	     //print_r(json_encode($wechatObj->get_department_userlist($departmentid)));die();
	     //获取模板详情
	     $template['template_id']='3TmACnEeaNnzBa2rNYCCRH75ZacUaDdfKw3VxnWy';//已有数据
	     print_r(json_encode($wechatObj->gettemplatedetail($template)));die();
		//获取审批模板（师傅完工结算申请）
//		print_r($wechatObj->gettemplatedetail(['template_id'=>'Bs7uczfFFNujAN7v9f9EX3MfDoM2WoFjdDRPtZ1hx']));die();
		//提交审批
       $image=file_get_contents('https://images.yiniao.co/uploads/shopowner/QQxRfS3UQR2HNHJss630qvHROphGSi4Fug7slfLW.jpeg');
//       $con=getimagesize($image);
       // 获取表单上传文件
       $file = request()->file('https://images.yiniao.co/uploads/shopowner/QQxRfS3UQR2HNHJss630qvHROphGSi4Fug7slfLW.jpeg');

       var_dump(strlen($file));die;
//
//       $im=$wechatObj->add_permanent_materials('image', ['filename'=>'https://images.yiniao.co/uploads/shopowner/QQxRfS3UQR2HNHJss630qvHROphGSi4Fug7slfLW.jpeg','filelength'=>6,'name'=>"media"]);
//		var_dump($im);die;
       $data=json_decode($wechatObj->Reimbursement(''),true);
//		print_r($wechatObj->Reimbursement());die();
		print_r(json_encode($wechatObj->applyevent($data)));die();
   	
   }

   public function getTemplateData(){
          $a=[
            "creator_userid"=>"ZiFeiYu",
            "template_id"=>"3TmACnEeaNnzBa2rNYCCRH75ZacUaDdfKw3VxnWy",
            "use_template_approver"=>0,
            "approver"=>[
                [
                    "attr"=>2,
                    "userid"=>["liying"]
                ],
                [
                    "attr"=>1,
                    "userid"=>["magic"]
                ]
            ],
            "notifyer"=>[ "ZouZouTingTing" ],
            "notify_type" => 1,
            "apply_data"=> [
                "contents"=>[
                    [
                    "control"=>"Text",
                        "id"=>"item-1493800492971",
                        "title"=> [
                            [
                                "text"=> "项目名称",
                                "lang"=> "zh_CN"
                            ]
                        ],
                        "value"=> [
                            "text"=> "测试一个材料审批"
                        ]
                    ],
                    [
                    "control"=>"Text",
                        "id"=>"Text-1584324084356",
                        "title"=> [
                            [
                                "text"=> "项目地址",
                                "lang"=> "zh_CN"
                            ]
                        ],
                        "value"=> [
                            "text"=> "项目地址金府路"
                        ]
                    ],
                    [
                    "control"=>"Number",
                        "id"=>"Number-1584324042596",
                        "title"=> [
                            [
                                "text"=> "项目id",
                                "lang"=> "zh_CN"
                            ]
                        ],
                        "value"=> [
                            "new_number"=> "9999"
                        ]
                    ],
                    [
                        "control"=>"Table",
                        "id"=>"item-1504669193743",
                        "title"=> [
                            [
                                "text"=> "采购明细",
                                "lang"=> "zh_CN"
                            ]
                        ],
                        "value"=>[
                            "children"=>[
                              [
                                "list"=>[
                                    [
                                        "control"=> "Text",
                                        "id"=>"item-1504669198282",
                                        "title"=>[
                                            [
                                                "text"=>"材料名称",
                                                "lang"=>"zh_CN",
                                            ]
                                        ],
                                        "value"=>[
                                            "text"=>"用户申报的材料名称",
                                        ]
                                    ],
                                    [
                                        "control"=> "Money",
                                        "id"=>"item-1504669229028",
                                        "title"=>[
                                            [
                                                "text"=>"材料单价",
                                                "lang"=>"zh_CN",
                                            ]
                                        ],
                                        "value"=>[
                                            "new_money"=>"100",
                                        ]
                                    ],
                                    [
                                        "control"=> "Selector",
                                        "id"=>"Selector-1584324613749",
                                        "title"=>[
                                            [
                                                "text"=>"材料单位",
                                                "lang"=>"zh_CN",
                                            ]
                                        ],
                                        "value"=>[
                                            "selector"=>[
                                                "type"=>"single",
                                                "options"=>[
                                                    [
                                                    "key"=>"option-1584324613749"
                                                    ]
                                                ]
                                            ],
                                        ]
                                    ],
                                    [
                                        "control"=> "Number",
                                        "id"=>"item-1504669227986",
                                        "title"=>[
                                            [
                                                "text"=>"采购数量",
                                                "lang"=>"zh_CN",
                                            ]
                                        ],
                                        "value"=>[
                                            "new_number"=>"10",
                                        ]
                                    ],
                                    [
                                        "control"=> "Textarea",
                                        "id"=>"item-1504669326318",
                                        "title"=>[
                                            [
                                                "text"=>"备注",
                                                "lang"=>"zh_CN",
                                            ]
                                        ],
                                        "value"=>[
                                            "text"=>"这里是备注信息",
                                        ]
                                    ],
                                ]
                              ]
                            ]
                        ]
                    ]
                ]
            ],
            "summary_list"=> [
                [
                    "summary_info"=> [[
                        "text"=> "摘要第1行",
                        "lang"=> "zh_CN"
                    ]]
                ],
                [
                    "summary_info"=> [[
                        "text"=> "摘要第2行",
                        "lang"=> "zh_CN"
                    ]]
                ],
                [
                    "summary_info"=> [[
                        "text"=> "摘要第3行",
                        "lang"=> "zh_CN"
                    ]]
                ]
            ]
        ];
   	   return json_encode($a);
   }

}
