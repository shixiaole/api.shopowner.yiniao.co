<?php

namespace app\api\controller\external\v1;



use shortLink\WxService;
use think\Controller;
use app\api\model\Common as Commons;
use think\Request;
use Think\Db;



/**
 * 用户端公共接口
 */
class Common extends Controller
{
   public function infrastructureRejection(Commons $Commons){
       
       $data            = Request::instance()->post(['orderId', 'type','relationId']);
       if(empty($data['orderId']) || empty($data['type'])||  empty($data['relationId'])){
           res_date(null,300,'参数不存在');
       }
       if($data['type']==1){
           db('capital_minus_beforehand')->where(['minus_id' => $data['relationId']])->where('delete_time', 0)->update(['delete_time' => time()]);
           $Commons->deleteRecalculatedCreditLimit($data['orderId'],$data['relationId']);
       }else{
           $capital       = db('capital')->where('uniqueNumber', $data['relationId'])->select();
           $capitalIdList = array_column($capital, 'capital_id');
           $approval_bef=db('approval_bef', config('database.zong'))->order('id desc')->where('approval_id',  $data['uniqueNumber'])->find();
           if(!empty($approval_bef['extended_info']) && $approval_bef['extended_info'] !=''){
               db('approval_record', config('database.zong'))->whereIn('id',  $approval_bef['extended_info'])->update(['status' =>2]);
           }
           db('capital_minus_beforehand')->whereIn('minus_id' , $capitalIdList)->where('delete_time', 0)->update(['delete_time' => time()]);
           $Commons->deleteRecalculatedCreditLimit($data['orderId'],0,0,3,$data['uniqueNumber']);
       }
      res_date(null,200);
   }
    /*
  * 分享
  */
    public function completionSharing(){
        $data            = Request::instance()->get(['orderId']);
        $list    = \db('order')->join('order_info', 'order.order_id=order_info.order_id', 'left')->where('order.order_id',$data['orderId'])->field('concat(order_info.city,order.addres) as addres,order.order_id as orderId,order.store_id,order.order_no')->find();
        $post_data = [
            'path' => 'pages/completion/completion',
            'query' => "order_id={$data['orderId']}",
            'env_version' => 'release',
            'expire_type' => 1,
            'expire_interval' => 179
        ];
        $WxService = new WxService();
        $url       = $WxService->getWxUrlLink($post_data,2);
        $url=explode("?",$url)[1];
        $work_check=Db::connect(config('database.zong'))->table('work_check')->where(['work_check.type' => 1, 'order_id' => $data['orderId'],'work_check.work_title_id' => 5])->where('delete_time',0)->find();
        $order_completion=Db::connect(config('database.zong'))->table('order_completion')->where('work_check_id',$work_check['id'])->where('inspection_type',0)->value('deadline_time');
        completionSharing($list['addres'],date('Y-m-d H:i:s',$order_completion),$list['order_no'],$url);
        res_date(null,200);
    }
    /*
* 分享
*/
    public function county(){
        $data            = Request::instance()->get(['cityName']);
        if ($data['cityName']) {
            $city_id = db('city')->where(['city' => ['like', "%{$data['cityName']}%"]])->value('city_id');
        } else {
            res_date(null, 300, '获取城市失败');
        }
        $county = db('county')->where(['city_id' => $city_id])->column('county');
        res_date($county, 200);
    }
    
}
