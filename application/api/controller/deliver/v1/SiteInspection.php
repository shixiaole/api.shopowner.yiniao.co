<?php
/**
 * Created by muniao.
 * User: muniao
 * Date: 2018/7/25
 * Time: 9:48
 */

namespace app\api\controller\deliver\v1;


use app\api\model\Aliyunoss;
use app\api\model\Authority;
use app\api\model\Capital;
use app\api\model\ConstructionNode;
use app\api\model\OrderDeliveryManagerModel;
use think\Controller;
use app\api\model\Common;
use app\api\model\OrderModel;
use think\Request;
use app\api\model\ccPush;
use redis\RedisPackage;
use think\Db;


class SiteInspection extends Controller
{
    protected $model;
    protected $us;
    
    
    public function _initialize()
    {
        $this->model = new Authority();
        $this->us    = Authority::check(1);
        
    }
    
    public function getList(OrderDeliveryManagerModel $deliveryManagerModel)
    {
        $data             = Authority::param(['orderId', 'type']);
        $IsStartupDelivere = $deliveryManagerModel->IsStartupDeliverer($data['orderId'], $this->us['user_id'], '');
        if (!empty($IsStartupDelivere)) {
            r_date(null, 300, '请接单后再操作');
        }
        $ConstructionNode = new ConstructionNode();
        $capital          = $ConstructionNode->List($data['orderId'], $data['type']);
        $fast_salary      = db('order_setting')->where('order_id', $data['orderId'])->value('fast_salary');
        $title            = "";
        if ($fast_salary == 2) {
            $title = "店长验收通过后，师傅不用等订单结算便可周结50-80%工资，请认真对待，虚假上传及审核，触碰红线， 10倍处罚(点击查看详情)";
        }
        r_date(['data' => $capital, 'title' => $title], 200);
    }
    
    
    public function info()
    {
        $data                = Authority::param(['id']);
        $list                = db('auxiliary_delivery_schedule')
            ->join('auxiliary_interactive', 'auxiliary_interactive.id=auxiliary_delivery_schedule.auxiliary_interactive_id', 'left')
            ->field('auxiliary_interactive.shot,auxiliary_interactive.acceptance_criteria,auxiliary_interactive.picture_url')
            ->where('auxiliary_delivery_schedule.id', $data['id'])
            ->find();
        $list['picture_url'] = empty($list['picture_url']) ? [] : unserialize($list['picture_url']);
        r_date($list, 200);
    }
    
    public function toExamine()
    {
        $data = Request::instance()->post();
        $list = db('app_user_order_capital')
            ->join('app_user', 'app_user.id=app_user_order_capital.user_id', 'left')
            ->where('app_user_order_capital.work_time', 0)
            ->where('app_user_order_capital.order_id', $data['orderId'])
            ->where('app_user.origin', "<>", 2)
            ->where('app_user.shortcut', 1)
            ->where('app_user_order_capital.capital_id', $data['capitalId'])
            ->whereNull('app_user_order_capital.deleted_at')->find();
        if (!empty($list)) {
            r_date(null, 300, "有师傅没标记工时，无法验收通过，如果这个师傅无需施工，请取消派单，或督促师傅标记工时");
        }
       
        $nodeIds          = json_decode($data['nodeIds'], true);
        $nodeIds          = implode($nodeIds, ',');
        $ConstructionNode = new ConstructionNode();
        $capitalNode      = $ConstructionNode->toExamine($nodeIds, $data['state'], $data['reason'], $data['orderId'],$this->us['user_id']);
        r_date(null, $capitalNode['code'], $capitalNode['msg']);
    }
    
    /*
     * 巡检确认
     */
    public function inspectionConfirmation()
    {
        $request = Authority::param(['capitalId']);
        $list    = db('app_user_order_capital')
            ->join('capital', 'app_user_order_capital.capital_id=capital.capital_id', 'left')
            ->join('app_user', 'app_user_order_capital.user_id=app_user.id', 'left')
            ->whereNull('app_user_order_capital.deleted_at')
            ->where('capital.capital_id', $request['capitalId'])
            ->field("capital.class_b,capital.labor_cost,capital.ordesr_id,
        CONCAT('[', GROUP_CONCAT(JSON_OBJECT('id',capital.capital_id,'name',app_user.username,'personalPrice',app_user_order_capital.personal_price,'workTime',app_user_order_capital.work_time) SEPARATOR ','), ']') as  content")
            ->group('capital.capital_id')
            ->select();
        if(empty($list)){
            r_date(null, 301, "该清单没有派师傅，请添加");
        }
        foreach ($list as $k => $item) {
            $name                  = json_decode($item['content'], true);
            $list[$k]['workTime']  = array_sum(array_column($name, 'workTime'));
            $list[$k]['laborCost'] = array_sum(array_column($name, 'personalPrice'));
            $list[$k]['content']   = $name;
        }
        $fast_salary = \db('order_setting')->where('order_id', $list[0]['ordesr_id'])->value('fast_salary');
        if ($fast_salary == 2) {
            $listArray               = empty($list) ? null : $list[0];
            $app_user_order_capital = \db('app_user_order_capital')
                ->field('app_user_order_capital.capital_id,app_user_order_capital.created_at')
                ->order('app_user_order_capital.created_at ASC')
                ->group('capital_id')
                ->buildSql();
            $auxiliary_delivery_node = db('auxiliary_delivery_node')
                ->field('auxiliary_delivery_node.upload_time,app_user_order_capital.created_at')
                ->join('auxiliary_delivery_schedule', 'auxiliary_delivery_schedule.id=auxiliary_delivery_node.auxiliary_delivery_schedul_id', 'left')
                ->join('auxiliary_project_list', 'auxiliary_project_list.id=auxiliary_delivery_schedule.auxiliary_project_list_id', 'left')
                ->join([$app_user_order_capital=>'app_user_order_capital'], 'app_user_order_capital.capital_id=auxiliary_project_list.capital_id', 'left')
                ->where('auxiliary_project_list.capital_id', $request['capitalId'])
                ->whereNull('auxiliary_project_list.delete_time')
                ->whereNull('auxiliary_delivery_schedule.delete_time')
                ->where('auxiliary_delivery_node.state', 0)
                ->where('auxiliary_delivery_node.type', 1)
                ->order('auxiliary_delivery_node.upload_time desc')
                ->find();
            // 计算时间差（以秒为单位）
            $timeDifference = abs($auxiliary_delivery_node['upload_time'] - $auxiliary_delivery_node['created_at']);
            // 将时间差转换为小时数
            $hours = floor($timeDifference / (60 * 60));
            // 判断是否在24小时内
            $listArray['approvalPrompt'] = "";
            if (!empty($auxiliary_delivery_node) && $hours <= 24) {
                $listArray['approvalPrompt'] = "此清单派单后不足24小时便完工，请确认是否真实完成";
            }
            r_date($listArray, 200);
        }
        r_date(null, 200);
    }
    
    public function addNode(Common $common)
    {
        $data = Request::instance()->post();
        db()->startTrans();
        Db::connect(config('database.db2'))->startTrans();
        try {
            $params   = request()->only(['order_id', 'title', 'describe', 'lng', 'lat', 'address', 'id']);
            $resource = json_decode($data['resource'], true);
            $startup  = db('startup')->join('order', 'order.order_id=startup.orders_id', 'left')->where('orders_id', $params['order_id'])->find();
            //派单师傅
            $relation     = \db('app_user')
                ->field('app_user.username,app_user.origin')
                ->whereIn('id', $startup['xiu_id'])
                ->select();
            $cooperation  = [];
            $cooperation1 = [];
            foreach ($relation as $item) {
                if ($item['origin'] == 2) {
                    $cooperation[] = $item;
                } else {
                    $cooperation1[] = $item;
                }
            }
            if (!empty($cooperation) && empty($cooperation1) && empty($startup['start_time'])) {
                db('order')->where('order_id', $params['order_id'])->whereNull('start_time')->update(['start_time' => time()]);
            }
            if (!empty($resource)) {
                $listNode=$this->addResourceIds($resource);
                $params['no_watermark_image_ids'] = implode(',', $listNode['no_watermark_image']);
                $params['resource_ids'] = implode(',', $listNode['resource_ids']);
            }
            $params['describe']    = '';
            $params['user_id']     = $this->us['user_id'];
            $params['type']        = 2;
            $params['status']      = 1;
            $params['distinguish'] = 1;
            $params['created_at']  = time();
            $params['updated_at']  = time();
            $orderNode             = Db::connect(config('database.db2'))->table('app_user_order_node')->insertGetId($params);
            \db('auxiliary_delivery_node')->insert(['auxiliary_delivery_schedul_id' => $data['nodeChildId'], 'state' => 0, 'node_id' => $orderNode, 'mast_id' => $this->us['user_id'], 'type' => 2, 'upload_time' => time()]);
            $common->MultimediaResources($params['order_id'],$this->us['user_id'],$resource,9);
            db()->commit();
            Db::connect(config('database.db2'))->commit();
            r_date(null, 200);
            
        } catch (\Exception $e) {
            Db::connect(config('database.db2'))->rollBack();
            db()->rollBack();
            r_date(null, 300, $e->getMessage());
        }
    }
    
    /**
     * 添加资源返回ID
     * @param $resources
     * @return array
     */
    public function addResourceIds($resources)
    {
        
        $resource_ids = [];
        $resource_old_ids = [];
        $aLi=new  Aliyunoss();
        foreach ($resources as $item) {
            if (!isset($item['mime_type']) || !isset($item['file_path'])) {
                r_date(null, 300, '资源格式错误');
            }
            $info           = Db::connect(config('database.db2'))->table('common_resource')->where('path', $item['file_path'])->find();
            $resource_ids[] = Db::connect(config('database.db2'))->table('common_resource')->insertGetId(['mime_type' => $item['mime_type'], 'path' =>$item['file_path'], 'size' => $item['size'] ?? 0, 'created_at' => time()]);
           
            if(isset($item['oldImg']) && $item['oldImg'] !=''){
                $path=$item['file_path'];
                $filename=pathinfo($path)['dirname'].'/'.pathinfo($path)['filename'].'_OLD.'.pathinfo($path)['extension'];
                $parts = parse_url($item['oldImg']);
                $l=$aLi->copyObject(ltrim($parts['path'],'/'),$filename);
                if ($l['info']['http_code'] != 200) {
                    r_date(null, 300, '图片上传错误');
                }
                $resource_old_ids[] = Db::connect(config('database.db2'))->table('common_resource')->insertGetId(['mime_type' => $item['mime_type'], 'path' => $filename, 'size' => $item['size'] ?? 0, 'created_at' => time()]);
                
            } else{
                $resource_old_ids[] = Db::connect(config('database.db2'))->table('common_resource')->insertGetId(['mime_type' => $item['mime_type'], 'path' => $item['file_path'], 'size' => $item['size'] ?? 0, 'created_at' => time()]);
            }
        }
        return ['resource_ids'=>$resource_ids,'no_watermark_image'=>$resource_old_ids];
    }
    
    public function del()
    {
        $data                    = Request::instance()->post();
        $app_user_order_node     = Db::connect(config('database.db2'))->table('app_user_order_node')->where('find_in_set(:id,resource_ids)', ['id' => $data['nodeId']])->find();
        $auxiliary_delivery_node = db('auxiliary_delivery_node')->where('auxiliary_delivery_schedul_id', $data['id'])->where('node_id', $app_user_order_node['id'])->find();
        $path = db('common_resource')->where('id', $data['nodeId'])->value('path');
        $filename=pathinfo($path)['dirname'].'/'.pathinfo($path)['filename'].'_OLD.'.pathinfo($path)['extension'];
        $pathsId = db('common_resource')->where('path', $filename)->value('id');
        $resource_ids = explode(',', $app_user_order_node['resource_ids']);
        $no_watermark_image_ids = explode(',', $app_user_order_node['no_watermark_image_ids']);
        foreach ($resource_ids as $k => $o) {
            if ($o == $data['nodeId']) {
                unset($resource_ids[$k]);
            }
        }
        if(!empty($no_watermark_image_ids)){
            foreach ($no_watermark_image_ids as $k => $o) {
                if ($o == $pathsId) {
                    unset($no_watermark_image_ids[$k]);
                }
            }
        }
        if (count($resource_ids) == 0) {
            db('auxiliary_delivery_node')->where('auxiliary_delivery_schedul_id', $data['id'])->where('node_id', $app_user_order_node['id'])->delete();
            Db::connect(config('database.db2'))->table('app_user_order_node')->where('id', $auxiliary_delivery_node['node_id'])->delete();
        } else {
            Db::connect(config('database.db2'))->table('app_user_order_node')->where('id', $auxiliary_delivery_node['node_id'])->update(['resource_ids' => implode($resource_ids, ','),'no_watermark_image_ids' => implode($no_watermark_image_ids, ',')]);
        }
        r_date(null, 200);
    }
    /*
     * a.大于X订单金额的工地，只能拍摄或者上传益鸟应用内水印相机的照片
     * b.小于X订单金额的工地，可以使用手机相册、益鸟水印相册、益鸟水印相机拍摄
     */
    public function orderAmount(){
        $data  = Authority::param(['orderId','lat','lng']);
        $total_price=Db::connect(config('database.db2'))->table('order_aggregate')->where('order_id', $data['orderId'])->value('total_price');
        $acceptance_watermark_switch=Db::connect(config('database.db2'))->table('order_setting')->where('order_id', $data['orderId'])->value('acceptance_watermark_switch');
//        $order=Db::connect(config('database.db2'))->table('order')->where('order_id', $data['orderId'])->find();
//        $completion_acceptance_range=Db::connect(config('database.db2'))->table('order_setting')->where('order_id', $data['orderId'])->value('completion_acceptance_range');
//        $Difference = getDistance($order['lat'], $order['lng'], $data['lat'], $data['lng']);
//        if($Difference>1000  && $completion_acceptance_range==1){
//            r_date(null, 300,'打卡位置超过工地范围1公里,不允许上传');
//        }
        $set=1;
        if($acceptance_watermark_switch==1){
            if($total_price>10000){
                $set=0;
            }else{
                $set=1;
            }
        }
        
        r_date($set, 200);
    }
}