<?php
/**
 * Created by muniao.
 * User: muniao
 * Date: 2018/5/31
 * Time: 15:20
 */

namespace app\api\controller\deliver\v1;


use think\Controller;
use think\Request;
use app\api\model\AliPay;
use think\Db;
use app\api\model\Authority;
use think\Exception;

class ConstructionPhotos extends Controller
{
    protected $us;
    
    public function _initialize()
    {
        $this->us = Authority::check(1);
        
    }
    
    public function index()
    {
        $data                    = Authority::param(['orderId']);
        $description             = \db('user_config', config('database.zong'))->where('key', 'construction_description')->value('value');
        $description             = json_decode($description, true);
        $constructionLocation    = \db('construction_location', config('database.zong'))->field('id,title')->order('sort asc')->select();
        $constructionExample     = \db('construction_example', config('database.zong'))->order('sort asc')->select();
        $constructionUploadOrder = \db('construction_upload_order', config('database.zong'))->field('id,username as userName,picture as imgPath,create_time as time,construction_example_id,around')->whereNull('deleted_at')->where('order_id', $data['orderId'])->select();
        foreach ($constructionExample as $k => $list) {
            $constructionExample[$k]['startImg'] = null;
            $constructionExample[$k]['endImg']   = null;
            if (!empty($constructionUploadOrder)) {
                foreach ($constructionUploadOrder as $item) {
                    if ($list['id'] == $item['construction_example_id']) {
                        if ($item['around'] == 1) {
                            $constructionExample[$k]['startImg'][] = $item;
                        }
                        if ($item['around'] == 2) {
                            $constructionExample[$k]['endImg'][] = $item;
                        }
                    }
                    
                }
            }
        }
        foreach ($constructionLocation as $k => $value) {
            foreach ($constructionExample as $item) {
                $item['picture'] = json_decode($item['picture']);
                if ($value['id'] == $item['construction_location_id']) {
                    unset($item['sort'], $item['construction_location_id']);
                    $constructionLocation[$k]['content'][] = $item;
                }
            }
        }
        $description['content'] = $constructionLocation;
        r_date($description, 200);
        
    }
    
    public function constructionInfo()
    {
        $data                            = Authority::param(['orderId', 'id']);
        $constructionExample             = \db('construction_example', config('database.zong'))->field('id,picture')->where('id', $data['id'])->find();
        $constructionUploadOrder         = \db('construction_upload_order', config('database.zong'))->where('construction_example_id', $data['id'])->whereNull('deleted_at')->field('id,username as userName,picture as imgPath,FROM_UNIXTIME(create_time,"%Y-%m-%d") as time,around,type as types,user_id')->where('order_id', $data['orderId'])->select();
        $constructionExample['picture']  = json_decode($constructionExample['picture'], true);
        $constructionExample['startImg'] = null;
        $constructionExample['endImg']   = null;
        foreach ($constructionUploadOrder as $item) {
            $item['type']=2;
            if($item['types']==1 && ($item['user_id'] == $this->us['user_id'])){
                $item['type']=1;
            }
            if ($item['around'] == 1) {
                $constructionExample['startImg'][] = $item;
            }
            if ($item['around'] == 2) {
                $constructionExample['endImg'][] = $item;
            }
            
        }
        r_date($constructionExample, 200);
    }
    
    
    public function constructionUpload()
    {
        $data = Authority::param(['orderId', 'img', 'around','id']);
        $img  = json_decode($data['img'], true);
        $listArray=[];
        foreach ($img as $k => $l) {
            $listArray[$k]['picture']     = $l;
            $listArray[$k]['order_id']    = $data['orderId'];
            $listArray[$k]['construction_example_id']    = $data['id'];
            $listArray[$k]['user_id']     = $this->us['user_id'];
            $listArray[$k]['username']    = $this->us['username'];
            $listArray[$k]['create_time'] = time();
            $listArray[$k]['type']        = 1;
            $listArray[$k]['around']      = $data['around'];
            
        }
        \db('order_times')->where('order_id', $data['orderId'])->update(['construction_upload_last_time'=>time()]);
        \db('construction_upload_order', config('database.zong'))->insertAll($listArray);
        r_date(null, 200);
    }
    
    public function constructionDel()
    {
        $data = Authority::param(['id']);
        if (empty($data['id'])) {
            r_date(null, 300, "参数错误");
        }
        $construction_upload_order=\db('construction_upload_order', config('database.zong'))->where('id', $data['id'])->find();
        if($construction_upload_order['user_id'] != $this->us['user_id'] ||  $construction_upload_order['type']==2){
            r_date(null, 300, "无法删除他人上传的图片");
        }
        \db('construction_upload_order', config('database.zong'))->where('id', $data['id'])->update(['deleted_at' => time()]);
        r_date(null, 200);
    }
    
}