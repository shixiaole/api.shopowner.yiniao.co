<?php
/**
 * Created by muniao.
 * User: muniao
 * Date: 2018/5/31
 * Time: 15:20
 */

namespace app\api\controller\deliver\v1;



use think\Controller;
use app\api\model\Authority;
use think\Db;


class CouponUser extends Controller
{
    protected $us;
    
    public function _initialize()
    {
        $this->us = Authority::check(1);
        
    }
    /*
     * 优惠券详情
     */
    
    public function index()
    {
        
        $data        = Authority::param(['id']);
        $coupon_user = Db::connect(config('database.zong'))->table('coupon_user')
            ->join('coupon', 'coupon.id=coupon_user.coupon_id', 'left')
            ->where('coupon_user.id', $data['id'])
            ->field('coupon_user.id,coupon.coupon_prize_quantity as quantity,coupon.coupon_prize_title as title,coupon.coupon_prize_unit as unit,FROM_UNIXTIME(IF (coupon.coupon_expire_type = 1, coupon.coupon_expire_time_end, coupon_user.create_time + (coupon.coupon_expire_days * 86400)),"%Y-%m-%d") as couponExpireTime,FROM_UNIXTIME(coupon_user.create_time,"%Y-%m-%d ") as createTime,coupon_user.mobile as mobile')
            ->group('coupon_user.id')
            ->find();
        
        
        
        r_date($coupon_user, 200);
    }
    /*
     * 优惠券核销
     */
    
    public function WriteOff()
    {
        
        $data        = Authority::param(['id']);
        $coupon_user = Db::connect(config('database.zong'))->table('coupon_user')->where('id', $data['id'])->find();
        if($coupon_user['usage_status']==2){
            r_date(null, 300,'该优惠卷已经核销');
        }
        Db::connect(config('database.zong'))->table('coupon_user')->where('id', $data['id'])->update(['usage_status'=>2,'usage_time'=>time(),'usage_type'=>2,'usage_user_type'=>2,'usage_user'=>$this->us['user_id']]);
        
        r_date(null, 200);
    }
    
    /*
     * 优惠券核销历史记录
     */
    
    public function history()
    {
        
        $coupon_user = Db::connect(config('database.zong'))->table('coupon_user')
            ->join('coupon', 'coupon.id=coupon_user.coupon_id', 'left')
            ->where('coupon_user.usage_user', $this->us['user_id'])
            ->where('coupon_user.usage_user_type',2)
            ->field('coupon_user.id,concat(coupon.coupon_prize_title,coupon.coupon_prize_quantity,coupon.coupon_prize_unit) as title,FROM_UNIXTIME(coupon_user.usage_time,"%Y-%m-%d ") as createTime,coupon_user.mobile as mobile')
            ->select();
        
        r_date($coupon_user, 200);
    }
    
    
    
}