<?php
/**
 * Created by PhpStorm.
 * User: Raytine
 * Date: 2019/11/29
 * Time: 15:23
 */

namespace app\api\controller\deliver\v1;
use app\api\model\OrderDeliveryManagerModel;
use app\api\model\OrderModel;
use app\api\model\Envelopes;
use think\Controller;
use think\Request;
use app\api\model\Authority;
use think\Db;
use think\Route;

class Warranty extends Controller
{
    protected $us;
    
    public function _initialize()
    {
        $this->us=Authority::check(1);
    }
    
    public function get_list()
    {
        $data    =\request()->get();
        $warranty=\db('warranty')->where('parents_id', 0);
        if (isset($data['category']) && $data['category'] != '') {
            $warranty->where('category', 2)->where('user_id', $this->us['user_id']);
        } else{
            $warranty->where('category', 1);
        }
        $date=$warranty->where('pro_types', 0)->field('id,title,term')->select();
        r_date($date, 200);
    }
    
    public function detail()
    {
        $date=\request()->get();
        $data=\db('warranty')->where('id', $date['id'])->field('title,term,service_contents,id,category')->find();
        
        $warranty                =db('warranty')->where('parents_id', $data['id'])->order('id desc')->select();
        $data['service_contents']=$warranty[0]['service_contents'];
        $data['data']            =$warranty;
        
        r_date($data, 200);
    }
    
    /*
     * 质保编辑回显
     */
    public function QualityAssuranceEditor()
    {
        $date=\request()->get();
        $data=\db('warranty_collection');
        
        if (isset($date['envelopes_id']) && $date['envelopes_id'] != 0) {
            $data->where('warranty_collection.envelopes_id', $date['envelopes_id']);
        }
        if($date['isHome']=='true'){
            $envelopes=db('envelopes')->where('ordesr_id', $date['order_id'])->where('type',1)->find();
            $data->where('warranty_collection.envelopes_id', $envelopes['envelopes_id']);
        }
       
        
        $warranty_collection=$data->where('warranty_collection.pro_types', 0)->join('warranty', 'warranty.id=warranty_collection.warranty_id', 'left')->where('warranty_collection.order_id', $date['order_id'])->field('warranty.title,warranty_collection.years,warranty_collection.type,warranty_collection.warranty_id,warranty_collection.id')->select();
        
        r_date($warranty_collection, 200);
    }
    
    /*
     * 质保编辑提交
     */
    public function QualityAssuranceCardSubmission()
    {
        $date=\request()->post();
        
        $Warranty=json_decode($date['quality_list'], true);
      
        foreach ($Warranty as $value) {
            $warranty_collection=\db('warranty_collection')->where(['order_id'=>$date['order_id']])->column('collection_time');
            if(empty($warranty_collection[0])){
              
                if (!empty($value['id'])) {
                    if (!$value['isSelected']) {
                        $da=['pro_types'=>1];
                    } else{
                        $da=['warranty_id'=>$value['warranty_id'], 'years'=>$value['years']];
                    }
                    
                    \db('warranty_collection')->where(['id'=>$value['id']])->update($da);
                } else{
                    if(empty($date['envelopes_id'])){
                        $envelopes=db('envelopes')->where('ordesr_id', $date['order_id'])->where('type',1)->find();
                        $type     =empty($envelopes['type'])? 0 : 1;
                        $date['envelopes_id']=$envelopes['envelopes_id'];
                        $date['through_id']=$envelopes['through_id'];
                    }else{
                        $envelopes=db('envelopes')->where('envelopes_id', $date['envelopes_id'])->find();
                        $type     =empty($envelopes['type'])? 0 : 1;
                    }
                    \db('warranty_collection')->insertGetId(['order_id'=>$date['order_id'], 'warranty_id'=>$value['warranty_id'], 'years'=>$value['years'], 'creation_time'=>time(), 'type'=>$type, 'through_id'=>!empty($date['through_id'])? $date['through_id'] : '', 'envelopes_id'=>$date['envelopes_id']]);
                }
            }else{
                r_date([], 300,'客户已领用,保存失败');
            }
            
            
        }
        r_date([], 200);
    }
    
    /*
     * 自定义质保
     */
    public function CustomWarranty()
    {
        $date=\request()->post();
        
        $id=\db('warranty')->insertGetId([
            'title'     =>$date['title1'],
            'parents_id'=>0,
            'category'  =>2,
            'term'      =>$date['term'],
            'user_id'   =>$this->us['user_id'],
        
        ]);
        \db('warranty')->insertGetId([
            'title'           =>$date['title'],
            'parents_id'      =>$id,
            'scesn'           =>$date['scesn'],
            'term'            =>$date['term'],
            'service_contents'=>$date['service_contents'],
            'category'        =>2,
            'user_id'         =>$this->us['user_id'],
        ]);
        
        r_date(['id'=>$id, 'title'=>$date['title1'], 'term'=>$date['term']], 200);
    }
    
    /*
     * 自定义质保
     */
    public function CustomWarrantyDel()
    {
        $date=\request()->get();
        
        \db('warranty')->where(['id'=>$date['id'], 'user_id'=>$this->us['user_id']])->update(['pro_types'=>1]);
        \db('warranty')->where(['parents_id'=>$date['id'], 'user_id'=>$this->us['user_id']])->update(['pro_types'=>1]);
        
        r_date('', 200);
    }
     /*
     * 新质保卡
     */
    public function newWarranty(Envelopes $envelopes,OrderDeliveryManagerModel $deliveryManagerModel)
    {
        $data             = Authority::param(['orderId']);
        $IsStartupDelivere=$deliveryManagerModel->IsStartupDeliverer($data['orderId'],$this->us['user_id'],'');
        if(!empty($IsStartupDelivere)){
            r_date(null, 300, '请接单后再操作');
        }
        $envelopesFind        = $envelopes->with(['CapitalList', 'CapitalList.specsList', 'OrderList'])
            ->where(['envelopes.ordesr_id' => $data['orderId'],'envelopes.type'=>1])
            ->find();
        $capital_list=$envelopesFind['capital_list'];
        foreach ($capital_list as $k => $value) {
            $value['isWarranty'] =0;
            $warranty=null;
            if ($value['capitalWarrantyYears'] >0 || $value['warranty_years'] != '0.00') {
                $warranty = ['warrantYears' => $value['warranty_years'], 'warrantyText1' => $value['warranty_text_1'], 'exoneration' => $value['warranty_text_2'],];
                if($value['zhi']==1){
                    $value['isWarranty'] = 1;
                    $warranty['warrantYears']=$value['capitalWarrantyYears'];
                }
                
            } else {
                unset($capital_list[$k]);
                
            }
            $value['warranty']=$warranty;
            $value['schemeIds']            = isset($value['schemeIds']) ? $value['schemeIds'] : 0;
            $value['artificialTag'] = 0;
            if (!empty($value['sectionTitle'])) {
                $value['artificialTag'] = 1;
            }
    
    
            if ($value['isMerge'] == 1) {
                $value['isMerge']      = true;
                $value['projectMoney'] = $value['mergePrice'];
                $value['mergePrice']   = $value['un_Price'];
        
            } else {
                $value['isMerge'] = false;
            }
        }
        array_merge($capital_list);
        $result   = array();
        $tageList = [];
       
        foreach ($capital_list as $k => $v) {
            $result[$v['categoryName']][] = $v;
        }
        foreach ($result as $k => $list) {
            $schemeTag['title'] = $k;
            $schemeTag['data']  = $result[$k];
            $tageList[]         = $schemeTag;
        }
        r_date($tageList, 200);
        
    }
    
}