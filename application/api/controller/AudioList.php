<?php

namespace app\api\controller;

use app\api\model\Aliyunoss;
use app\api\model\ccPush;
use app\api\model\TelephoneNotification;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use think\Log;


class AudioList
{
    public function consumer()
    {
        
        $exchangeName = 'delay_topic'; //交换机名
        $queueName    = 'template_audio'; //队列名称
        $routingKey   = 'template.Audio'; //路由关键字(也可以省略)
        
        $conn    = new AMQPStreamConnection( //建立生产者与mq之间的连接
            ccPush::HOST, ccPush::PORT, ccPush::LOGIN, ccPush::PASSWORD, ccPush::VHOST
        );
        $channel = $conn->channel(); //在已连接基础上建立生产者与mq之间的通道
        $channel->exchange_declare($exchangeName, 'x-delayed-message', false, true, false); //声明初始化交换机
        $channel->queue_declare($queueName, false, true, false, false); //声明初始化一条队列
        $channel->queue_bind($queueName, $exchangeName, $routingKey); //将队列与某个交换机进行绑定，并使用路由关键字
//1:queue 要取得消息的队列名
//2:consumer_tag 消费者标签
//3:no_local false这个功能属于AMQP的标准,但是rabbitMQ并没有做实现.参考
//4:no_ack  false收到消息后,是否不需要回复确认即被认为被消费
//5:exclusive false排他消费者,即这个队列只能由一个消费者消费.适用于任务不允许进行并发处理的情况下.比如系统对接
//6:nowait  false不返回执行结果,但是如果排他开启的话,则必须需要等待结果的,如果两个一起开就会报错
//7:callback  null回调函数
//8:ticket  null
//9:arguments null
        $callBack = function ($msg) use (&$rData) {
            $this->ossAudio($msg->body);
        };
        $channel->basic_consume($queueName, '', false, true, false, false, $callBack);
//监听消息
        while (count($channel->callbacks)) {
            $channel->wait();
        }
        
        
    }
    
    private function ossAudio($id)
    {
        $order_audio_list = db('order_audio_list', config('database.zong'))->where('id', $id)->find();
        $url              = $order_audio_list['url'];
        $path             = ROOT_PATHS . '/uploads/';
        $ch               = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
        $file = curl_exec($ch);
        curl_close($ch);
        $filename = pathinfo($url, PATHINFO_BASENAME);
        $resource = fopen($path . $filename, 'a');
        fwrite($resource, $file);
        fclose($resource);
        $path       = $path . $filename;
        $ali        = new Aliyunoss();
        $oss_result = $ali->upload('audio/' . date('Ymd') . '/' . $id.'.mp3', $path);
        if ($oss_result['info']['http_code'] == 200) {
            $paths = parse_url($oss_result['info']['url'])['path'];
            if (file_exists($path)) {
                unlink($path);
            };
            $telephoneNotification = new TelephoneNotification();
            $GetSecretAsr          = $telephoneNotification->GetSecretAsrDetailRequest(['callId' => $order_audio_list['requestId'], 'callTime' => date('Y-m-d H:i:s', $order_audio_list['call_time'])]);
            $oss_url               = 'https://images.yiniao.co' . $paths;
            $order_audio_list      = \db('order_audio_list', config('database.zong'))->where('id', $id)->update(['oss_url' => $oss_url, 'content' =>!empty($GetSecretAsr)?json_encode($GetSecretAsr):'']);
            Log::write(json_encode(['APP' => '店长端电话处理', 'log' => "成功" . $id]),true);
        }
        Log::write(json_encode(['APP' => '店长端电话处理', 'log' => "失败" . $id]),true);
    }
    
}

