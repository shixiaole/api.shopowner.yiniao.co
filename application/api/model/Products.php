<?php

namespace app\api\model;

use constant\config;
use think\Model;

class Products extends Model
{
    protected $table = 'products';
    protected $connection = 'database.zong';
    protected $createTime = 'create_time';

    public function productsTag()
    {
        
        return $this->hasMany('products_tag', 'products_id','id')->field('products_tag,id,products_id');
    }

    public function productsSpec()
    {
        return $this->hasMany('products_spec', 'products_id','id')->field('round(products_spec_price/100,2) as price,products_id,products_spec_title,products_spec_description as productsDescription,scheme_id');
    }
    public function productsUnit()
    {
        return $this->hasMany('Unit', 'id','products_unit')->field('title,id');
    }
    
    public function productsPosition()
    {
        return $this->hasMany('products_position', 'products_id','id');
    }
    
    public function productsFamily()
    {
        return $this->hasMany('products_family', 'id','products_family_id');
    }
    public function productsImg()
    {
        return $this->hasMany('products_images', 'products_id','id');
    }
    
   

   


}
