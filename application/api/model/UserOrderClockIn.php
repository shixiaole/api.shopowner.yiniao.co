<?php

namespace app\api\model;


use think\Model;

class UserOrderClockIn extends Model
{
    protected $table = 'user_order_clock_in';
    protected $autoWriteTimestamp = true;
    protected $createTime = 'created_at';            //数据添加的时候，create_time 这个字段不自动写入时间戳
    protected $updateTime = false;
    public static $snakeAttributes = false;
    
    
    /**
     * 打卡类型
     */
    const TYPE_CLOCK_IN = 11; //开工打卡
    const TYPE_CLOCK_IN_REFRESH = 12; //开工补卡
    const TYPE_CLOCK_OUT = 21; //完工打卡
    const TYPE_CLOCK_OUT_REFRESH = 22; //完工补卡
    const TYPE_CLOCK_OUT_STORE = 23; //店铺打卡
    const TYPE_CLOCK_OUT_WORK = 24; //下班打卡
    const common_images_url = "https://images.yiniao.co"; //下班打卡
    
    /**
     * 状态
     */
    const STATUS_ON = 1; //有效
    const STATUS_OFF = 0; //无效
    
    /**
     * 审批人类型
     */
    const APPROVAL_TYPE = 1; //店长
    
    /**
     * 审批状态
     */
    const APPROVAL_TYPE_UNREAD = 0; //未读
    const APPROVAL_TYPE_PASS = 1; //通过
    const APPROVAL_TYPE_WAIT = 2; //待审核
    const APPROVAL_TYPE_REFUSE = 3; //拒绝
    
    
    /**
     * 添加验证
     * @param $request
     * @return bool
     */
    public function addValidate($request)
    {
        $rules = [
            'resource' => ['json'],
            'lng' => ['required', 'numeric'],
            'lat' => ['required', 'numeric'],
            'address' => ['required', 'max:200'],
        ];
        $messages = [
            'type.integer' => '类型必须是整数',
            'resource.array' => '资源数据必须是一个数组',
            'lng.required' => '经度不能为空',
            'lng.numeric' => '经度必须为数字',
            'lat.required' => '纬度不能为空',
            'lat.numeric' => '纬度必须为数字',
            'address.required' => '地址不能为空',
            'address.max' => '地址超过限制',
        ];
        $validator = Validator::make($request, $rules, $messages);
        if ($validator->fails()) {
            return array_values($validator->messages()->toArray())[0][0];
        }
        return true;
    }
    /**
     * 关联资源
     * @return mixed
     */
    public function resource($resource_ids)
    {
        $resource_ids = explode(',',$resource_ids);
        $resources = db('common_resource')->whereIn('id', $resource_ids)->select();
        $data=[];
        foreach ($resources as $list){
            $data[]=[
                'mime_type'=>$list['mime_type'],
                'path'=>self::common_images_url. '/'.$list['path'],
                'size'=>$list['size'],
            ];
        }
        return $data;
 
      
    }
    
    /**
     * 获取所有签退信息
     * @param $user_id
     * @param $order_id
     * @return mixed
     */
    public function clockEndAll($user_id, $order_id)
    {
        $day = dayBetween();
        $query = $this->whereIn('type', [$this::TYPE_CLOCK_OUT, $this::TYPE_CLOCK_OUT_REFRESH]);
        $query->where('user_id', $user_id);
        $query->where('order_id', $order_id);
        $query->whereBetween('clock_in_at', [$day['first'], $day['last']]);
        $query->where('status', $this::STATUS_ON);
        $query->orderBy('clock_in_at', 'desc');
        $result = $query->get();
        return $result;
    }
    
    
}
