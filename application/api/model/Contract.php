<?php

namespace app\api\model;

use constant\config;
use think\Model;

class Contract extends Model
{
    protected $table = 'contract';
    
    public function queryOne($orderId)
    {
        return $this->where('orders_id', $orderId)->find();
    }
    
    public function addQueryOne($data)
    {
        return $this->insertGetId($data);
    }
    
    
}
