<?php
/**
 * Created by PhpStorm.
 * User: Raytine
 * Date: 2019/10/10
 * Time: 10:58
 */

namespace app\api\model;


use think\Model;
use redis\RedisPackage;

class PollingModel extends Model
{


    public function Alls($order_id, $type, $manage_time, $management, $clock = 0)
    {

        if ($type == 0) {
            db('crm_cc_task', config('database.zong'))->insertGetId([
                'order_id' => $order_id,
                'type' => $type,
                'status' => 0,
                'category' => 1,
                'manage_time' => $management,
                'management' => $management,
            ]);
        }
        if ($type == 2) {
            db('crm_cc_task', config('database.zong'))->where('order_id', $order_id)->where('type', 1)->where('status', 0)->update(['writetime' => $management, 'admin_id' => 0, 'status' => 1]);
            $time=1*24*60*60;
//            $time   = 120;
            $Client = new \app\api\model\KafkaProducerDelay();
            $Client->add(json_encode([
                'id' => $order_id,
                'type' => $type,
                'time' => strtotime(date('Y-m-d H:i:s', $management + 1 * 24 * 60 * 60)),
                'clock' => $clock
            ]));
        }
        if ($type == 3) {
            $redis = new RedisPackage();
            if (!$redis::get($order_id)) {
                $time=7*24*60*60;
//                $time   = 180;
                $Client = new \app\api\model\KafkaProducerDelay();
                $Client->add(json_encode([
                    'id' => $order_id,
                    'type' => $type,
                    'time' => strtotime(date('Y-m-d H:i:s', $management + 7 * 24 * 60 * 60))
                ]));
                $redis::set($order_id, $order_id, 7200);
            }

        }

        if ($type == 4) {
            db('crm_cc_task', config('database.zong'))->where('order_id', $order_id)->where('type', 3)->where('status', 0)->update(['writetime' => $management, 'admin_id' => 0, 'status' => 1]);
            $time   = 1;//指定时间戳+1天 2017-01-10 21:10:16
            $Client = new \app\api\model\KafkaProducerDelay();
            $Client->add(json_encode([
                'id' => $order_id,
                'type' => $type,
                'time' => strtotime(date('Y-m-d H:i:s', $management))
            ]));
        }


    }

    public function automatic($order_id, $manage_time, $type = 1)
    {
        if ($type == 1) {
            db('crm_cc_task', config('database.zong'))->where('order_id', $order_id)->where(function ($quer){
                $quer->where('type', 2)->whereOr('type',1);
            })->where('status', 0)->update(['writetime' => $manage_time, 'admin_id' => 0, 'status' => 1]);
        } else {
            db('crm_cc_task', config('database.zong'))->where('order_id', $order_id)->where('type', 1)->where('status', 0)->update(['writetime' => $manage_time, 'admin_id' => 0, 'status' => 1]);
        }

    }

    public function file_exists_S3($url, $save_to)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
        $file = curl_exec($ch);
        curl_close($ch);
        $filename = pathinfo($url, PATHINFO_BASENAME);
        $resource = fopen($save_to . $filename, 'a');
        fwrite($resource, $file);
        fclose($resource);
        return $filename;

    }


}

