<?php
/**
 * Created by muniao.
 * User: muniao
 * Date: 2018/8/2
 * Time: 13:20
 */

namespace app\api\model;


use constant\config;
use think\Model;
use  work\workWechatCallbackApi;


class Reimbursement extends Model
{


    protected $table = 'reimbursement';

    public function List($user_id, $data = null, $page, $limit, $distinct = 0)
    {
        $start = date('Y-m-01 00:00:00', $data);
        $end = date('Y-m-d 23:59:59', strtotime("$start +1 month -1 day"));

        $quer = $this->where(['type' => 1, 'user_id' => $user_id]);
        if (!empty($data)) {
            $quer ->whereBetween('cleared_time',[strtotime($start),strtotime($end)]);
        }

        $quer->field('count(`order_id`) as  count,FROM_UNIXTIME(cleared_time,"%Y-%m-%d %H:%i:%s") as  cleared_time,sum(`money`) as  money,order_id')->group('cleared_time');

        return $quer->page($page, $limit)->select();

    }

    public function ListInfo($user_id,$startTime)
    {

        $quer = $this->where(['reimbursement.type' => 1, 'reimbursement.user_id' => $user_id])
            ->Join('reimbursement_relation','reimbursement_relation.reimbursement_id=reimbursement.id','left')
            ->Join('bank_card','reimbursement_relation.bank_id=bank_card.bank_id','left')
            ->Join('agency','reimbursement_relation.bank_id=agency.agency_id','left')
            ->where('cleared_time',strtotime($startTime));

        $quer->field('reimbursement.reimbursement_name,FROM_UNIXTIME(reimbursement.created_time,"%Y-%m-%d %H:%i:%s") as created_time,FROM_UNIXTIME(reimbursement.cleared_time,"%Y-%m-%d %H:%i:%s") as cleared_time,FROM_UNIXTIME(reimbursement.adopt,"%Y-%m-%d %H:%i:%s") as adopt,reimbursement.voucher,reimbursement.classification,if(reimbursement.classification=4,reimbursement_relation.money,reimbursement.money) as money,if(reimbursement.classification=4,agency.agency_name,bank_card.account_name) as account_name,reimbursement.secondary_classification');

        return $quer->select();

    }
    
    public function materialsInfo($capitalId)
    {
        $quer = db('reimbursement_relation')
            ->Join('reimbursement','reimbursement_relation.reimbursement_id=reimbursement.id','left')
            ->where('reimbursement_relation.capital_id',$capitalId)
            ->where('reimbursement.classification', 4)
            ->field('reimbursement_relation.bank_id,reimbursement.order_id');
        return $quer->order('relation_id desc')->find();

    }
    public function materialsSelect($orderId,$bank_id,$capitalId)
    {
        $quer = db('reimbursement_relation')
            ->Join('reimbursement','reimbursement_relation.reimbursement_id=reimbursement.id','left')
            ->where('reimbursement_relation.bank_id',$bank_id)
            ->where('reimbursement.order_id',$orderId)
            ->where('reimbursement_relation.capital_id','<>',$capitalId)
            ->where('reimbursement.classification', 4)
            ->field('reimbursement_relation.capital_id');
        return $quer->order('relation_id desc')->find();

    }
    
    public function QuantityOfMainMaterialsPurchasedOnBehalf($orderId){
        return $this->field('sum(if(reimbursement.classification !=4,1,0)) as manReimbursement,sum(if(reimbursement.classification=4,1,0)) as reimbursementAnge,sum(if(reimbursement.classification=3,1,0)) as cooperation')->where('reimbursement.order_id', $orderId)->select();
    }
}
