<?php

namespace app\api\model;

use constant\config;
use think\Model;

class Detailed extends Model
{
    protected $table = 'detailed';

    public function unit()
    {
      return $this->hasOne('Unit','id','un_id')->field('title,id');
    }

    public function detailedListValue()
    {
        return $this->hasMany('DetailedOptionValue','detailed_id')->where('is_enable', 1)->whereNull('deleted_at')->field('detailed_id,option_value_title,option_value_id,craft,material,desc_attachment');
    }
    //��ȡ��һ��
    public function detailedCategoryTitle($detailed_id)
    {
      return $this->join('detailed_category','detailed_category.id=detailed.detailed_category_id','left')->where('detailed_id',$detailed_id)->field('detailed_category.title')->find();
    }

}
