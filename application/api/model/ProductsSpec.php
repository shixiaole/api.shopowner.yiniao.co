<?php

namespace app\api\model;

use constant\config;
use think\Model;

class ProductsSpec extends Model
{
    protected $table = 'products_spec';
    protected $connection = 'database.zong';
    
    public function schemeProject()
    {
           return $this->hasMany('scheme_project', 'plan_id','scheme_id')->field('plan_id,projectId,projectMoney,projectTitle,category,company,agency,to_price,square,remarks,grouping,id,square_ratio as squareRatio,fixed_price as fixedPrice');
    }
   


}
