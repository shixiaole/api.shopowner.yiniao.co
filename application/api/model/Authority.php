<?php
/**
 * Created by muniao.
 * User: muniao
 * Date: 2018/8/2
 * Time: 13:20
 */

namespace app\api\model;


use think\Model;
use think\Request;
use jwt\Token;

class Authority extends Model
{
    public static function check($type)
    {
        $access_token = request()->header("accesstoken");
        if (empty($access_token)) {
            r_date([], 401, '您还未登录账号，请先登录');
        }
        
        $objToken = new Token;
        $s=$objToken->checkToken($access_token);
        $where=['user.access_token' => $access_token];
        if($s['status']==200){
            $access_token=$s['data']['data']->user_id;
            $where=['user.user_id' => $access_token];
        }
       
        if ($type == 1) {
            $user = db('user')->join('store','store.store_id=user.store_id','left')->field('user.*,store.Inventory,store.store_name,store.store_id,user.openid')->where($where)->find();
        } elseif ($type == 3) {
            $user = db('personal')->where(['access_token' => $access_token])->find();
        } elseif ($type == 2) {
            $user = db('customers')->where(['access_token' => $access_token])->find();
        }
        if (empty($user)) {
            r_date([], 401, '账号已下线，请重新登录');
        }
        if ($user['status'] == 1) {
            r_date([], 401, '请等待审核');
        }
        if ($user['status'] == 2) {
            r_date([], 401, '该用户已冻结');
        }
        return $user;
    }

    /**
     * @param array $data
     * @return array
     * $data=['id','name'=>'123']  id必填，name选填，默认值为123（有默认值时key不能为int）
     */
    public function post()
    {
        $return = Request::instance()->post();
        
      
        return $return;
    }
    /**
     * @param array $data
     * @return array
     * $data=['id','name'=>'123']  id必填，name选填，默认值为123（有默认值时key不能为int）
     */
    public static function param(array $data)
    {

        if (empty($data)) {
            r_date([], 300, '提交数据不正确');
        }
        $return = [];

        foreach ($data as $key => $item) {

            if (!is_int($key)) {

                $val = Request::instance()->post($key);
                $get = Request::instance()->get($key);

                if (isset($val) || isset($get)) {
                    $return[$key] = isset($val)?trim($val):trim($get);

                } else {
                    $return[$key] = trim($item);
                }
            } else {
                $val = Request::instance()->post($item);
                $get = Request::instance()->get($item);

                if (!isset($val) && !isset($get)) {
                    r_date(null, 300, "缺少参数");
                }
                $return[$item] = isset($val)?trim($val):$get;
            }
        }

        return $return;
    }
}