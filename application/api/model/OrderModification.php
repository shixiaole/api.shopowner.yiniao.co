<?php

namespace app\api\model;

use constant\config;
use think\Model;

class OrderModification extends Model
{
    protected $table = 'order_modification';
    protected $autoWriteTimestamp = true;
    protected $createTime = 'created_time';            //数据添加的时候，create_time 这个字段不自动写入时间戳
    protected $updateTime = false;


    public function Ranking()
    {
        $list = $this
            ->field('sum(if(status=1,1,0)) as complete,(round((sum(if(status=1,1,0))/count(order_id)),2)*100)  as jindu,user_id,username,store_name,count(order_id) as zong')
            ->group('user_id')
            ->order('jindu desc,id desc')
            ->select();
        foreach ($list as $k => $item) {
            $list[$k]['xu'] = $k + 1;
        }
        return $list;
}


}
