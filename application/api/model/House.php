<?php

namespace app\api\model;

use constant\config;
use think\Model;

class House extends Model
{
    protected $table = 'house';
    protected $connection='database.zong';

    /**
     * @return string
     */
    public function getHouse($data)
    {
      $list=$this->where('province',$data['province'])->where('city',$data['city'])->where('area',$data['area']);

      if(strpos($data['communityName'],'·') !== false){
          $newCommunityName=str_replace('·','',$data['communityName']);
          $list->where("name='".$data['communityName']."' or name ='".$newCommunityName."'");
      }else{

          $list->where('name',$data['communityName']);
      }
      $listList=$list->value('id');
      if(empty($listList)){
          $map=$this->map_bd2tx($data['latitude'],$data['longitude']);
          $id=$this->insertGetId([
              'province'=>$data['province'],
              'city'=>$data['city'],
              'area'=>$data['area'],
              'name'=>$data['communityName'],
              'address'=>$data['address'],
              'latitude'=>$map['lat'],
              'longitude'=>$map['lng'],
              'created_time'=>time(),
          ]);
          $listList=$id;
      }
        return $listList;
    }
    /*
     *��Ѷ��ͼGCJ02����---->�ٶȵ�ͼBD09����
     */
    public function map_bd2tx($lat, $lng)
    {
        $x_pi = 3.14159265358979324 * 3000.0 / 180.0;
        $x = $lng;
        $y = $lat;
        $z =sqrt($x * $x + $y * $y) + 0.00002 * sin($y * $x_pi);
        $theta = atan2($y, $x) + 0.000003 * cos($x * $x_pi);
        $lng = $z * cos($theta) + 0.0065;
        $lat = $z * sin($theta) + 0.006;
        return array('lng'=>$lng,'lat'=>$lat);
    }


}
