<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2019/10/24
 * Time: 10:49
 */

namespace app\api\model;


use redis\RedisPackage;
use think\Db;
use think\Model;


class ConstructionNode extends Model
{
    /*
     *
     * 施工节点
     */
    public function List($orderId, $type)
    {
        $auxiliary_delivery_node = \db('auxiliary_delivery_node')
            ->field('count(auxiliary_delivery_schedul_id) as nodeId,sum(DISTINCT if(auxiliary_delivery_node.state=1,1,0)) as nodeIds,auxiliary_delivery_schedul_id')
            ->group('auxiliary_delivery_schedul_id')
            ->buildSql();
        
        $auxiliary_delivery_schedule = \db('auxiliary_delivery_schedule')
            ->field('count(auxiliary_delivery_schedule.id) as scheduleId,node.nodeId,sum(nodeIds) as nodeIds,node.auxiliary_delivery_schedul_id,auxiliary_delivery_schedule.auxiliary_project_list_id')
            ->join([$auxiliary_delivery_node => 'node'], 'node.auxiliary_delivery_schedul_id=auxiliary_delivery_schedule.id', 'left')
            ->whereNull('auxiliary_delivery_schedule.delete_time')
            ->group('auxiliary_project_list_id')
            ->buildSql();
        
        
        $capital = db('auxiliary_project_list')
            ->join([$auxiliary_delivery_schedule => 'schedule'], 'auxiliary_project_list.id=schedule.auxiliary_project_list_id', 'left')
            ->join('capital', 'capital.capital_id=auxiliary_project_list.capital_id', 'left')
            ->join('order', 'order.order_id=auxiliary_project_list.order_id', 'left')
            ->field('auxiliary_project_list.capital_id,schedule.scheduleId,ifnull(schedule.nodeId,0) as nodeId,ifnull(schedule.nodeIds,0) as nodeIds,schedule.auxiliary_delivery_schedul_id,concat(capital.class_b,"(",square,"/",company,")") as class_b,capital.cooperation,auxiliary_project_list.auxiliary_id,auxiliary_project_list.id,order.state')
            ->where('auxiliary_project_list.order_id', $orderId)
            ->where('capital.types', 1)
            ->where('capital.enable', 1)
            ->whereNull('auxiliary_project_list.delete_time');
        if ($type == 1) {
            $capital->where(
                function ($quer) {
                    $quer->whereNull('nodeId')->whereOr('scheduleId !=nodeIds');;
                })->where('scheduleId', '<>', 0);
        } else {
            $capital->where(function ($quer) {
                $quer->whereOr('scheduleId = nodeIds')->whereOr(function ($quers) {
                    $quers->where('scheduleId', 'null')->where('state', 7);
                });
            });
        }
        $capital = $capital->group('auxiliary_project_list.capital_id')
            ->select();
        if (count($capital) == 0) {
//            (new Common())->PushCompletion(3, $orderId);
        }
        foreach ($capital as $k => $item) {
            $username = Db::connect(config('database.db2'))->table('app_user_order_capital')->where('capital_id', $item['capital_id'])->join('app_user', 'app_user.id=app_user_order_capital.user_id', 'left')->where('order_id', $orderId)->whereNull('app_user_order_capital.deleted_at')->column('app_user.username');
//            if ($type != 1) {
//                db('auxiliary_project_list')->whereNull('completion_time')->where('capital_id', $item['capital_id'])->update(['completion_time' => time()]);
//            }
            $capital[$k]['class_b']        .= "         " . implode($username, '/');
            $capital[$k]['Specifications'] = implode(db('capital_value')->where('capital_id', $item['capital_id'])->column('title'), '/');
            $auxiliary_delivery_schedule   = db('auxiliary_delivery_schedule')->join('auxiliary_interactive', 'auxiliary_interactive.id=auxiliary_delivery_schedule.auxiliary_interactive_id', 'left')->field('auxiliary_interactive.title,auxiliary_interactive.main_node,auxiliary_delivery_schedule.id,auxiliary_interactive.upload_acceptance as uploadAcceptance')->where('auxiliary_project_list_id', $item['id'])->select();
            foreach ($auxiliary_delivery_schedule as $p => $value) {
                $app_user_order_nodes = db('auxiliary_delivery_node')->whereIn('auxiliary_delivery_schedul_id', $value['id'])->select();
              
                if (!empty($app_user_order_nodes)) {
                    $paths = [];
                    foreach ($app_user_order_nodes as $l) {
                        $app_user_order_nodes_list = Db::connect(config('database.db2'))->table('app_user_order_node')
                            ->join('app_user', 'app_user.id=app_user_order_node.user_id', 'left')
                            ->join(config('database')['database'] . '.user', 'user.user_id=app_user_order_node.user_id', 'left')
                            ->field('app_user_order_node.id,app_user_order_node.resource_ids,app_user_order_node.no_watermark_image_ids,app_user_order_node.created_at,if(app_user_order_node.distinguish=1,user.username,app_user.username) as username,app_user_order_node.distinguish')
                            ->whereIn('app_user_order_node.id', $l['node_id'])->select(); 
                            $pathId                    = implode(array_column($app_user_order_nodes_list, 'resource_ids'), ',');
                            if($l['type']==3){
                                $pathId                    = implode(array_column($app_user_order_nodes_list, 'no_watermark_image_ids'), ',');
                              
                            }
                            $path                      = Db::connect(config('database.db2'))->table('common_resource')->where('status', 1)->whereIn('id', $pathId)->field('mime_type,path,id')->select();
//                        var_dump($path);
                        
                        foreach ($path as $k1 => $o) {
                            
                            $list['path']       = 'https://images.yiniao.co/' . $o['path'];
                            $list['id']         = $o['id'];
                            $list['type']       = $app_user_order_nodes_list[0]['distinguish'];
                            $list['username']   = empty($app_user_order_nodes_list[0]['username'])?"后台上传":$app_user_order_nodes_list[0]['username'];
                            $list['created_at'] = date('Y-m-d H:i', $app_user_order_nodes_list[0]['created_at']);
                            $paths[]            = $list;
                        }
                        
                        
                    }
                    $auxiliary_delivery_schedule[$p]['data']    = $paths;
                    $auxiliary_delivery_schedule[$p]['state']   = $app_user_order_nodes[0]['state'];
                    $auxiliary_delivery_schedule[$p]['reason']  = $app_user_order_nodes[0]['reason'];
                    $auxiliary_delivery_schedule[$p]['node_id'] = $app_user_order_nodes[0]['node_id'];
                } else {
                    $auxiliary_delivery_schedule[$p]['state']   = 0;
                    $auxiliary_delivery_schedule[$p]['reason']  = '';
                    $auxiliary_delivery_schedule[$p]['data']    = [];
                    $auxiliary_delivery_schedule[$p]['node_id'] = 0;
                }
                
            }
            
            $capital[$k]['data'] = $auxiliary_delivery_schedule;
        }
        return $capital;
    }
    
    /*
     *
     * 施工节点审核
     */
    public function toExamine($nodeIds, $state, $reason, $orderId, $userId)
    {
        db()->startTrans();
        try {
            
            $time = time();
            
            $auxiliary_delivery_schedule = db('auxiliary_delivery_schedule')
                ->field('auxiliary_delivery_schedule.id,auxiliary_interactive.title,GROUP_CONCAT(auxiliary_delivery_node.node_id) as node_id,auxiliary_interactive.upload_acceptance,auxiliary_interactive.user_confirm')
                ->join('auxiliary_interactive', 'auxiliary_interactive.id=auxiliary_delivery_schedule.auxiliary_interactive_id', 'left')
                ->join('auxiliary_delivery_node', 'auxiliary_delivery_node.auxiliary_delivery_schedul_id=auxiliary_delivery_schedule.id', 'left')
                ->whereIn('auxiliary_delivery_schedule.id', $nodeIds)
                ->group('auxiliary_delivery_schedule.auxiliary_interactive_id')
//                ->where('user_confirm', 1)
                ->select();
            $upload_acceptance           = [];
            if (!empty($auxiliary_delivery_schedule)) {
                foreach ($auxiliary_delivery_schedule as $item) {
                    if ($item['user_confirm'] == 1) {
                        $ccPush = new ccPush(1, 'template_node_check', 'exchange_topic', 'template.nodeCheck');
                        if(!empty($item['node_id'])){
                            $list   = [
                                'orderId' => $orderId,
                                'node' => $item['title'],
                                "time" => date('Y-m-d H:i:s', $time),
                                "nodeId" => $item['node_id']
                            ];
                            $ccPush->push(json_encode($list), 'template_node_check', 'exchange_topic', 'template.nodeCheck');
                            nodeAutoConfirm($item['node_id'], $orderId);
                        }
                  
                    }
                    if ($item['upload_acceptance'] == 2 &&  $item['node_id']==0) {
                        $upload_acceptance[] = [
                            'auxiliary_delivery_schedul_id' => $item['id'],
                            'state' => 0,
                            'upload_time' => time(),
                            'mast_id' => $userId,
                            'type' => 2,
                        ];
                    }
                    
                }
                
            }
            if (!empty($upload_acceptance)) {
                db('auxiliary_delivery_node')->insertAll($upload_acceptance);
            }
            $auxiliary_delivery_node = db('auxiliary_delivery_node')->whereIn('auxiliary_delivery_schedul_id', $nodeIds)->update(['state' => $state, 'reason' => $reason, 'audit_time' => $time]);
            if ($auxiliary_delivery_node) {
                $auxiliary_delivery_schedule = db('auxiliary_delivery_schedule')->join('auxiliary_project_list', 'auxiliary_project_list.id=auxiliary_delivery_schedule.auxiliary_project_list_id', 'left')->whereIn('auxiliary_delivery_schedule.id', $nodeIds)->field('auxiliary_project_list.capital_id,auxiliary_delivery_schedule.auxiliary_interactive_id')->group('auxiliary_interactive_id')->select();
                $payment                     = db('payment')->where('orders_id', $orderId)->where('cleared_time', '>', 0)->select();
                $Lasttime                    = strtotime(date('Y-m-d 21:00:00', $time));
                $more                        = $Lasttime - $time;
                $redis                       = new RedisPackage();
                $clock_in_scan               = $redis::get('template_proccess' . $orderId);
                $sign                        = db('sign')->where('order_id', $orderId)->find();
                if (!empty($sign) && !empty($sign['main_mode_type1'])) {
                    foreach ($auxiliary_delivery_schedule as $li) {
                        if ($li['auxiliary_interactive_id'] == $sign['auxiliary_interactive_id'] && $li['capital_id'] == $sign['main_mode_type1']) {
                            (new Common())->PushCompletion(2, $orderId, strtotime(date("Y-m-d 09:30:00", strtotime("+1 day"))) - time());
                        }
                        
                    }
                    
                }
                if (empty($clock_in_scan) && $clock_in_scan != 1) {
                    $ccPush = new ccPush($more, 'delay_template_proccess', 'delay_topic', 'delay.template_proccess');
                    $list   = [
                        'orderId' => $orderId,
                        'zhifu' => !empty($payment) ? 1 : 0,
                        "time" => date('Y-m-d H:i:s', $time)
                    ];
                    $ccPush->push(json_encode($list), 'delay_template_proccess', 'delay_topic', 'delay.template_proccess');
                    $redis::set('template_proccess' . $orderId, 1);
                    
                }
            }
            db()->commit();
            Db::connect(config('database.db2'))->commit();
            $auxiliary_delivery_node = \db('auxiliary_delivery_node')
                ->field('count(auxiliary_delivery_schedul_id) as nodeId,sum(DISTINCT if(auxiliary_delivery_node.state=1,1,0)) as nodeIds,auxiliary_delivery_schedul_id')
                ->group('auxiliary_delivery_schedul_id')
                ->buildSql();
            
            $auxiliary_delivery_schedule = \db('auxiliary_delivery_schedule')
                ->field('count(auxiliary_delivery_schedule.id) as scheduleId,node.nodeId,sum(nodeIds) as nodeIds,node.auxiliary_delivery_schedul_id,auxiliary_delivery_schedule.auxiliary_project_list_id')
                ->join([$auxiliary_delivery_node => 'node'], 'node.auxiliary_delivery_schedul_id=auxiliary_delivery_schedule.id', 'left')
                ->whereNull('auxiliary_delivery_schedule.delete_time')
                ->group('auxiliary_project_list_id')
                ->buildSql();
            
            
            $capital         = db('auxiliary_project_list')
                ->join([$auxiliary_delivery_schedule => 'schedule'], 'auxiliary_project_list.id=schedule.auxiliary_project_list_id', 'left')
                ->join('capital', 'capital.capital_id=auxiliary_project_list.capital_id', 'left')
                ->join('order', 'order.order_id=auxiliary_project_list.order_id', 'left')
                ->field('auxiliary_project_list.capital_id,schedule.scheduleId,ifnull(schedule.nodeId,0) as nodeId,ifnull(schedule.nodeIds,0) as nodeIds,schedule.auxiliary_delivery_schedul_id,concat(capital.class_b,"(",square,"/",company,")") as class_b,capital.cooperation,auxiliary_project_list.auxiliary_id,auxiliary_project_list.id,order.state')
                ->where('auxiliary_project_list.order_id', $orderId)
                ->where('capital.types', 1)
                ->where('capital.enable', 1)
                ->whereNull('auxiliary_project_list.delete_time')
                ->where(function ($quer) {
                    $quer->whereOr('scheduleId = nodeIds')->whereOr(function ($quers) {
                        $quers->where('scheduleId', 'null')->where('state', 7);
                    });
                });
            $capital         = $capital->group('auxiliary_project_list.capital_id')
                ->column('auxiliary_project_list.capital_id');
            $completion_time = time();
            db('auxiliary_project_list')->whereNull('completion_time')->whereIn('capital_id', $capital)->update(['completion_time' => $completion_time]);
           
            $order_setting = db('order_setting')->join('order','order_setting.order_id=order.order_id','left')->field('order_setting.*,order.*')->where('order.order_id', $orderId)->find();
            if ($order_setting['fast_salary'] == 2) {
                $appcapital_labor_costs = [];
                $app_user_order_capital = db('app_user_order_capital')
                    ->field('app_user_order_capital.personal_price,app_user_order.salary_ratio,app_user_order_capital.id,app_user_order_capital.capital_id,app_user_order_capital.user_id,app_user_order.status,app_user_order.order_id')
                    ->join('app_user_order', 'app_user_order.order_id=app_user_order_capital.order_id and app_user_order.user_id=app_user_order_capital.user_id', 'left')
                    ->join('app_user', 'app_user.id=app_user_order_capital.user_id', 'left')
                    ->where('app_user.origin', 1)
                    ->where('app_user.shortcut', 1)
                    ->whereNull('app_user_order_capital.deleted_at')
                    ->whereIn('app_user_order_capital.capital_id', $capital)
                    ->select();
                    
                $appUser                = db('app_user_order')
                    ->join('app_user_order_capital', 'app_user_order.order_id=app_user_order_capital.order_id and app_user_order.user_id=app_user_order_capital.user_id', 'left')
                    ->join('app_user', 'app_user.id=app_user_order_capital.user_id', 'left')
                    ->join('app_work_type', 'app_work_type.id=app_user.work_type_id', 'left')
                    ->whereNull('app_user_order_capital.deleted_at')
                    ->orderRaw("field(work_type_id,4,5,2,3,1)")
                    ->where('app_user_order.order_id', $orderId)
                    ->where('app_user_order.status', '<', 3)
                    ->where('app_user.origin', 1)
                    ->where('app_user.shortcut', 1)
                    ->group('app_user_order.user_id')
                    ->column('app_user_order.user_id');
                    journal(['capital_id'=>$capital,'list'=>$app_user_order_capital],4);
                foreach ($app_user_order_capital as $k => $item) {
                    $app_user_order_capital[$k]['state'] = 0;
                    $listArray                           = $appUser;
                    if ($item['status'] < 3 && count($appUser) > 1) {
                        $app_user_order_capital[$k]['state'] = "-1";
                        foreach ($listArray as $Key => $list) {
                            if ($list == $item['user_id']) {
                                unset($listArray[$Key]);
                            }
                        }
                        $listArray                                 = array_merge($listArray);
                        $app_user_order_capital[$k]['examineName'] = $listArray[0];
                    } else {
                        $app_user_order_capital[$k]['examineName'] = 0;
                    }
                    
                }
                foreach ($app_user_order_capital as $item) {
                    $withdrawal_amount                  = round($item['personal_price'] * ($item['salary_ratio'] / 100), 2);
                    $app_user_order_capital_labor_costs = db('app_user_order_capital_labor_costs')->where(['order_id' => $orderId, 'capital_id' => $item['capital_id'], 'user_id' => $item['user_id']])->lock(true)->find();
                    $examine                            = db('app_user_order_capital_labor_costs')->where(['order_id' => $orderId, 'user_id' => $item['user_id'], 'examine_user_id' => ['<>', 0], 'adopt_time' => ['<>', 0]])->order('id ASC')->find();
                    if (empty($app_user_order_capital_labor_costs)) {
                        $adopt_time = 0;
                        if ($item['examineName'] == 0) {
                            $adopt_time = $completion_time;
                        }
                        if ($item['state'] == "-1" && !empty($examine)) {
                            $item['state']       = "0";
                            $adopt_time          = $examine['adopt_time'];
                            $item['examineName'] = $examine['examine_user_id'];
                        }
                        db('app_user_order_capital_labor_costs')->insert(['order_id' => $orderId, 'capital_id' => $item['capital_id'], 'user_id' => $item['user_id'], 'personal_price' => $withdrawal_amount, 'created_at' => $completion_time, 'state' => $item['state'], 'examine_user_id' => $item['examineName'], 'adopt_time' => $adopt_time]);
                        $appcapital_labor_costs[] = [
                            'capital_id' => $item['capital_id'],
                            'withdrawal_amount' => $withdrawal_amount
                        ];
                    }
                    
                }
                
                if (!empty($appcapital_labor_costs)) {
                    $result   = array();
                    $tageList = [];
                    
                    foreach ($appcapital_labor_costs as $k => $v) {
                        $result[$v['capital_id']][] = $v;
                    }
                    foreach ($result as $k => $list) {
                        $schemeTag['capital_id'] = $k;
                        $schemeTag['sum']        = array_sum(array_column($list, 'withdrawal_amount'));
                        $tageList[]              = $schemeTag;
                    }
                    if (!empty($tageList)) {
                        foreach ($tageList as $value) {
                            $u8c_voucher = Db::connect(config('database.zong'))->table('u8c_voucher')->lock(true)->where(['orderId' => $orderId, 'relation_id' => $value['capital_id'], 'type' => 53])->find();
                            if (empty($u8c_voucher) && $value['sum'] != 0) {
                                Db::connect(config('database.zong'))->table('u8c_voucher')->insert([
                                    'explanation' => '收到工资快结工序验收师傅金额',
                                    'money' => $value['sum'],
                                    'orderId' => $orderId,
                                    'relation_id' => $value['capital_id'],
                                    'type' => 53,
                                    'create_time' => time() * 1000,
                                    'prepareddate' => date('Y-m-d', time()),
                                ], true);
                            }
                            
                        }
                        
                    }
                }
                
            }
            return ['code' => 200, 'msg' => ""];
        } catch (\Exception $e) {
            db()->rollBack();
            return ['code' => 300, 'msg' => $e->getMessage()];
            
        }
    }
}
