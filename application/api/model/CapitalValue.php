<?php

namespace app\api\model;

use constant\config;
use think\Model;

class CapitalValue extends Model
{
    protected $table = 'capital_value';


    public function Add($data,$ids)
    {
       $list= $this->where(['capital_id' => $ids,'option_value_id' => $data['option_value_id']])->find();
       $spec_doc= db('detailed_option_value')->where(['option_value_id' => $data['option_value_id']])->value('spec_doc');
       $id=0;
    
       if(isset($data['condition']) ){
          $conditionss= $data['condition'];
       }elseif (isset($data['conditions'])){
           $conditionss= $data['conditions'];
       }
        if(empty($list)){
            $id= $this->insertGetId([
                'capital_id' => $ids,//单位
                'title' => $data['option_value_title'],//方量
                'option_value_id' => $data['option_value_id'],//单价
                'conditionss' => $conditionss,//规则
                'price' => $data['price'],//规则
                'spec_doc' =>$spec_doc,//规则
                'artificial' => $data['artificial'],//规则
            ]);
        }else{
            $id=$list['id'];
        }
      
        return $id;
    }


}
