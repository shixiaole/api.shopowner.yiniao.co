<?php

namespace app\api\model;
require_once ROOT_PATH . 'vendor/aliyuncs/oss-sdk-php/autoload.php';

use OSS\OssClient;
use OSS\Core\OssException;
use think\Model;

use think\config;


class Aliyunoss extends Model
{
    protected $oss;
    
    public function initialize()
    {
       
        $accessKeyId    =config('oss.accessKeyId'); //��ȡ������oss��accessKeyId
        $accessKeySecret=config('oss.accessKeySecret');     //��ȡ������oss��accessKeySecret
        $endpoint       =config('oss.endPoint');            //��ȡ������oss��endPoint
       ;
       $this->oss      =new OssClient($accessKeyId, $accessKeySecret, $endpoint); //ʵ����OssClient����
    }
    
    /**
     * ʹ�ð�����oss�ϴ��ļ�
     * @param $object  ���浽������oss���ļ���
     * @param $filepath �ļ��ڱ��صľ���·��
     * @return bool   �ϴ��Ƿ�ɹ�
     */
    public function upload($object, $filepath)
    {
        $bucket=Config::get('oss.bucket');        //��ȡ������oss��bucket
        return$this->oss->uploadFile($bucket, $object, $filepath);
    }
    
    /**
     * ʹ�ð�����oss�ϴ��ļ�
     * @param $object  ���浽������oss���ļ���
     * @param $filepath �ļ��ڱ��صľ���·��
     * @return bool   �ϴ��Ƿ�ɹ�
     */
    public function copyObject($fromObject, $toObject)
    {
        $bucket=Config::get('oss.bucket');        //��ȡ������oss��bucket
        $o=$this->oss->copyObject($bucket, $fromObject,$bucket, $toObject);
        $this->oss->deleteObject($bucket,$fromObject);
        return  $o;
    }
    /**
     * ʹ�ð�����oss�ϴ��ַ���
     */
    public function putStr($object, $content)
    {
        $bucket=Config::get('oss.bucket');        //��ȡ������oss��bucket
        return $this->oss->putObject($bucket, $object, $content);
    }
    
    /**
     * ɾ��ָ���ļ�
     * @param $object ��ɾ�����ļ���
     * @return bool  ɾ���Ƿ�ɹ�
     */
    public function deletel($object)
    
    {
        $res   =false;
        $bucket=Config::get('oss.bucket');        //��ȡ������oss��bucket
        if ($this->oss->deleteObject($bucket, $object)) { //����deleteObject�����ѷ������ļ��ϴ���������oss
            $res=true;
        }
        return $res;
    }
    
    public function test()
    {
        echo "success";
    }
}
