<?php

namespace app\api\model;

use constant\config;
use think\Model;

class schemeMaster extends Model
{
    protected $table = 'scheme_master';
    protected $connection = 'database.zong';
    protected $createTime = 'create_at';

    public function schemeProject()
    {
       return $this->hasMany('scheme_project', 'plan_id')->field('plan_id,projectId,projectMoney,projectTitle,category,company,agency,to_price,square,remarks,grouping,id,square_ratio as squareRatio,fixed_price as fixedPrice');
    }

    public function schemeImg()
    {
        return $this->hasMany('scheme_img', 'plan_id')->field('plan_id,title,path');
    }

    public function schemeCommunity()
    {
        return $this->hasMany('scheme_community', 'plan_id')->field('plan_id,community,name,area');
    }

    public function schemeHouse()
    {
        return $this->hasMany('scheme_house', 'plan_id')->field('house,name,plan_id');
    }

    public function schemeTag()
    {
        return $this->hasMany('scheme_tag', 'plan_id')->field('tag,name,plan_id,type,pid');
    }

    public function schemeCategory()
    {
        return $this->hasMany('scheme_category', 'plan_id')->field('category_id,category_name,plan_id');
    }
    public function schemeCharacteristic()
    {
        return $this->hasMany('scheme_characteristic', 'plan_id')->field('plan_id,title');
    }

    public function city()
    {
        return $this->hasOne('city', 'city_id', 'city_id')->field('city,city_id,province_id');
    }

    public function county()
    {
        return $this->hasOne('county', 'county_id', 'area')->field('county,county_id');
    }

    public function user()
    {
        return $this->hasOne('user', 'user_id', 'user_id')->field('user_id,username');
    }

    public function SchemeCollection()
    {
        return $this->hasOne('scheme_collection', 'plan_id')->field('plan_id,id');

    }

    public function schemeUse()
    {
        return $this->hasMany('scheme_use', 'plan_id')->field('plan_id,id');
    }

    public function getList($orderId,$user_id)
    {

        $scheme_category = db('cc_scheme_label', config('database.zong'))->join('scheme_label_top', 'scheme_label_top.pid=cc_scheme_label.pro_id', 'left')
            ->where('order_id', $orderId)
            ->field('scheme_label_top.id as topId,cc_scheme_label.*')
            ->select();

        foreach ($scheme_category as $p) {
            if ($p['select'] == 1) {
                $pos[] = $p['topId'];
            }
            $house_id[] = $p['house_id'];

        }
        if (!empty($pos)) {
            $scheme_category = db('scheme_category', config('database.zong'));
            $scheme_category->whereIn('category_id', $pos)->group('plan_id');
            $scheme_category = $scheme_category->column('plan_id');

            if (!empty($scheme_category)) {
                $this->whereIn('scheme_master.plan_id', $scheme_category);
            } else {
                $this->whereIn('scheme_master.plan_id', '');
            }

        }
        if (!empty($house_id)) {
            $schemeHouse = db('scheme_community', config('database.zong'))->whereIn('community', $house_id)->group('plan_id')->column('plan_id');
            $scheme_communityUnlimited = db('scheme_community', config('database.zong'))->where('community', 0)->group('plan_id')->column('plan_id');
            $schemeHouse=array_merge($schemeHouse,$scheme_communityUnlimited);
            if (!empty($schemeHouse)) {
                $this->whereIn('scheme_master.plan_id', $schemeHouse);
            } else {
                $this->whereIn('scheme_master.plan_id', '');
            }
        }
        $count = $this->where(function ($query) use ($user_id) {
            $query->where('scheme_master.private', 1)->whereOr('scheme_master.user_id', $user_id);;

        })->group('plan_id')->where('scheme_master.state', 1)->count('plan_id');

        return $count;
    }


}
