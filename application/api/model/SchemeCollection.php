<?php

namespace app\api\model;

use constant\config;
use think\Model;
use traits\model\SoftDelete;

class SchemeCollection extends Model
{
    use SoftDelete;
    protected $table = 'scheme_collection';
    protected $connection = 'database.zong';
    protected $deleteTime = 'delete_time';


}
