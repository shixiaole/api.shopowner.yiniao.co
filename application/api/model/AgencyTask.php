<?php
/**
 * Created by muniao.
 * User: muniao
 * Date: 2018/8/2
 * Time: 13:20
 */

namespace app\api\model;


use think\Db;
use think\Model;
use traits\model\SoftDelete;


class AgencyTask extends Model
{
    
    protected $table = 'agency_task';
    // 定义时间戳字段名
    protected $createTime = 'create_time';
    protected $updateTime = 'updata_time';
    protected $autoWriteTimestamp = true;
    protected $connection = 'database.zong';
    protected $type = [
        'id' => 'bigint',
        'order_id' => 'bigint',
        'capital_id' => 'bigint',
        'agency_staff_id' => 'bigint',
        'agency_id' => 'bigint',
        'unit_price' => 'decimal',
        'square' => 'decimal',
        'total_cost' => 'decimal',
        'manual_cost' => 'decimal',
        'agency_order_id' => 'decimal',
        'status' => 'int',
        'create_time' => 'int',
        'update_time' => 'int',
        'delete_time' => 'int',
    ];
    
    /*
     * 初始化数据
     */
    public function addAllInit($orderId)
    {
        
        $find = $this->where('delete_time', 0)->where('order_id', $orderId)->find();
        $order_setting =  \db('order_setting')->where('order_id', $orderId)->find();
        if (empty($find) && $order_setting['stop_agent_reimbursement']==1) {
            $capital = \db('capital')->where('ordesr_id', $orderId)
                ->where('enable', 1)
                ->where('types', 1)
                ->where('agency', 1)
                ->where('increment', 0)
                ->field('capital_id,un_Price as unit_price,square,labor_cost as manual_cost,ordesr_id as order_id,round(base_cost*square,2) as total_cost,projectId')->select();
            $agency_task=[];
            foreach ($capital as $k=>$item){
                $detailed_agency=db('detailed_agency')->where('detailed_id',$item['projectId'])->column('agency_id');
                $agency=db('agency',config('database.zong'))->whereIn('agency_id',$detailed_agency)->where('cooperation_type',1)->where('state',1)->count();
                $agency_task[$k]['capital_id']=$item['capital_id'];
                $agency_task[$k]['unit_price']=$item['unit_price'];
                $agency_task[$k]['square']=$item['square'];
                $agency_task[$k]['manual_cost']=$item['manual_cost'];
                $agency_task[$k]['order_id']=$item['order_id'];
                $agency_task[$k]['total_cost']=$item['total_cost'];
                $agency_task[$k]['cooperation_type']=empty($agency)?2:1;
                $agency_task[$k]['create_time']=time();
                $agency_task[$k]['base_cost']=$item['total_cost'];
                $agency_task[$k]['overrun']=5;
            }
            \db('agency_task')->insertAll($agency_task);
        }
        
    }
    
    public function CapitalList()
    {
        return $this->hasMany('Capital', 'capital_id', 'capital_id')->where('types', 1)->where('enable', 1)->join('detailed', 'detailed.detailed_id=capital.projectId', 'left')->join('detailed_agency', 'detailed.detailed_id=detailed_agency.detailed_id', 'left')->field('projectId,capital_id,class_b as  projectTitle,company,square,un_Price as projectMoney,group_concat(detailed_agency.agency_id) AS agency_id,labor_cost,to_price as toPrice,capital.base_cost,capital.small_order_fee as smallOrderFee')->group('capital_id')->order('sort asc');
    }
    
    /*
     * 供应商详情
     */
    public function SupplierDetails($id)
    {
        return $this->join('agency', 'agency_task.agency_id=agency.agency_id', 'left')
            ->join('agency_staff', 'agency_staff.id=agency_task.agency_staff_id', 'left')
            ->where('agency_task.agency_order_id', $id)
            ->where('agency_task.delete_time', 0)
            ->field('agency.agency_name,concat(agency_staff.name,agency_staff.mobile) as name')
            ->select();
    
    }
    
}





