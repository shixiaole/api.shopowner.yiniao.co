<?php

namespace app\api\model;


use think\Model;

class Sign extends Model
{
    protected $table = 'sign';
    protected $autoWriteTimestamp = true;
    protected $createTime = 'con_time';            //数据添加的时候，create_time 这个字段不自动写入时间戳
    protected $updateTime = false;
    public static $snakeAttributes = false;
    
    /*
     * bcadd  精确结算加法
     * bcsub   精确计算减法
     * bcmul   精确计算乘法
     * bcdiv    精确计算除法
     */
    public function signAdd($orderModel, array $listArray, array $projectInfoArray, array $op, array $offer, string $userId, $type = 1)
    {
        $mainPayTpe1      = 0;
        $mainPayTpe1Money = 0;
        $mainPayTpe1Bi    = 0;
        
        $mainPayTpe2              = 0;
        $mainPayTpe2Money         = 0;
        $mainPayTpe2Bi            = 0;
        $categoryName             = 0;
        $auxiliary_interactive_id = 0;
        $combination_name         = 0;
        
        $mainPayTpe3      = 0;
        $mainPayTpe3Money = 0;
        $mainPayTpe3Bi    = 0;
        $main_pay_once    = 0;
        $round_discount   = 0;
        $common           = new Common();
        
        if (!empty($offer['MainMaterialMoney'])) {
            $main_pay_once = $listArray['collectionInformation']['MainCollectionInfo']['pay'];
            if ($offer['MainMaterialMoney'] + $offer['agencyMoney'] < 10000 && $main_pay_once != 1 && !in_array(config('cityId'), [172, 239, 375, 200, 216])) {
                r_date(null, 300, '合同金额小于1万，仅能选择一次性收款');
            }
            foreach ($listArray['collectionInformation']['MainCollectionInfo']['mainPayTpe1'] as $list) {
                if ($list['check'] == 1) {
                    $mainPayTpe1Bi    = $list['proportion'];
                    $mainPayTpe1      = $list['type'];
                    $mainPayTpe1Money = (string)round($offer['MainMaterialMoney'] * $mainPayTpe1Bi, 2);
                    if ($listArray['collectionInformation']['MainCollectionInfo']['deposit'] == 1 && $listArray['collectionInformation']['MainCollectionInfo']['depositMoney'] > $mainPayTpe1Money && $offer['MainMaterialMoney'] != 0) {
                        r_date(null, 300, '定金' . $listArray['collectionInformation']['MainCollectionInfo']['depositMoney'] . '不能大于主合同首笔款金额' . $mainPayTpe1Money);
                    }
                    if ($listArray['collectionInformation']['MainCollectionInfo']['deposit'] == 1 && $offer['MainMaterialMoney'] != "0.00") {
                        $mainPayTpe1Money = (string)round($mainPayTpe1Money - $listArray['collectionInformation']['MainCollectionInfo']['depositMoney'], 2);
                    }
                    
                }
            }

//            foreach ($listArray['collectionInformation']['MainCollectionInfo']['mainPayTpe2'] as $list) {
//                if ($list['check'] == 1) {
//                    $mainPayTpe2              = $list['type'];
//                    $categoryName             = $list['categoryName'];
//                    $auxiliary_interactive_id = $list['id'];
//                    $mainPayTpe2Money         = $list['money'];
//                    $mainPayTpe2Bi            = $list['proportion'];
//                }
//            }
            if (!empty($projectInfoArray) && $main_pay_once == 3) {
                
                $mainPayTpe2              = $projectInfoArray['capitalId'];
                $categoryName             = $projectInfoArray['categoryName'];
                $auxiliary_interactive_id = $projectInfoArray['nodeId'];
                $mainPayTpe2Money         = (string)round($offer['MainMaterialMoney'] * $projectInfoArray['proportion'], 2);
                $mainPayTpe2Bi            = $projectInfoArray['proportion'];
                $combination_name         = $projectInfoArray['stringName'];
//                if (empty($mainPayTpe2Money) || $mainPayTpe2Money == 0) {
//                    r_date(null, 300, '请重新设置中期款收款比例');
//                }
            }
            
            if ($main_pay_once != 1) {
                foreach ($listArray['collectionInformation']['MainCollectionInfo']['mainPayTpe3'] as $list) {
                    if ($list['check'] == 1) {
                        $mainPayTpe3 = $list['type'];
//                        $mainPayTpe3Money = bcmul($offer['MainMaterialMoney'] ,$list['proportion'], 2);;
                        $mainPayTpe3Money = (string)round($offer['MainMaterialMoney'] - ($mainPayTpe1Money + $mainPayTpe2Money), 2);
                        $mainPayTpe3Bi    = $list['proportion'];
                    }
                }
            }
        }
        $projectContent = [];
        foreach ($listArray['contractInfo']['projectContent'] as $list) {
            if ($list['check'] == 1) {
                $projectContent[] = $list['type'];
            }
        }
        
        $purchasing_pay_once    = 0;
        $purchasingPayTpe1      = 0;
        $purchasingPayTpe1Money = 0;
        $purchasingPayTpe1Bi    = 0;
        
        $purchasingPayTpe2      = 0;
        $purchasingPayTpe2Money = 0;
        $purchasingPayTpe2Bi    = 0;
        
        $purchasingPayTpe3Money = 0;
        $purchasingPayTpe3Bi    = 0;
      
        if (!empty($offer['agencyMoney'])) {
            if ($type == 1) {
                $purchasing_pay_once = $listArray['collectionInformation']['PurchasingInfo']['pay'];
                if($purchasing_pay_once==3){
                    r_date(null, 300, '代购合同暂时无法选择3次收款，请谅解');
                }
                if ($offer['MainMaterialMoney'] + $offer['agencyMoney'] < 10000 && $purchasing_pay_once != 1 && !in_array(config('cityId'), [172, 239, 375, 200, 216])) {
                    r_date(null, 300, '合同金额小于1万，仅能选择一次性收款');
                }
                foreach ($listArray['collectionInformation']['PurchasingInfo']['purchasingPayTpe1'] as $list) {
                    if ($list['check'] == 1) {
                        $purchasingPayTpe1      = $list['type'];
                        $purchasingPayTpe1Bi    = $list['proportion'];
                        $purchasingPayTpe1Money = (string)round($offer['agencyMoney'] * $purchasingPayTpe1Bi, 2);
                        if ($listArray['collectionInformation']['MainCollectionInfo']['deposit'] == 1 && $listArray['collectionInformation']['MainCollectionInfo']['depositMoney'] > $purchasingPayTpe1Money && $offer['MainMaterialMoney'] == 0) {
                            r_date(null, 300, '定金' . $listArray['collectionInformation']['MainCollectionInfo']['depositMoney'] . '不能大于代购合同首笔款金额', $purchasingPayTpe1Money);
                        }
                        if ($listArray['collectionInformation']['MainCollectionInfo']['deposit'] == 1 && (empty($offer['MainMaterialMoney']) || $offer['MainMaterialMoney'] == "0.00")) {
                            $purchasingPayTpe1Money = (string)round($purchasingPayTpe1Money - $listArray['collectionInformation']['MainCollectionInfo']['depositMoney'], 2);
                        }
                     
                    }
                }
                if($offer['MainMaterialMoney'] == 0){
                    $listArray['collectionInformation']['PurchasingInfo']['additionalTerms']=$listArray['collectionInformation']['MainCollectionInfo']['additionalTerms'];
                    $listArray['collectionInformation']['MainCollectionInfo']['additionalTerms']='';
                }
                if ($purchasing_pay_once == 2) {
                    foreach ($listArray['collectionInformation']['PurchasingInfo']['purchasingTpe2'] as $list) {
                        if ($list['check'] == 1) {
                            $purchasingPayTpe3      = $list['type'];
                            $purchasingPayTpe3Money = (string)round($offer['agencyMoney'] - ($purchasingPayTpe1Money + $purchasingPayTpe2Money), 2);
                            $purchasingPayTpe3Bi    = $list['proportion'];
                        }
                    }
                }
            }
            if ($type == 2) {
                if (!empty($mainPayTpe1Bi)) {
                    $purchasing_pay_once    = $listArray['collectionInformation']['MainCollectionInfo']['pay'];
                    $purchasingPayTpe1      = $mainPayTpe1;
                    $purchasingPayTpe1Money = (string)round($offer['agencyMoney'] * $mainPayTpe1Bi, 2);
                    if ($listArray['collectionInformation']['MainCollectionInfo']['deposit'] == 1 && $listArray['collectionInformation']['MainCollectionInfo']['depositMoney'] > $purchasingPayTpe1Money && $offer['MainMaterialMoney'] == 0) {
                        r_date(null, 300, '定金' . $listArray['collectionInformation']['MainCollectionInfo']['depositMoney'] . '不能大于代购合同首笔款金额', $purchasingPayTpe1Money);
                    }
                    if ($listArray['collectionInformation']['MainCollectionInfo']['deposit'] == 1 && (empty($offer['MainMaterialMoney']) || $offer['MainMaterialMoney'] == "0.00")) {
                        $purchasingPayTpe1Money = (string)round($purchasingPayTpe1Money - $listArray['collectionInformation']['MainCollectionInfo']['depositMoney'], 2);
                    }
                    $purchasingPayTpe1Bi = $mainPayTpe1Bi;
                }
                
                if (!empty($mainPayTpe2Bi)) {
                    $purchasingPayTpe2      = $list['type'];
                    $purchasingPayTpe2Money = (string)round($offer['agencyMoney'] * $mainPayTpe2Bi, 2);
                    $purchasingPayTpe2Bi    = $mainPayTpe2Bi;
                }
                
                
                if (!empty($mainPayTpe3Bi)) {
                    $purchasingPayTpe3Money = (string)round($offer['agencyMoney'] - ($purchasingPayTpe1Money + $purchasingPayTpe2Money), 2);
                    $purchasingPayTpe3Bi    = $mainPayTpe3Bi;
                }
            }
            if ($op['assignor'] != $op['user_id']) {
                $userId = $op['assignor'];
            }
        }

//        if ($listArray['collectionInformation']['MainCollectionInfo']['deposit'] == 1 && empty($offer['MainMaterialMoney'])) {
//            $purchasingPayTpe1Money = $purchasingPayTpe1Money - $listArray['collectionInformation']['MainCollectionInfo']['depositMoney'];
//        }
        if ((($offer['agencyMoney'] == "0.00" && $offer['MainMaterialMoney'] != "0.00") || ($offer['agencyMoney'] != "0.00" && $offer['MainMaterialMoney'] != "0.00")) && $listArray['collectionInformation']['MainCollectionInfo']['deposit'] == 1 && $main_pay_once != 1) {
            $mainPayTpe3Money = $mainPayTpe3Money - $listArray['collectionInformation']['MainCollectionInfo']['depositMoney'];
        } elseif ($offer['MainMaterialMoney'] == "0.00" && $offer['agencyMoney'] != "0.00" && $listArray['collectionInformation']['MainCollectionInfo']['deposit'] == 1 && $purchasing_pay_once != 1) {
            $purchasingPayTpe3Money = $purchasingPayTpe3Money - $listArray['collectionInformation']['MainCollectionInfo']['depositMoney'];
        }
        if (!empty($offer['MainMaterialMoney']) && $offer['MainMaterialMoney'] != "0.00") {
            if (bcadd(bcadd($mainPayTpe1Bi, $mainPayTpe2Bi, 2), $mainPayTpe3Bi, 2) != 1) {
                r_date(null, 300, '收款比例错误，请返回详情后重新签约');
            }
        }
        if (!empty($offer['agencyMoney']) && $offer['agencyMoney'] != "0.00") {
            if (bcadd(bcadd($purchasingPayTpe1Bi, $purchasingPayTpe2Bi, 2), $purchasingPayTpe3Bi, 2) != 1) {
                r_date(null, 300, '收款比例错误，请返回详情后重新签约');
            }
        }
        $mainPayTpe1MoneyNode     = 0;
        $mainPayTpe1MoneyNode1    = 0;
        $mainPayTpe1MoneyNode2    = 0;
        $purchasingTpe1MoneyNode   = 0;
        $purchasingTpe1MoneyNode1  = 0;
        $purchasingTpe1MoneyNode2  = 0;
        $addBack  = 0;
        $purchasingAddBack  = 0;
       if($type==1){
           $round_discount=0;
           $mainPayTpe1MoneyNode      = $common->decimalPart($mainPayTpe1Money + $purchasingPayTpe1Money);
           $mainPayTpe1MoneyNode1     = $common->decimalPart($mainPayTpe2Money+ $mainPayTpe1MoneyNode + $purchasingPayTpe2Money);
           $mainPayTpe1MoneyNode2     = $common->decimalPart($mainPayTpe3Money+ $mainPayTpe1MoneyNode1 + $purchasingPayTpe3Money);
           if($mainPayTpe1MoneyNode2=='0.'){
               $mainPayTpe1MoneyNode2=0;
               $addBack=$mainPayTpe1MoneyNode;
              if(bcadd($mainPayTpe2Money, $purchasingPayTpe2Money,2) >0){
                  $addBack=$mainPayTpe1MoneyNode+$mainPayTpe1MoneyNode1;
              }
          }
           if(bcadd($mainPayTpe1Money ,$purchasingPayTpe1Money,2) >0 && bcadd($mainPayTpe2Money ,$purchasingPayTpe2Money,2) >0 && bcadd($mainPayTpe3Money ,$purchasingPayTpe3Money,2) >0){
               //分3笔付款
               $round_discount            = $mainPayTpe1MoneyNode2;
               $mainPayTpe2Money=$mainPayTpe2Money+ $mainPayTpe1MoneyNode;
               $mainPayTpe3Money=$mainPayTpe3Money+ $mainPayTpe1MoneyNode1;
               if($mainPayTpe1MoneyNode2 =='0.'){
                   $mainPayTpe3Money=$mainPayTpe3Money-$addBack;
               }
               if($mainPayTpe2Money ==0 && $purchasingPayTpe2Money>0){
                   $purchasingPayTpe2Money=$purchasingPayTpe2Money+ $mainPayTpe1MoneyNode;
               }
           }elseif (bcadd($mainPayTpe1Money ,$purchasingPayTpe1Money,2) >0 && bcadd($mainPayTpe2Money ,$purchasingPayTpe2Money,2) =='0.00' && bcadd($mainPayTpe3Money ,$purchasingPayTpe3Money,2) >0 ){
               //分2笔付款
               $round_discount            = $mainPayTpe1MoneyNode2;
               if( $mainPayTpe3Money>0 && $addBack==0){
                   $mainPayTpe3Money=$mainPayTpe3Money+ $mainPayTpe1MoneyNode1;
               }
               if($mainPayTpe3Money ==0 && $purchasingPayTpe3Money>0 && $addBack==0){
                   $purchasingPayTpe3Money=$purchasingPayTpe3Money+ $mainPayTpe1MoneyNode1;
               }
             
           }elseif (bcadd($mainPayTpe1Money ,$purchasingPayTpe1Money,2) >0 && bcadd($mainPayTpe2Money ,$purchasingPayTpe2Money,2) =='0.00' && bcadd($mainPayTpe3Money ,$purchasingPayTpe3Money,2) =='0.00'){
               //分1笔付款
               $round_discount            = $mainPayTpe1MoneyNode;
           }
           $main_round_discount       = 0;
           $purchasing_round_discount = 0;
           if ($offer['MainMaterialMoney'] != 0 && $offer['agencyMoney'] == 0 && $offer['MainMaterialMoney'] > 1) {
               $main_round_discount = $round_discount;
           } elseif ($offer['MainMaterialMoney'] == 0 && $offer['agencyMoney'] != 0 && $offer['agencyMoney'] > 1) {
               $purchasing_round_discount = $round_discount;
               
           } elseif (($offer['MainMaterialMoney'] != 0 && $offer['MainMaterialMoney'] > 1) && ($offer['agencyMoney'] != 0 && $offer['agencyMoney'] > 1)) {
               if (($offer['MainMaterialMoney'] - $round_discount) >= 0) {
                   $main_round_discount = $round_discount;
               } elseif (($offer['agencyMoney'] - $round_discount) >= 0) {
                   $purchasing_round_discount = $round_discount;
               }
           }
           if ($offer['agencyMoney'] != 0 && $offer['MainMaterialMoney'] == 0 && $offer['agencyMoney'] > 1) {
               $purchasingTpe1MoneyNode  = $mainPayTpe1MoneyNode;
               if($mainPayTpe2Money + $purchasingPayTpe2Money>0){
                   $purchasingTpe1MoneyNode1 = $mainPayTpe1MoneyNode1;
               }
             
               $purchasingTpe1MoneyNode2 = $mainPayTpe1MoneyNode2;
               $purchasingAddBack=$addBack;
               $addBack=0;
               $mainPayTpe1MoneyNode     = 0;
               $mainPayTpe1MoneyNode1    = 0;
               $mainPayTpe1MoneyNode2    = 0;
           }
       }
       if ($type == 2) {
            $main_round_discount       = 0;
            $purchasing_round_discount = 0;
        }
    
        if($main_pay_once !=1){
            $mainPayTpe3Money=$mainPayTpe3Money - $mainPayTpe1MoneyNode2;
        }
        if($purchasing_pay_once !=1){
            if($purchasingPayTpe3Money>0 && $main_pay_once ==1 &&  $offer['MainMaterialMoney'] != 0){
                $purchasingTpe1MoneyNode2 = $mainPayTpe1MoneyNode2;
            }
            $purchasingPayTpe3Money=$purchasingPayTpe3Money - $purchasingTpe1MoneyNode2;
        };
        $mainPayTpe3Money=$mainPayTpe3Money+$addBack;
        $purchasingPayTpe3Money=$purchasingPayTpe3Money+$purchasingAddBack;
        $sign = [
            'idnumber' => isset($listArray['userInfo']['idnumber']) ? $listArray['userInfo']['idnumber'] : '',
            'username' => $listArray['userInfo']['username'],
            'order_id' => $listArray['orderInfo']['orderId'],
            'user_id' => $userId,
            'confirm' => 3,
            'addres' => $listArray['userInfo']['address'],
            'main_deposit' => $listArray['collectionInformation']['MainCollectionInfo']['deposit'],
            'ding' => !empty($listArray['collectionInformation']['MainCollectionInfo']['deposit']) ? $listArray['collectionInformation']['MainCollectionInfo']['depositMoney'] : 0,
            'main_pay_once' => $main_pay_once,
            'main_mode_type' => $mainPayTpe1,
            'main_pay_type' => $mainPayTpe1Bi,
            'main_pay_money' => $mainPayTpe1Money - $mainPayTpe1MoneyNode,
            'main_mode_type1' => $listArray['collectionInformation']['MainCollectionInfo']['pay'] == 1 || $listArray['collectionInformation']['MainCollectionInfo']['pay'] == 2 ? 0 : $mainPayTpe2,
            'main_pay_type1' => $listArray['collectionInformation']['MainCollectionInfo']['pay'] == 1 || $listArray['collectionInformation']['MainCollectionInfo']['pay'] == 2 ? 0 : $mainPayTpe2Bi,
            'main_pay_type1_money' => $listArray['collectionInformation']['MainCollectionInfo']['pay'] == 1 || $listArray['collectionInformation']['MainCollectionInfo']['pay'] == 2 ? 0 : $mainPayTpe2Money - $mainPayTpe1MoneyNode1,
            'categoryName' => $listArray['collectionInformation']['MainCollectionInfo']['pay'] == 1 || $listArray['collectionInformation']['MainCollectionInfo']['pay'] == 2 ? 0 : $categoryName,
            'auxiliary_interactive_id' => $listArray['collectionInformation']['MainCollectionInfo']['pay'] == 1 || $listArray['collectionInformation']['MainCollectionInfo']['pay'] == 2 ? 0 : $auxiliary_interactive_id,
            'combination_name' => $listArray['collectionInformation']['MainCollectionInfo']['pay'] == 1 || $listArray['collectionInformation']['MainCollectionInfo']['pay'] == 2 ? 0 : $combination_name,
            'main_pay_type2' => $listArray['collectionInformation']['MainCollectionInfo']['pay'] == 1 ? 0 : $mainPayTpe3Bi,
            'main_pay_type2_money' =>  $mainPayTpe3Money,
            'main_mode_type2' => $listArray['collectionInformation']['MainCollectionInfo']['pay'] == 1 ? 0 : $mainPayTpe3,
            'purchasing_pay_once' => $purchasing_pay_once,
            'purchasing_mode_type' => $purchasingPayTpe1,
            'purchasing_pay_type' => $purchasingPayTpe1Bi,
            'purchasing_pay__type_money' => $purchasingPayTpe1Money - $purchasingTpe1MoneyNode,
            'purchasing_mode_type1' => $purchasingPayTpe2,
            'purchasing_pay_type1' => $purchasingPayTpe2Bi,
            'purchasing_pay_type1_money' => $purchasingPayTpe2Money - $purchasingTpe1MoneyNode1,
            'additional' => $listArray['collectionInformation']['MainCollectionInfo']['additionalTerms'],
            'additional_agent' => $listArray['collectionInformation']['PurchasingInfo']['additionalTerms'],
            'engineering' => implode(',', $projectContent),
            'con_time' => time(),
            'contract' => $op['order_no'],
            'overdue_days' => isset($listArray['contractInfo']['moneyDay']) ? $listArray['contractInfo']['moneyDay'] : 0,
            'purchasing_mode_type2' => $purchasingPayTpe3Bi,
            'purchasing_pay_type2_money' =>$purchasingPayTpe3Money,
        ];
        if (!empty($op['sign_id'])) {
            $sign['sign_id'] = $op['sign_id'];
            
            $this->isUpdate(true)->save($sign);
        } else {
            $this->isUpdate(false)->save($sign);
        }
        $sign['main_round_discount']       = $main_round_discount;
        $sign['purchasing_round_discount'] = $purchasing_round_discount;
        $listDate['addres']=$listArray['userInfo']['address'];
        $listDate['contacts']=$listArray['userInfo']['username'];
        if(isset($listArray['userInfo']['addressInfo'])){
        $listDate['residential_quarters']=$listArray['userInfo']['addressInfo']['communityName'];
        $listDate['building']=($listArray['userInfo']['addressInfo']['building'] !== '0') ? $listArray['userInfo']['addressInfo']['building'] : '';
        $listDate['unit']=($listArray['userInfo']['addressInfo']['unit'] !== '0') ? $listArray['userInfo']['addressInfo']['unit'] : '';
        $listDate['room']=($listArray['userInfo']['addressInfo']['room'] !== '0') ? $listArray['userInfo']['addressInfo']['room'] : '';
        $listDate['addres']=$listArray['userInfo']['addressInfo']['communityName'];
            if($listDate['building'] != ''){
                $listDate['addres']=$listDate['addres'].$listDate['building'].'栋';
            }
            if($listDate['unit'] !=''){
                $listDate['addres']=$listDate['addres'].$listDate['unit'].'单元';
            }
            if($listDate['room']  !=''){
                $listDate['addres']=$listDate['addres'].$listDate['room'];
            }
        }
        db('order')->where('order_id', $listArray['orderInfo']['orderId'])->update($listDate);
        return $sign;
    }
    
    
}
