<?php

namespace app\api\model;

use constant\config;
use think\Model;

class ProductsV2 extends Model
{
    protected $table = 'products_v2';
    protected $connection = 'database.zong';
    protected $createTime = 'create_time';

    public function productsTag()
    {
        
        return $this->hasMany('products_v2_tag', 'products_id','id')->field('products_tag,id,products_id');
    }
    public function productsContent()
    {
        
        return $this->hasMany('products_v2_content', 'products_id','id')->field('products_content_title,id,products_content_value,products_id');
    }

    public function productsSpec()
    {
        return $this->hasMany('products_v2_spec', 'products_id','id')->field('round(products_spec_price,2) as price,products_id,products_spec_title,is_required_module');
    }
    public function productsUnit()
    {
        return $this->hasMany('Unit', 'id','products_unit')->field('title,id');
    }
    
    public function productsPosition()
    {
        return $this->hasMany('products_v2_position', 'products_id','id');
    }
    
    
    public function productsImg()
    {
        return $this->hasMany('products_v2_images', 'products_id','id');
    }
    
   

   


}
