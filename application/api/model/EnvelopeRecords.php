<?php

namespace app\api\model;

use constant\config;
use think\Model;

class EnvelopeRecords extends Model
{
    protected $table = 'envelope_records';
    protected $autoWriteTimestamp = true;
    protected $createTime = 'creation_time';
    protected $updateTime = false;
    public function record($list)
    {
        $this->saveAll($list);
    }


}
