<?php

namespace app\api\model;

use constant\config;
use think\Model;

class DetailedCategory extends Model
{
    protected $table = 'detailed_category';

    public function detailedList()
    {
         return $this->hasMany('Detailed','detailed_category_id','id')->where('is_compose',1)->where('display', 1)->where('detailed_category_2_id', 0)->whereNull('deleted_at')->field('detailed_id,detailed_title,detailed_category_id,artificial,agency,warranty_years as warrantYears,warranty_text_1 as warrantyText1,warranty_text_2 as exoneration,groupss');
    }
    
    public function detailedListAnge()
    {
        return $this->hasMany('Detailed','detailed_category_2_id','id')->where('is_compose',1)->where('display', 1)->where('detailed_category_2_id', 0)->whereNull('deleted_at')->field('detailed_id,detailed_title,detailed_category_id,artificial,agency,warranty_years as warrantYears,warranty_text_1 as warrantyText1,warranty_text_2 as exoneration,groupss,detailed_category_2_id,user_id as userId,total_price as allMoney,number as square,un_id');
    }

}
