<?php

namespace app\api\model;

use constant\config;
use think\Model;
use think\Cache;
use redis\RedisPackage;

class MyOrderWithdrawal extends Model
{
    protected $table = 'my_order_withdrawal';
    protected $connection='database.zong';



    //判断TOKEN
    public function checkToken($token,$user_id)
    {
    
        $redis         = new RedisPackage();
        if ($token == $redis::get((string)$user_id)) {
            $redis::del((string)$user_id);
            return TRUE;
        } else {
            return FALSE;
        }
    }


}
