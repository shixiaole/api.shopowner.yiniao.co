<?php

namespace app\api\model;

use constant\config;
use think\Model;

class ProductsV2Spec extends Model
{
    protected $table = 'products_v2_spec';
    protected $connection = 'database.zong';
    
    public function schemeProject()
    {
           return $this->hasMany('products_v2_spec_detailed', 'products_spec_id','ids')->field('products_spec_id,projectId,projectMoney,projectTitle,company,agency,to_price,square,remarks,id,projectOptionValueIds,increment_per_unit_volume as unitVolume');
    }
   


}
