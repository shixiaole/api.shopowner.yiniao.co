<?php

namespace app\api\model;

use constant\config;
use think\Model;

class DetailedOption extends Model
{
    protected $table = 'detailed_option';

    public function detailedListOptionValues()
    {
        return $this->hasMany('DetailedOptionValue','option_id')->where('is_enable',1)->whereNull('deleted_at')->field('option_id,option_value_id,option_value_title,type,price,condition,artificial,work_time,sales,desc_tag,desc_text,desc_attachment');
    }

}
