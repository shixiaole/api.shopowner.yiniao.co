<?php

namespace app\api\model;

use Kafka\Producer;
use Think\Db;

/**
 * Created by PhpStorm.
 * User: pandeng
 * Date: 2017-07-26
 * Time: 21:51
 */
class KafkaProducerDelay
{
    private $content = null;
    
    
    public function add($data)
    {
      
          $this->content = $data;
        $this->_config = config('kafka_server');
        $config        = \Kafka\ProducerConfig::getInstance();
        $config->setMetadataRefreshIntervalMs(10000);
        $config->setMetadataBrokerList(config('kafka_server.host'));
        $config->setBrokerVersion('3.0.1');
        $config->setRequiredAck(1);
        $config->setIsAsyn(false);
        $config->setProduceInterval(500);
        $producer = new \Kafka\Producer();
        $result = $producer->send(array(
                array(
                        'topic' => 'delay_cc_task',
                        'value' =>  $this->content,
                        'key' => '',
                ),
        ));
        
        
    }
    
    
    
    
}


