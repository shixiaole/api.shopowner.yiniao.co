<?php

namespace app\api\model;

use constant\config;
use think\Model;

class SchemeProject extends Model
{
    protected $table = 'scheme_project';
    protected $connection = 'database.zong';

    public function specsList()
    {
        return $this->hasMany('SchemeProjectValue', 'scheme_project_id')->field('scheme_project_id,title as option_value_title,option_value_id,condition,price,artificial');
    }
}
