<?php
/**
 * Created by muniao.
 * User: muniao
 * Date: 2018/8/2
 * Time: 13:20
 */

namespace app\api\model;


use constant\config;
use think\Model;
use  work\workWechatCallbackApi;

class Approval extends Model
{
    
    protected $wechatObj;
    //    const PATH='/home/wwwroot/weixiu.yiniaoweb.com/public/app/'; //用户
    const PATH='/data/wwwroot/cd.yiniaoweb.com/public/app/'; //用户
    
    protected function initialize()
    {
        //使用方法
        $enterprise_weChat=db('enterprise_wechat')->order('data_time desc')->find();
        $corpid           ="wwafc9a61f8dd402b1";
        $corpsecret       ="f_fskBOm_dz397xPv8HYKnfLMEoz0hvfwEeY13J-Rbc";
        $agentid          ="3010040";
        $this->wechatObj  =new workWechatCallbackApi($corpid, $corpsecret, $agentid, $enterprise_weChat['access_token'], $enterprise_weChat['data_time']);
    }

    /*
     * 报销审批
     * type等于1 报销审批  等于2 主材报销
     * approval_type  1只抄送   2全部审批
     */
    public function Reimbursement($reimbursementId, $title,$name,$Type=1,$movement=1,$extended_info='',$auto=0)
    {
        $city=Common::city(config('cityId'))['city_id'];
        if ($Type == 4) {
            $types = 2;
        } elseif ($Type == 18) {
            $types = 3;
        } elseif ($Type == 6) {
            $types = 6;
        } elseif ($Type == 8) {
            $types = 7;
        }elseif ($Type == 10) {
            $types = 10;
        }elseif ($Type == 9) {
            $types = 8;
        }elseif ($Type == 20) {
            $types = 9;
        }elseif ($Type == 11) {
            $types = 11;
        }elseif ($Type == 12) {
            $types = 12;
        }elseif($Type == 13) {
            $types = 13;
        }else {
            $types = 1;
        }
        if($movement==5){
            $city=substr($reimbursementId, 0, 3);
        }
        $list=["approval_id"=>$reimbursementId, "type"=>$types, "title"=>$title, "city_id"=>$city, "name"=>$name, 'other'=>0,'create_time'=>time(),'extended_info'=>$extended_info,'auto'=>$auto];
//        $approval_bef=db('approval_bef',config('database.zong'))->where(["approval_id"=>$reimbursementId, "type"=>$types, "title"=>$title, "city_id"=>$city])->find();
//        if(empty($approval_bef)){
//
//        }
        db('approval_bef',config('database.zong'))->insert($list);
//        $list=json_encode(["approval_id"=>$reimbursementId, "type"=>$types, "title"=>$title, "city_id"=>$city, "name"=>$name, 'other'=>0]);
//        $client=new KafkaProducer();
//        $client->add($list,'slave_addErProval');
    }
    
    public function Purchase($id, $title,$username, $other)
    {
        $city=config('cityId');
        if($other==1){
            $type=5;
        }else{
            $type=4;
        }
//        $client=new KafkaProducer();
        $list=["approval_id"=>$id, "type"=>$type, "title"=>$title, "city_id"=>$city, "name"=>$username,'other'=>0,'create_time'=>time()];
//        $client->add($list,'slave_addErProval');
        db('approval_bef',config('database.zong'))->insert($list);
    }
    
    function filter_by_value($array, $index, $value)
    {
        $newarray=[];
        if (is_array($array) && count($array) > 0) {
            foreach (array_keys($array) as $key) {
                $temp[$key]=$array[$key][$index];
                if ($temp[$key] == $value) {
                    $newarray[$key]=$array[$key];
                }
            }
        }
        return $newarray;
    }
    
    public function listReimbursement()
    {
        $data = [['name' => '建渣打拆', 'type' => 1,], ['name' => '小材料', 'type' => 2, 'data' => [
            ['type' => 8, 'name' => '泥工辅材'],
            ['type' => 9, 'name' => '木工及墙板材料'],
            ['type' => 10, 'name' => '水电工材料'],
            ['type' => 11, 'name' => '油漆工材料'],
            ['type' => 12, 'name' => '小耗材及小工具'],
            ['type' => 13, 'name' => '小主材'],
        ]], ['name' => '协作人工', 'type' => 3,], ['name' => '代购主材', 'type' => 4], ['type' => 7, 'name' => '运费'], ['type' => 8, 'name' => '泥工辅材'],
            ['type' => 9, 'name' => '木工及墙板材料'],
            ['type' => 10, 'name' => '水电工材料'],
            ['type' => 11, 'name' => '油漆工材料'],
            ['type' => 12, 'name' => '小耗材及小工具'],
            ['type' => 13, 'name' => '小主材']];
        return  $data;
        
    }
}
