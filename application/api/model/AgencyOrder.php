<?php
/**
 * Created by muniao.
 * User: muniao
 * Date: 2018/8/2
 * Time: 13:20
 */

namespace app\api\model;


use think\Db;
use think\Model;
use traits\model\SoftDelete;


class AgencyOrder extends Model
{
    
    protected $table = 'agency_order';
    // 定义时间戳字段名
    protected $createTime = 'create_time';
    protected $updateTime = 'updata_time';
    protected $autoWriteTimestamp = true;
    
    public function info($agencyOrderId)
    {
        $agencyTask     = new AgencyTask();
        $agencyOrder    = $this->where('id', $agencyOrderId)->find();
        if(empty($agencyOrder)){
            return ['code'=>300,'msg'=>"等待设计师操作"];
        }
        $cost           = $agencyTask->where('delete_time', 0)->where('agency_order_id', $agencyOrderId)->field('sum(total_cost) as totalCost,sum(manual_cost) as manualCost,cooperation_type,app_user_start_time')->select();
        //测量时间
        //预计
        $measurementTimeExpectedTo = empty($agencyOrder['measurement_time']) ? '' : date('Y-m-d', $agencyOrder['measurement_time']);
        //实际
        $measurementTimeReality = empty($agencyOrder['measurement_actual_time']) ? '' : date('Y-m-d', $agencyOrder['measurement_actual_time']);
        //状态
        $measurementTimeState = empty($measurementTimeReality) ? 0 : 3;
        //方案完成时间
        //预计
        $solutionCompletionTimeExpectedTo = empty($agencyOrder['solution_completion_time']) ? '' : date('Y-m-d', $agencyOrder['solution_completion_time']);
        //实际
        $solutionCompletionTimeReality = empty($agencyOrder['measurement_actual_time']) ? '' : date('Y-m-d', $agencyOrder['measurement_actual_time']);
        //状态
        $solutionCompletionTimeState = empty($solutionCompletionTimeReality) ? 0 : 3;
        
        //方案确认时间
        //预计
        $storeConfirmTimeExpectedTo = "";
        //实际
        $storeConfirmTimeReality = empty($agencyOrder['store_confirm_time']) ? '' : date('Y-m-d', $agencyOrder['store_confirm_time']);
        //状态
        $storeConfirmTimeState=empty($storeConfirmTimeReality) ?0: 3;
        if($agencyOrder['status']==2){
            $storeConfirmTimeState =1;
        }
        //0未开始1方案确认2去验收3已完成
        
        //供应商产确认时间
        //预计
        $agencyConfirmTimeExpectedTo = "";
        //实际
        $agencyConfirmTimeReality = empty($agencyOrder['agency_confirm_time']) ? '' : date('Y-m-d', $agencyOrder['agency_confirm_time']);
        //状态
        $agencyConfirmTimeState =  empty($agencyConfirmTimeReality) ? 0 : 3;;
       
        
        
        //设计师审核   供应链经理审核
        //预计
        $designerConfirmTimeExpectedTo = "";
        //实际
        $designerConfirmTimeReality = empty($agencyOrder['designer_confirm_time']) ? '' : date('Y-m-d', $agencyOrder['designer_confirm_time']);
        //状态
        $designerConfirmTimeState = empty($designerConfirmTimeReality) ? 0 : 3;
        //供应商排单确认
        //预计
        $schedulingConfirmTimeExpectedTo = empty($agencyOrder['generation_time']) ? '' : date('Y-m-d', $agencyOrder['generation_time']);
        //实际
        $schedulingConfirmTimeReality = empty($agencyOrder['scheduling_confirm_time']) ? '' : date('Y-m-d', $agencyOrder['scheduling_confirm_time']);
        //状态
        $schedulingConfirmTimeState = empty($schedulingConfirmTimeReality) ? 0 : 3;
        
        //配送时间
        //预计
        $deliverableTimeExpectedTo = "";
        //实际
        $deliverableTimeReality = empty($agencyOrder['deliverable_time']) ? '' : date('Y-m-d', $agencyOrder['deliverable_time']);
        //状态
        $deliverableTimeState = empty($deliverableTimeReality) ? 0 : 3;
        //配送完成
        //预计
        $deliveryCompletionTimeExpectedTo = empty($agencyOrder['generation_completion_time']) ? '' : date('Y-m-d', $agencyOrder['generation_completion_time']);
        //实际
        $deliveryCompletionTimeReality = empty($agencyOrder['delivery_completion_time']) ? '' : date('Y-m-d', $agencyOrder['delivery_completion_time']);
        //状态
        $deliveryCompletionTimeState = empty($deliveryCompletionTimeReality) ? 0 : 3;
        
        //安装开始时间
        //预计
        $startAtTimeExpectedTo = "";
        //实际
        $startAtReality = empty($cost[0]['app_user_start_time']) ? '' : date('Y-m-d', $cost[0]['app_user_start_time']);
        //状态
        $startAtTimeState = empty($startAtReality) ? 0 : 3;
        
        //安装完成时间
        //预计
        $completionTimeExpectedTo = empty($agencyOrder['installation_time']) ? '' : date('Y-m-d', $agencyOrder['installation_time']);
        //实际
        $completionTimeReality = empty($agencyOrder['completion_time']) ? '' : date('Y-m-d', $agencyOrder['completion_time']);
        //状态
        $completionTimeState = empty($completionTimeReality) ? 0 : 3;
        
        //店长验收
        //预计
        $acceptancePassTimeExpectedTo = "";
        //实际
        $acceptancePassTimeReality = empty($agencyOrder['acceptance_pass_time']) ? '' : date('Y-m-d', $agencyOrder['acceptance_pass_time']);
        //状态
        $acceptancePassTimeState =0;
        if(empty($agencyOrder['completion_time']) && $agencyOrder['status']==13){
            $acceptancePassTimeState =2;
        }
        if(!empty($agencyOrder['completion_time']) && ($agencyOrder['status']==14 || $agencyOrder['status']==15)){
            $acceptancePassTimeState =3;
        }
       
        
        //供应商结算
        //预计
        $lastPaymentConfirmTimeExpectedTo = "";
        //实际
        $lastPaymentConfirmTimeReality = empty($agencyOrder['last_payment_confirm_time']) ? '': date('Y-m-d', $agencyOrder['last_payment_confirm_time']);
        //状态
        $lastPaymentConfirmTimeState = empty($lastPaymentConfirmTimeReality) ? 0 : 3;
        
        
        $list = [
            [
                'title' => "测量及方案确认",
                'data' => [
                    [
                        'title' => "测量时间:",
                        'state' => $measurementTimeState,
                        'estimatedTime' => $measurementTimeExpectedTo,//预计时间
                        'completeTime' => $measurementTimeReality,//完成时间
                    ],
                    [
                        'title' => "方案完成时间:",
                        'state' => $solutionCompletionTimeState,
                        'estimatedTime' => $solutionCompletionTimeExpectedTo,//预计时间
                        'completeTime' => $solutionCompletionTimeReality,//完成时间
                    ],
                    [
                        'title' => "方案确认时间:",
                        'state' => $storeConfirmTimeState,
                        'estimatedTime' => $storeConfirmTimeExpectedTo,//预计时间
                        'completeTime' => $storeConfirmTimeReality,//完成时间
                        'img' => empty($agencyOrder['attachment']) ? null : json_decode($agencyOrder['attachment'], true),
                        "totalCost" =>  $cost[0]['totalCost'],
                        "manualCost" => $cost[0]['manualCost'],
                    ],
                    
                ],
                
            ],
            [
                'title' => "下单及生产",
                'data' => [
                    [
                        'title' => "供应商确认:",
                        'state' => $agencyConfirmTimeState,
                        'estimatedTime' => $agencyConfirmTimeExpectedTo,//预计时间
                        'completeTime' => $agencyConfirmTimeReality,//完成时间
                    ],
                    [
                        'title' => "设计师审核:",
                        'state' => $designerConfirmTimeState,
                        'estimatedTime' => $designerConfirmTimeExpectedTo,//预计时间
                        'completeTime' => $designerConfirmTimeReality,//完成时间
                    ],
                    [
                        'title' => "供应链经理审核:",
                        'state' => $designerConfirmTimeState,
                        'estimatedTime' => $designerConfirmTimeExpectedTo,//预计时间
                        'completeTime' => $designerConfirmTimeReality,//完成时间
                    ], [
                        'title' => "供应商排单确认:",
                        'state' => $schedulingConfirmTimeState,
                        'estimatedTime' => $schedulingConfirmTimeExpectedTo,//预计时间
                        'completeTime' => $schedulingConfirmTimeReality,//完成时间
                    ],
                
                
                ],
            ],
            [
                'title' => "配送及施工",
                'data' => [
                    [
                        'title' => "配送时间:",
                        'state' => $deliverableTimeState,
                        'estimatedTime' => $deliverableTimeExpectedTo,//预计时间
                        'completeTime' => $deliverableTimeReality,//完成时间
                    ],
                    [
                        'title' => "配送完成:",
                        'state' => $deliveryCompletionTimeState,
                        'estimatedTime' => $deliveryCompletionTimeExpectedTo,//预计时间
                        'completeTime' => $deliveryCompletionTimeReality,//完成时间
                    ],
                    [
                        'title' => "安装开始时间:",
                        'state' => $startAtTimeState,
                        'estimatedTime' => $startAtTimeExpectedTo,//预计时间
                        'completeTime' => $startAtReality,//完成时间
                    ],
                    ['title' => "安装完成时间:",
                        'state' => $completionTimeState,
                        'estimatedTime' => $completionTimeExpectedTo,//预计时间
                        'completeTime' => $completionTimeReality,//完成时间
                    ],
                    [
                        'title' => "店长验收:",
                        'state' => $acceptancePassTimeState,
                        'estimatedTime' => $acceptancePassTimeExpectedTo,//预计时间
                        'completeTime' => $acceptancePassTimeReality,//完成时间
                    ],
                ]
            ],
            [
                'title' => "结算",
                'data' => [
                    [
                        'title' => "供应商结算:",
                        'state' => $lastPaymentConfirmTimeState,
                        'estimatedTime' => $lastPaymentConfirmTimeExpectedTo,//预计时间
                        'completeTime' => $lastPaymentConfirmTimeReality,//完成时间
                    ],
                ]
            ]
        ];
        foreach ($list as $k => $item) {
            foreach ($item['data'] as $key => $v) {
            
                if ($cost[0]['cooperation_type'] == 1 && $v['title'] == "供应链经理审核:") {
                    unset($list[$k]['data'][$key]);
                   
                }
            }
            $list[$k]['data']=array_merge($list[$k]['data']);
        }
        return ['code'=>200,'data'=>$list];
        
    }
    
}





