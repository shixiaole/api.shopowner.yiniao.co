<?php

namespace app\api\model;

use constant\config;
use think\Model;

class ProductsFamily extends Model
{
    protected $table = 'products_family';
    protected $connection = 'database.zong';
    
    
    public function productsFamilyCategory()
    {
        return $this->hasMany('products_family_category', 'products_family_id','id');
    }


}
