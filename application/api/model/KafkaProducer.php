<?php

namespace app\api\model;

use Kafka\Producer;
use Think\Db;


/**
 * Created by PhpStorm.
 * User: pandeng
 * Date: 2017-07-26
 * Time: 21:51
 */
class KafkaProducer
{
    private $content = null;


    public function add($data,$topic)
    {
        
        $this->content = $data;
        $this->_config = config('kafka_server');
        $config        = \Kafka\ProducerConfig::getInstance();
        $config->setMetadataRefreshIntervalMs(10000);
        $config->setMetadataBrokerList(config('kafka_server.host'));
        $config->setBrokerVersion('3.0.1');
        $config->setRequiredAck(1);
        $config->setIsAsyn(false);
        $config->setProduceInterval(500);
        $producer = new \Kafka\Producer();
        $result = $producer->send(array(
                array(
                        'topic' => $topic,
                        'value' =>  $this->content,
                        'key' => '',
                ),
        ));
    
        // $producer = new \Kafka\Producer(function () use($topic){

        //     return array(
        //         array(
        //             'topic' => $topic,
        //             'value' => $this->content,
        //             'key' => '',
        //         ),
        //     );
        // });

        //  $producer->success(function ($result) use ($data) {
        //      db('log',config('database.zong'))->insertGetId(['admin_id' => 0, 'created_at' => time(), 'content' =>$data,'type' => 2, 'msg'=>'发送成功']);
        //     // Db::connect(config('database.zong'))->table('log')->insertGetId(['admin_id' => 0, 'created_at' => time(), 'content' =>$data,'type' => 2, 'msg'=>'发送成功']);
        // });
        
        // $producer->error(function ($errorCode)use ($data) {
        //      db('log',config('database.zong'))->insertGetId(['admin_id' => 0, 'created_at' => time(), 'content' =>$data,'type' => 2, 'msg'=>'发送失败']);
        //     // Db::connect(config('database.zong'))->table('log')->insertGetId(['admin_id' => 0, 'created_at' => time(), 'content' =>$data,'type' => 2, 'msg'=>'发送失败']);
            
        // });

        // $producer->send(true);
    }




}


