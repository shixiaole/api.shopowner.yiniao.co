<?php

namespace app\api\model;

use constant\config;
use think\Model;

class DetailedWorkProcess extends Model
{
    protected $table = 'detailed_work_process';
    
    public function duration($companyJson, $type = 1, $subordinate = 0)
    {
        $work_process_template = db('work_process_template', config('database.zong'))->where('city_id', config('cityId'))->where('delete_time', 0)->where('status', 1)->find();
        if (empty($work_process_template)) {
            r_date(null, 300, '暂未配置工序关联');
        }
        $content                    = json_decode($work_process_template['content'], true);
        $work_process_template_rule = db('work_process_template_rule', config('database.zong'))->where('work_process_template_id', $work_process_template['id'])->select();
        if (empty($work_process_template_rule)) {
            r_date(null, 300, '暂未配置工序关联');
        }
        $agency = [];
        $main   = [];
        foreach ($companyJson as $k => $v) {
            $detailedList = [];
            if ($v['agency'] == 1) {
                $detailedList   = [];
                $detailedList[] = [
                    'std_construction_volume' => db('detailed')->where('detailed_id', $v['product_id'])->value('delivery_period_days_2'),
                    'work_process_id' => db('detailed')->where('detailed_id', $v['product_id'])->join('detailed_category', 'detailed_category.id=detailed.detailed_category_id')->where('pid', 0)->value('detailed_category.id'),
                    'dry_period_days' => 0
                
                ];
                
            }
            if ($v['agency'] == 0) {
                $detailed = $this->where('detailed_id', $v['product_id'])->field('work_process_id,volume_coefficient')->select();
                
                foreach ($detailed as $k1 => $o) {
                    $std_construction_volume = db('work_process', config('database.zong'))->where('id', $o['work_process_id'])->field('std_construction_volume,dry_period_days')->find();
                    if ($std_construction_volume['std_construction_volume'] == "0.0") {
                        $std_construction_volume = 0;
                    }
                    $fang=$v['fang'];
                    if($o['volume_coefficient'] !='0.0'){
                        $fang=round($fang*$o['volume_coefficient'],2);
                        
                    }
                    $detailedList[$k1]['std_construction_volume'] = empty($std_construction_volume) ? 0 : (string)round($fang / $std_construction_volume['std_construction_volume'], 2);
                    $detailedList[$k1]['work_process_id']         = $o['work_process_id'];
                    $detailedList[$k1]['dry_period_days']         = $std_construction_volume['dry_period_days'];
                }
                $result = [];
                
                foreach ($detailedList as $item) {
                    $work_process_id = $item['work_process_id'];
                    $std_construction_volume = $item['std_construction_volume'];
                    $dry_period_days=$item['dry_period_days'];
                    if (isset($result[$work_process_id])) {
                        $result[$work_process_id]['std_construction_volume'] += $std_construction_volume;
                    } else {
                        $result[$work_process_id] = [
                            'work_process_id' => $work_process_id,
                            'std_construction_volume' => $std_construction_volume,
                            'dry_period_days' => $dry_period_days
                        ];
                    }
                }
                $final_result = array_values($result);
                $detailedList=$final_result;
            }
            $companyJson[$k]['work_process_list'] = $detailedList;
        }
        foreach ($content['work_process_tree'] as  $k=>$item){
            $content['work_process_tree'][$k]['agency']=0;
        }
        foreach ($content['detailed_category_tree'] as  $k=>$item){
            $content['detailed_category_tree'][$k]['agency']=1;
        }
        $content1 = array_merge($content['work_process_tree'], $content['detailed_category_tree']);

        if ($type == 1 && $subordinate == 0) {
            $content = $content1;
            foreach ($content as $k => $item) {
                foreach ($item['list'] as $k1 => $value) {
                    foreach ($work_process_template_rule as $val) {
                        if ($value['id'] == $val['work_process_id']) {
                            $work_process_content   = json_decode($val['content'], true);
                            $work_process_tree      = $work_process_content['work_process_tree'];
                            $detailed_category_tree = $work_process_content['detailed_category_tree'];
                            $p                      = [];
                            if ($val['type'] == 1) {
                                foreach ($work_process_tree as $o) {
                                    if (!empty($o['list'])) {
                                        foreach ($o['list'] as $o1) {
                                            if ($o1['checked']) {
                                                $p[] = [
                                                    'title' => $value['title'],
                                                    'value' => $o1['value'],
                                                    'agency' => 0,
                                                    'id' => $o1['id'],
                                                ];
                                            }
                                        }
                                    }
                                    
                                }
                            } else {
                                foreach ($detailed_category_tree as $o) {
                                    $p[] = [
                                        'title' => '',
                                        'value' => $o['value'],
                                        'agency' => 1,
                                        'id' => $o['id'],
                                    ];
                                }
                                
                            }
                            
                            $content[$k]['list'][$k1]['data2'] = $p;
                        }
                        
                    }
                }
            }
            foreach ($content as $k => $value) {
                $list  = [];
                $list1 = [];
                foreach ($companyJson as $item) {
                    if ($value['list']) {
                        foreach ($value['list'] as $key => $i) {
                            foreach ($item['work_process_list'] as $s) {
                                if ($i['id'] == $s['work_process_id']) {
                                    if ($i['sort'] < 10) {
                                        $i['sort'] = '0' . $i['sort'];
                                    }
                                    $sort   = $value['sort'] . '.' . $i['sort'];
                                    $groupingTitle=$i['title'] . '(' . $item['categoryName'] . ')/' . $item['fang'];
                                    if($item['agency']==1){
                                        $groupingTitle=$item['projectTitle'] . '(' . $item['categoryName'] . ')/' . $item['fang'];
                                      
                                    }
                                    if($item['agency'] == 0 && $value['agency']==0){
                                        $list[] = [
                                            'categoryName' => $item['categoryName'],
                                            'std_construction_volume' => $s['std_construction_volume'],
                                            'dry_period_days' => $s['dry_period_days'],
                                            'title' => $i['title'],
                                            'sort' => $sort,
                                            'fang' => $item['fang'],
                                            'value' => 0,
                                            'groupingTitle' =>$groupingTitle,
                                            'agency' => $item['agency'],
                                            'id' => $i['id'],
                                        ];
                                    }elseif ($item['agency'] == 1 && $value['agency']==1){
                                        $list[] = [
                                            'categoryName' => $item['categoryName'],
                                            'std_construction_volume' => $s['std_construction_volume'],
                                            'dry_period_days' => $s['dry_period_days'],
                                            'title' => $i['title'],
                                            'sort' => $item['product_id'],
                                            'fang' => $item['fang'],
                                            'value' => 0,
                                            'groupingTitle' =>$groupingTitle,
                                            'agency' => $item['agency'],
                                            'id' => $i['id'],
                                        ];
                                    }
                   
                                    
                                }
                            }
                        }
                    }
                }
                $work_process_treeData = array_merge($list, $list1);
                $work_process_treeData = arraySort($work_process_treeData, 'sort', SORT_ASC);
                $content[$k]['data']   = $work_process_treeData;
                if (empty($content[$k]['data'])) {
                    unset($content[$k]);
                }
            }
            $content = array_merge($content);

//            foreach ($content as $k=>$o) {
//                foreach ($o['list'] as $h => $u) {
//                    foreach ($o['data'] as $y) {
//                        if ($u['id'] != $y['id']) {
//                            unset($content[$k]['list'][$h]);
//                        }
//                    }
//                }
//                $content[$k]['list']=array_merge($content[$k]['list']);
//            }
            
            for ($i = count($content); $i >= 0; $i--) {
                if ($i > 0) {
                    foreach ($content[$i - 1]['data'] as $l => $item) {
                        if ($l == 0 && ($i - 2) >= 0) {
                            foreach ($content[$i - 2]['list'] as $o) {
                                foreach ($o['data2'] as $g) {
                                    if ($g['id'] == $item['id']) {
                                        $content[$i - 1]['data'][$l]['value'] = $g['value'];
                                    }
                                }
                                
                            }
                            
                        } else {
                            if (($l - 1) >= 0 && $item['sort'] > $content[$i - 1]['data'][$l - 1]['sort']) {
                                foreach ($content[$i - 1]['list'] as $o) {
                                    foreach ($o['data2'] as $g) {
                                        if ($g['id'] == $item['id']) {
                                            $content[$i - 1]['data'][$l]['value'] = $g['value'];
                                        }
                                    }
                                    
                                }
                            } else {
                                if (($l - 1) >= 0 && $item['sort'] == $content[$i - 1]['data'][$l - 1]['sort']) {
                                    $content[$i - 1]['data'][$l]['value'] = $content[$i - 1]['data'][$l - 1]['value'];
                                } else {
                                    $content[$i - 1]['data'][$l]['value'] = 0;
                                }
                                
                                
                            }
                        }
                        
                        
                    }
                }
                
            }
            
        }
        if ($type == 2 && $subordinate == 0) {
            $content = $content1;
            foreach ($content as $k => $value) {
                $list = [];
                foreach ($value['list'] as $key => $i) {
                    foreach ($companyJson as $item) {
                        foreach ($item['work_process_list'] as $g) {
                            if ($i['id'] == $g['work_process_id']) {
                                $sort = 0;
                                if ($i['sort'] < 10) {
                                    $sort = '0' . $i['sort'];
                                }
                                $sort = $value['sort'] . '.' . $sort;
                                if ($item['agency'] == 0 && $value['agency']==0) {
                                    $list[] = [
                                        'categoryName' => $item['categoryName'],
                                        'title' => $i['title'],
                                        'fang' => $item['fang'],
                                        'sort' => $sort,
                                        'groupingTitle' => $i['title'] . '(' . $item['categoryName'] . ')/' . $item['fang'] . $item['unit'],
                                        'std_construction_volume' => $g['std_construction_volume'],
                                        'dry_period_days' => $g['dry_period_days'],
                                        'agency' => $item['agency'],
                                        'value' => 0,
                                        'id' => $i['id'],
                                    ];
                                } elseif ($item['agency'] == 1 && $value['agency']==1) {
                                    $list[] = [
                                        'categoryName' => $item['categoryName'],
                                        'title' => $i['title'],
                                        'fang' => $item['fang'],
                                        'sort' => $item['product_id'],
                                        'groupingTitle' => $item['projectTitle'] . '(' . $item['categoryName'] . ')/' . $item['fang'],
                                        'std_construction_volume' => $g['std_construction_volume'],
                                        'dry_period_days' => $g['dry_period_days'],
                                        'agency' => $item['agency'],
                                        'value' => 0,
                                        'id' => $i['id'],
                                    ];
                                }
                                
                            }
                        }
                        $content[$k]['data'] = array_merge($list);
                        if (empty($content[$k]['data'])) {
                            unset($content[$k]);
                        }
                        
                    }
                }
            }
            
        }
        if ($type == 1 && $subordinate == 1) {
            $content = $content['detailed_category_tree'];
            foreach ($content as $k => $value) {
                $list = [];
                foreach ($value['list'] as $key => $i) {
                    foreach ($companyJson as $item) {
                        foreach ($item['work_process_list'] as $g) {
                            if ($i['id'] == $g['work_process_id'] && $item['agency'] == 1) {
                                $sort = 0;
                                if ($i['sort'] < 10) {
                                    $sort = '0' . $i['sort'];
                                }
                                $sort   = $value['sort'] . '.' . $sort;
                                $list[] = [
                                    'categoryName' => $item['categoryName'],
                                    'title' => $i['title'],
                                    'fang' => $item['fang'],
                                    'sort' => $sort,
                                    'groupingTitle' => $i['title'] . '(' . $item['categoryName'] . ')',
                                    'std_construction_volume' => $g['std_construction_volume'],
                                    'dry_period_days' => $g['dry_period_days'],
                                    'agency' => $item['agency'],
                                    'value' => 0,
                                    'id' => $i['id'],
                                ];
                            }
                        }
                        $content[$k]['data'] = array_merge($list);
                        if (empty($content[$k]['data'])) {
                            unset($content[$k]);
                        }
                        
                    }
                }
            }
            
        }
        if ($type == 2 && $subordinate == 1) {
            $content = $content['detailed_category_tree'];
            foreach ($content as $k => $value) {
                $list = [];
                foreach ($value['list'] as $key => $i) {
                    foreach ($companyJson as $item) {
                        foreach ($item['work_process_list'] as $g) {
                            if ($i['id'] == $g['work_process_id'] && $item['agency'] == 1) {
                                $sort = 0;
                                if ($i['sort'] < 10) {
                                    $sort = '0' . $i['sort'];
                                }
                                $sort   = $value['sort'] . '.' . $sort;
                                $list[] = [
                                    'categoryName' => $item['categoryName'],
                                    'title' => $i['title'],
                                    'fang' => $item['fang'],
                                    'sort' => $sort,
                                    'groupingTitle' => $i['title'] . '(' . $item['categoryName'] . ')',
                                    'std_construction_volume' => $g['std_construction_volume'],
                                    'dry_period_days' => $g['dry_period_days'],
                                    'agency' => $item['agency'],
                                    'value' => 0,
                                    'id' => $i['id'],
                                ];
                            }
                        }
                        $content[$k]['data'] = array_merge($list);
                        if (empty($content[$k]['data'])) {
                            unset($content[$k]);
                        }
                        
                    }
                }
            }
            
        }
        
        $content=array_merge($content);
        $listArray = [];
        foreach ($content as $o) {
            if(isset($o['data'])){
                foreach ($o['data'] as $o1) {
                    array_push($listArray, $o1);
                }
            }
            
        }
        $listArray = arraySort($listArray, 'sort', SORT_ASC);
    
        return $listArray;
    }
}
