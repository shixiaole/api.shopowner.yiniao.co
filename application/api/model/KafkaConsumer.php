<?php

namespace app\api\model;
use Kafka\ConsumerConfig;
class KafkaConsumer
{


    public function run(){
        $config = ConsumerConfig::getInstance();
        $config->setMetadataRefreshIntervalMs(1000);
        $config->setMetadataBrokerList(config('kafka_server.host'));
        $config->setGroupId('test');
        $config->setBrokerVersion('3.0.1');
        $config->setTopics(array(config('kafka_server.topic')));
//        $config->setOffsetReset('earliest');
        $consumer = new \Kafka\Consumer();

        $consumer->start(function($topic, $part, $message) {
            var_dump($message);
        });

    }



}