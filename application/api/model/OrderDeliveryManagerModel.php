<?php
/**
 * Created by muniao.
 * User: muniao
 * Date: 2018/8/2
 * Time: 13:20
 */

namespace app\api\model;


use think\Model;


class OrderDeliveryManagerModel extends Model
{
    protected $table = 'order_delivery_manager';
    protected $autoWriteTimestamp = true;
    protected $createTime = 'created_time';            //数据添加的时候，create_time 这个字段不自动写入时间戳
    protected $updateTime = false;
    public static $snakeAttributes = false;
    
    /*
     * 弹窗选择交付负责人
     */
    public function StartupDeliverer($orderId)
    {
        $list = $this->where('order_id', $orderId)
            ->join('user', 'user.user_id=order_delivery_manager.user_id', 'left')
            ->field('if(order_delivery_manager.assign_time=0,"",FROM_UNIXTIME(order_delivery_manager.assign_time,"%Y-%m-%d ")) as assignTime,if(order_delivery_manager.taking_orders_time=0,"暂未接单",FROM_UNIXTIME(order_delivery_manager.taking_orders_time,"%Y-%m-%d ")) as takingOrdersTime,order_delivery_manager.state,user.username')
            ->order('id desc')->where('delete_time', 0)->find();
        return empty($list) ? null : $list;
    }
    
    /*
   * 弹窗提示是否有交付负责人
   */
    public function IsStartupDeliverer($orderId, $userId, $username)
    {
        $list = $this->where('order_id', $orderId)
            ->where('user_id', $userId)
            ->where('state', 0)
            ->field('if(order_delivery_manager.assign_time=0,"",FROM_UNIXTIME(order_delivery_manager.assign_time,"%Y-%m-%d ")) as assignTime,if(order_delivery_manager.taking_orders_time=0,"暂未接单",FROM_UNIXTIME(order_delivery_manager.taking_orders_time,"%Y-%m-%d ")) as takingOrdersTime,order_delivery_manager.state')
            ->order('id desc')->where('delete_time', 0)->find();
        return empty($list) ? '' : '【销售店长' . $username . '】于【' . $list['assignTime'] . '】将订单指派给您，请选择接单或拒绝，拒绝后订单将退回。';
    }
    
    /*
     * 确认接单
     */
    public function OrderConfirm($orderId, $userId, $state)
    {
        $list = $this->where('order_id', $orderId)
            ->where('order_id', $orderId)
            ->where('user_id', $userId)
            ->where('state',0)
            ->update(['state' => $state, 'taking_orders_time' => time()]);
        return true;
    }
    
    /*
    * 派单接单
    */
    public function addOrderDeliveryManager($orderId, $userId)
    {
        $list = $this->insert(['order_id' => $orderId,
            'assign_time' => time(),
            'user_id' => $userId,
            'created_time' => time()
        ]);
        return true;
    }
}





