<?php

namespace app\api\model;

use constant\config;
use think\Model;

class RoutineUsage extends Model
{
    protected $table = 'routine_usage';

    public function routineCart()
    {


        return $this->hasMany('routine_cart', 'id')->field('FROM_UNIXTIME(adopt,"%Y-%m-%d %H:%i") as adopt,id,invname,measname,invclcode,chooseNumber,status,content,purchase_car,original_number');
    }

}
