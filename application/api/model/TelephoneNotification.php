<?php

namespace app\api\model;

use AlibabaCloud\SDK\Dyplsapi\V20170525\Dyplsapi;
use Darabonba\OpenApi\Models\Config;
use AlibabaCloud\SDK\Dyplsapi\V20170525\Models\BindAxgRequest;
use AlibabaCloud\SDK\Dyplsapi\V20170525\Models\CreateAxgGroupRequest;
use AlibabaCloud\SDK\Dyplsapi\V20170525\Models\OperateAxgGroupRequest;
use AlibabaCloud\SDK\Dyplsapi\V20170525\Models\UpdateSubscriptionRequest;
use AlibabaCloud\SDK\Dyplsapi\V20170525\Models\GetSecretAsrDetailRequest;
use AlibabaCloud\Tea\Utils\Utils\RuntimeOptions;

class TelephoneNotification
{
    
    /**
     * 使用AK&SK初始化账号Client
     * @param string $accessKeyId
     * @param string $accessKeySecret
     * @return Dyplsapi Client
     */
    public static function createClient($accessKeyId, $accessKeySecret)
    {
        $config = new Config([
            // 您的 AccessKey ID
            "accessKeyId" => $accessKeyId,
            // 您的 AccessKey Secret
            "accessKeySecret" => $accessKeySecret
        ]);
        // 访问的域名
        $config->endpoint = "dyplsapi.aliyuncs.com";
        return new Dyplsapi($config);
    }
    
    /**
     * @param string[] $args
     * @return void
     */
    public static function main($args)
    {
        $client  = self::createClient("LTAI4FtUWnzVJMvT5mxSRh5k", "IGPGxXkIMHgSy2wexMyPkvTnP942Wj");
        $poolKey = 0;
        if (\config('city') == 'cd') {
            $poolKey = 'FC100000164842776';
        } elseif (\config('city') == 'cq' || \config('city') == 'bj') {
            $poolKey = 'FC100000165414388';
        }elseif (\config('city') == 'wh' || \config('city') == 'gz' || \config('city') == 'fs') {
            $poolKey = 'FC100000165448368';
        }elseif (\config('city') == 'gy' || \config('city') == 'sh' ) {
            $poolKey = 'FC100000165584386';
        }elseif ( \config('city') == 'sz' ) {
            $poolKey = 'FC100000165560378';
        }
        
        if (empty($poolKey)) {
            return ['code' => 300];
        }
        $bindAxgRequest = new BindAxgRequest([
            "phoneNoX" =>  trim($args['bind_ax_mobile']),
            "expiration" => trim($args['date']),
            "phoneNoB" => trim($args['telephone']),
            "groupId" => trim($args['groupId']),
            "phoneNoA" =>  trim($args['mobile']),
            "isRecordingEnabled" => true,
            "poolKey" => $poolKey,
            "outId" => $args['outId'],
        ]);
        
        $runtime = new RuntimeOptions([]);
        
        $data = $client->bindAxgWithOptions($bindAxgRequest, $runtime);
        if ($data->body->code == "OK") {
            $code = ['code' => 200, 'data' => ['extension' => $data->body->secretBindDTO->extension, 'secretNo' => $data->body->secretBindDTO->secretNo, 'subsId' => $data->body->secretBindDTO->subsId]];
        } else {
            $code = ['code' => 300, 'msg' => $data->body->code];
        }
        return $code;
    }
    
    public static function addNumbers($args)
    {
        $client  = self::createClient("LTAI4FtUWnzVJMvT5mxSRh5k", "IGPGxXkIMHgSy2wexMyPkvTnP942Wj");
        $poolKey = 0;
        if (\config('city') == 'cd') {
            $poolKey = 'FC100000164842776';
        } elseif (\config('city') == 'cq' || \config('city') == 'bj') {
            $poolKey = 'FC100000165414388';
        }elseif (\config('city') == 'wh' || \config('city') == 'gz' || \config('city') == 'fs') {
            $poolKey = 'FC100000165448368';
        }elseif (\config('city') == 'gy' || \config('city') == 'sh' ) {
            $poolKey = 'FC100000165584386';
        }elseif ( \config('city') == 'sz' ) {
            $poolKey = 'FC100000165560378';
        }
        
        if (empty($poolKey)) {
            return ['code' => 300];
        }
        
        $operateAxgGroupRequest = new OperateAxgGroupRequest([
            "groupId" => trim($args['groupId']),
            "operateType" => "addNumbers",
            "numbers" => trim($args['telephone']),
            "poolKey" => $poolKey
        ]);
        $runtime = new RuntimeOptions([]);
        $data = $client->operateAxgGroupWithOptions($operateAxgGroupRequest, $runtime);
        if ($data->body->code == "OK") {
            return true;
        } else {
            return false;
        }
        
    }
    
    public static function UpdateSubscription($args)
    {
        $client  = self::createClient("LTAI4FtUWnzVJMvT5mxSRh5k", "IGPGxXkIMHgSy2wexMyPkvTnP942Wj");
        $poolKey = 0;
        if (\config('city') == 'cd') {
            $poolKey = 'FC100000164842776';
        } elseif (\config('city') == 'cq' || \config('city') == 'bj') {
            $poolKey = 'FC100000165414388';
        }elseif (\config('city') == 'wh' || \config('city') == 'gz' || \config('city') == 'fs') {
            $poolKey = 'FC100000165448368';
        }elseif (\config('city') == 'gy' || \config('city') == 'sh' ) {
            $poolKey = 'FC100000165584386';
        }elseif ( \config('city') == 'sz' ) {
            $poolKey = 'FC100000165560378';
        }
        
        if (empty($poolKey)) {
            return ['code' => 300];
        }
        $updateSubscriptionRequest = new UpdateSubscriptionRequest([
            "poolKey" => $poolKey,
            "phoneNoA" => trim($args['mobile']),
            "phoneNoB" => trim($args['telephone']),
            "subsId" =>  trim($args['subsId']),
            "phoneNoX" => trim($args['bind_ax_mobile']),
            "groupId" => trim($args['groupId']),
            "isRecordingEnabled" => true,
            "operateType" => "updateNoB",
            "outId" => trim($args['outId']),
        ]);
        $runtime = new RuntimeOptions([]);
        $data = $client->updateSubscriptionWithOptions($updateSubscriptionRequest, $runtime);
        if ($data->body->code == "OK") {
            return true;
        } else {
            return false;
        }
        
    }
    public static function CreateAxgGroup($user){
        $client  = self::createClient("LTAI4FtUWnzVJMvT5mxSRh5k", "IGPGxXkIMHgSy2wexMyPkvTnP942Wj");
        $poolKey = 0;
        if (\config('city') == 'cd') {
            $poolKey = 'FC100000164842776';
        } elseif (\config('city') == 'cq' || \config('city') == 'bj') {
            $poolKey = 'FC100000165414388';
        }elseif (\config('city') == 'wh' || \config('city') == 'gz' || \config('city') == 'fs') {
            $poolKey = 'FC100000165448368';
        }elseif (\config('city') == 'gy' || \config('city') == 'sh' ) {
            $poolKey = 'FC100000165584386';
        }elseif ( \config('city') == 'sz' ) {
            $poolKey = 'FC100000165560378';
        }
        
        if (empty($poolKey)) {
            return ['code' => 300];
        }
        
        $operateAxgGroupRequest = new CreateAxgGroupRequest([
            "poolKey" => $poolKey,
            "name" => $user['username'],
            "remark" =>  "店长user_id".$user['user_id'],
        ]);
        $runtime = new RuntimeOptions([]);
        $data = $client->createAxgGroupWithOptions($operateAxgGroupRequest, $runtime);
        if ($data->body->code == "OK") {
            return $data->body->groupId;
        } else {
            return false;
        }
    }
    
    public static function GetSecretAsrDetailRequest($args)
    {
        $client  = self::createClient("LTAI4FtUWnzVJMvT5mxSRh5k", "IGPGxXkIMHgSy2wexMyPkvTnP942Wj");
        $poolKey = 0;
        if (\config('city') == 'cd') {
            $poolKey = 'FC100000164842776';
        } elseif (\config('city') == 'cq' || \config('city') == 'bj') {
            $poolKey = 'FC100000165414388';
        }elseif (\config('city') == 'wh' || \config('city') == 'gz' || \config('city') == 'fs') {
            $poolKey = 'FC100000165448368';
        }elseif (\config('city') == 'gy' || \config('city') == 'sh' ) {
            $poolKey = 'FC100000165584386';
        }elseif ( \config('city') == 'sz' ) {
            $poolKey = 'FC100000165560378';
        }
        
        if (empty($poolKey)) {
            return ['code' => 300];
        }
        $getSecretAsrDetailRequest = new GetSecretAsrDetailRequest([
            "callId" => $args['callId'],
            "callTime" => $args['callTime'],
            "poolKey" => $poolKey
        ]);
        $runtime = new RuntimeOptions([]);
        $data =  $client->getSecretAsrDetailWithOptions($getSecretAsrDetailRequest, $runtime);
        if ($data->body->code == "OK") {
            $sentences=isset($data->body->data->sentences)?$data->body->data->sentences:null;
            return $sentences;
        } else {
            return false;
        }
        
    }
}

