<?php

namespace app\api\model;

use constant\config;
use think\Model;

class User extends Model
{
    protected $table = 'user';

    
    /*
     * 根据电话查电话号码
     */
    public function Userlist($mobile)
    {
        return $this->field('user.user_id,user.mobile,if(user.sales_or_deliverer=1,concat(user.username,"(销售)"),concat(user.username,"(交付)")) as username,user.avatar,user.sex,user.access_token,st.store_name,user.reserve,user.sales_or_deliverer as deliverer')
            ->join('store st', 'user.store_id=st.store_id', 'left')
            ->where(['user.mobile' => $mobile])->select();
    }
    /*
    * 根据电话查电话号码
    */
    public function UserDeliverer()
    {
        return $this->field('user.user_id,user.username')
            ->where(['user.sales_or_deliverer' => 2,'status'=>0,'quit_jobs'=>0,'ce'=>1])->select();
    }
}
