<?php

namespace app\api\model;


use think\model;

class AuditMaterials extends Model
{
    protected $table = 'audit_materials';
    
    
    public function add($data)
    {
        return $this->insertGetId(['prove' => $data['prove'], 'materials_remarks' => $data['materials_remarks'], 'capital_id' => $data['capital_id'], 'user_id' => $data['user_id'], 'creationtime' => time(), 'store_id' => $data['store_id']]);
    }
    
    public function queryFirst(string $capitalId)
    {
        return $this->where('capital_id',$capitalId)->order('materials_id desc')->find();
    }
    
    public function auditEvaluate()
    {
        return $this->hasMany('AuditMaterialsEvaluate','materials_id');
    }
}
