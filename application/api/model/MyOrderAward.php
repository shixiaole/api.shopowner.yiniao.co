<?php

namespace app\api\model;

use constant\config;
use think\Model;
use think\Cache;
use redis\RedisPackage;

class MyOrderAward extends Model
{
    protected $table = 'my_order_award';
    protected $connection = 'database.zong';


    public function Add($data, $ids)
    {
        $id = $this->insertGetId([
            'capital_id' => $ids,//单位
            'title' => $data['option_value_title'],//方量
            'option_value_id' => $data['option_value_id'],//单价
            'condition' => $data['condition'],//规则
            'price' => $data['price'],//规则
            'artificial' => $data['artificial'],//规则
        ]);
        return $id;
    }

    public function list($data)
    {
        $id = $this
            ->join('order', 'order.order_id=my_order_award.order_id', 'left')
            ->join('my_order_withdrawal', 'my_order_withdrawal.my_order_award_id=my_order_award.id', 'left')
            ->join('order_info', 'order_info.order_id=my_order_award.order_id', 'left');
        if (!empty($data['stareTime'])) {
            $start = strtotime(date('Y-m-01 00:00:00', strtotime($data['stareTime'])));//获取指定月份的第一天
            $end   = strtotime(date('Y-m-t 23:59:59', strtotime($data['stareTime']))); //获取指定月份的最后一天
            $id->whereBetween('order.created_time', [$start, $end]);
        }
        if ($data['state'] != 4 && $data['state'] != 20) {
            $id->where('my_order_award.state', $data['state']);
        }

        $list = $id->where('my_order_award.user_id', $data['user_id'])
            ->where('my_order_award.type', 1)
//            ->page($data['page'], $data['limit'])
            ->field('order.contacts,order.order_id,order.telephone,concat(order_info.channel_title,if(ifnull(order_info.channel_details_title,0)=0," ",concat("-",order_info.channel_details_title))) as title,order_info.pro_id_title,FROM_UNIXTIME(order.created_time,"%Y-%m-%d ")as createdTime,
        CASE my_order_award.state
        WHEN 0 THEN
            "未审核"
        WHEN 2 THEN
            concat("审核未通过","(",my_order_award.reason,")") 
            
         WHEN 1 and (my_order_withdrawal.start is null or my_order_withdrawal.start=1) THEN  
            "审核通过"
             WHEN 1 and my_order_withdrawal.start IS NOT NULL and my_order_withdrawal.start =2 THEN
            "审核通过，已提现"
       END as state,
        my_order_award.profit,
        CASE my_order_award.state
        WHEN 0 THEN
           0
        WHEN 2 THEN
            2
            
         WHEN 1 and (my_order_withdrawal.start is null or my_order_withdrawal.start=1) THEN
            3
             WHEN 1 and my_order_withdrawal.start IS NOT NULL and my_order_withdrawal.start =2 THEN
            1
       
        END  as states,ifnull(my_order_withdrawal.amount,0) as amount,my_order_award.commission,my_order_award.deal,if(ifnull(my_order_withdrawal.uptme,0)!=0,FROM_UNIXTIME(my_order_withdrawal.uptme,"%Y-%m-%d %H:%i")," ") as uptme,my_order_withdrawal.id')->select();


        return $list;
    }

//创建TOKEN
    public function creatToken($user_id)
    {
        $code = chr(mt_rand(0xB0, 0xF7)) . chr(mt_rand(0xA1, 0xFE)) . chr(mt_rand(0xB0, 0xF7)) . chr(mt_rand(0xA1, 0xFE)) . chr(mt_rand(0xB0, 0xF7)) . chr(mt_rand(0xA1, 0xFE));
        $code = $this->authcode($code);
        $redis         = new RedisPackage();
        $redis::set((string)$user_id, $code);
        return $code;

    }
    public function sum($data){
        $list  = $this
            ->field('sum(if(my_order_award.state !=2,profit,0)) as sum,sum(if(my_order_award.state=1 and (my_order_withdrawal.start is null or my_order_withdrawal.start=1),profit,0)) as alreadySum')
            ->join('my_order_withdrawal', 'my_order_withdrawal.my_order_award_id=my_order_award.id', 'left')
            ->where('my_order_award.user_id', $data['user_id'])
             ->where('my_order_award.type', 1)

            ->select();

        return $list[0];
    }

    /* 加密TOKEN */
    public function authcode($str)
    {
        $key = "ANDIAMON";
        $str = substr(md5($str), 8, 10);
        return md5($key . $str);
    }



}
