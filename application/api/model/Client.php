<?php

namespace app\api\model;



use PhpAmqpLib\Connection\AMQPStreamConnection;
use think\Model;
use PhpAmqpLib\Message\AMQPMessage;
use think\Db;


/**
 * Created by PhpStorm.
 * User: pandeng
 * Date: 2017-07-26
 * Time: 21:51
 */
class Client extends model

{
    //�������֪ͨ�ͷ��ñ�������
    const exchange = 'queue_declare';
    const queue = 'slave_addOrder';
    const slave_addErProval = 'slave_addErProval';
    const addOrder_routingKey = 'order.slave_addOrder';
    const addErProval_routingKey = 'order.slave_addErProval';
    //u8c ֪ͨ
    const u8c_exchange = 'exchange_topic';
    const u8c_queue = 'u8c_bdjobbasfil_save';
    const u8c_routingkey = 'u8c.bdjobbasfil_save';
    //�������루�빺����
    const u8c_Material_queue = 'u8c_praybill_saveapprove';
    const u8c_Material_routingkey = 'u8c.praybill_saveapprove';
    //app���۶�������
    const  order_exchange='exchange_topic';
    const  order_queue='u8c_sale_order';
    const  order_routingkey='u8c.sale_order';
    /**
     * ������Ϣ
     */
    public static  function pushMessage($data,$queue,$routingKey)
    {

        $host = '192.168.0.18';
        $port = 5672;
        $user = 'admin';
        $pwd = 'Yiniao3137';
        $vhost = '/psi';
//        $host = '47.107.131.55';
//        $port = 5672;
//        $user = 'admin';
//        $pwd = 'Yiniao@3137';
//        $vhost = '/psi';
        $connection = new AMQPStreamConnection($host, $port, $user, $pwd, $vhost);
        $channel = $connection->channel();
        //推送成功
        $channel->set_ack_handler(
            function (AMQPMessage $message) {
                Db::connect(config('database.zong'))->table('log')->insertGetId(['admin_id' => 0, 'created_at' => time(), 'content' => $message -> body,'type' => 2, 'msg'=>'发送成功']);

            }
        );
        //推送失败
        $channel->set_nack_handler(
            function (AMQPMessage $message) {
                Db::connect(config('database.zong'))->table('log')->insertGetId(['admin_id' => 0, 'created_at' => time(), 'content' => $message -> body,'type' => 2, 'msg'=>'发送失败']);
            }
        );
        $channel->exchange_declare('exchange_topic', 'topic', false, true, false);
        $channel->queue_declare($queue, false, true, false, false);
        $channel->queue_bind($queue, 'exchange_topic');
        $messageBody = $data;
        $message = new AMQPMessage($messageBody, array('content_type' => 'text/plain', 'delivery_mode' => AMQPMessage::DELIVERY_MODE_PERSISTENT));
        $channel->confirm_select();
        $channel->basic_publish($message, 'exchange_topic',$routingKey);

        $channel->wait_for_pending_acks();
        $channel->close();
        $connection->close();
    }
    /**
     * ����pushMessage�Ϳ��Է�����Ϣ
     */
    public function index($data,$queue,$routingKey)
    {
        self::pushMessage($data,$queue,$routingKey);
    }
}
    

