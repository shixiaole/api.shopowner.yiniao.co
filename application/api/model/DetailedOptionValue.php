<?php

namespace app\api\model;

use constant\config;
use think\Model;

class DetailedOptionValue extends Model
{
    protected $table = 'detailed_option_value';

    /*
     * 价格匹配
     */
    public function matching($data)
    {
        $option_value_id = '';
        foreach ($data as $list) {
            foreach ($list['data'] as $item) {
                $option_value_id .= implode(array_column($item['specsList'], 'option_value_id'), ',').',';
            }
        }
        $optionValue = $this->whereIn('option_value_id', $option_value_id)->select();

        foreach ($data as $key => $list) {
            foreach ($list['data'] as $l => $item) {
                if (empty($item['specsList'])) {
                    $data[$key]['data'][$l]['square'] = 0;
                } else {
                    foreach ($item['specsList'] as $m => $op) {
                        foreach ($optionValue as $kl) {
                            if ($kl['option_value_id'] == $op['option_value_id'] ) {
                                if($kl['is_enable']==1){
                                    $data[$key]['data'][$l]['specsList'][$m]['artificial']         = $kl['artificial'];
                                    $data[$key]['data'][$l]['specsList'][$m]['condition']          = $kl['condition'];
                                    $data[$key]['data'][$l]['specsList'][$m]['option_value_id']    = $kl['option_value_id'];
                                    $data[$key]['data'][$l]['specsList'][$m]['option_value_title'] = $kl['option_value_title'];
                                    $data[$key]['data'][$l]['specsList'][$m]['price']              = $kl['price'];
                                    if($kl['condition']==2){
                                        $data[$key]['data'][$l]['section']=json_decode($kl['price'],true);
                                    }
                                }else{
//                                    $data[$key]['data'][$l]['square'] = 0;
                                }

                            }

                        }
                    }


                }


            }
        }

        return $data;
    }

}
