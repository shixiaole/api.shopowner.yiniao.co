<?php

namespace app\api\model;

use constant\config;
use think\Model;

class Unit extends Model
{
    protected $table = 'unit';

    public  function unitTitle($id)
    {
       return $this->where('id', $id)->value('title');

    }

}
