<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2018 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------

return [
    // 数据库类型
    'type' => 'mysql',
    // 服务器地址
    'hostname'       =>'192.168.0.245',
    
    // 数据库名
    'database'       =>'yiniao_dg_3_0',
    // 用户名
    'username'       =>'sys_prime_dg',
    // 密码
    'password'       =>'39cTu59q87A',
//    'password'        => 'root',
    // 端口
    'hostport' => '3306',
    // 连接dsn
    'dsn' => '',
    // 数据库连接参数
    'params' => [],
    // 数据库编码默认采用utf8
    'charset' => 'utf8mb4',
    // 数据库表前缀
    'prefix' => '',
    // 数据库调试模式
    'debug' => false,
    // 数据库部署方式:0 集中式(单一服务器),1 分布式(主从服务器)
    'deploy' => 0,
    // 数据库读写是否分离 主从式有效
    'rw_separate' => false,
    // 读写分离后 主服务器数量
    'master_num' => 1,
    // 指定从服务器序号
    'slave_no' => '',
    // 自动读取主库数据
    'read_master' => false,
    // 是否严格检查字段是否存在
    'fields_strict' => true,
    // 数据集返回类型
    'resultset_type' => 'array',
    // 自动写入时间戳字段
    'auto_timestamp' => false,
    // 时间字段取出后的默认时间格式
    'datetime_format' => 'Y-m-d H:i:s',
    // 是否需要进行SQL性能分析
    'sql_explain' => false,
    
    'db2' => [
// 数据库类型
        'type' => 'mysql',
        // 服务器地址
        'hostname'       =>'192.168.0.245',
        // 数据库名
        'database'       =>'yiniao_dg_3_0',
        // 用户名
        'username'       =>'sys_prime_dg',
        // 密码
        'password'       =>'39cTu59q87A',
        // 端口
        'hostport' => '3306',
        // 连接dsn
        'dsn' => '',
        // 数据库连接参数
        'params' => [],
        // 数据库编码默认采用utf8
        'charset' => 'utf8mb4',
        // 数据库表前缀
        'prefix' => '',
        // 数据库调试模式
        'debug' => false,
        // 数据库部署方式:0 集中式(单一服务器),1 分布式(主从服务器)
        'deploy' => 0,
        // 数据库读写是否分离 主从式有效
        'rw_separate' => false,
        // 读写分离后 主服务器数量
        'master_num' => 1,
        // 指定从服务器序号
        'slave_no' => '',
        // 自动读取主库数据
        'read_master' => false,
        // 是否严格检查字段是否存在
        'fields_strict' => true,
        // 数据集返回类型
        'resultset_type' => 'array',
        // 自动写入时间戳字段
        'auto_timestamp' => false,
        // 时间字段取出后的默认时间格式
        'datetime_format' => 'Y-m-d H:i:s',
        // 是否需要进行SQL性能分析
        'sql_explain' => false,
    ],
    'cd'=>[
        // 数据库类型
        'type'           =>'mysql',
        // 服务器地址
        'hostname'       =>'192.168.0.128',
        
        // 数据库名
        'database'       =>'yiniao_cd_3_0',
        // 用户名
        'username'       =>'sys_prime_cd',
        // 密码
        'password'       =>'Mzg5YzZlOG',
        // 端口
        'hostport'       =>'3306',
        // 连接dsn
        'dsn'            =>'',
        // 数据库连接参数
        'params'         =>[],
        // 数据库编码默认采用utf8
        'charset'        =>'utf8mb4',
        // 数据库表前缀
        'prefix'         =>'',
        // 数据库调试模式
        'debug'          =>false,
        // 数据库部署方式:0 集中式(单一服务器),1 分布式(主从服务器)
        'deploy'         =>0,
        // 数据库读写是否分离 主从式有效
        'rw_separate'    =>false,
        // 读写分离后 主服务器数量
        'master_num'     =>1,
        // 指定从服务器序号
        'slave_no'       =>'',
        // 自动读取主库数据
        'read_master'    =>false,
        // 是否严格检查字段是否存在
        'fields_strict'  =>true,
        // 数据集返回类型
        'resultset_type' =>'array',
        // 自动写入时间戳字段
        'auto_timestamp' =>false,
        // 时间字段取出后的默认时间格式
        'datetime_format'=>'Y-m-d H:i:s',
        // 是否需要进行SQL性能分析
        'sql_explain'    =>false,
    ],
    'cc' => [
// 数据库类型
        'type' => 'mysql',
        // 服务器地址
        'hostname'       =>'192.168.0.114',
        // 数据库名
        'database'       =>'yiniao_cc_3_1',
        // 用户名
        'username'       =>'sys_core',
        // 密码
        'password'       =>'TD341ykRV6',
        // 端口
        'hostport'       =>'3306',
        // 连接dsn
        'dsn' => '',
        // 数据库连接参数
        'params' => [],
        // 数据库编码默认采用utf8
        'charset' => 'utf8mb4',
        // 数据库表前缀
        'prefix' => '',
        // 数据库调试模式
        'debug' => false,
        // 数据库部署方式:0 集中式(单一服务器),1 分布式(主从服务器)
        'deploy' => 0,
        // 数据库读写是否分离 主从式有效
        'rw_separate' => false,
        // 读写分离后 主服务器数量
        'master_num' => 1,
        // 指定从服务器序号
        'slave_no' => '',
        // 自动读取主库数据
        'read_master' => false,
        // 是否严格检查字段是否存在
        'fields_strict' => true,
        // 数据集返回类型
        'resultset_type' => 'array',
        // 自动写入时间戳字段
        'auto_timestamp' => false,
        // 时间字段取出后的默认时间格式
        'datetime_format' => 'Y-m-d H:i:s',
        // 是否需要进行SQL性能分析
        'sql_explain' => false,
    ],
    'zong' => [
// 数据库类型
        'type' => 'mysql',
        // 服务器地址
        'hostname'       =>'192.168.0.114',
        // 数据库名
        'database'       =>'yiniao_slave_3_0',
        // 用户名
        'username'       =>'sys_core',
        // 密码
        'password'       =>'TD341ykRV6',
        // 端口
        'hostport'       =>'3306',
        // 连接dsn
        'dsn' => '',
        // 数据库连接参数
        'params' => [],
        // 数据库编码默认采用utf8
        'charset' => 'utf8mb4',
        // 数据库表前缀
        'prefix' => '',
        // 数据库调试模式
        'debug' => false,
        // 数据库部署方式:0 集中式(单一服务器),1 分布式(主从服务器)
        'deploy' => 0,
        // 数据库读写是否分离 主从式有效
        'rw_separate' => false,
        // 读写分离后 主服务器数量
        'master_num' => 1,
        // 指定从服务器序号
        'slave_no' => '',
        // 自动读取主库数据
        'read_master' => false,
        // 是否严格检查字段是否存在
        'fields_strict' => true,
        // 数据集返回类型
        'resultset_type' => 'array',
        // 自动写入时间戳字段
        'auto_timestamp' => false,
        // 时间字段取出后的默认时间格式
        'datetime_format' => 'Y-m-d H:i:s',
        // 是否需要进行SQL性能分析
        'sql_explain' => false,
    ],
    'public'=>[
// 数据库类型
        'type'           =>'mysql',
        // 服务器地址
        'hostname'       =>'192.168.0.114',
        // 数据库名
        'database'       =>'public_1_0',
        // 用户名
        'username'       =>'sys_core',
        // 密码
        'password'       =>'TD341ykRV6',
        // 端口
        'hostport'       =>'3306',
        // 连接dsn
        'dsn'            =>'',
        // 数据库连接参数
        'params'         =>[],
        // 数据库编码默认采用utf8
        'charset'        =>'utf8mb4',
        // 数据库表前缀
        'prefix'         =>'',
        // 数据库调试模式
        'debug'          =>false,
        // 数据库部署方式:0 集中式(单一服务器),1 分布式(主从服务器)
        'deploy'         =>0,
        // 数据库读写是否分离 主从式有效
        'rw_separate'    =>false,
        // 读写分离后 主服务器数量
        'master_num'     =>1,
        // 指定从服务器序号
        'slave_no'       =>'',
        // 自动读取主库数据
        'read_master'    =>false,
        // 是否严格检查字段是否存在
        'fields_strict'  =>true,
        // 数据集返回类型
        'resultset_type' =>'array',
        // 自动写入时间戳字段
        'auto_timestamp' =>false,
        // 时间字段取出后的默认时间格式
        'datetime_format'=>'Y-m-d H:i:s',
        // 是否需要进行SQL性能分析
        'sql_explain'    =>false,
    ],
    
    'wh' => [
        // 数据库类型
        'type' => 'mysql',
        // 服务器地址
        'hostname'       =>'192.168.0.139',
        // 数据库名
        'database'       =>'yiniao_wh_3_0',
        // 用户名
        'username'       =>'sys_prime_wh',
        // 密码
        'password'       =>'9zt46apiiv',
        // 端口
        'hostport'       =>'3306',
        // 连接dsn
        'dsn' => '',
        // 数据库连接参数
        'params' => [],
        // 数据库编码默认采用utf8
        'charset' => 'utf8mb4',
        // 数据库表前缀
        'prefix' => '',
        // 数据库调试模式
        'debug' => false,
        // 数据库部署方式:0 集中式(单一服务器),1 分布式(主从服务器)
        'deploy' => 0,
        // 数据库读写是否分离 主从式有效
        'rw_separate' => false,
        // 读写分离后 主服务器数量
        'master_num' => 1,
        // 指定从服务器序号
        'slave_no' => '',
        // 自动读取主库数据
        'read_master' => false,
        // 是否严格检查字段是否存在
        'fields_strict' => true,
        // 数据集返回类型
        'resultset_type' => 'array',
        // 自动写入时间戳字段
        'auto_timestamp' => false,
        // 时间字段取出后的默认时间格式
        'datetime_format' => 'Y-m-d H:i:s',
        // 是否需要进行SQL性能分析
        'sql_explain' => false,
    ],
    'gy' => [
        // 数据库类型
        'type' => 'mysql',
        // 服务器地址
        'hostname'       =>'192.168.0.113',
        
        // 数据库名
        'database'       =>'yiniao_gy_3_0',
        // 用户名
        'username'       =>'sys_prime_gy',
        // 密码
        'password'       =>'rPsUA0b8oC',
        // 端口
        'hostport' => '3306',
        // 连接dsn
        'dsn' => '',
        // 数据库连接参数
        'params' => [],
        // 数据库编码默认采用utf8
        'charset' => 'utf8mb4',
        // 数据库表前缀
        'prefix' => '',
        // 数据库调试模式
        'debug' => false,
        // 数据库部署方式:0 集中式(单一服务器),1 分布式(主从服务器)
        'deploy' => 0,
        // 数据库读写是否分离 主从式有效
        'rw_separate' => false,
        // 读写分离后 主服务器数量
        'master_num' => 1,
        // 指定从服务器序号
        'slave_no' => '',
        // 自动读取主库数据
        'read_master' => false,
        // 是否严格检查字段是否存在
        'fields_strict' => true,
        // 数据集返回类型
        'resultset_type' => 'array',
        // 自动写入时间戳字段
        'auto_timestamp' => false,
        // 时间字段取出后的默认时间格式
        'datetime_format' => 'Y-m-d H:i:s',
        // 是否需要进行SQL性能分析
        'sql_explain' => false,
    ],
    'bj'=>[
        // 数据库类型
        'type'           =>'mysql',
        // 服务器地址
        'hostname'       =>'192.168.0.243',
        
        // 数据库名
        'database'       =>'yiniao_bj_3_0',
        // 用户名
        'username'       =>'sys_prime_bj',
        // 密码
        'password'       =>'wbSc-nN9VN',
//    'password'        => 'root',
        // 端口
        'hostport'       =>'3306',
        // 连接dsn
        'dsn'            =>'',
        // 数据库连接参数
        'params'         =>[],
        // 数据库编码默认采用utf8
        'charset'        =>'utf8mb4',
        // 数据库表前缀
        'prefix'         =>'',
        // 数据库调试模式
        'debug'          =>false,
        // 数据库部署方式:0 集中式(单一服务器),1 分布式(主从服务器)
        'deploy'         =>0,
        // 数据库读写是否分离 主从式有效
        'rw_separate'    =>false,
        // 读写分离后 主服务器数量
        'master_num'     =>1,
        // 指定从服务器序号
        'slave_no'       =>'',
        // 自动读取主库数据
        'read_master'    =>false,
        // 是否严格检查字段是否存在
        'fields_strict'  =>true,
        // 数据集返回类型
        'resultset_type' =>'array',
        // 自动写入时间戳字段
        'auto_timestamp' =>false,
        // 时间字段取出后的默认时间格式
        'datetime_format'=>'Y-m-d H:i:s',
        // 是否需要进行SQL性能分析
        'sql_explain'    =>false,
    ],
    'sz' => [
        // 数据库类型
        'type'           =>'mysql',
        // 服务器地址
        'hostname'       =>'192.168.0.139',
        
        // 数据库名
        'database'       =>'yiniao_sz_3_0',
        // 用户名
        'username'       =>'sys_prime_sz',
        // 密码
        'password'       =>'vlUKfmKzC7',
        // 端口
        'hostport' => '3306',
        // 连接dsn
        'dsn' => '',
        // 数据库连接参数
        'params' => [],
        // 数据库编码默认采用utf8
        'charset' => 'utf8mb4',
        // 数据库表前缀
        'prefix' => '',
        // 数据库调试模式
        'debug' => false,
        // 数据库部署方式:0 集中式(单一服务器),1 分布式(主从服务器)
        'deploy' => 0,
        // 数据库读写是否分离 主从式有效
        'rw_separate' => false,
        // 读写分离后 主服务器数量
        'master_num' => 1,
        // 指定从服务器序号
        'slave_no' => '',
        // 自动读取主库数据
        'read_master' => false,
        // 是否严格检查字段是否存在
        'fields_strict' => true,
        // 数据集返回类型
        'resultset_type' => 'array',
        // 自动写入时间戳字段
        'auto_timestamp' => false,
        // 时间字段取出后的默认时间格式
        'datetime_format' => 'Y-m-d H:i:s',
        // 是否需要进行SQL性能分析
        'sql_explain' => false,
    
    ],
    'sh' => [
        // 数据库类型
        'type' => 'mysql',
        // 服务器地址
        'hostname'       =>'192.168.0.113',
        
        // 数据库名
        'database'       =>'yiniao_sh_3_0',
        // 用户名
        'username'       =>'sys_prime_sh',
        // 密码
        'password'       =>'WIufkB8DJe',
//    'password'        => 'root',
        // 端口
        'hostport'       =>'3306',
        // 连接dsn
        'dsn' => '',
        // 数据库连接参数
        'params' => [],
        // 数据库编码默认采用utf8
        'charset' => 'utf8mb4',
        // 数据库表前缀
        'prefix' => '',
        // 数据库调试模式
        'debug' => false,
        // 数据库部署方式:0 集中式(单一服务器),1 分布式(主从服务器)
        'deploy' => 0,
        // 数据库读写是否分离 主从式有效
        'rw_separate' => false,
        // 读写分离后 主服务器数量
        'master_num' => 1,
        // 指定从服务器序号
        'slave_no' => '',
        // 自动读取主库数据
        'read_master' => false,
        // 是否严格检查字段是否存在
        'fields_strict' => true,
        // 数据集返回类型
        'resultset_type' => 'array',
        // 自动写入时间戳字段
        'auto_timestamp' => false,
        // 时间字段取出后的默认时间格式
        'datetime_format' => 'Y-m-d H:i:s',
        // 是否需要进行SQL性能分析
        'sql_explain' => false,
    
    ],
    'cq' => [
        // 数据库类型
        'type' => 'mysql',
        // 服务器地址
        'hostname'       =>'192.168.0.243',
        // 数据库名
        'database'       =>'yiniao_cq_3_0',
        // 用户名
        'username'       =>'sys_prime_cq',
        // 密码
        'password'       =>'fb1ojee6FA',
        // 端口
        'hostport'       =>'3306',
        // 连接dsn
        'dsn' => '',
        // 数据库连接参数
        'params' => [],
        // 数据库编码默认采用utf8
        'charset' => 'utf8mb4',
        // 数据库表前缀
        'prefix' => '',
        // 数据库调试模式
        'debug' => false,
        // 数据库部署方式:0 集中式(单一服务器),1 分布式(主从服务器)
        'deploy' => 0,
        // 数据库读写是否分离 主从式有效
        'rw_separate' => false,
        // 读写分离后 主服务器数量
        'master_num' => 1,
        // 指定从服务器序号
        'slave_no' => '',
        // 自动读取主库数据
        'read_master' => false,
        // 是否严格检查字段是否存在
        'fields_strict' => true,
        // 数据集返回类型
        'resultset_type' => 'array',
        // 自动写入时间戳字段
        'auto_timestamp' => false,
        // 时间字段取出后的默认时间格式
        'datetime_format' => 'Y-m-d H:i:s',
        // 是否需要进行SQL性能分析
        'sql_explain' => false,
    ],
    'gz'=>[
        // 数据库类型
        'type' => 'mysql',
        // 服务器地址
        'hostname' => '192.168.0.108',
        // 数据库名
        'database' => 'yiniao_gz_3_0',
        // 用户名
        'username' => 'sys_prime_gz',
        // 密码
        'password' => '1jZU0veJ9',
//    'password'        => 'root',
        // 端口
        'hostport' => '3306',
        // 连接dsn
        'dsn' => '',
        // 数据库连接参数
        'params' => [],
        // 数据库编码默认采用utf8
        'charset' => 'utf8mb4',
        // 数据库表前缀
        'prefix' => '',
        // 数据库调试模式
        'debug' => false,
        // 数据库部署方式:0 集中式(单一服务器),1 分布式(主从服务器)
        'deploy' => 0,
        // 数据库读写是否分离 主从式有效
        'rw_separate' => false,
        // 读写分离后 主服务器数量
        'master_num' => 1,
        // 指定从服务器序号
        'slave_no' => '',
        // 自动读取主库数据
        'read_master' => false,
        // 是否严格检查字段是否存在
        'fields_strict' => true,
        // 数据集返回类型
        'resultset_type' => 'array',
        // 自动写入时间戳字段
        'auto_timestamp' => false,
        // 时间字段取出后的默认时间格式
        'datetime_format' => 'Y-m-d H:i:s',
        // 是否需要进行SQL性能分析
        'sql_explain' => false,
    ],
    'fs' => [
        // 数据库类型
        'type' => 'mysql',
        // 服务器地址
        'hostname' => '192.168.0.108',
        // 数据库名
        'database' => 'yiniao_fs_3_0',
        // 用户名
        'username' => 'sys_prime_fs',
        // 密码
        'password' => '3PNxRePDz',
//    'password'        => 'root',
        // 端口
        'hostport' => '3306',
        // 连接dsn
        'dsn' => '',
        // 数据库连接参数
        'params' => [],
        // 数据库编码默认采用utf8
        'charset' => 'utf8mb4',
        // 数据库表前缀
        'prefix' => '',
        // 数据库调试模式
        'debug' => false,
        // 数据库部署方式:0 集中式(单一服务器),1 分布式(主从服务器)
        'deploy' => 0,
        // 数据库读写是否分离 主从式有效
        'rw_separate' => false,
        // 读写分离后 主服务器数量
        'master_num' => 1,
        // 指定从服务器序号
        'slave_no' => '',
        // 自动读取主库数据
        'read_master' => false,
        // 是否严格检查字段是否存在
        'fields_strict' => true,
        // 数据集返回类型
        'resultset_type' => 'array',
        // 自动写入时间戳字段
        'auto_timestamp' => false,
        // 时间字段取出后的默认时间格式
        'datetime_format' => 'Y-m-d H:i:s',
        // 是否需要进行SQL性能分析
        'sql_explain' => false,
    ],
    'bi' => [
// 数据库类型
        'type' => 'mysql',
        // 服务器地址
        'hostname'       =>'192.168.0.114',
        // 数据库名
        'database'       =>'yiniao_bi_1_0',
        // 用户名
        'username'       =>'sys_core',
        // 密码
        'password'       =>'TD341ykRV6',
        // 端口
        'hostport'       =>'3306',
        // 连接dsn
        'dsn' => '',
        // 数据库连接参数
        'params' => [],
        // 数据库编码默认采用utf8
        'charset' => 'utf8mb4',
        // 数据库表前缀
        'prefix' => '',
        // 数据库调试模式
        'debug' => false,
        // 数据库部署方式:0 集中式(单一服务器),1 分布式(主从服务器)
        'deploy' => 0,
        // 数据库读写是否分离 主从式有效
        'rw_separate' => false,
        // 读写分离后 主服务器数量
        'master_num' => 1,
        // 指定从服务器序号
        'slave_no' => '',
        // 自动读取主库数据
        'read_master' => false,
        // 是否严格检查字段是否存在
        'fields_strict' => true,
        // 数据集返回类型
        'resultset_type' => 'array',
        // 自动写入时间戳字段
        'auto_timestamp' => false,
        // 时间字段取出后的默认时间格式
        'datetime_format' => 'Y-m-d H:i:s',
        // 是否需要进行SQL性能分析
        'sql_explain' => false,
    ],
    'dg'=>[
        // 数据库类型
        'type'           =>'mysql',
        // 服务器地址
        'hostname'       =>'192.168.0.245',
        // 数据库名
        'database'       =>'yiniao_dg_3_0',
        // 用户名
        'username'       =>'sys_prime_dg',
        // 密码
        'password'       =>'39cTu59q87A',
        // 端口
        'hostport'       =>'3306',
        // 连接dsn
        'dsn'            =>'',
        // 数据库连接参数
        'params'         =>[],
        // 数据库编码默认采用utf8
        'charset'        =>'utf8mb4',
        // 数据库表前缀
        'prefix'         =>'',
        // 数据库调试模式
        'debug'          =>false,
        // 数据库部署方式:0 集中式(单一服务器),1 分布式(主从服务器)
        'deploy'         =>0,
        // 数据库读写是否分离 主从式有效
        'rw_separate'    =>false,
        // 读写分离后 主服务器数量
        'master_num'     =>1,
        // 指定从服务器序号
        'slave_no'       =>'',
        // 自动读取主库数据
        'read_master'    =>false,
        // 是否严格检查字段是否存在
        'fields_strict'  =>true,
        // 数据集返回类型
        'resultset_type' =>'array',
        // 自动写入时间戳字段
        'auto_timestamp' =>false,
        // 时间字段取出后的默认时间格式
        'datetime_format'=>'Y-m-d H:i:s',
        // 是否需要进行SQL性能分析
        'sql_explain'    =>false,
    ],
    'statistics'=>[
// 数据库类型
        'type'           =>'mysql',
        // 服务器地址
        'hostname'       =>'192.168.0.114',
        // 数据库名
        'database'       =>'yiniao_datatrack_1_0',
        // 用户名
        'username'       =>'sys_core',
        // 密码
        'password'       =>'TD341ykRV6',
        // 端口
        'hostport'       =>'3306',
        // 连接dsn
        'dsn'            =>'',
        // 数据库连接参数
        'params'         =>[],
        // 数据库编码默认采用utf8
        'charset'        =>'utf8mb4',
        // 数据库表前缀
        'prefix'         =>'',
        // 数据库调试模式
        'debug'          =>false,
        // 数据库部署方式:0 集中式(单一服务器),1 分布式(主从服务器)
        'deploy'         =>0,
        // 数据库读写是否分离 主从式有效
        'rw_separate'    =>false,
        // 读写分离后 主服务器数量
        'master_num'     =>1,
        // 指定从服务器序号
        'slave_no'       =>'',
        // 自动读取主库数据
        'read_master'    =>false,
        // 是否严格检查字段是否存在
        'fields_strict'  =>true,
        // 数据集返回类型
        'resultset_type' =>'array',
        // 自动写入时间戳字段
        'auto_timestamp' =>false,
        // 时间字段取出后的默认时间格式
        'datetime_format'=>'Y-m-d H:i:s',
        // 是否需要进行SQL性能分析
        'sql_explain'    =>false,
    ],


];
