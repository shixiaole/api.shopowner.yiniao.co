<?php 
namespace app\index\model;

use think\Model;

class XcxRelationDouble extends Model
{
    protected $autoWriteTimestamp = true;

    // 定义时间戳字段名
    protected $createTime = 'created_at';
    protected $updateTime = 'updated_at';

    const PID_TYPE_CUSTOMER = 1; //用户
    const PID_TYPE_PERSONAL = 2; //合伙人

    const STATUS_WAIT = 0; //待付款
    const STATUS_ALREADY = 1; //已付款
    const STATUS_DISCARD = 2; //已作废

    /**
     * 用户奖励金设置
     * @var array
     */
    public $user_bonus_set = [
        1 => [
            'price' => 20,
            'max' => 50
        ],
        2 => [
            'price' => 30,
            'max' => 33
        ],
        3 => [
            'price' => 40,
            'max' => 25
        ],
        4 => [
            'price' => 50,
            'max' => 20
        ],
        5 => [
            'price' => 50,
            'max' => 20
        ],
        6 => [
            'price' => 50,
            'max' => 20
        ],
        7 => [
            'price' => 50,
            'max' => 20
        ],
        8 => [
            'price' => 50,
            'max' => 20
        ],
        9 => [
            'price' => 50,
            'max' => 20
        ],
        10 => [
            'price' => 50,
            'max' => 20
        ],
    ];

    /**
     * 计算用户奖励金
     * @param $sort_day
     * @param $count
     * @return int
     */
    public function countCustomersBonus($sort_day, $count)
    {
        foreach ($this->user_bonus_set as $key => $set) {
            if ($key == $sort_day) {
                $price = $count > $set['max'] ? 0 : $set['price'];
                return $price;
            }
        }
        return 0;
    }

    /**
     * 计算合伙人励金
     * @return int
     */
    public function countPersonalBonus()
    {
        return mt_rand(4000, 6000);
    }
 public function customer() {
        return $this->hasOne('Customers', 'customers_id', 'customer_id');
    }
}

