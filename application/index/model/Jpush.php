<?php
/**
 * Created by muniao.
 * User: muniao
 * Date: 2018/8/2
 * Time: 13:20
 */

namespace app\index\model;

use app\common\model\Version;
use think\Model;
use think\Request;


class Jpush extends Model
{
    protected $app_key;
    protected $master_secret;
    protected $client;
    
    
    protected function initialize()
    {
        
        require_once ROOT_PATH . 'extend/jpush/autoload.php';
        $this->app_key       = "93f21e7e6307ceca0ee63ca6";
        $this->master_secret = "906af07d6b10bff936e0627a";
        $this->client        = new \JPush\Client($this->app_key, $this->master_secret);
    }
    
    public function whole($content, $RegistrationId, $data)
    {
        
        $client               = $this->client;
        $android_notification = array(
            'title' => 'hello jpush',
            'builder_id' => 2,
            'extras' => $data
        );
        
        $result = $client->push()
            ->setPlatform('android')
            ->androidNotification($content, $android_notification)
            ->addRegistrationId($RegistrationId)
            ->options(['apns_production' => 1])
            ->send();
        
        if ($result['http_code'] == 200) {
            return true;
        }
        return false;
    }
    
    /*
     * 推送给师傅
     */
    public function master($data)
    {
        if (!empty($data['jpush_id'])) {
            $titleContent = "通过，请注意查看！";
            if ($data['status'] == 2) {
                $titleContent = "驳回，请注意查看！";
            }
            //1建渣打拆，2小材料，3协作人工，4主材代购，7运费，8大工地结算'
            /**30  费用报销 31 主材费用报销，32返工费用报销 33 常规材料  34 采购材料
             * 费用报销内容
             */
        $title='';
            switch ($data['classification']) {
                case 1:
                case 2:
                case 3:
                    $type = 30;
                    $title="费用报销";
                    break;
                case 4:
                    $title="费用报销";
                    $type = 31;
                    break;
                case 5:
                    $title="材料领用";
                    $type = 33;
                    break;
                default:
                    $title="费用报销";
                    $type = 30;
            }
            
            $list                 = array('order_id' => $data['order_id'], 'content' => $data['title'] . $titleContent, 'type' => $type, 'user_id' => $data['user_id']);
            $client               = new \JPush\Client("18d24f5c4e408814fbead85f", "14ac0a1df64e92727b582a96");
            $android_notification = array(
                'title' => $title,
                'builder_id' => 2,
                'extras' => $list
            );
            $result               = $client->push()
                ->setPlatform('android')
                ->androidNotification($list['content'], $android_notification)
                ->addRegistrationId($data['jpush_id'])
                ->options(['apns_production' => 1])
                ->send();
//            if ($result['http_code'] == 200) {
//                db('app_user_message')->insertGetId([
//                    'type' => $type,
//                    'title' => $data['title'],
//                    'content' => $data['remake'],
//                    'order_id' => $data['order_id'],
//                    'time' => time(),
//                    'user_id' => $data['user_id'],
//                ]);
//            }
          
        }
    }
    
    public function tui($content, $RegistrationId, $data = [],$DB='')
    {
        $client               = $this->client;
        $android_notification = array(
            'title' => $content,
            'builder_id' => 2,
            'extras' => $data
        );
        $message = db('message');
        IF($DB  !=''){
        $message = db('message',config('database.' . $DB));
        }
        $message=$message->where(['type' => $data['type'], 'content' => $data['content'], 'order_id' => $data['order_id'], 'user_id' => $data['user_id']])->find();
        if (!$message) {
            $result = $client->push()
                ->setPlatform('android')
                ->androidNotification($content, $android_notification)
                ->addRegistrationId($RegistrationId)
                ->options(['apns_production' => 1])
                ->send();
            if ($result['http_code'] == 200) {
                $messageInsert = db('message');
                IF($DB  !=''){
                    $messageInsert = db('message',config('database.' . $DB));
                }
                $messageInsert->insertGetId([
                    'type' => $data['type'],
                    'content' => $data['content'],
                    'order_id' => $data['order_id'],
                    'time' => time(),
                    'user_id' => $data['user_id'],
                ]);
            }
            
        }
        return true;
    }
    
    
}