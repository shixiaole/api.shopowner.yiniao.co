<?php
namespace app\index\model;

use think\Model;

class Customers extends Model
{
    /**
     * 更新红包
     * @param $customers_id
     * @param $bonus
     * @return bool
     * @throws \think\Exception
     */
    public static function updateBonus($customers_id, $bonus)
    {
        $res = static::where(['customers_id' => $customers_id])->setInc('hongbao', $bonus * 0.01);
        if ($res === false) {
            return false;
        }
        return true;
    }
}

