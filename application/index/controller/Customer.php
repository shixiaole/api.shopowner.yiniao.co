<?php
/**
 * Created by muniao.
 * User: muniao
 * Date: 2018/7/31
 * Time: 9:39
 */

namespace app\index\controller;


use think\Request;
use think\Exception;

class Customer extends Backend
{
    /**
     * 客户预约
     */
    public function index()
    {
        $data = db('customer')
            ->order('customer_id desc')->paginate(20)
            ->each(function ($item, $key) {
                if (!empty($item['last_login_time'])) {
                    $item['last_login_time'] = date('Y-m-d H:i:s', $item['last_login_time']);

                } else {
                    $item['last_login_time'] = '';
                }
                if (!empty($item['admin_id'])) {
                    $item['admin_id'] = db('admin')->where('admin_id', $item['admin_id'])->value('username');
                } else {
                    $item['admin_id'] = '';
                }
                if (!empty($item['lai'])) {
                    $item['lai'] = db('personal')->where('personal_id', $item['lai'])->value('username');
                } else {
                    $item['lai'] = '';
                }
                if (!empty($item['type'])) {
                    if ($item['type']==1) {
                        $item['type'] = '墙面翻新锦鲤，价值12000';
                    } elseif($item['type']==2) {
                        $item['type'] = '卫生间翻新锦鲤，价值18800';
                    }else{
                        $item['type'] = '参与开业活动，其它维修翻新';
                    }
                } else {
                    $item['type'] = '暂无';
                }


                return $item;

            });

        return $this->fetch('index', ['data' => $data]);
    }
    /**
     * @return array|mixed|string
     * 添加
     */
    public function edits($customer_id = 0)
    {
        if ($data = Request::instance()->post()) {
            if(empty($data['addres'])){
                return json_data([], 300, '请输入详细地址');
            }
            if(empty($data['tui_jian'])){
                return json_data([], 300, '订单有误');
            }

            try{
                $user = [
                    'channel_id' => $data['channel_id'],//渠道
                    'order_no' => order_sn(),//渠道
                    'pro_id' => $data['pro_id'],//一级问题
                    'pro_id1' => $data['pro_id1'],//二级问题
                    'pro_id2' => $data['pro_id2'],//三级问题
                    'province_id' => $data['province'],//省
                    'city_id' => $data['city'],//市
                    'county_id' => $data['area'],//区
                    'addres' => $data['addres'],//地址
                    'contacts' => $data['contacts'],//联系人
                    'telephone' => $data['telephone'],//联系电话
                    'remarks' => $data['remarks'],//联系电话
                    'logo' => !empty($data['logo'])?serialize($data['logo']):'',//联系电话
                    'state' => $data['state'],//待指派
                    'created_time' => !empty($data['created_time'])?strtotime($data['created_time']):time(),//创建时间
                    'tui_jian' => $data['tui_jian'],//创建时间
                ];
                $users = $this->check_authority();
                $res = db('order')->insertGetId($user);
                $this->log('添加了一个订单',$res,$users);
                $us = [
                    'context'=>'订单号：'.$user['order_no'].'<br>备注：'.$data['remarks'],
                    'admin_id'=>$users['admin_id'],
                    'status'=>1,
                    'last_login_time'=>time()

                ];

                db('customer')->where(['customer_id' => $data['customer_id']])->update($us);
                db()->commit();
                return json_data([], 200, '成功');
            }catch (Exception $e){
                db()->rollback();
                return json_data([],300,$e->getMessage());
            }
        }

        if (!$customer_id) {
            $this->error('请刷新后重试');
        }
        $res = db('customer')
            ->where(['customer_id' => $customer_id])
            ->find();

        $chanel = db('chanel')->select();
        $go=db('goods_category')->where(['pro_type'=>1,'parent_id'=>0])->select();

        return $this->fetch('edits', ['data' => $res,'chanel'=>$chanel,'go'=>$go]);
    }
    private  function log($content,$ord_id,$user)
    {

        $user=[
            'admin_id'=>$user['admin_id'],
            'created_at'=>time(),
            'content'=>$content,
            'ord_id'=>$ord_id,

        ];

        db('log')->insertGetId($user);
    }

    }