<?php
/**
 * Created by muniao.
 * User: muniao
 * Date: 2018/7/31
 * Time: 9:39
 */

namespace app\index\controller;


use think\Request;

class Channel extends Backend
{

    public function index(){
        $data=db('chanel')->where('type',1)->paginate(20);
        return $this->fetch('index',['data'=>$data]);
    }

    /**
     * @return array|mixed|string
     * 添加短信
     */
    public function add(){
        if($data =Request::instance()->post()){

            $user = [
                'title' => $data['title'],
		  'create_at'=>date('Y-m-d H:i:s',time()),
                'type'=>1
            ];

            $res = db('chanel')->insertGetId($user);
            if($res===false){
                return json_data([], 300, '添加失败');
            }
            return json_data([], 200, '添加成功');
        }

        return $this->fetch('add');
    }

    public function ad($id = 0)
    {
        if ($data = Request::instance()->post()) {

            $user = [
                'title'     => $data['title'],
                'create_at' => date('Y-m-d H:i:s', time()),
                'explain' =>  $data['explain'],
                'type'      => 1,
                'pid'       => $data['id'],
            ];

            $res = db('channel_details')->insertGetId($user);
            if ($res === false) {
                return json_data([], 300, '添加失败');
            }
            return json_data([], 200, '添加成功');
        }

        return $this->fetch('ad',['id' => $id]);
    }

    /**
     * @return array|mixed|string
     * 编辑短信
     */
    public function edit($id = 0)
    {
        if ($data = Request::instance()->post()) {
            $user = [
                'title' => $data['title'],
            ];

            $res = db('chanel')->where(['id' => $data['id']])->update($user);
            if ($res === false) {
                return json_data([], 300, '编辑失败');
            }
            return json_data([], 200, '编辑成功');
        }

        if (!$id) {
            $this->error('请刷新后重试');
        }
        $res = db('chanel')
            ->where(['id' => $id])
            ->find();


        return $this->fetch('edit', ['data' => $res]);
    }

    public function ed($id = 0)
    {
        if ($data = Request::instance()->post()) {
            $user = [
                'title' => $data['title'],
                'explain' => $data['explain'],
            ];

            $res = db('channel_details')->where(['id' => $data['id']])->update($user);
            if ($res === false) {
                return json_data([], 300, '编辑失败');
            }
            return json_data([], 200, '编辑成功');
        }

        if (!$id) {
            $this->error('请刷新后重试');
        }
        $res = db('channel_details')
            ->where(['id' => $id])
            ->find();


        return $this->fetch('ed', ['data' => $res]);
    }

    public function del()
    {
        $id = Request::instance()->post();

        $res = false;
        if ($id) {
            $res = db('chanel')->where(['id' => $id['id']])->update(['type' => 2]);
        }
        if ($res !== false) {
            return json_data([], 200, '隐藏成功');
        }
        return json_data([], 300, '隐藏失败');
    }

    public function de()
    {
        $id = Request::instance()->post();

        $res = false;
        if ($id) {
            $res = db('channel_details')->where(['id' => $id['id']])->update(['type' => 2]);
        }
        if ($res !== false) {
            return json_data([], 200, '隐藏成功');
        }
        return json_data([], 300, '隐藏失败');
    }

    public function info($id = 0)
    {
        $data = db('channel_details')->where(['type'=>1,'pid'=>$id])->paginate(20);
        return $this->fetch('info', ['data' => $data]);
    }

}
