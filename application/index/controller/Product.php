<?php
/**
 * Created by muniao.
 * User: muniao
 * Date: 2018/8/9
 * Time: 11:22
 */

namespace app\index\controller;


use think\Request;
use think\Cache;
use think\Exception;

class Product extends Backend
{
    /**
     * @return mixed
     * 问题
     */
    public function index(){
        $data=db('product_chan')->where(['pro_types'=>1,'parents_id'=>0])->paginate(10);
        return $this->fetch('index',['data'=>$data]);
    }
    /**
     * @return mixed
     * 修改
     */
    public function edi($id=0)
    {

        if ($data = Request::instance()->post()) {
            $use = [
                'product_title'=>$data['product_title'],
                'parents_id'=>!empty($data['parents_id'])?$data['parents_id']:0,
                'units_id'=>!empty($data['units_id'])?$data['units_id']:0,
                'materials'=>!empty($data['materials'])?$data['materials']:'',
                'scesn'=>!empty($data['scesn'])?$data['scesn']:'',
                'prompts'=>!empty($data['prompts'])?$data['prompts']:'',
                'prices'=>!empty($data['prices'])?$data['prices']:'',
                'price_rules'=>!empty($data['price_rules'])?$data['price_rules']:'',
                'pro_types'=>empty($data['pro_types'])?$data['pro_types']:1,
                'service_contents'=>!empty($data['service_contents'])?$data['service_contents']:'',
                'service_timess'=>!empty($data['service_timess'])?$data['service_timess']:'',
                'updated_at'=>time(),

            ];

            $res = db('product_chan')->where(['product_id' => $data['product_id']])->update($use);
            if ($res === false) {
                return json_data([], 300, '编辑失败');
            }
            return json_data([], 200, '编辑成功');
        }
            if (!$id) {
                $this->error('请刷新后重试');
            }
            $res = db('product_chan')
                ->join('unit u','product_chan.units_id=u.id','left')
                ->where(['product_id' => $id])
                ->find();

        $g=db('unit')->where(['id'=>['neq',$res['units_id']]])->select();

            return $this->fetch('edi', ['data' => $res,'g'=>$g]);

    }
    /**
     * @return mixed
     * 添加
     */
    public function ad($id){

        if ($data = Request::instance()->post()) {
            $use=[
                   'product_title'=>$data['product_title'],
                   'parents_id'=>!empty($data['parents_id'])?$data['parents_id']:0,
                   'units_id'=>0,
                   'materials'=>'',
                   'scesn'=>'',
                   'prompts'=>'',
                   'prices'=>'',
                   'price_rules'=>'',
                   'pro_types'=>1,
                   'service_contents'=>'',
                   'service_timess'=>'',
                   'updated_at'=>time(),

            ];
         if(empty($data['product_id'])){
             $res = db('product_chan')->insertGetId($use);
         }else{
             $res = db('product_chan')->where('product_id',$data['product_id'])->update($use);
         }

            if ($res) {
                return json_data([], 200, '添加成功');

            }
            return json_data([], 300, '添加失败');
        }

        $res = db('product_chan')->where(['product_id' => $id])
            ->find();

        return $this->fetch('ad',['data'=>$res]);

        }
    /**
     * @return mixed
     * 添加
     */
    public function add($id){

        if ($data = Request::instance()->post()) {

            $use=[
                    'product_title'=>$data['product_title'],
                    'parents_id'=>!empty($data['parents_id'])?$data['parents_id']:0,
                    'units_id'=>!empty($data['units_id'])?$data['units_id']:0,
                    'materials'=>!empty($data['materials'])?$data['materials']:'',
                    'scesn'=>!empty($data['scesn'])?$data['scesn']:'',
                    'prompts'=>!empty($data['prompts'])?$data['prompts']:'',
                    'prices'=>!empty($data['prices'])?$data['prices']:'',
                    'price_rules'=>!empty($data['price_rules'])?$data['price_rules']:'',
                    'pro_types'=>empty($data['pro_types'])?$data['pro_types']:1,
                    'service_contents'=>!empty($data['service_contents'])?$data['service_contents']:'',
                    'service_timess'=>!empty($data['service_timess'])?$data['service_timess']:'',
                    'updated_at'=>time(),

                ];
            $res = db('product_chan')->insertGetId($use);
            if ($res) {
                return json_data([], 200, '添加成功');

            }
            return json_data([], 300, '添加失败');
            }

            $g=db('unit')->select();
        return $this->fetch('add', ['g'=>$g,'data'=>$id]);



    }
    /**
     * @return mixed
     * 删除
     */
    public function de(){
        $store_id = Request::instance()->post();
        $re = db('product_chan')->where(['parents_id' => $store_id['id'],'pro_types'=>1])->find();
        if($re) {
            return json_data([], 300, '删除失败,请先删除子分类');
        }
        $res = false;
        if ($store_id) {
            $res = db('product_chan')->where(['product_id' => $store_id['id']])->update(['pro_types'=>0]);
        }
        if ($res !== false) {
            return json_data([], 200, '删除成功');
        }
        return json_data([], 300, '删除失败');
    }
    /**
     * @return mixed
     * 查看
     */
    public function info($id=0){


        if (!$id) {
            $this->error('请刷新后重试');
        }

        $res = db('product_chan')
            ->where(['parents_id' =>$id,'pro_types'=>1])
            ->paginate(10)
            ->each(function($item, $key){
                $item['materials']=!empty($item['materials'])?$item['materials']:'暂无数据';
                $item['scesn']=!empty($item['scesn'])?$item['scesn']:'暂无数据';
                $item['prompts']=!empty($item['prompts'])?$item['prompts']:'暂无数据';
                $item['prices']=!empty($item['prices'])?$item['prices']:'暂无数据';
                $item['price_rules']=!empty($item['price_rules'])?$item['price_rules']:'暂无数据';
                $item['pro_types']=!empty($item['pro_types'])?'已上线':'暂未开放';
                $item['service_contents']=!empty($item['service_contents'])?$item['service_contents']:'暂无数据';
                $item['service_timess']=!empty($item['service_timess'])?$item['service_timess']:'暂无数据';
                if( empty($item['units_id'])){
                    $item['unit_id']='暂无数据';
                }else{
                    $g=db('unit')->where('id',$item['units_id'])->find();
                    $item['units_id']=$g['title'];
                }

                return $item;

            });
        return $this->fetch('info',['data'=>$res]);

    }
   }