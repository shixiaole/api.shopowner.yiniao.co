<?php
/**
 * Created by muniao.
 * User: muniao
 * Date: 2018/7/31
 * Time: 9:39
 */

namespace app\index\controller;


use think\Request;

class planting extends Backend
{
    /**
     * 轮播首页
     */
    public function index(){
        $datas=Request::instance()->get();
        $data=db('planting');
        if (isset($datas['type']) && $datas['type'] != '') {

            $data->where(['type' => $datas['type']]);
        }
        $list=$data->order('type ace')
            ->paginate(30)->each(function($item, $key){
                if( $item['type']==1){
                    $item['types']='工具';
                }elseif( $item['type']==0){
                    $item['types']='轮播';
                }elseif( $item['type']==3){
                    $item['types']='热门推荐';
                }
                if( $item['fen']==1){
                    $item['fen']='显示';
                }else{
                    $item['fen']='不显示';
                }
                if( $item['state']==1){
                    $item['states']='启用';
                }else{
                    $item['states']='关闭';
                }
                return $item;
            });
        return $this->fetch('index',['data'=>$list,'type'=>$datas['type']]);
    }

    /**
     * @return array|mixed|string
     * 添加
     */
    public function add(){
        if($data =Request::instance()->post()){
            $fen=0;
            if( $data['type']==1){
                $fen=$data['fen'];
            }
            $user = [
                'title' => $data['title'],
                'type' => $data['type'],
                'fen' => $fen,
                'logo' =>$data['logo'],
            ];

            $res = db('planting')->insertGetId($user);
            if($res===false){
                return json_data([], 300, '添加失败');
            }
            return json_data([], 200, '添加成功');
        }
        $type=Request::instance()->get();

        return $this->fetch('add',['type'=>$type['type']]);
    }
    /**
     * @return array|mixed|string
     * 编辑
     */
    public function edit($planting_id = 0)
    {
        if ($data = Request::instance()->post()) {
            $fen=0;

           if( $data['type']==1){
            $fen=$data['fen'];
           }
   $user = [
                'title' => $data['title'],

                'type' => $data['type'],
                'fen' => $fen,
              'logo' =>$data['logo'],
       ];

            $res = db('planting')->where(['planting_id' => $data['planting_id']])->update($user);
            if ($res === false) {
                return json_data([], 300, '编辑失败');
            }
            return json_data([], 200, '编辑成功');
        }

        if (!$planting_id) {
            $this->error('请刷新后重试');
        }
        $res = db('planting')
            ->where(['planting_id' => $planting_id])
            ->find();


        $res['pro_ids'] = !empty($res['type']) ? '工具' : '轮播';

        return $this->fetch('edit', ['data' => $res]);
    }
    /**
     * 取消
     */
    public function del()
    {
        $store_id = Request::instance()->post();

        $res = false;
        if ($store_id) {
            $res = db('planting')->where(['planting_id' => $store_id['planting_id']])->delete();
        }
        if ($res !== false) {
            return json_data([], 200, '删除成功');
        }
        return json_data([], 300, '删除失败');
    }
    /**
     * 启用 禁用
     */
    public function disable(){
        $planting_id=Request::instance()->post('planting_id');
        $status=Request::instance()->post('status');
        if($planting_id && ($status==0 || $status==1)){

            $res=db('planting')->where(['planting_id'=>$planting_id])->update(['state'=>$status]);
            if($res!==false){
                $this->result([],200,'操作成功');
            }
        }
        $this->result([],300,'请刷新后重试');
    }
    /**
     * 短信模板
     */
    public function tel(){
        $data=db('tool')->paginate(10);

        return $this->fetch('tel',['data'=>$data]);
    }

    /**
     * @return array|mixed|string
     * 添加短信
     */
    public function adds(){
        if($data =Request::instance()->post()){

            $user = [
                'content' => $data['content'],
                'logo' => !empty($data['logo'])?$data['logo']:'',
                'state'=>$data['state']
            ];

            $res = db('tool')->insertGetId($user);
            if($res===false){
                return json_data([], 300, '添加失败');
            }
            return json_data([], 200, '添加成功');
        }

        return $this->fetch('adds',['data'=>$data]);
    }
    /**
     * @return array|mixed|string
     * 编辑短信
     */
    public function edits($tool_id = 0)
    {
        if ($data = Request::instance()->post()) {

            $user = [
                'content' => $data['content'],
                'logo' => !empty($data['logo'])?$data['logo']:'',
                'state'=>$data['state']
            ];

            $res = db('tool')->where(['tool_id' => $data['tool_id']])->update($user);
            if ($res === false) {
                return json_data([], 300, '编辑失败');
            }
            return json_data([], 200, '编辑成功');
        }

        if (!$tool_id) {
            $this->error('请刷新后重试');
        }
        $res = db('tool')
            ->where(['tool_id' => $tool_id])
            ->find();


        return $this->fetch('edits', ['data' => $res]);
    }
    /**
     * 取消
     */
    public function dels()
    {
        $store_id = Request::instance()->post();

        $res = false;
        if ($store_id) {
            $res = db('tool')->where(['tool_id' => $store_id['tool_id']])->delete();
        }
        if ($res !== false) {
            return json_data([], 200, '删除成功');
        }
        return json_data([], 300, '删除失败');
    }
}