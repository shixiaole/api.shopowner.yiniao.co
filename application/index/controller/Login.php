<?php
/**
 * Created by muniao.
 * User: muniao
 * Date: 2018/7/25
 * Time: 18:14
 */

namespace app\index\controller;


use app\api\model\Common;

use think\captcha\Captcha;
use think\Controller;
use think\Exception;
use think\Request;
use think\Session;
use think\Cookie;
use redis\RedisPackage;

class Login extends Controller
{
    public function login()
    {
        if ($post = Request::instance()->post()) {
            if (!$post['code']) {
                
                return json_data([], 300, '请输入验证码');
            }
            if (!$post['city']) {
                
                return json_data([], 300, '请选择城市');
            }
            if (!$post['username'] || !$post['password'] || !$post['code']) {
                return json_data([], 300, '账号或密码错误');
            }
//            if(!captcha_check($post['code'])){
//                return json_data([], 300,'验证码错误');
//            };
            $res = db('admin')->where(['username' => $post['username'], 'password' => encrypt($post['password'])])->find();
            if (!$res) {
                return json_data([], 300, '账号或密码错误');
            }
            if ($res['status'] != 0) {
                return json_data([], 300, '账号已禁用');
                
            }
            try {
                db('admin')->where(['admin_id' => $res['admin_id']])->update(['access_token' => md5(uniqid('check', true)), 'last_login_time' => time()]);
                $redis = new RedisPackage();
                Cookie::set('token', $res['admin_id'] . '.' . $post['city']);
                $redis::set($res['admin_id'] . '.' . $post['city'], json_encode(db('admin')->where(['admin_id' => $res['admin_id']])->find()));
//                Session::set('token',db('admin')->where(['admin_id'=>$res['admin_id']])->value('access_token'));
//                Session::set('username',db('admin')->where(['admin_id'=>$res['admin_id']])->value('username'));
//                Session::set('logo',db('admin')->where(['admin_id'=>$res['admin_id']])->value('logo'));
//                Session::set('admin_id',db('admin')->where(['admin_id'=>$res['admin_id']])->value('admin_id'));
                
                return json_data($res['admin_id'] . $post['city'], 200, '登陆成功');
            } catch (Exception $e) {
                $this->error($e->getMessage());
            }
            
        }
        $city = (new  Common())->obtainCity('', 2);
        foreach ($city as $k => $item) {
            if ($item['id'] == 241) {
                $city[$k]['id'] = 'cd';
            } elseif ($item['id'] == 239) {
                $city[$k]['id'] = 'cq';
            } elseif ($item['id'] == 262) {
                $city[$k]['id'] = 'gy';
            } elseif ($item['id'] == 172) {
                $city[$k]['id'] = 'wh';
            } elseif ($item['id'] == '202') {
                $city[$k]['id'] = 'sz';
            } elseif ($item['id'] == 375) {
                $city[$k]['id'] = 'sh';
            } elseif ($item['id'] == 501) {
                $city[$k]['id'] = 'bj';
            }
        }
        return $this->fetch('login', ['city' => $city]);
    }
    
    /**
     * 生成验证码
     */
    public function code()
    {
        $captcha           = new Captcha();
        $captcha->fontSize = 30;
        $captcha->length   = 5;
        $captcha->useNoise = false;
        return $captcha->entry();
    }
    
    /**
     * 退出
     */
    public function logout()
    {
        $redis = new RedisPackage();
        $access_token = Cookie::get('token');
        RedisPackage::del($access_token);
        Cookie::delete('token');
        $this->success('成功退出，返回登录界面中', 'index/login/login');
    }
    
}
