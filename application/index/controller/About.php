<?php
/**
 * Created by muniao.
 * User: muniao
 * Date: 2018/7/31
 * Time: 9:39
 */

namespace app\index\controller;


use think\Request;

class About extends Backend
{



    public function index(){
        $data=db('about')->paginate(10);
        return $this->fetch('index',['data'=>$data]);
    }


    public function add(){
        if($data =Request::instance()->post()){

            $user = [
                'title' => $data['title'],
                'content' => $data['content'],
                'created_at'=>time()
            ];

            $res = db('about')->insertGetId($user);
            if($res===false){
                return json_data([], 300, '添加失败');
            }
            return json_data([], 200, '添加成功');
        }

        return $this->fetch('add',['data'=>$data]);
    }
    /**
     * @return array|mixed|string
     * 编辑短信
     */
    public function edit($id = 0)
    {
        if ($data = Request::instance()->post()) {

            $user = [
                'title' => $data['title'],
                'content' => $data['content'],
            ];

            $res = db('about')->where(['id' => $data['id']])->update($user);
            if ($res === false) {
                return json_data([], 300, '编辑失败');
            }
            return json_data([], 200, '编辑成功');
        }

        if (!$id) {
            $this->error('请刷新后重试');
        }
        $res = db('about')
            ->where(['id' => $id])
            ->find();


        return $this->fetch('edit', ['data' => $res]);
    }
    /**
     * 取消
     */
    public function del()
    {
        $id = Request::instance()->post();

        $res = false;
        if ($id) {
            $res = db('about')->where(['id' => $id['id']])->delete();
        }
        if ($res !== false) {
            return json_data([], 200, '删除成功');
        }
        return json_data([], 300, '删除失败');
    }


}