<?php
/**
 * Created by muniao.
 * User: muniao
 * Date: 2018/7/25
 * Time: 18:52
 */

namespace app\index\controller;



use redis\RedisPackage;
use think\Controller;
use think\Request;
use think\Cookie;
use think\Session;

class Backend extends Controller
{
    protected $admin;
   
   

    public function _initialize($name = null)
    {
        $this->admin = $this->check_authority();
    }

    /**
     * 检测用户登录和权限
     */
    public function check_authority()
    {

        // $access_token = Cookie::get('token');
        // $redis         = new RedisPackage();
        // $access_token=$redis::get($access_token);
        
        // if (empty($access_token)) {
        //     $this->error('您还未登录账号，请先登录', 'login/login');
        // }
        // $access_token=json_decode($access_token);
        // $admin = db('admin')->where(['access_token' => $access_token->access_token])->find();

        // if (empty($admin)) {
        //     $this->error('账号已下线，请重新登录', 'login/login');
        // }
        // if ($admin['status'] != 0) {
        //     $this->error('账号已下线，请重新登录', 'login/login');
        // }
        // // 检测权限
        // $control  = lcfirst(request()->controller());
        // $action   = lcfirst(request()->action());
        // $relation = db('relation')->where('user_id', $admin['admin_id'])->find();

        // $node = db('role')->where('id', $relation['role_id'])->find();
        //   $pc=[];
        // if ($node['rule'] != '*') {
        //     $nods = db('node')->where(['id' => ['in', $node['rule']]])->where('none',0)->select();
        //     foreach ($nods as $k => $o) {
		
        //        if($o['pId'] !=0){
        //             $nods[$k]['co'] = $o['control_name'] . '/' . $o['action_name'];
        //         }else{
        //           $pc[$k]['node_name']=$o['node_name'];
        //           $pc[$k]['uid']=$o['id'];
        //         }


        //     }


        //     $ids = array_column($nods, 'control_name');

        //     $a = [
        //         0 => 'index',
        //         1 => 'need',
        //         2 => 'user',
        //         3 => 'order',
        //     ];
        //     $b = array_merge($ids, $a);
        //       if($control !='export'){
        //           if (!in_array($control, $b)) {
        //               $this->error('没有权限', 'login/login');
        //           }
        //       }


        //     $admin['ss'] = $nods;
        //     $admin['ks'] = array_values($pc);
        // } else {
        //     $admin['ss'] = '*';
        // }

       exit();
    }

    private function authCheck($rule)
    {
        $control = explode('/', $rule)['0'];
        if (in_array($control, ['login', 'index'])) {
            return true;
        }

        if (in_array($rule, session('action'))) {
            return true;
        }

        return false;
    }

    /**
     * 返回json数据
     */
    public function return_json($data, $total)
    {
        $draw   = isset($_REQUEST['draw']) ? $_REQUEST['draw'] : 0;
        $result = [
            "draw" => intval($draw),
            "recordsTotal" => $total,
            "recordsFiltered" => $total,
            "data" => $data
        ];
        exit(json_encode($result));
    }

}
