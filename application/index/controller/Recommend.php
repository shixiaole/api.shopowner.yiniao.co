<?php
/**
 * Created by muniao.
 * User: muniao
 * Date: 2018/8/9
 * Time: 11:22
 */

namespace app\index\controller;


use think\Request;
use think\Cache;
use think\Exception;

class Recommend extends Backend
{
    /**
     * 推荐订单列表
     */
    public function index()
    {
        $data=Request::instance()->get();
        $model = db('order')
            ->alias('a')
            ->field('a.*,b.title,p.province,c.city,u.county')
            ->join('chanel b','a.channel_id=b.id','left')
            ->join('province p', 'a.province_id=p.province_id','left')
            ->join('city c', 'a.city_id=c.city_id','left')
            ->join('county u', 'a.county_id=u.county_id','left');


        if (isset($data['start_time']) && $data['start_time'] != '') {
            $model->where(['a.created_time' => ['>=', strtotime($data['start_time'])]]);
        }
        if (isset($data['end_time']) && $data['end_time'] != '') {
            $model->where(['a.created_time' => ['<=', strtotime($data['end_time']) + 24 * 3600]]);
        }
        if (isset($data['status']) && $data['status'] != '') {
            $model->where(['a.state' => $data['status']]);
        }
        if (isset($data['tui_jian']) && $data['tui_jian'] != '') {
            $title = db('personal')->where(['username' => ['like', "%{$data['tui_jian']}%"]])->field('personal_id')->select();

            $s = '';
            foreach ($title as $key => $val) {
                $s .= $val["personal_id"] . ',';
            }
            $s=substr($s,0,-1);

            $model->where(['a.tui_jian' => ['in', $s]]);
        }
        if (isset($data['contacts']) && $data['contacts'] != '') {
            $model->where(['a.contacts' => ['like', "%{$data['contacts']}%"]]);
        };
        if (isset($data['addres']) && $data['addres'] != '') {
            $model->where(['a.addres' => ['like', "%{$data['addres']}%"]]);
        };
        $model->where(['a.tui_jian' => ['neq', 0]]);

        $list = $model->order('a.created_time desc')
            ->paginate(30)
            ->each(function($item, $key){
                $item['states']=$this->type($item['state']);
                $item['tui_jian']=db('personal')->where('personal_id',$item['tui_jian'])->value('username');

                return $item;

            });


        return $this->fetch('index', ['list' => $list]);
    }


    /**
     * 订单详情
     */
    public function info($order_id = 0)
    {

        if (!$order_id) {
            $this->error('请刷新后重试');
        }

        $res = db('order')
            ->field('order.*,u.username,b.*,p.province,c.city,y.county,co.*,st.*,ac.*,ac.uptime as upda,be.*,be.uptime as uptimes')
            ->join('chanel b','order.channel_id=b.id', 'left')
            ->join('user u','order.assignor=u.user_id ','left')
            ->join('province p', 'order.province_id=p.province_id','left')
            ->join('city c', 'order.city_id=c.city_id','left')
            ->join('county y', 'order.county_id=y.county_id','left')
            ->join('contract co', 'order.contract_id=co.contract_id','left')
            ->join('startup st', 'order.startup_id=st.startup_id','left')
            ->join('product ac', 'order.product_id=ac.product_id','left')
            ->join('before be', 'order.before_id=be.before_id','left')
            ->where(['order_id' => $order_id])
            ->find();

        $s = db('goods_category')->where(['id' => $res['pro_id'], 'pro_type' => 1])->find();
        $res['pro_id'] = !empty($s['title']) ? $s['title'] : '暂无数据';
        $timediff = abs($res['uptime'] - $res['sta_time']);
        //计算天数
        $days = intval($timediff / 86400);
        //计算小时数
        $remain = $timediff % 86400;
        $hours = intval($remain / 3600);
        //计算分钟数
        $remain = $remain % 3600;
        $mins = intval($remain / 60);

        $res['shichang'] = $days.'天'.$hours.'时'.$mins.'分';

        $res['states']=$this->type($res['state']);
        if (!$res) {
            $this->error('未找到该需求');
        }

        if ($res['logo']) {
            $res['logo'] = unserialize($res['logo']);
        }

        if ($res['logos']) {
            $res['logos'] = unserialize($res['logos']);
        }

        if ($res['contract']) {
            $res['contract'] = unserialize($res['contract']);
        }

        if ($res['deposit']) {
            $res['deposit'] = unserialize($res['deposit']);
        }
        if ($res['invoice']) {
            $res['invoice'] = unserialize($res['invoice']);
        }

    //问题
        $capital=db('capital')->where(['ordesr_id'=>$res['order_id'],'types'=>1])->paginate(5)
            ->each(function($item, $key){
                if ($item['capitalgo']) {
                    $item['capitalgo'] = unserialize($item['capitalgo']);

                }
                $item['give_d'] =$item['give_a']."\r\n".$item['give_b'] ;
                return $item;

            });
        //遠程溝通
        $count=db('capital')->where(['ordesr_id'=>$res['order_id'],'types'=>1])->count();

        $cap=db('capital')->where(['ordesr_id'=>$res['order_id'],'types'=>1])->sum('to_price');
        $c=db('capital')->where(['ordesr_id'=>$order_id,'types'=>1])->sum('give_money');

        $r=array($res['dep'],$res['dep_money'],$res['tail']);
        $through=db('through')->where('order_ids','in',$res['order_id'])->order('th_time desc')->paginate(5);
        $thro=db('through')->where(['order_ids'=>$res['order_id'],'baocun'=>1])->order('th_time desc')->find();

        if($thro['baocun']==1){
            $res['totalprice']=$thro['amount'];
        }else{
            $res['totalprice']=$cap-$res['discount']-$res['artificial']-$res['material']-$c;
        }
        $res['total']=array_sum($r);
        //标签
        $label=db('label')->where('label_id','in',$res['label_id'])->select();
        return $this->fetch('info', ['data' => $res,'capital'=>$capital,'label'=>$label,'through'=>$through,'count'=>$count,'c'=>$c]);
    }


    public function type($type)
    {
        if ($type == 0) {
            $title = "待指派";
        } elseif ($type == 1) {
            $title = "待接单";
        } elseif ($type == 2) {
            $title = "待上门";
        } elseif ($type == 3) {
            $title = "待签约";
        } elseif ($type == 4) {
            $title = "待开工";
        } elseif ($type == 5) {
            $title = "待完工";
        } elseif ($type == 6) {
            $title = "已完成";
        } elseif ($type == 7) {
            $title = "已完工";
        } elseif ($type == 8) {
            $title = "已取消";
        } elseif ($type == 9) {
            $title = "退单";
        }elseif ($type == 10){
            $title = "无效订单";
        }
        return $title;
    }


}