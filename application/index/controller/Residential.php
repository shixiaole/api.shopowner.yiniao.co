<?php
/**
 * Created by muniao.
 * User: muniao
 * Date: 2018/7/31
 * Time: 9:39
 */

namespace app\index\controller;


use think\Request;

class Residential extends Backend
{
    /**
     * 短信模板
     */
    public function index(){
        $data=db('residential')->paginate(10);
        return $this->fetch('index',['data'=>$data]);
    }

    /**
     * @return array|mixed|string
     * 添加短信
     */
    public function add(){
        if($data =Request::instance()->post()){

            $user = [
                'content' => $data['content'],
            ];

            $res = db('residential')->insertGetId($user);
            if($res===false){
                return json_data([], 300, '添加失败');
            }
            return json_data([], 200, '添加成功');
        }

        return $this->fetch('add',['data'=>$data]);
    }
    /**
     * @return array|mixed|string
     * 编辑短信
     */
    public function edit($label_id = 0)
    {
        if ($data = Request::instance()->post()) {

            $user = [
                'content' => $data['content'],
            ];

            $res = db('residential')->where(['residential_id' => $data['label_id']])->update($user);
            if ($res === false) {
                return json_data([], 300, '编辑失败');
            }
            return json_data([], 200, '编辑成功');
        }

        if (!$label_id) {
            $this->error('请刷新后重试');
        }
        $res = db('residential')
            ->where(['residential_id' => $label_id])
            ->find();


        return $this->fetch('edit', ['data' => $res]);
    }
    /**
     * 取消
     */
    public function del()
    {
        $store_id = Request::instance()->post();

        $res = false;
        if ($store_id) {
            $res = db('residential')->where(['residential_id' => $store_id['label_id']])->delete();
        }
        if ($res !== false) {
            return json_data([], 200, '删除成功');
        }
        return json_data([], 300, '删除失败');
    }



}