<?php 
namespace app\index\controller;
use app\index\model\XcxRelationDouble;
use phpcvs\Csv;
use think\Db;

class Activity extends Backend {



    public function count(XcxRelationDouble $relationDouble)
    {
        $info = [];
        $pay_num = $relationDouble->field(["from_unixtime(updated_at, '%Y-%m-%d') AS ymd", "count(*) AS pay_num"])->where(["status" => $relationDouble::STATUS_ALREADY])->group("from_unixtime(now(), '%Y-%m-%d')")->select();
        foreach (collection($pay_num)->toArray() as $item) {
            $info[$item['ymd']]['pay_num'] = $item['pay_num'];
        }
        $poster_num = $relationDouble->field(["from_unixtime(updated_at, '%Y-%m-%d') AS ymd", "count(*) AS poster_num"])->where(["pid_type" => $relationDouble::PID_TYPE_CUSTOMER, "status" => $relationDouble::STATUS_ALREADY])->whereOr(["pid_type" => $relationDouble::PID_TYPE_PERSONAL])->group("from_unixtime(updated_at, '%Y-%m-%d')")->select();
        foreach (collection($poster_num)->toArray() as $item) {
            $info[$item['ymd']]['poster_num'] = $item['poster_num'];
        }
        $add_num = $relationDouble->field(["from_unixtime(created_at, '%Y-%m-%d') AS ymd", "count(*) AS add_num"])->group("from_unixtime(created_at, '%Y-%m-%d')")->select();
        foreach (collection($add_num)->toArray() as $item) {
            $info[$item['ymd']]['add_num'] = $item['add_num'];
        }
        $bonus_num = $relationDouble->field(["from_unixtime(created_at, '%Y-%m-%d') AS ymd", "count(*) AS bonus_num", "sum(bonus) AS bonus_sum"])->where(["pid_type" => $relationDouble::PID_TYPE_CUSTOMER])->where('bonus', '<>', 0)->group("from_unixtime(created_at, '%Y-%m-%d')")->select();
        foreach (collection($bonus_num)->toArray() as $item) {
            $info[$item['ymd']]['bonus_num'] = $item['bonus_num'];
            $info[$item['ymd']]['bonus_sum'] = $item['bonus_sum'];
        }
        $recharge_num = Db::name('recharge')->field(["from_unixtime(created_time, '%Y-%m-%d') AS ymd", "count(*) AS recharge_num", "sum(money) AS recharge_sum"])->where(["status" => 1, "type" => 3, "mode" => 4])->group("from_unixtime(created_time, '%Y-%m-%d')")->select();
        foreach ($recharge_num as $item) {
            $info[$item['ymd']]['recharge_num'] = $item['recharge_num'];
            $info[$item['ymd']]['recharge_sum'] = $item['recharge_sum'];
        }
        return $info;
    }

    public function export(XcxRelationDouble $relationDouble)
    {
        $lists = $this->count($relationDouble);
        ksort($lists);
        $first_date = strtotime(key($lists) . ' 00:00:00');
        krsort($lists);
        $end_date = strtotime(key($lists) . ' 00:00:00');
        $count = ($end_date - $first_date) / (24 * 60 * 60);
        $res = [];
        $item = [
            'ymd' => null,
            'pay_new' => 0,
            'pay_all' => 0,
            'poster_new' => 0,
            'add_all' => 0,
            'add_new' => 0,
            'bonus_num_all' => 0,
            'bonus_num_new' => 0,
            'bonus_sum_all' => 0,
            'bonus_sum_avg' => 0,
            'bonus_sum_new' => 0,
            'cash_sum_all' => 0,
            'cash_num_new' => 0
        ];
        for ($i = 0; $i <= $count; $i++) {
            $ymd = date("Y-m-d", $first_date + $i * 24 * 60 * 60);
            $item['ymd'] = date("Y-m-d", $first_date + $i * 24 * 60 * 60);
            $item['week'] =$this->week(date("w",$first_date + $i * 24 * 60 * 60));
            $item['pay_new'] = isset($lists[$ymd]['pay_num']) ? $lists[$ymd]['pay_num'] : 0;
            $item['pay_all'] += isset($lists[$ymd]['pay_num']) ? $lists[$ymd]['pay_num'] : 0;
            $item['poster_new'] = isset($lists[$ymd]['poster_num']) ? $lists[$ymd]['poster_num'] : 0;
            $item['add_all'] += isset($lists[$ymd]['add_num']) ? $lists[$ymd]['add_num'] : 0;
            $item['add_new'] = isset($lists[$ymd]['add_num']) ? $lists[$ymd]['add_num'] : 0;
            $item['bonus_num_all'] += isset($lists[$ymd]['bonus_num']) ? $lists[$ymd]['bonus_num'] : 0;
            $item['bonus_num_new'] = isset($lists[$ymd]['bonus_num']) ? $lists[$ymd]['bonus_num'] : 0;
            $item['bonus_sum_all'] += isset($lists[$ymd]['bonus_sum']) ? $lists[$ymd]['bonus_sum'] : 0;
            $item['bonus_sum_avg'] = $item['bonus_sum_all'] / $item['bonus_num_all'];
            $item['bonus_sum_new'] = isset($lists[$ymd]['bonus_sum']) ? $lists[$ymd]['bonus_sum'] : 0;
            $item['cash_sum_all'] = isset($lists[$ymd]['recharge_sum']) ? $lists[$ymd]['recharge_sum'] : 0;
            $item['cash_num_new'] = isset($lists[$ymd]['recharge_num']) ? $lists[$ymd]['recharge_num'] : 0;
            $res[] = $item;
        }
        return $this->fetch('index', ['data' => $res]);

    }
    public function week($v){
        $weekarray=array("星期日","星期一","星期二","星期三","星期四","星期五","星期六");
        return $weekarray[$v];
    }
   public function detail(XcxRelationDouble $relationDouble)
    {
        $lists = $relationDouble->select();
        $res = [];
        foreach ($lists as $list) {
            $item['customer_mobile'] = $list->customer->mobile;
            $item['is_pay'] = $list['status'] == $relationDouble::STATUS_ALREADY ? 1 : 0;
            $item['pid'] = $list['pid_type'] == $relationDouble::PID_TYPE_CUSTOMER ? '用户：' . Db::name('customers')->where('customers_id', $list['pid'])->find()['mobile']
                : '合伙人：' . Db::name('personal')->where('personal_id', $list['pid'])->find()['mobile'];
            $item['pay_at'] = $list['pay_at'];
            $item['is_poster'] = $list['status'] == $relationDouble::STATUS_ALREADY ? 1 : 0;
            $item['bonus_user'] = $list['pid_type'] == $relationDouble::PID_TYPE_CUSTOMER ? '用户：' . Db::name('customers')->where('customers_id', $list['pid'])->find()['mobile']
                : '合伙人：' . Db::name('personal')->where('personal_id', $list['pid'])->find()['mobile'];
            $item['bonus'] = $list['bonus'];
            if ($list['pid_type'] == $relationDouble::PID_TYPE_CUSTOMER) {
                $recharge = Db::name('recharge')->field(['*', "SUM(money) as all_money"])->where('customers_id', $list['pid'])->where(["status" => 1, "type" => 3, "mode" => 4])->order('created_time asc')->find();
                $item['cash_money'] = $recharge['all_money'] ?? 0;
                $item['cash_at'] = $recharge['created_time'] ?? 0;
            } else {
                $item['cash_money'] = 0;
                $item['cash_at'] = 0;
            }
            $res[] = $item;
        }
        return $this->fetch('detail', ['data' => $res]);

    }
}
