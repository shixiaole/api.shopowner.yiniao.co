<?php
/**
 * Created by muniao.
 * User: muniao
 * Date: 2018/7/31
 * Time: 9:39
 */

namespace app\index\controller;


use think\Request;

class Lie extends Backend
{
    /**
     * 首页
     */
    public function index(){
        $data=Request::instance()->get();
        $k['type']=isset($data['type'])?$data['type']:$data['type'];
        $data=db('lie')->where('type', $k['type'])->paginate(10)
            ->each(function($item, $key){
            if ($item['planting_id']) {
                $item['planting_id'] = db('planting')->where('planting_id',$item['planting_id'])->value('title');
            }
                if ($item['type']==1) {
                    $item['type'] = '工具';
                }else{
                    $item['type'] = '热点资讯';
                }
            return $item;

        });

        return $this->fetch('index',['data'=>$data,'k'=>$k['type']]);
    }

    /**
     * @return array|mixed|string
     * 添加
     */
    public function add(){
        if($data =Request::instance()->post()){

            $user = [
                'title' => $data['title'],
                'url' => $data['url'],
                'logo' => !empty($data['logo'])?$data['logo']:'',
                'type' => $data['type'],
                'planting_id' => $data['planting_id'],
                'state' => 0,
            ];

            $res = db('lie')->insertGetId($user);
            if($res===false){
                return json_data([], 300, '添加失败');
            }
            return json_data([], 200, '添加成功');
        }
        $fen=Request::instance()->get();

        if($fen['fen']==1){
            $chanel = db('planting')->where('type',1)->select();
            foreach ($chanel as $m=>$p){
                if($p['title']=='更多'){
                    unset($chanel[$m]);
                }
            }

        }elseif($fen['fen']==3){
            $chanel = db('planting')->where('type',3)->select();

        }



        return $this->fetch('add',['data'=>$data,'oo'=>$chanel,'k'=>$fen['fen']]);
    }
    /**
     * @return array|mixed|string
     * 编辑
     */
    public function edit($lie_id = 0)
    {
        if ($data = Request::instance()->post()) {

            $user = [
                'title' => $data['title'],
                'url' => $data['url'],
                'logo' => !empty($data['logo'])?$data['logo']:'',
                'planting_id' => $data['planting_id'],
            ];

            $res = db('lie')->where(['lie_id' => $data['lie_id']])->update($user);
            if ($res === false) {
                return json_data([], 300, '编辑失败');
            }
            return json_data([], 200, '编辑成功');
        }

        if (!$lie_id) {
            $this->error('请刷新后重试');
        }

        $res = db('lie')
            ->where(['lie_id' => $lie_id])
            ->find();

        if($res['type']==1){
            $go = db('planting')->where(['planting_id'=>['neq',$res['planting_id']],'type'=>1])->select();
            foreach ($go as $m=>$p){
                if($p['title']=='更多'){
                    unset($go[$m]);
                }
            }

        }elseif($res['type']==3){
            $go = db('planting')->where(['planting_id'=>['neq',$res['planting_id']],'type'=>3])->select();

        }

        $g1=db('planting')->where(['planting_id'=>$res['planting_id']])->value('title');
        return $this->fetch('edit', ['data' => $res,'go1'=>$go,'g1'=>$g1]);
    }
    /**
     * 取消
     */
    public function del()
    {
        $store_id = Request::instance()->post();

        $res = false;
        if ($store_id) {
            $res = db('lie')->where(['lie_id' => $store_id['lie_id']])->delete();
        }
        if ($res !== false) {
            return json_data([], 200, '删除成功');
        }
        return json_data([], 300, '删除失败');
    }
    /**
     * 启用 禁用
     */
    public function disable(){
        $lie_id=Request::instance()->post('lie_id');
        $status=Request::instance()->post('status');
        if($lie_id && ($status==0 || $status==1)){
            $res=db('lie')->where(['lie_id'=>$lie_id])->update(['state'=>$status]);
            if($res!==false){
                $this->result([],200,'操作成功');
            }
        }
        $this->result([],300,'请刷新后重试');
    }

}
