<?php
/**
 * Created by muniao.
 * User: muniao
 * Date: 2018/7/31
 * Time: 9:39
 */

namespace app\index\controller;


use think\Request;

class Stock extends Backend
{
    /*
     * 1保护类2福利类3粉料类4管控材料类5其他小件6水电五金类7涂料类
     */
    public function index()
    {
        $data=db('stock')->field('id,class_table,type')->group('type')->paginate(20);
        return $this->fetch('index', ['data'=>$data]);
    }
    

    public function add($id=0)
    {
        if ($data=Request::instance()->post()) {
         
            $user=[
                'code'    =>$data['code'],
                'stock_name'=>$data['stock_name'],
                'stock_model'  =>$data['stock_model'],
                'valuation_method'     =>$data['valuation_method'],
                'img'     =>isset($data['img'])?$data['img']:'',
                'class_table'      =>$data['class_table'],
                'brand'      =>$data['brand'],
                'company'      =>$data['company'],
                'specifications'      =>$data['specifications'],
                'describe'      =>$data['describe'],
                'reference_cost'      =>$data['reference_cost'],
                'latest_cost'      =>$data['latest_cost'],
                'created_date'      =>date('Y-m-d H:i:s', time()),
                'type'      =>$data['type'],
            ];
            
            $res=db('stock')->insertGetId($user);
            if ($res === false) {
                return json_data([], 300, '添加失败');
            }
            return json_data([], 200, '添加成功');
        }
        
        return $this->fetch('add', ['id'=>$id]);
    }
    
  
    
    public function edit($id=0)
    {
    
        if ($data=Request::instance()->post()) {
            $user=[
                'code'    =>$data['code'],
                'stock_name'=>$data['stock_name'],
                'stock_model'  =>$data['stock_model'],
                'valuation_method'     =>$data['valuation_method'],
                'class_table'      =>$data['class_table'],
                'img'     =>isset($data['img'])?$data['img']:'',
                'brand'      =>$data['brand'],
                'specifications'      =>$data['specifications'],
                'describe'      =>$data['describe'],
                'company'      =>$data['company'],
                'reference_cost'      =>$data['reference_cost'],
                'latest_cost'      =>$data['latest_cost'],
            ];
            
            $res=db('stock')->where(['id'=>$data['id']])->update($user);
            if ($res === false) {
                return json_data([], 300, '编辑失败');
            }
            return json_data([], 200, '编辑成功');
        }
        
        if (!$id) {
            $this->error('请刷新后重试');
        }
        $res=db('stock')
            ->where(['id'=>$id])
            ->find();
        
        return $this->fetch('edit', ['data'=>$res]);
    }
    
    public function del()
    {
        $id = Request::instance()->post();
        
        if ($id) {
            $res=db('stock')->where(['id'=>$id['id']])->update(['deleted_at'=>time()]);
    
            if ($res !== false) {
                return json_data([], 200, '成功');
            }
        }
        return json_data([], 300, '失败');
    }
    
    public function info($id=0)
    {
        $data=db('stock')->where(['type'=>$id])->whereNull('deleted_at')->paginate(20);
        return $this->fetch('info', ['data'=>$data]);
    }
    
}
