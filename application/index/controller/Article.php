<?php
/**
 * Created by muniao.
 * User: muniao
 * Date: 2018/7/31
 * Time: 9:39
 */

namespace app\index\controller;


use think\Request;

class Article extends Backend
{
    /**
     * 首页
     */
    public function index(){
        $data=Request::instance()->get();
        $k['type']=isset($data['type'])?$data['type']:$data['type'];
        $data=db('article')->where('fen', $k['type'])->paginate(10)
            ->each(function($item, $key){
            if ($item['type']) {
                $item['type'] = db('tool')->where('tool_id',$item['type'])->value('content');

            }
            if ($item['fen']==1) {
                $item['fens'] = 'black';

            }else{
                $item['fens'] = 'none';
            }
            return $item;

        });
        if ($k['type']==1) {
            $S['fens'] = 'black';

        }else{
            $S['fens'] = 'none';
        }
        return $this->fetch('index',['data'=>$data,'k'=>$k['type'],'s'=>$S]);
    }

    /**
     * @return array|mixed|string
     * 添加
     */
    public function add(){
        if($data =Request::instance()->post()){
            if($data['fen']==0){
                $type=$data['type'];
                $url='';
            }else{
                $type='';
                $url=$data['url'];
            }
            $user = [
                'title' => $data['title'],
                'url' => $url,
                'logo' => $data['logo'],
                'rmake' => $data['rmake'],
                'type' => $type,
                'fen' => $data['fen'],
                'state' => 0,
            ];

            $res = db('article')->insertGetId($user);
            if($res===false){
                return json_data([], 300, '添加失败');
            }
            return json_data([], 200, '添加成功');
        }
        $chanel = db('tool')->select();
        $fen=Request::instance()->get();
        return $this->fetch('add',['data'=>$data,'oo'=>$chanel,'k'=>$fen['fen']]);
    }
    /**
     * @return array|mixed|string
     * 编辑
     */
    public function edit($article_id = 0)
    {
        if ($data = Request::instance()->post()) {
            if($data['fen']==0){
                $type=$data['type'];
                $url='';
            }else{
                $type='';
                $url=$data['url'];
            }
            $user = [
                'title' => $data['title'],
                'url' => $url,
                'type' => $type,
                'logo' =>$data['logo'],
                'fen' => $data['fen'],
                'rmake' => $data['rmake'],
            ];

            $res = db('article')->where(['article_id' => $data['article_id']])->update($user);
            if ($res === false) {
                return json_data([], 300, '编辑失败');
            }
            return json_data([], 200, '编辑成功');
        }

        if (!$article_id) {
            $this->error('请刷新后重试');
        }
        $res = db('article')
            ->where(['article_id' => $article_id])
            ->find();

        $s = db('tool')->where(['tool_id' => $res['type']])->find();
        $res['pro_ids'] = !empty($s['content']) ? $s['content'] : '暂无数据';
        $chanel = db('tool')->select();
        $go=db('tool')->where(['tool_id'=>['neq',$res['type']]])->select();
        return $this->fetch('edit', ['data' => $res,'chanel'=>$chanel,'go'=>$go]);
    }
    /**
     * 取消
     */
    public function del()
    {
        $store_id = Request::instance()->post();

        $res = false;
        if ($store_id) {
            $res = db('article')->where(['article_id' => $store_id['article_id']])->delete();
        }
        if ($res !== false) {
            return json_data([], 200, '删除成功');
        }
        return json_data([], 300, '删除失败');
    }
    /**
     * 启用 禁用
     */
    public function disable(){
        $article_id=Request::instance()->post('article_id');
        $status=Request::instance()->post('status');
        if($article_id && ($status==0 || $status==1)){
            $res=db('article')->where(['article_id'=>$article_id])->update(['state'=>$status]);
            if($res!==false){
                $this->result([],200,'操作成功');
            }
        }
        $this->result([],300,'请刷新后重试');
    }
    /**
     * 短信模板
     */
    public function tel(){
        $data=db('tool')->paginate(10);

        return $this->fetch('tel',['data'=>$data]);
    }

    /**
     * @return array|mixed|string
     * 添加短信
     */
    public function adds(){
        if($data =Request::instance()->post()){

            $user = [
                'content' => $data['content'],
                'logo' => !empty($data['logo'])?$data['logo']:'',
                'state'=>$data['state']
            ];

            $res = db('tool')->insertGetId($user);
            if($res===false){
                return json_data([], 300, '添加失败');
            }
            return json_data([], 200, '添加成功');
        }

        return $this->fetch('adds',['data'=>$data]);
    }
    /**
     * @return array|mixed|string
     * 编辑短信
     */
    public function edits($tool_id = 0)
    {
        if ($data = Request::instance()->post()) {

            $user = [
                'content' => $data['content'],
                'logo' => !empty($data['logo'])?$data['logo']:'',
                'state'=>$data['state']
            ];

            $res = db('tool')->where(['tool_id' => $data['tool_id']])->update($user);
            if ($res === false) {
                return json_data([], 300, '编辑失败');
            }
            return json_data([], 200, '编辑成功');
        }

        if (!$tool_id) {
            $this->error('请刷新后重试');
        }
        $res = db('tool')
            ->where(['tool_id' => $tool_id])
            ->find();


        return $this->fetch('edits', ['data' => $res]);
    }
    /**
     * 取消
     */
    public function dels()
    {
        $store_id = Request::instance()->post();

        $res = false;
        if ($store_id) {
            $res = db('tool')->where(['tool_id' => $store_id['tool_id']])->delete();
        }
        if ($res !== false) {
            return json_data([], 200, '删除成功');
        }
        return json_data([], 300, '删除失败');
    }
}