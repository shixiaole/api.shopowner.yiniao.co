<?php
/**
 * Created by muniao.
 * User: muniao
 * Date: 2018/7/27
 * Time: 9:20
 */

namespace app\index\controller;


use app\common\model\Version;
use think\Exception;
use think\Request;

class Customers extends Backend
{
    /**
     * 用户列表
     */
    public function index(){
        $data=Request::instance()->get();

        $model=db('customers');
        if(isset($data['start_time']) && $data['start_time']!=''){
            $model->where(['customers.created_time'=>['>=',strtotime($data['start_time'])]]);
        }
        if(isset($data['end_time']) && $data['end_time']!=''){
            $model->where(['customers.created_time'=>['<=',strtotime($data['end_time'])+24*3600]]);
        }
        if(isset($data['username']) && $data['username']!=''){
            $model->where(['customers.username'=>['like',"%{$data['username']}%"]]);
        }
        if(isset($data['mobile']) && $data['mobile']!=''){
            $model->where(['customers.mobile'=>['like',"%{$data['mobile']}%"]]);
        }

            $model->where(['customers.status'=>['<>','2']]);

        $list=$model->order('customers.created_time desc')
            ->paginate(30);


        return $this->fetch('index',['list'=>$list,'data'=>$data]);
    }
    /**
     * 修改
     */
    public function edit($customers_id=0)
    {
        if ($data = Request::instance()->post()) {
            $user = [
                'username' => $data['username'],
                'mobile'=>$data['mobile'],
                'sex' => $data['sex'],
                'avatar' => $data['logo'],
                'email' => $data['email'],

            ];

            $res = db('customers')->where(['customers_id' => $data['customers_id']])->update($user);

            if ($res) {

                return json_data([], 200, '编辑成功');
            }
            return json_data([], 300, '编辑失败');
        }else{
            if(!$customers_id){
                $this->error('请刷新后重试');
            }
            $data=db('Customers')->where(['Customers_id'=>$customers_id,])->find();

            if(!$data){
                $this->error('用户不存在或已删除');
            }
            return $this->fetch('edit',['data'=>$data]);
        }
    }
    /**
 * 添加
 */
    public function add()
    {
        if ($data = Request::instance()->post()) {
            $user = [
                'username' => $data['username'],
                'password'=>encrypt($data['password']),
                'mobile'=>$data['mobile'],
                'sex' => $data['sex'],
                'avatar' => empty($data['logo'])?'https://www.yiniaoweb.com/static/images/login/tx.png':$data['logo'],
                'email' => $data['email'],
                'age' => 18,
                'status' =>0,
                'created_time'=>time(),
            ];


            $res = db('customers')->insertGetId($user);

            if ($res !== false) {
                return json_data([], 200, '添加成功');
            }
            return json_data([], 300, '添加失败');
        }
            return $this->fetch('add');

    }
    /**
     * 删除用户
     */
    public function del(){
        $Customers_id=Request::instance()->post('customers_id');
        $res=false;

        if($Customers_id){

            $res=db('customers')->where(['customers_id'=>$Customers_id])->update(['status'=>2]);
        }
        if($res){

            return json_data([],200,'删除成功');

        }
        return json_data([],300,'删除失败');
    }
    /**
     * 启用 禁用
     */
    public function disable(){
        $Customers_id=Request::instance()->post('customers_id');
        $status=Request::instance()->post('status');

        if($Customers_id && ($status==0 || $status==1)){
            $res=db('customers')->where(['customers_id'=>$Customers_id])->update(['status'=>$status]);
            if($res!==false){
                $this->result([],200,'操作成功');
            }
        }
        $this->result([],300,'请刷新后重试');
    }






}