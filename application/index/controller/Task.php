<?php
/**
 * Created by PhpStorm.
 * User: Raytine
 * Date: 2019/10/10
 * Time: 10:58
 */

namespace app\api\controller;

use think\Controller;
use app\api\model\Authority;
use  think\Request;
use app\api\model\TaskModel;
use app\api\model\Capital;


class Task extends Controller
{
    protected $model;
    protected $us;

    public function _initialize()
    {
        $this->model = new Authority();
        $this->us    = $this->model->check();
    }


    /*
     * 好
     */
    public function index(TaskModel $taskModel)
    {
        $data   = Request::instance()->post();

        $accept = '';
        if ($data) {

            $accept = $taskModel::AcceptanceParameter($data,$this->us);}

        $parem = [
            'table' => $taskModel::parameter(),
            'condition' => $accept ,
            'fields' => 'order.order_id,order.order_no,order.addres,order.state,order.contacts,order.telephone',
            'page' => $data['page'],
        ];

        $sign = sign($parem, config('Verification'));
        $op   = send_post(U_SR . '/api/v1/fullOrder?' . http_build_query($parem) . '&sign=' . $sign, []);
        $ops  = json_decode($op, true);
var_dump($ops);die;
       if($ops['code']==200){
           foreach ($ops['data']['data'] as $key => $item) {
               $ops['data']['data'][$key]['states']              = $taskModel::type($item['state']);
               $ops['data']['data'][$key]['Residential_address'] = $item['province'][0]['province'] . $item['city'][0]['city'] . $item['county'][0]['county'] . $item['addres'];
               $xing                                             = substr($item['telephone'], 3, 4);  //获取手机号中间四位
               $ops['data']['data'][$key]['telephone']           = str_replace($xing, '****', $item['telephone']);

           }
       }else{
           $ops=[];
       }



        json_data($ops);
    }

    public function down(TaskModel $taskModel)
    {
        $Capital=new Capital();
        $data = Request::instance()->post();
        $accept = '';
        if ($data) {
            $accept = $taskModel::AcceptanceParameter($data,$this->us);
        }

        $parem = [
            'table' => $taskModel::parameter(),
            'condition' => $accept ,
            'fields' => 'order.order_id,order.created_time,order.order_no,order.addres,order.state,order.contacts,order.product_id,order.telephone,order.planned',
            'limit' =>$data['count'],
        ];

        $sign = sign($parem, config('Verification'));
        $op   = send_post(U_SR . '/api/v1/fullOrder?' . http_build_query($parem) . '&sign=' . $sign, []);
        $list  = json_decode($op, true);
        foreach ($list['data']['data'] as $k => $v) {
            $ues[$k]['order_id']   = $v['order_id'];
            $ues[$k]['channel_id'] = $v['chanel'][0]['title'];
            $ues[$k]['order_no']   = $v['order_no'] . "\t";
            $ues[$k]['contacts']   = $v['contacts'];
            $ues[$k]['telephone']  = $v['telephone'] . "\t";
            $ues[$k]['addres']     = $v['province'][0]['province'] . $v['city'][0]['city'] . $v['county'][0]['county'] . $v['addres'];
            if ($v['state'] == 7 && $v['product_id'] == 0) {
                $o = "已完工";
            } elseif ($v['state'] == 7 && $v['product_id'] != 0) {
                $o = "已完成";
            } else {
                $o =$taskModel::type($v['state']);
            }
            $ues[$k]['states']       = $o;
            $ues[$k]['created_time'] = date('Y-m-d H:i:s', $v['created_time']) . "\t";
            if ($v['planned']) {
                $ues[$k]['planned'] = date('Y-m-d H:i:s', $v['planned']) . "\t";
            } else {
                $ues[$k]['planned'] = '';
            }
            if ($v['state'] != 10) {
                $ues[$k]['you'] = "有效";
            } else {
                $ues[$k]['you'] = "无效";
            }

            $turnover0             = $Capital->where(['ordesr_id' => $v['order_id'], 'types' => $Capital::Parameter_Deletion])->sum('to_price');//成交额

            $capital = db('envelopes',config('database.order'))->where(['ordesr_id' =>$v['order_id'],'through_id'=>['eq',0]])->sum('give_money');
            $turnover1             = db('through',config('database.order'))->where(['order_ids' =>6, 'baocun' =>$Capital::Parameter_Deletion])->field('amount,through_id')->find();//成交额
            $envelopes = db('envelopes',config('database.order'))->where(['through_id' =>$turnover1['through_id']])->sum('give_money');
            $ues[$k]['turnover']   = (int)$turnover0 + (int)$turnover1 - (int)$envelopes-(int)$capital;
            $ues[$k]['namekl']     = $v['user'][0]['username'];
            $ues[$k]['store_name'] = $v['store'][0]['store_name'];
            $ues[$k]['score']      = $v['product']?$v['product'][0]['score']:'';
            $ues[$k]['full']       = $v['product']?$v['product'][0]['full']:'';;
            $ues[$k]['content1']   = $v['residential']?$v['residential'][0]['content']:'';;
            $ues[$k]['management'] = $v['community']?$v['community'][0]['management']:'';
            if($v['cost']){
                if ($v['cost'][0]['type'] == 1) {
                    $ues[$k]['type'] = '是';
                } elseif ($v['cost'][0]['type'] == 5) {
                    $ues[$k]['type'] = '否';
                } else {
                    $ues[$k]['type'] = '待定';
                }
            }else{
                $ues[$k]['type'] = '待定';
            }

            $ues[$k]['content'] =$v['cost']?$v['cost'][0]['content']:'' ;
            unset($list[$k]);
        }
        json_data($ues);


    }

}
