<?php
/**
 * Created by muniao.
 * User: muniao
 * Date: 2018/7/31
 * Time: 9:39
 */

namespace app\index\controller;


use think\Request;
use think\Exception;

class Recharge extends Backend
{
    /**
     * 充值记录
     */
    public function index()
    {
        $data=Request::instance()->get();
        $da = db('recharge');
            if (isset($data['customers_id']) && $data['customers_id'] != '') {
                $title = db('customers')->where(['username' => ['like', "%{$data['customers_id']}%"]])->field('customers_id')->select();

                $s=arr_to_str($title);
                $da->where(['customers_id' => ['in', $s]]);
            };
        if (isset($data['order_no']) && $data['order_no'] != '') {
            $da->where(['order_no' => $data['order_no']]);
        };
        $datas=$da->order('recharge_id desc')->where('type',$data['type'])->paginate(20)
            ->each(function ($item, $key) {
            if($item['status']==0){
                $item['status']='未支付';
            }else{
                $item['status']='已成功';
            }
                if (isset($item['youhui_id']) && $item['youhui_id'] != '') {
                     $i=db('discount')->join('youhui yo','yo.youhui_id=discount.youhui_id','left')->where(['discount.youhui_id' => $item['youhui_id'], 'discount.customers_id' => $item['customers_id'],'discount.status' => 1])->find();
                    $i=$i['jine']*$i['double'];

                    $item['youhui_id'] = '使用优惠券' . $i . '/元';
                }else{
                    $item['youhui_id']='未使用优惠券';
                }
                if ($item['mode'] == 1) {
                    $item['mode'] = '微信';
                } elseif ($item['mode'] == 2) {
                    $item['mode'] = '余额';
                } else if($item['mode'] == 3){
                    $item['mode'] = '翻倍活动';
                }else if($item['mode'] == 4){
                    $item['mode'] = '红包提现/用户端';
                    $item['customers_id'] = db('customers')->where('customers_id', $item['customers_id'])->value('username');
                }else if($item['mode'] == 5){
                    $item['mode'] = '红包提现/合伙人';
                    $item['customers_id'] = db('personal')->where('personal_id', $item['customers_id'])->value('username');
                }
                    return $item;

            });

        return $this->fetch('index', ['data' => $datas,'ta'=>$data['type']]);
    }


}
