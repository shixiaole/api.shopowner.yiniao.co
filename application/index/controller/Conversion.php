<?php
/**
 * Created by muniao.
 * User: muniao
 * Date: 2018/8/9
 * Time: 11:22
 */

namespace app\index\controller;


use think\Request;
use think\Cache;
use think\Db;

use phpcvs\Csv;

class Conversion extends Backend
{
    /**
     * 订单列表
     */
    public function index()
    {
        $data=Request::instance()->get();
        $model = db('conversion');
        if (isset($data['start_time']) && $data['start_time'] != '') {
            $start = strtotime(date('Y-m-d 00:00:00',strtotime($data['start_time'])));
            $end   = strtotime(date('Y-m-d 23:59:59',strtotime($data['end_time'])));
            $model->where(['date' => ['between', [$start, $end]]]);
        }
        $list = $model->order('date desc')
            ->paginate(30);
        return $this->fetch('index', ['list' => $list]);
    }



}
