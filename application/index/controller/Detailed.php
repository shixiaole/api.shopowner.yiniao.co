<?php
/**
 * Created by muniao.
 * User: muniao
 * Date: 2018/8/9
 * Time: 11:22
 */

namespace app\index\controller;


use think\Request;
use think\Cache;
use think\Exception;

class Detailed extends Backend
{
    /**
     * @return mixed
     * 问题
     */
    public function index()
    {
        $data=db('detailed')->where(['detaileds_id'=>0,'display'=>1])->paginate(10);
        return $this->fetch('index', ['data'=>$data]);
    }
    
    /**
     * @return mixed
     * 修改
     */
    public function edi($id=0)
    {
        
        if ($data=Request::instance()->post()) {
            $use=[
                'detailed_title'=>$data['detailed_title'],
                'detaileds_id'  =>!empty($data['detaileds_id'])? $data['detaileds_id'] : 0,
                'serial'        =>!empty($data['serial'])? $data['serial'] : 0,
                'un_id'         =>!empty($data['un_id'])? $data['un_id'] : 0,
                'artificial'    =>!empty($data['artificial'])? $data['artificial'] : '',
                'rmakes'        =>!empty($data['rmakes'])? $data['rmakes'] : '',
                'pri'           =>!empty($data['pri'])? $data['pri'] : '',
                'pris'          =>!empty($data['pris'])? $data['pris'] : '',
                'updated_at'    =>time(),
                'xu'            =>!empty($data['xu'])? $data['xu'] : '',
                'agency_id'=>isset($data['agency_id']) && !empty($data['agency_id'])?implode($data['agency_id'],','):'',
                ];
            
            $res=db('detailed')->where(['detailed_id'=>$data['detailed_id']])->update($use);
            if ($res === false) {
                return json_data([], 300, '编辑失败');
            }
            return json_data([], 200, '编辑成功');
        }
        if (!$id) {
            $this->error('请刷新后重试');
        }
        $res=db('detailed')
            ->join('unit u', 'detailed.un_id=u.id', 'left')
            ->where(['detailed_id'=>$id])
            ->find();
        
        $g=db('unit')->where(['id'=>['neq', $res['un_id']]])->select();
        $agency=db('agency')->whereIn('agency_id',$res['agency_id'])->select();
        
        return $this->fetch('edi', ['data'=>$res, 'g'=>$g,'agency'=>$agency]);
        
    }
    public function agency(){
        $data=Request::instance()->post();
        $agency=db('agency')->where(['state'=>1])->whereLike('random_number',"%{$data['name']}%")->select();
        
        return json_data($agency,200);
    }
    
    /**
     * @return mixed
     * 添加
     */
    public function ad($id)
    {
        
        if ($data=Request::instance()->post()) {
            $use=[
                'detailed_title'=>$data['detailed_title'],
                'detaileds_id'  =>!empty($data['detaileds_id'])? $data['detaileds_id'] : 0,
                'serial'        =>!empty($data['serial'])? $data['serial'] : 0,
                'un_id'         =>0,
                'artificial'    =>'',
                'rmakes'        =>'',
                'pri'           =>'',
                'pris'          =>'',
                'updated_at'    =>time(),
                'agency'    =>$data['agency'],
            
            ];
            if (empty($data['detailed_id'])) {
                $res=db('detailed')->insertGetId($use);
            } else{
                $res=db('detailed')->where('detailed_id', $data['detailed_id'])->update($use);
            }
            
            if ($res) {
                return json_data([], 200, '添加成功');
                
            }
            return json_data([], 300, '添加失败');
        }
        
        $res=db('detailed')->where(['detailed_id'=>$id])
            ->find();
        
        return $this->fetch('ad', ['data'=>$res]);
        
    }
    
    /**
     * @return mixed
     * 添加
     */
    public function add($id)
    {
        
        if ($data=Request::instance()->post()) {
        
            $use=[
                'detailed_title'=>$data['detailed_title'],
                'detaileds_id'  =>!empty($data['detaileds_id'])? $data['detaileds_id'] : 0,
                'serial'        =>!empty($data['serial'])? $data['serial'] : 0,
                'un_id'         =>!empty($data['un_id'])? $data['un_id'] : 0,
                'artificial'    =>!empty($data['artificial'])? $data['artificial'] : '',
                'rmakes'        =>!empty($data['rmakes'])? $data['rmakes'] : '',
                'pri'           =>!empty($data['pri'])? $data['pri'] : '',
                'pris'          =>!empty($data['pris'])? $data['pris'] : '',
                'updated_at'    =>time(),
                'xu'            =>!empty($data['xu'])? $data['xu'] : '',
                'agency_id'=>isset($data['agency_id']) && !empty($data['agency_id'])?implode($data['agency_id'],','):'',
               
            
            ];
            $res=db('detailed')->insertGetId($use);
            if ($res) {
                return json_data([], 200, '添加成功');
                
            }
            return json_data([], 300, '添加失败');
        }
        
        $g=db('unit')->select();
        return $this->fetch('add', ['g'=>$g, 'data'=>$id]);
        
        
    }
    
    /**
     * @return mixed
     * 代购主材
     */
    public function dai()
    {
        
        if ($data=Request::instance()->post()) {
            $res=db('detailed')->where(['detaileds_id'=>$data['id']])->update(['agency'=>$data['agency']]);
           db('detailed')->where(['detailed_id'=>$data['id']])->update(['agency'=>$data['agency']]);
            if ($res) {
                return json_data([], 200, '成功');
                
            }
            return json_data([], 300, '失败');
        }
        
    }
    /**
     * @return mixed
     * 代购主材
     */
    public function da1()
    {
        
        if ($data=Request::instance()->post()) {
            $res=db('detailed')->where(['detailed_id'=>$data['id']])->update(['agency'=>$data['agency']]);
            if ($res) {
                return json_data([], 200, '成功');
                
            }
            return json_data([], 300, '失败');
        }
        
    }
    
    /**
     * @return mixed
     * 删除
     */
    public function de()
    {
        $store_id=Request::instance()->post();
        $re      =db('detailed')->where(['detaileds_id'=>$store_id['id']])->find();
        if ($re) {
            return json_data([], 300, '删除失败,请先删除子分类');
        }
        $res=false;
        if ($store_id) {
            $res=db('detailed')->where(['detailed_id'=>$store_id['id']])->update(['display'=>0]);
        }
        if ($res !== false) {
            return json_data([], 200, '删除成功');
        }
        return json_data([], 300, '删除失败');
    }
    
    /**
     * @return mixed
     * 查看
     */
    public function info($id=0)
    {
        
        
        if (!$id) {
            $this->error('请刷新后重试');
        }
        
        $res=db('detailed')
            ->where(['detaileds_id'=>$id])
            ->where('display',1)
            ->paginate(10)
            ->each(function ($item, $key) {
                $item['serial']        =!empty($item['serial'])? $item['serial'] : '暂无数据';
                $item['detailed_title']=!empty($item['detailed_title'])? $item['detailed_title'] : '暂无数据';
                $item['artificial']    =!empty($item['artificial'])? $item['artificial'] : '暂无数据';
                $item['rmakes']        =!empty($item['rmakes'])? $item['rmakes'] : '暂无数据';
                $item['pri']           =!empty($item['pri'])? $item['pri'] : '暂无数据';
                $item['pris']          =!empty($item['pris'])? $item['pris'] : '暂无数据';
                
                if (empty($item['un_id'])) {
                    $item['un_id']='暂无数据';
                } else{
                    $g            =db('unit')->where('id', $item['un_id'])->find();
                    $item['un_id']=$g['title'];
                }
                
                return $item;
                
            });
        return $this->fetch('info', ['data'=>$res]);
        
    }
}