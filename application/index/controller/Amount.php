<?php
/**
 * Created by muniao.
 * User: muniao
 * Date: 2018/7/30
 * Time: 17:51
 */

namespace app\index\controller;


use think\Request;

class Amount extends Backend
{
    /**
     * 店铺列表
     */
    public function index()
    {
        $data = Request::instance()->get();
        $BeginDate=date('Y-m-01', strtotime(date("Y-m-d",time())));
        $starttime = strtotime($BeginDate);
        $enttime = strtotime(date('Y-m-d', strtotime("$BeginDate +1 month -1 day")));
        $list = db('amount')
            ->field('amount.*,u.username,u.store_id,s.*,p.province,c.city,y.county,a.content,a.achievement_id')
            ->join('user u','amount.user_id=u.user_id','left')
            ->join('achievement a','a.user_id=u.user_id','left')
            ->join('store s','u.store_id=s.store_id','left')
            ->join('province p','s.province=p.province_id','left')
            ->join('city c','s.city=c.city_id','left')
            ->join('county y','s.area=y.county_id','left')
            ->where(['amount.status'=>0,'a.created_time' => ['between', [$starttime, $enttime]]])
            ->order('amount.created_time desc')
            ->paginate(20);
        
       return $this->fetch('index', ['list' => $list, 'data' => $data]);
    }

    /**
     * 删除
     */
    public function del()
    {
        $store_id = Request::instance()->post('amount_id');
        $res = false;
        if ($store_id) {
            $res = db('amount')->delete(['amount_id' => $store_id]);
        }
        if ($res !== false) {
            return json_data([], 200, '删除成功');
        }
        return json_data([], 300, '删除失败');
    }
    /**
     * 修改
     */
    public function edit($amount_id)
    {
        if ($data = Request::instance()->post()) {

            $user = [
                'user_id'=>$data['user_id'],
                   'income'=>$data['income'],//收入
                  'material' =>$data['material'],//材料成本
                  'labor' =>$data['labor'],//人工成本
                  'engineering' =>$data['engineering'],//工程利润,
                  'wages'=>$data['wages'],//师傅工资
                  'rents' =>$data['rents'],//房租
                  'office' =>$data['office'],//办公费
                  'freight' =>$data['freight'],//运费
                  'welfare' =>$data['welfare'],//福利费
                  'water' =>$data['water'],//水电物管费
                  'rent' =>$data['rent'],//租金
                  'slash'=>$data['slash'],//建渣清运费
                  'depreciation' =>$data['depreciation'],//折旧费
                  'traffic' =>$data['traffic'],//交通费
                  'marketing' =>$data['marketing'],//营销费
                  'created_time' =>time(),
                    'di' =>$data['di'],
                  'status' =>0,//状态
                  'yida' =>(int)$data['income']-(int)$data['material']-(int)$data['labor']-(int)$data['engineering']-((int)$data['wages']+(int)$data['rents']+(int)$data['office']+(int)$data['freight']+(int)$data['welfare']+(int)$data['water']+(int)$data['rent']+(int)$data['slash']+(int)$data['depreciation']+(int)$data['traffic']+(int)$data['marketing']),//已达成
            ];

            if((int)$user['yida']<=30000){
                $user['gongzi']=((int)$user['di']+(int)$user['yida'])*0.1;
            }elseif((int)$user['yida']>30000){
                $user['gongzi']=(int)$user['di']+3000+((int)$user['yida']-30000)*0.2;
            }

            $res = db('amount')->where(['amount_id' => $data['amount_id']])->update($user);
            if ($res) {
                return json_data([], 200, '修改成功');

            }
            return json_data([], 300, '修改失败');

        }else{
            if(!$amount_id){
                $this->error('请刷新后重试');
            }
            $data=db('amount')
                ->field('amount.*,u.username')
                ->join('user u','amount.user_id=u.user_id','left')
                ->where(['amount_id'=>$amount_id,])
                ->find();

            if(!$data){
                $this->error('不存在或已删除');
            }
            $da=db('user')->where(['status'=>0,'user_id'=>['neq',$data['user_id']]])->select();

            return $this->fetch('edit',['data'=>$data,'da'=>$da]);
        }
    }
    /**
     * 新增
     */
    public function add()
    {
        if ($data = Request::instance()->post()) {
            $list = db('amount')->where(['user_id'=>$data['user_id']])->order('created_time desc')->find();

            if(date('m',$list['created_time'])==date('m',time()) && date('y',$list['created_time'])==date('y',time())){
                return json_data([], 300, '该月份已存在');
            }
            $user = [
                'user_id'=>$data['user_id'],
                'income'=>$data['income'],//收入
                'material' =>$data['material'],//材料成本
                'labor' =>$data['labor'],//人工成本
                'engineering' =>$data['engineering'],//工程利润,
                'wages'=>$data['wages'],//师傅工资
                'rents' =>$data['rents'],//房租
                'office' =>$data['office'],//办公费
                'freight' =>$data['freight'],//运费
                'welfare' =>$data['welfare'],//福利费
                'water' =>$data['water'],//水电物管费
                'rent' =>$data['rent'],//租金
                'slash'=>$data['slash'],//建渣清运费
                'depreciation' =>$data['depreciation'],//折旧费
                'traffic' =>$data['traffic'],//交通费
                'marketing' =>$data['marketing'],//营销费
                'created_time' =>time(),
                'di' =>$data['di'],
                'status' =>0,//状态
                'yida' =>(int)$data['income']-(int)$data['material']-(int)$data['labor']-(int)$data['engineering']-((int)$data['wages']+(int)$data['rents']+(int)$data['office']+(int)$data['freight']+(int)$data['welfare']+(int)$data['water']+(int)$data['rent']+(int)$data['slash']+(int)$data['depreciation']+(int)$data['traffic']+(int)$data['marketing'])//已达成
            ];
            if((int)$user['yida']<=30000){
                $user['gongzi']=((int)$user['di']+(int)$user['yida'])*0.1;//工资
            }elseif((int)$user['yida']>30000){
                $user['gongzi']=(int)$user['di']+3000+((int)$user['yida']-30000)*0.3;//工资
            };


             $res = db('amount')->insertGetId($user);
            if ($res) {
                return json_data([], 200, '添加成功');

            }
            return json_data([], 300, '添加失败');


        }
        $data=db('user')->where(['status'=>0])->select();
        return $this->fetch('add',['data'=>$data]);

    }



}
