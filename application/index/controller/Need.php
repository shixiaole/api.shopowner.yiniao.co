<?php
/**
 * Created by muniao.
 * User: muniao
 * Date: 2018/7/30
 * Time: 17:51
 */

namespace app\index\controller;


use think\Request;

class Need extends Backend
{
    /**
     * 店铺列表
     */
    public function index()
    {
        $data = Request::instance()->get();
        $list = db('store')
            ->join('province p','store.province=p.province_id','left')
            ->join('city c','store.city=c.city_id','left')
            ->join('county y','store.area=y.county_id','left')
            ->field('store.*,p.province as province_id,c.city as city_id,y.county as county_id')
            ->order('store_id desc')
            ->paginate(10);
        return $this->fetch('index', ['list' => $list, 'data' => $data]);
    }

    /**
     * 删除
     */
    public function del()
    {
        $store_id = Request::instance()->post('store_id');
        $res = false;
        if ($store_id) {
            $res = db('store')->delete(['store_id' => $store_id]);
        }
        if ($res !== false) {
            return json_data([], 200, '删除成功');
        }
        return json_data([], 300, '删除失败');
    }
    /**
     * 修改
     */
    public function edit($store_id)
    {
        if ($data = Request::instance()->post()) {

            $user = [
                'store_name' => $data['store_name'],
                'province' =>$data['province'],
                'city' => $data['city'],
                'area' => $data['area'],
                'addres'=>$data['addres']

            ];

            $res = db('store')->where(['store_id' => $data['store_id']])->update($user);
            if ($res !== false) {
                $this->success('编辑成功','/index/need/index');
            }
            $this->error('编辑失败','index/need/index');

        }else{
            if(!$store_id){
                $this->error('请刷新后重试');
            }
            $data=db('store')
                ->join('province p','store.province=p.province_id','left')
                ->join('city c','store.city=c.city_id','left')
                ->join('county y','store.area=y.county_id','left')
                ->field('store.*,p.province as province_id,c.city as city_id,y.county as county_id')
                ->where(['store_id'=>$store_id,])
                ->find();

            if(!$data){
                $this->error('店铺不存在或已删除');
            }
            return $this->fetch('edit',['data'=>$data]);
        }
    }
    /**
     * 新增
     */
    public function add()
    {
        if ($data = Request::instance()->post()) {
              if(empty($data['store_name'])){
                  $this->error('请输入店铺名','/index/need/index',0);
              }
            $re = db('store')->where(['store_name'=>$data['store_name']])->find();
            if($re){
                $this->error('该店铺已存在','/index/need/index');
            }
            $user = [
                'store_name' => $data['store_name'],
                'province' =>$data['province'],
                'city' => $data['city'],
                'area' => $data['area'],
                'addres'=>$data['addres']

            ];

             $res = db('store')->insertGetId($user);
            if ($res) {
               $this->success('添加成功','/index/need/index');
            }

        }
        return $this->fetch('add');

    }
    public function privance()
    {

        $list = cache('province');//去缓存
        if(!$list){
            $list = db('province')->cache('province')->select();
        }
        return $list;
    }

//读取市
    public function city($province_id)
    {

        if ($province_id) {
            $where = "province_id=$province_id";
        } else {
            $where = "province_id=110000";
        }
        $list =db('city')->where($where)->select();
        return $list;
    }

//读取区
    public function area($city_id)
    {
        if ($city_id) {
            $where = "city_id=$city_id";
        } else {
            $where = "city_id=110100";
        }
        $list = db('county')->where($where)->select();
        return $list;
    }


}