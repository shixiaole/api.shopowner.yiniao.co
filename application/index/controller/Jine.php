<?php
/**
 * Created by muniao.
 * User: muniao
 * Date: 2018/7/31
 * Time: 9:39
 */

namespace app\index\controller;


use think\Request;

class Jine extends Backend
{
    /**
     * 充值
     */
    public function index(){


        $data=db('jine')->paginate(10)
            ->each(function($item, $key){

            return $item;

        });

        return $this->fetch('index',['data'=>$data]);
    }

    /**
     * @return array|mixed|string
     * 添加
     */
    public function add(){
        if($data =Request::instance()->post()){

            $user = [
                'money' => $data['money'],
            ];

            $res = db('jine')->insertGetId($user);
            if($res===false){
                return json_data([], 300, '添加失败');
            }
            return json_data([], 200, '添加成功');
        }

        $chanel=db('planting')->where('type',2)->select();

        return $this->fetch('add',['data'=>$data,'oo'=>$chanel,]);
    }
    /**
     * @return array|mixed|string
     * 编辑
     */
    public function edit($jine_id = 0)
    {
        if ($data = Request::instance()->post()) {

            $user = [
                'money' => $data['money'],

            ];

            $res = db('jine')->where(['jine_id' => $data['jine_id']])->update($user);
            if ($res === false) {
                return json_data([], 300, '编辑失败');
            }
            return json_data([], 200, '编辑成功');
        }

        if (!$jine_id) {
            $this->error('请刷新后重试');
        }
        $res = db('jine')
            ->where(['jine_id' => $jine_id])
            ->find();

        return $this->fetch('edit', ['data' => $res]);
    }
    /**
     * 取消
     */
    public function del()
    {
        $store_id = Request::instance()->post();

        $res = false;
        if ($store_id) {
            $res = db('jine')->where(['jine_id' => $store_id['jine_id']])->delete();
        }
        if ($res !== false) {
            return json_data([], 200, '删除成功');
        }
        return json_data([], 300, '删除失败');
    }


}