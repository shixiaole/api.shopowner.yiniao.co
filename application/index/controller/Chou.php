<?php
/**
 * Created by muniao.
 * User: muniao
 * Date: 2018/7/31
 * Time: 9:39
 */

namespace app\index\controller;


use think\Request;

class Chou extends Backend
{
    /**
     * 轮播首页
     */
    public function index(){
        $list=db('chou')->paginate(30)->each(function($item, $key){
                if( $item['state']==1){
                    $item['states']='启用';
                }else{
                    $item['states']='关闭';
                }
                return $item;
            });

        $l=db('gai')->find();
        return $this->fetch('index',['data'=>$list,'l'=>$l]);
    }

    /**
     * @return array|mixed|string
     * 添加
     */
    public function add(){
        if($data =Request::instance()->post()){
            $user = [
                'title' => $data['title'],
                'logo' =>!empty($data['logo'])?$data['logo']:'',//联系电话,
                'colole' =>$data['colole'],
            ];

            $res = db('chou')->insertGetId($user);
            if($res===false){
                return json_data([], 300, '添加失败');
            }
            return json_data([], 200, '添加成功');
        }
        return $this->fetch('add');
    }
    /**
     * @return array|mixed|string
     * 编辑
     */
    public function edit($chou_id = 0)
    {
        if ($data = Request::instance()->post()) {

        $user = [
                'title' => $data['title'],
                'logo' =>!empty($data['logo'])?$data['logo']:'',//联系图片,
                'colole' =>$data['colole'],
       ];

            $res = db('chou')->where(['chou_id' => $data['chou_id']])->update($user);
            if ($res === false) {
                return json_data([], 300, '编辑失败');
            }
            return json_data([], 200, '编辑成功');
        }

        if (!$chou_id) {
            $this->error('请刷新后重试');
        }
        $res = db('chou')
            ->where(['chou_id' => $chou_id])
            ->find();

        return $this->fetch('edit', ['data' => $res]);
    }
    /**
     * 取消
     */
    public function del()
    {
        $store_id = Request::instance()->post();

        $res = false;
        if ($store_id) {
            $res = db('chou')->where(['chou_id' => $store_id['chou_id']])->delete();
        }
        if ($res !== false) {
            return json_data([], 200, '删除成功');
        }
        return json_data([], 300, '删除失败');
    }
    /**
     * 取消
     */
    public function gai()
    {
        $store_id = Request::instance()->post();
        $user = [
            'gai' => $store_id['reason'],
        ];
        if($store_id['id']){
            $res = db('gai')->where(['id' => $store_id['id']])->update($user);
        }else{
            $res = db('gai')->insertGetId($user);
        }

        if ($res !== false) {
            return json_data([], 200, '成功');
        }
        return json_data([], 300, '失败');

    }
    /**
     * 启用 禁用
     */
    public function disable(){
        $chou_id=Request::instance()->post('chou_id');
        $status=Request::instance()->post('status');
        if($chou_id && ($status==0 || $status==1)){

            $res=db('chou')->where(['chou_id'=>$chou_id])->update(['state'=>$status]);
            if($res!==false){
                $this->result([],200,'操作成功');
            }
        }
        $this->result([],300,'请刷新后重试');
    }

}