<?php
/**
 * Created by muniao.
 * User: muniao
 * Date: 2018/7/26
 * Time: 13:56
 */

namespace app\index\controller;


use think\Controller;
use think\Request;
use app\api\model\Aliyunoss;

class Upload extends Controller
{
    /**
     * webupload单张图片上传
     */
    public function upload_file()
    {
        $file = Request::instance()->file('file');
        
        if ($file == null) {
            res_date([], 300, '未获取到文件');
        }
        $info = $file->validate(['ext' => 'gif,jpg,jpeg,bmp,png'])->move(ROOT_PATH . 'public' . DS . 'uploads/img');
        if ($info) {
            $path = ROOT_PATH . 'public' . '/uploads/img/' . date("Ymd") . '/' . $info->getFilename();
            $ali =new Aliyunoss();
            
            $oss_result  =$ali->upload(config('city').'/' .$info->getFilename(), $path);
            if ($oss_result['info']['http_code'] == 200) {
                $paths=parse_url($oss_result['info']['url'])['path'];
                if (file_exists($path)) {
                    unlink($path);
                }
                
            }
            
            res_date('https://images.yiniao.co'.$paths);
        }
        res_date([], 300, $file->getError());
    }
    
}