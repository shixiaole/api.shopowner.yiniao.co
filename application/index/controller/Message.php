<?php
/**
 * Created by muniao.
 * User: muniao
 * Date: 2018/7/30
 * Time: 17:51
 */

namespace app\index\controller;


use think\Model;
use think\Request;
use app\index\model\Jpush;

class Message extends Backend
{
    /**
     * 消息列表
     * 1系统消息2订单提醒3订单跟进提醒4上门提醒5公司通知
     */
    public function index()
    {
        $data = Request::instance()->get();
        $model = db('message')
            ->field('message.*,us.username');
             if (isset($data['already']) && $data['already'] != '') {
                 $model->where(['already' => $data['already']]);
             }
           $list =$model->join('user us','us.user_id=message.user_id','left')
               ->paginate(10)
        ->each(function ($item,$key){
            if($item['type']==1){
               $type="系统消息";
            }elseif($item['type']==2){
                $type="订单提醒";
            }elseif($item['type']==3){
                $type="订单跟进提醒";
            }elseif($item['type']==4){
                $type="上门提醒";
            }elseif($item['type']==5){
                $type="公司通知";
            }elseif ($item['type']==6){
                $type="新订单";
            }
            if($item['already']==1) {
                $item['already'] = "未读";
            }else{
                $item['already'] = "已读";
            }
            $item['types']=$type;

            return $item;
        });
        return $this->fetch('index', ['list' => $list, 'data' => $data]);
    }

    /**
     * 删除
     */
    public function del()
    {
        $store_id = Request::instance()->post('id');
        $res      = false;
        if ($store_id) {
            $res = db('message')->delete(['id' => $store_id]);
        }
        if ($res !== false) {
            return json_data([], 200, '删除成功');
        }
        return json_data([], 300, '删除失败');
    }

    /**
     * 修改
     */
    public function edit()
    {
        if ($data = Request::instance()->post()) {

            $res = db('message')->where(['id' => $data['id']])->update([
                'content'=> $data['content']
            ]);
            if ($res !== false) {
                return json_data([], 200, '编辑成功');
            }
            return json_data([], 200, '编辑失败');
        }
        $store_id=$data = Request::instance()->get('store_id');
            $data = db('message')
                ->where(['id' => $store_id,])
                ->find();

            if (!$data) {
                $this->error('店铺不存在或已删除');
            }
            return $this->fetch('edit', ['data' => $data]);

    }

    /**
     * 新增
     */
    public function add()
    {
        //  1系统消息2订单提醒3订单跟进提醒4上门提醒5公司通知6新订单
        if ($data = Request::instance()->post()) {
            $user=db('user')->where('status',0)->field('user_id,registrationId')->select();
            foreach ($user as $item){
                $user = [
                    'content' => $data['content'],
                    'type' => $data['type'],
                    'time' => time(),
                    'order_id' =>0,
                    'user_id' => $item['user_id'],
                ];
                $res = db('message')->insertGetId($user);
                if(!empty($item['registrationId'])){
                    $new=new Jpush();
                    $data = array('order_id' =>0, 'content' =>$data['content'],'type'=>$data['type'],'user_id'=>0);
                    $new->whole('',$item['registrationId'],$data);
                }

            }
            if ($res) {
                return json_data([], 200, '添加成功');
            }
            return json_data([], 300, '添加失败');
        }
        return $this->fetch('add');

    }

    public function tui()
    {
        if ($data = Request::instance()->post()) {
            $res            = db('message')->where('id', $data['id'])->find();
             $user            = db('user')->where(['status'=> 0,'user_id'=>$res['user_id']])->field('registrationId')->find();

            $new=new Jpush();

            if(!empty($user["registrationId"])){
                $data = array('order_id' =>0, 'content' =>$res['content'],'type'=>$res['type'],'user_id'=>0);
               
                $re= $new->whole('',$user["registrationId"],$data);
                if($re) {
                    return json_data([], 200, '推送成功');
                }
                return json_data([], 300, '推送失败');
            }else{
                return json_data([], 300, '推送失败');
            }

        }
    }


}
