<?php
/**
 * Created by muniao.
 * User: muniao
 * Date: 2018/7/31
 * Time: 13:43
 */

namespace app\index\controller;


use think\Request;

class Question extends Backend
{
    /**
     * 问题反馈列表
     */
    public function index(){
        $data=Request::instance()->get();
        $model=db('question')
            ->join('user','user.user_id=question.user_id','left')
            ->join('vender','vender.vender_id=question.vender_id','left')
            ->field('question.*,user.username,user.mobile,vender.name as vender_name,vender.mobile as vender_mobile')
            ->where(['question.status'=>0]);
        if(isset($data['start_time']) && $data['start_time']!=''){
            $model->where(['question.created_time'=>['>=',strtotime($data['start_time'])]]);
        }
        if(isset($data['end_time']) && $data['end_time']!=''){
            $model->where(['question.created_time'=>['<=',strtotime($data['end_time'])+24*3600]]);
        }
        if(isset($data['type']) && $data['type']!=''){
            $model->where(['question.type'=>$data['type']]);
            if($data['type']==1){
                if(isset($data['username']) && $data['username']!=''){
                    $model->where(['user.username'=>['like',"%{$data['username']}%"]]);
                }
                if(isset($data['mobile']) && $data['mobile']!=''){
                    $model->where(['user.mobile'=>['like',"%{$data['mobile']}%"]]);
                }
            }elseif ($data['type']==2){
                if(isset($data['username']) && $data['username']!=''){
                    $model->where(['vender.name'=>['like',"%{$data['username']}%"]]);
                }
                if(isset($data['mobile']) && $data['mobile']!=''){
                    $model->where(['vender.mobile'=>['like',"%{$data['mobile']}%"]]);
                }
            }
        }
        $list=$model
            ->order('question.created_time desc')
            ->paginate(10)
            ->each(function ($val,$key){
                $val['img']=explode(',',$val['img']);
                return $val;
            });
        return $this->fetch('index',['list'=>$list,'data'=>$data]);
    }
    /**
     * 删除
     */
    public function del(){
        $question_id=Request::instance()->post('question_id');
        $res=false;
        if($question_id){
            $res=db('question')->where(['question_id'=>$question_id])->update(['status'=>1]);
        }
        if($res){
            return json_data([],200,'删除成功');
        }
        return json_data([],300,'删除失败');
    }

}