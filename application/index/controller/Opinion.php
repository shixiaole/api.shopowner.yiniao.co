<?php
/**
 * Created by muniao.
 * User: muniao
 * Date: 2018/7/31
 * Time: 9:39
 */

namespace app\index\controller;


use think\Request;

class Opinion extends Backend
{
    /**
     * 轮播首页
     */
    public function index(){
        Request::instance()->get();
        $data=db('opinion')->paginate(20)
            ->each(function($item, $key){
                $item['customer']=db('customers')->where('customers_id',$item['customers_id'])->value('username');
                $item['admin']=db('admin')->where('admin_id',$item['admin_id'])->value('username');
                if($item['state']==0){
                    $item['states']='未处理';
                }else{
                    $item['states']='已处理';
                }
                return $item;
            });

        return $this->fetch('index',['data'=>$data]);
    }


    /**
     * @return array|mixed|string
     * 编辑
     */
    public function edit($opinion_id = 0)
    {
        if ($data = Request::instance()->post()) {
            $user=$this->check_authority();
            $user = [
                'huifu' => $data['huifu'],
                'last_time' => time(),
                'admin_id' => $user['admin_id'],
                'state' => 1,


            ];

            $res = db('opinion')->where(['opinion_id' => $data['opinion_id']])->update($user);
            if ($res === false) {
                return json_data([], 300, '编辑失败');
            }
            return json_data([], 200, '编辑成功');
        }

        if (!$opinion_id) {
            $this->error('请刷新后重试');
        }
        $res = db('opinion')
            ->where(['opinion_id' => $opinion_id])
            ->find();

        return $this->fetch('edit', ['data' => $res]);
    }


}