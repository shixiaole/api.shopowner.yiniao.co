<?php
/**
 * Created by muniao.
 * User: muniao
 * Date: 2018/7/31
 * Time: 9:39
 */

namespace app\index\controller;


use think\Request;

class Config extends Backend
{
    /**
     * 短信模板
     */
    public function tel(){
        $data=db('label')->paginate(10);
        return $this->fetch('tel',['data'=>$data]);
    }

    /**
     * @return array|mixed|string
     * 添加短信
     */
    public function add(){
        if($data =Request::instance()->post()){

            $user = [
                'content' => $data['content'],
            ];

            $res = db('label')->insertGetId($user);
            if($res===false){
                return json_data([], 300, '添加失败');
            }
            return json_data([], 200, '添加成功');
        }

        return $this->fetch('add',['data'=>$data]);
    }
    /**
     * @return array|mixed|string
     * 编辑短信
     */
    public function edit($label_id = 0)
    {
        if ($data = Request::instance()->post()) {

            $user = [
                'content' => $data['content'],
            ];

            $res = db('label')->where(['label_id' => $data['label_id']])->update($user);
            if ($res === false) {
                return json_data([], 300, '编辑失败');
            }
            return json_data([], 200, '编辑成功');
        }

        if (!$label_id) {
            $this->error('请刷新后重试');
        }
        $res = db('label')
            ->where(['label_id' => $label_id])
            ->find();


        return $this->fetch('edit', ['data' => $res]);
    }
    /**
     * 取消
     */
    public function del()
    {
        $store_id = Request::instance()->post();

        $res = false;
        if ($store_id) {
            $res = db('label')->where(['label_id' => $store_id['label_id']])->delete();
        }
        if ($res !== false) {
            return json_data([], 200, '删除成功');
        }
        return json_data([], 300, '删除失败');
    }

    /**
     * @return mixed
     * 问题
     */
    public function category(){
        $data=db('goods_category')->where(['pro_type'=>1,'parent_id'=>0])->paginate(10);
        return $this->fetch('category',['data'=>$data]);
    }
    /**
     * @return mixed
     * 修改
     */
    public function edi($id=0)
    {

        if ($data = Request::instance()->post()) {
            $use = [

                'title' => $data['title'],
                'material' => !empty($data['material']) ? $data['material'] : '',
                'scen' => !empty($data['scen']) ? $data['scen'] : '',
                'prompt' => !empty($data['prompt']) ? $data['prompt'] : '',
                'price' => !empty($data['price']) ? $data['price'] : '',
                'price_rule'=>!empty($data['price_rule'])?$data['price_rule']:'',
                'unit_id'=>!empty($data['unit_id'])?$data['unit_id']:0,
                'service_content' => !empty($data['service_content']) ? $data['service_content'] : '',
                'service_times' => !empty($data['service_times']) ? $data['service_times'] : '',

            ];

            $res = db('goods_category')->where(['id' => $data['id']])->update($use);
            if ($res === false) {
                return json_data([], 300, '编辑失败');
            }
            return json_data([], 200, '编辑成功');
        } else {
            if (!$id) {
                $this->error('请刷新后重试');
            }
            $res = db('goods_category')
                ->field('goods_category.*,u.title as titles')
                ->join('unit u','goods_category.unit_id=u.id','left')
                ->where(['goods_category.id' => $id])
                ->find();
//            $res['title']=!empty($res['title'])?$res['title']:"暂无数据";
//            $res['material']=!empty($res['material'])?$res['material']:"暂无数据";
//            $res['scen']=!empty($res['scen'])?$res['scen']:"暂无数据";
//            $res['prompt']=!empty($res['prompt'])?$res['prompt']:"暂无数据";
//            $res['price']=!empty($res['price'])?$res['price']:"暂无数据";
//            $res['price_rule']=!empty($res['price_rule'])?$res['price_rule']:"暂无数据";
//            $res['unit_id']=!empty($res['unit_id'])?$res['unit_id']:"0";
//            $res['titles']=!empty($res['titles'])?$res['titles']:"暂无数据";
//            $res['service_content']=!empty($res['service_content'])?$res['service_content']:"暂无数据";
//            $res['service_times']=!empty($res['service_times'])?$res['service_times']:"暂无数据";
            $g=db('unit')->select();
            return $this->fetch('edi', ['data' => $res,'g'=>$g]);
        }
    }
        /**
     * @return mixed
     * 添加
     */
    public function ad($id){

        if ($data = Request::instance()->post()) {

              $use=[
                  'title'=>$data['title'],
                  'material'=>!empty($data['material'])?$data['material']:'',
                  'scen'=>!empty($data['scen'])?$data['scen']:'',
                  'prompt'=>!empty($data['prompt'])?$data['prompt']:'',
                  'price'=>!empty($data['price'])?$data['price']:'',
                  'sort'=>!empty($data['sort'])?$data['sort']:20,
                  'parent_id'=>!empty($data['parent_id'])?$data['parent_id']:0,
                  'price_rule'=>!empty($data['price_rule'])?$data['price_rule']:'',
                  'unit_id'=>!empty($data['unit_id'])?$data['unit_id']:0,
                  'pro_type'=>1,
                  'service_content'=>!empty($data['service_content'])?$data['service_content']:'',
                  'service_times'=>!empty($data['service_times'])?$data['service_times']:'',

              ];

            $res = db('goods_category')->insertGetId($use);
            if ($res) {
                return json_data([], 200, '添加成功');

            }
            return json_data([], 300, '添加失败');
        }
        $g=db('unit')->select();
        return $this->fetch('ad', ['data' => $id,'g'=>$g]);



    }
    /**
     * @return mixed
     * 删除
     */
    public function de(){
        $store_id = Request::instance()->post();
        $re = db('goods_category')->where(['parent_id' => $store_id['id'],'pro_type'=>1])->find();
        if($re) {
            return json_data([], 300, '删除失败,请先删除子分类');
        }
        $res = false;
        if ($store_id) {
            $res = db('goods_category')->where(['id' => $store_id['id']])->update(['pro_type'=>2]);
        }
        if ($res !== false) {
            return json_data([], 200, '删除成功');
        }
        return json_data([], 300, '删除失败');
    }
    /**
     * @return mixed
     * 查看
     */
    public function info($id=0){


        if (!$id) {
            $this->error('请刷新后重试');
        }

        $res = db('goods_category')
            ->where(['parent_id' => $id,'pro_type'=>1])
            ->paginate(10)
            ->each(function($item, $key){
                $item['material']=!empty($item['material'])?$item['material']:'暂无数据';
                $item['scen']=!empty($item['scen'])?$item['scen']:'暂无数据';
                $item['prompt']=!empty($item['prompt'])?$item['prompt']:'暂无数据';
                $item['price']=!empty($item['price'])?$item['price']:'暂无数据';
                $item['service_content']=!empty($item['service_content'])?$item['service_content']:'暂无数据';
                $item['service_times']=!empty($item['service_times'])?$item['service_times']:'暂无数据';
                $item['price_rule']=!empty($item['price_rule'])?$item['price_rule']:"暂无数据";
                if( empty($item['unit_id'])){
                    $item['unit_id']='暂无数据';
                }else{
                    $g=db('unit')->where('id',$item['unit_id'])->find();
                    $item['unit_id']=$g['title'];
                }

                return $item;

            });
        return $this->fetch('info',['data'=>$res]);

    }
   /***
    *
    * 单位修改
    */

    public function index(){
        $data=db('unit')->paginate(10);
        return $this->fetch('index',['data'=>$data]);
    }

    /**
     * @return array|mixed|string
     * 添加短信
     */
    public function dan_add(){
        if($data =Request::instance()->post()){

            $user = [
                'title' => $data['title'],
                'created_at'=>date('Y-m-d H:i:s',time())
            ];

            $res = db('unit')->insertGetId($user);
            if($res===false){
                return json_data([], 300, '添加失败');
            }
            return json_data([], 200, '添加成功');
        }

        return $this->fetch('dan_add',['data'=>$data]);
    }
    /**
     * @return array|mixed|string
     * 编辑短信
     */
    public function dan_edit($id = 0)
    {
        if ($data = Request::instance()->post()) {

            $user = [
                'title' => $data['title'],
                'created_at'=>date('Y-m-d H:i:s',time())
            ];

            $res = db('unit')->where(['id' => $data['id']])->update($user);
            if ($res === false) {
                return json_data([], 300, '编辑失败');
            }
            return json_data([], 200, '编辑成功');
        }

        if (!$id) {
            $this->error('请刷新后重试');
        }
        $res = db('unit')
            ->where(['id' => $id])
            ->find();


        return $this->fetch('dan_edit', ['data' => $res]);
    }
    /**
     * 取消
     */
    public function dan_del()
    {
        $id = Request::instance()->post();

        $res = false;
        if ($id) {
            $res = db('unit')->where(['id' => $id['id']])->delete();
        }
        if ($res !== false) {
            return json_data([], 200, '删除成功');
        }
        return json_data([], 300, '删除失败');
    }
    /**
     * 取消
     */
    public function log()
    {
        $data=db('log')->order('created_at desc')->paginate(50)->each(function($item, $key){
            if(substr($item['content'],0,2)=="客服" || substr($item['content'],0,2)=="取消" || substr($item['content'],0,2)=="退单"){
                $item['admin_id']=db('admin',config('database.zong'))->where('admin_id',$item['admin_id'])->value('username');
            }else{
                $item['admin_id']=db('admin')->where('admin_id',$item['admin_id'])->value('username');
            }

            $item['ord_id']=db('order')->where('order_id',$item['ord_id'])->value('order_no');

            return $item;

        });;
        return $this->fetch('log',['data'=>$data]);
    }
    /**
     * @return mixed
     * 沟通记录
     */
    public function remote(){
        $data=db('remote')->where(['is_enable'=>1,'parent_id'=>0])->paginate(10);
        return $this->fetch('remote',['data'=>$data]);
    }
    /**
     * @return mixed
     * 修改
     */
    public function remote_edi($id=0)
    {

        if ($data = Request::instance()->post()) {
            $use = [
                'title' => $data['title'],
            ];

            $res = db('remote')->where(['id' => $data['id']])->update($use);
            if ($res === false) {
                return json_data([], 300, '编辑失败');
            }
            return json_data([], 200, '编辑成功');
        } else {
            if (!$id) {
                $this->error('请刷新后重试');
            }
            $res = db('remote')->where(['id' => $id])
                ->find();

            $g=db('unit')->select();
            return $this->fetch('remote_edi', ['data' => $res,'g'=>$g]);
        }
    }
    /**
     * @return mixed
     * 添加
     */
    public function remote_ad($id){

        if ($data = Request::instance()->post()) {

            $use=[
                'title' => $data['title'],
                'parent_id'=>!empty($data['parent_id'])?$data['parent_id']:0,

            ];

            $res = db('remote')->insertGetId($use);
            if ($res) {
                return json_data([], 200, '添加成功');

            }
            return json_data([], 300, '添加失败');
        }
        $g=db('unit')->select();
        return $this->fetch('remote_ad', ['data' => $id,'g'=>$g]);



    }
    /**
     * @return mixed
     * 删除
     */
    public function remote_de(){
        $store_id = Request::instance()->post();
        $re = db('remote')->where(['parent_id' => $store_id['id'],'is_enable'=>1])->find();
        if($re) {
            return json_data([], 300, '删除失败,请先删除子分类');
        }
        $res = false;
        if ($store_id) {
            $res = db('remote')->where(['id' => $store_id['id']])->update(['is_enable'=>2]);
        }
        if ($res !== false) {
            return json_data([], 200, '删除成功');
        }
        return json_data([], 300, '删除失败');
    }
    /**
     * @return mixed
     * 查看
     */
    public function remote_info($id=0){


        if (!$id) {
            $this->error('请刷新后重试');
        }

        $res = db('remote')
            ->where(['parent_id' => $id,'is_enable'=>1])
            ->paginate(10);
        return $this->fetch('remote_info',['data'=>$res]);

    }

}