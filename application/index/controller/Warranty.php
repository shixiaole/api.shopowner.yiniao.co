<?php
/**
 * Created by muniao.
 * User: muniao
 * Date: 2018/8/9
 * Time: 11:22
 */

namespace app\index\controller;


use think\Request;
use think\Cache;
use think\Exception;

class Warranty extends Backend
{
    /**
     * @return mixed
     * 问题
     */
    public function index()
    {
        $data=db('warranty')->where(['pro_types'=>0, 'parents_id'=>0])->paginate(30);
        return $this->fetch('index', ['data'=>$data]);
    }
    
    /**
     * @return mixed
     * 修改
     */
    public function edi($id=0)
    {
        
        if ($data=Request::instance()->post()) {
            $use=[
                'title'           =>$data['title'],
                'scesn'           =>$data['scesn'],
                'service_contents'=>$data['service_contents'],
            
            ];
            $res=db('Warranty')->where(['id'=>$data['id']])->update($use);
            if ($res === false) {
                return json_data([], 300, '编辑失败');
            }
            return json_data([], 200, '编辑成功');
        }
        if (!$id) {
            $this->error('请刷新后重试');
        }
        $res=db('Warranty')->where(['id'=>$id])
            ->find();
        
        return $this->fetch('edi', ['data'=>$res,]);
        
    }
    
    /**
     * @return mixed
     * 添加
     */
    public function ad($id)
    {
        
        if ($data=Request::instance()->post()) {
            db()->startTrans();
            try {
            $use=[
                'title'           =>$data['title'],
                'parents_id'      =>!empty($data['parents_id'])? $data['parents_id'] : 0,
                'term'            =>$data['term'],
                'scesn'           =>'',
                'service_contents'=>'',
            
            ];
            if (empty($data['id'])) {
                db('Warranty')->insertGetId($use);
            } else{
                db('Warranty')->where('parents_id', $data['id'])->update(['term'=>$data['term']]);
                db('Warranty')->where('id', $data['id'])->update(['term'=>$data['term'], 'title'=>$data['title']]);
                
            }
                db()->commit();
                return json_data([], 200, '添加成功');
            } catch (Exception $e) {
                db()->rollback();
                return json_data([], 200, '添加失败');
            }
           
            
        }
        
        $res=db('Warranty')->where(['id'=>$id])
            ->find();
        
        return $this->fetch('ad', ['data'=>$res]);
        
    }
    
    /**
     * @return mixed
     * 添加
     */
    public function add($id)
    {
        
        if ($data=Request::instance()->post()) {
            
            $use=[
                'title'           =>$data['title'],
                'parents_id'      =>!empty($data['parents_id'])? $data['parents_id'] : 0,
                'scesn'           =>$data['scesn'],
                'service_contents'=>$data['service_contents'],
            ];
            $res=db('Warranty')->insertGetId($use);
            if ($res) {
                return json_data([], 200, '添加成功');
                
            }
            return json_data([], 300, '添加失败');
        }
        
        
        return $this->fetch('add', ['data'=>$id]);
        
        
    }
    
    /**
     * @return mixed
     * 删除
     */
    public function de()
    {
        $store_id=Request::instance()->post();
        $re      =db('Warranty')->where(['parents_id'=>$store_id['id'], 'pro_types'=>0])->find();
        if ($re) {
            return json_data([], 300, '删除失败,请先删除子分类');
        }
        $res=false;
        if ($store_id) {
            $res=db('Warranty')->where(['id'=>$store_id['id']])->update(['pro_types'=>1]);
        }
        if ($res !== false) {
            return json_data([], 200, '删除成功');
        }
        return json_data([], 300, '删除失败');
    }
    
    /**
     * @return mixed
     * 查看
     */
    public function info($id=0)
    {
        
        
        if (!$id) {
            $this->error('请刷新后重试');
        }
        
        $res=db('Warranty')
            ->where(['parents_id'=>$id, 'pro_types'=>0])
            ->paginate(10)
            ->each(function ($item, $key) {
                $item['scesn']           =!empty($item['scesn'])? $item['scesn'] : '暂无数据';
                $item['term']            =!empty($item['term'])? $item['term'] : '暂无数据';
                $item['service_contents']=!empty($item['service_contents'])? $item['service_contents'] : '暂无数据';
                return $item;
                
            });
        return $this->fetch('info', ['data'=>$res]);
        
    }
}