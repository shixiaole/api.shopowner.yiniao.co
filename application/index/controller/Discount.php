<?php
/**
 * Created by muniao.
 * User: muniao
 * Date: 2018/7/30
 * Time: 17:51
 */

namespace app\index\controller;


use think\Request;

class Discount extends Backend
{
    /**
     * 店铺列表
     */
    public function index()
    {
        $data = Request::instance()->get();
        $list = db('discount')->paginate(10)
            ->each(function($item, $key){
                $item['created_time']=db('youhui')->where('youhui_id',$item['youhui_id'])->value('lalst_time');
                $item['youhui_id']=db('youhui')->where('youhui_id',$item['youhui_id'])->value('jine');
                $item['customers_id']=db('customers')->where('customers_id',$item['customers_id'])->value('username');
                if($item['status']==0){
                    $item['status']='正常';
                }elseif($item['status']==1){
                    $item['status']='已使用';
                }elseif ($item['status']==2){
                    $item['status']='已过期';
                }
                return $item;

            });

        return $this->fetch('index', ['list' => $list, 'data' => $data]);
    }

}