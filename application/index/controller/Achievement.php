<?php
/**
 * Created by muniao.
 * User: muniao
 * Date: 2018/7/30
 * Time: 17:51
 */

namespace app\index\controller;


use think\Request;

class Achievement extends Backend
{
    /**
     * 店铺列表
     */
    public function index()
    {
        $data = Request::instance()->get();
        $list = db('achievement')
            ->field('achievement.*,u.username,u.store_id,s.*,p.province,c.city,y.county')
            ->join('user u','achievement.user_id=u.user_id','left')
            ->join('store s','u.store_id=s.store_id','left')
            ->join('province p','s.province=p.province_id','left')
            ->join('city c','s.city=c.city_id','left')
            ->join('county y','s.area=y.county_id','left')
            ->order('achievement.created_time desc')
            ->paginate(10);

        return $this->fetch('index', ['list' => $list, 'data' => $data]);
    }

    /**
     * 删除
     */
    public function del()
    {
        $store_id = Request::instance()->post('store_id');
        $res = false;
        if ($store_id) {
            $res = db('achievement')->delete(['store_id' => $store_id]);
        }
        if ($res !== false) {
            return json_data([], 200, '删除成功');
        }
        return json_data([], 300, '删除失败');
    }
    /**
     * 修改
     */
    public function edit($achievement_id)
    {
        if ($data = Request::instance()->post()) {

            $user = [
                'user_id' => $data['user_id'],
                'content' =>$data['content'],
                'created_time' =>time(),
            ];

            $res = db('achievement')->where(['achievement_id' => $data['achievement_id']])->update($user);
            if ($res) {
                return json_data([], 200, '修改成功');

            }
            return json_data([], 300, '修改失败');

        }else{
            if(!$achievement_id){
                $this->error('该业绩不存在无法编辑');
            }
            $data=db('achievement')
                ->field('achievement.*,u.username')
                ->join('user u','achievement.user_id=u.user_id','left')
                ->where(['achievement_id'=>$achievement_id,])
                ->find();

            if(!$data){
                $this->error('不存在或已删除');
            }
            $da=db('user')->where(['status'=>0,'user_id'=>['neq',$data['user_id']]])->select();

            return $this->fetch('edit',['data'=>$data,'da'=>$da]);
        }
    }
    /**
     * 新增
     */
    public function add()
    {
        if ($data = Request::instance()->post()) {
            $list = db('achievement')->where(['user_id'=>$data['user_id']])->order('created_time desc')->find();

            if(date('m',$list['created_time'])==date('m',time()) && date('y',$list['created_time'])==date('y',time())){
                return json_data([], 300, '该月份已存在');
            }
            $user = [
                'user_id' => $data['user_id'],
                'content' =>$data['content'],
                'created_time' =>time(),
            ];

             $res = db('achievement')->insertGetId($user);
            if ($res) {
                return json_data([], 200, '添加成功');

            }
            return json_data([], 300, '添加失败');


        }
        $data=db('user')->where(['status'=>0])->select();
        return $this->fetch('add',['data'=>$data]);

    }



}