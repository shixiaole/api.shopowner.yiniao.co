<?php
/**
 * Created by muniao.
 * User: muniao
 * Date: 2018/7/31
 * Time: 9:39
 */

namespace app\index\controller;


use app\api\model\Approval;
use app\api\model\Common;
use think\Request;

class Agency extends Backend
{
    /**
     * 供应商
     */
    public function index(Common $common,Approval $approval)
    {
        $data=db('agency')->join('province p','agency.province=p.province_id','left')
            ->join('city c','agency.city=c.city_id','left')
            ->join('county y','agency.area=y.county_id','left')
            ->join('city yo','agency.belonging_to=yo.city_id','left')
            ->join('city yi','agency.scope_business=yi.city_id','left')
            ->field('agency.*,p.province as province_id,c.city as city_id,y.county as county_id,yo.city as belonging_to,yi.city as scope_business')
            ->order('agency_id desc')
            ->paginate(10)->each(function ($item, $key)use ($common, $approval) {
                $item['rangess']=$item['rangess']==1?"主材供应商":'';
                $item['account_type']=$item['account_type']==1?"对公账户":'个人账户';
                $item['state']=$item['state']==1?"正常":'冻结';
                $item['business']=is_numeric($item['business'])?array_values($approval->filter_by_value($common->supply(), 'id', $item['business']))[0]['title']:$item['business'];
                return $item;
              
        
            });;
        return $this->fetch('index', ['data'=>$data]);
    }
    
    /**
     * @return array|mixed|string
     * 添加供应商
     */
    public function add(Common $common)
    {
        if ($data=Request::instance()->post()) {
            
            $res=db('agency')->insertGetId([
                'agency_name'=>$data['agency_name'],
                'rangess'=>$data['range'],
                'province'=>$data['province'],
                'city'=>$data['citys'],
                'area'=>$data['area'],
                'business'=>$data['business'],
                'product_notes'=>$data['product_notes'],
                'belonging_to'=>$data['belonging_to'],
                'scope_business'=>$data['scope_business'],
                'contacts'=>$data['contacts'],
                'telephone'=>$data['telephone'],
                'account_type'=>$data['account_type'],
                'collection_name'=>$data['collection_name'],
                'collection_number'=>$data['collection_number'],
                'bank'=>$data['bank'],
                'bank_of_deposit'=>$data['bank_of_deposit'],
                'state'=>$data['state'],
                'creation_time'=>time(),
                'random_number'=>date('Ymdhi') . substr(implode(NULL, array_map('ord', str_split(substr(uniqid(), 7, 13), 1))), 0, 2),
            ]);
            if($res===false){
                return json_data([], 300, '添加失败');
            }
            return json_data([], 200, '添加成功');
        }
        $supply=$common->supply();
        
       $list =db('city')->whereIn('city_id',[241,239,262,172,501,375,202])->select();
        return $this->fetch('add', ['data'=>$data,'city'=>$list,'supply'=>$supply]);
    }
    
    /**
     * @return array|mixed|string
     * 编辑供应商  Approval
     */
    public function edit($agency_id,Common $common,Approval $approval)
    {
        if ($data = Request::instance()->post()) {
        
            $res = db('agency')->where(['agency_id' => $data['agency_id']])->update([
                'agency_name'=>$data['agency_name'],
                'rangess'=>$data['range'],
                'province'=>$data['province'],
                'city'=>$data['citys'],
                'area'=>$data['area'],
                'business'=>$data['business'],
                'product_notes'=>$data['product_notes'],
                'belonging_to'=>$data['belonging_to'],
                'scope_business'=>$data['scope_business'],
                'contacts'=>$data['contacts'],
                'telephone'=>$data['telephone'],
                'account_type'=>$data['account_type'],
                'collection_name'=>$data['collection_name'],
                'collection_number'=>$data['collection_number'],
                'bank'=>$data['bank'],
                'bank_of_deposit'=>$data['bank_of_deposit'],
                'state'=>$data['state'],
            ]);
            if($res===false){
                return json_data([], 300, '编辑失败');
            }
            return json_data([], 200, '编辑成功');
        
        }else{
            if(!$agency_id){
                $this->error('请刷新后重试');
            }
            $data=db('agency')
                ->join('province p','agency.province=p.province_id','left')
                ->join('city c','agency.city=c.city_id','left')
                ->join('county y','agency.area=y.county_id','left')
                ->field('agency.*,p.province_id,p.province,p.country_id,c.*,y.*')
                ->where(['agency_id'=>$agency_id])
                ->find();
        
            if(!$data){
                $this->error('不存在或已删除');
            }
            $supply=$common->supply();
            $data['business']=array_values($approval->filter_by_value($common->supply(), 'id', $data['business']));
           $list =db('city')->whereIn('city_id',[241,239,262,172,501,375,202])->select();
            return $this->fetch('edit',['data'=>$data,'city'=>$list,'supply'=>$supply]);
        }
    }
    
    /**
     * 取消
     */
    public function del()
    {
        $store_id=Request::instance()->post();
        
        $res=false;
         if ($store_id) {
            $res=db('agency')->where(['agency_id'=>$store_id['agency_id']])->update(['state'=>0]);
        }
        if ($res !== false) {
            return json_data([], 200, '删除成功');
        }
        return json_data([], 300, '删除失败');
    }
    
    
}