<?php
/**
 * Created by muniao.
 * User: muniao
 * Date: 2018/8/9
 * Time: 11:22
 */

namespace app\index\controller;


use think\Request;
use think\Cache;
use foxyZeng\huyi\HuYiSMS;
use think\Exception;

class Income extends Backend
{
    /**
     * 订单列表
     */
    public function index()
    {

        $list = db('income')->paginate(20)
            ->each(function($item, $key){
                $starttime=strtotime(date('Y-m', strtotime($item['uptime'])) . '-01 00:00:00');
                $enttime=strtotime(date('Y-m', strtotime($item['uptime'])) . '-' . date('t', time()) . ' 23:59:59');
                $item['ss']=db('xian')->where(['personal_id' => $item['personal_id'],'start'=>1,'crtime'=>['between',[$starttime,$enttime]]])->sum('qian');

                $item['personal_ids']=db('personal')->where('personal_id',$item['personal_id'])->value('username');
                if($item['grade']==0){
                    $item['grade']='普通合伙人';
                }elseif( $item['grade']==1){
                    $item['grade']='高级合伙人';
                }else{
                    $item['grade'] ='钻石合伙人';
                }
                $item['sheng']=$item['profit']-$item['ss'];
                return $item;
        });
            return $this->fetch('index', ['list' => $list]);
    }

    public function ti()
    {
        $list = db('ti')->paginate(20)
            ->each(function($item, $key){
                if($item['title']==0){
                    $item['title']='普通合伙人';
                }elseif($item['title']==1){
                    $item['title']='高级合伙人';
                }else{
                    $item['title']='钻石合伙人';
                }

                return $item;
            });

        return $this->fetch('ti', ['list' => $list]);
    }
    /**
     * @return array|mixed|string
     * 添加短信
     */
    public function add(){
        if($data =Request::instance()->post()){

            $user = [
                'title' => $data['title'],
                'bi' => $data['bi'],
                'yong' => $data['yong'],
            ];

            $res = db('ti')->insertGetId($user);
            if($res===false){
                return json_data([], 300, '添加失败');
            }
            return json_data([], 200, '添加成功');
        }

        return $this->fetch('add',['data'=>$data]);
    }
    /**
     * @return array|mixed|string
     * 编辑短信
     */
    public function edit($ti = 0)
    {
        if ($data = Request::instance()->post()) {

            $user = [
                'title' => $data['title'],
                'bi' => $data['bi'],
                'yong' => $data['yong'],
            ];

            $res = db('ti')->where(['ti' => $data['ti']])->update($user);
            if ($res === false) {
                return json_data([], 300, '编辑失败');
            }
            return json_data([], 200, '编辑成功');
        }

        if (!$ti) {
            $this->error('请刷新后重试');
        }
        $res = db('ti')
            ->where(['ti' => $ti])
            ->find();


        return $this->fetch('edit', ['data' => $res]);
    }
    /**
     * 取消
     */
    public function del()
    {
        $store_id = Request::instance()->post();

        $res = false;
        if ($store_id) {
            $res = db('ti')->where(['ti' => $store_id['ti']])->delete();
        }
        if ($res !== false) {
            return json_data([], 200, '删除成功');
        }
        return json_data([], 300, '删除失败');
    }
    public function info()
    {
        $data=Request::instance()->get();

        $model=db('xian')
            ->field('xian.*,pe.mobile,pe.username')
            ->join('personal pe','xian.personal_id=pe.personal_id','left')
            ->where(['xian.personal_id'=>$data['personal_id'],'xian.start'=>1]);
        $list=$model->order('xian.crtime desc')
            ->paginate(10)
            ->each(function($item, $key){
                if($item['admin_id']){
                    $item['admin_id']=db('admin')->where('admin_id',$item['admin_id'])->value('username');

                }

                return $item;
            });

        $this->assign('list', $list);

        return $this->fetch('info',['list'=>$list,'data'=>$data]);
    }
    public function xian()
    {
        $data=Request::instance()->get();

        $model=db('xian')
            ->field('xian.*,pe.mobile,pe.username')

        ->join('personal pe','xian.personal_id=pe.personal_id','left');
        if (isset($data['status']) && $data['status'] != '') {
            $model->where(['xian.start' => $data['status']]);
        }
        if (isset($data['start_time']) && $data['start_time'] != '') {
            $model->where(['xian.crtime' => ['>=', strtotime($data['start_time'])]]);
        }
        if (isset($data['end_time']) && $data['end_time'] != '') {
            $model->where(['xian.crtime' => ['<=', strtotime($data['end_time']) + 24 * 3600]]);
        }
        if (isset($data['username']) && $data['username'] != '') {
            $title = db('personal')->where(['username' => ['like', "%{$data['username']}%"]])->field('personal_id')->select();

            $s = '';
            foreach ($title as $key => $val) {
                $s .= $val["personal_id"] . ',';
            }
            $s=substr($s,0,-1);

            $model->where(['xian.personal_id' => ['in', $s]]);
        };
        if (isset($data['mobile']) && $data['mobile'] != '') {
            $title = db('personal')->where(['mobile' => ['like', "%{$data['mobile']}%"]])->field('personal_id')->select();

            $s = '';
            foreach ($title as $key => $val) {
                $s .= $val["personal_id"] . ',';
            }
            $s=substr($s,0,-1);

            $model->where(['xian.personal_id' => ['in', $s]]);
        };
        $list=$model->order('xian.crtime desc')
            ->paginate(20)
            ->each(function($item, $key){
                if($item['admin_id']){
                    $item['admin_id']=db('admin')->where('admin_id',$item['admin_id'])->value('username');

                }

                return $item;
            });

        return $this->fetch('xian',['list'=>$list,'data'=>$data]);
    }
    public function tong()
    {
        $user = $this->check_authority();
        $personal_id=Request::instance()->post('xian_id');

        if($personal_id){
            $yu=db('xian')->where(['xian_id'=>$personal_id])->find();
            $zong=db('personal')->where(['personal_id'=>$yu['personal_id']])->value('sheng');
            $sheng=$zong-$yu['qian'];
            if($sheng<0){
                return json_data([],300,'金额超限');
            }
            try {
               db('xian')->where(['xian_id'=>$personal_id])->update(['start'=>1,'admin_id'=>$user['admin_id']]);
               db('personal')->where(['personal_id' => $yu['personal_id']])->update(['sheng' => $sheng]);
                db()->commit();
                return json_data([], 200, '成功');
            } catch (Exception $e) {
                db()->rollback();
                return json_data([], 300, $e->getMessage());
            }
        }


        return json_data([],300,'失败');
    }
}