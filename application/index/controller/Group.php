<?php
/**
 * Created by muniao.
 * admin: muniao
 * Date: 2018/7/27
 * Time: 9:20
 */

namespace app\index\controller;


use think\Exception;
use think\Request;

class Group extends Backend
{
    /**
     * 用户列表
     */
    public function index(){
        $data=Request::instance()->get();

        $model=db('personal');
        if(isset($data['start_time']) && $data['start_time']!=''){
            $model->where(['personal.created_time'=>['>=',strtotime($data['start_time'])]]);
        }
        if(isset($data['end_time']) && $data['end_time']!=''){
            $model->where(['personal.created_time'=>['<=',strtotime($data['end_time'])+24*3600]]);
        }
        if(isset($data['username']) && $data['username']!=''){
            $model->where(['personal.username'=>['like',"%{$data['username']}%"]]);
        }
        if(isset($data['mobile']) && $data['mobile']!=''){
            $model->where(['personal.mobile'=>['like',"%{$data['mobile']}%"]]);
        }
            $model->where(['personal.group'=>1,'personal.status'=>['<>',2]]);

        $list=$model->order('personal.created_time desc')
            ->paginate(10)
            ->each(function($item, $key){
                if(empty($item['num'])){
                    $item['num']="暂无";
                }else{
                    $item['num']=db('admin')->where('admin_id',$item['num'])->value('username');
                }
                return $item;
            });

        $this->assign('list', $list);

        return $this->fetch('index',['list'=>$list,'data'=>$data]);
    }
    /**
     * 修改
     */
    public function edit($personal_id=0)
    {
        if ($data = Request::instance()->post()) {
            $user = [
                'username' => $data['username'],
                'mobile'=>$data['mobile'],
                'sex' => $data['sex'],
                'avatar' => $data['logo'],
                'email' => $data['email'],
            ];

            $res = db('personal')->where(['personal_id' => $data['personal_id']])->update($user);
            if ($res !== false) {
                return json_data([], 200, '编辑成功');
            }
            return json_data([], 300, '编辑失败');
        }else{
            if(!$personal_id){
                $this->error('请刷新后重试');
            }
            $data=db('personal')->where(['personal_id'=>$personal_id,])->find();

            if(!$data){
                $this->error('用户不存在或已删除');
            }
            return $this->fetch('edit',['data'=>$data]);
        }
    }
    /**
     * 添加
     */
    public function add()
    {
        if ($data = Request::instance()->post()) {
            $admin=$this->check_authority();

            $user = [
                'username' => $data['username'],
                'password'=>encrypt($data['password']),
                'mobile'=>$data['mobile'],
                'sex' => $data['sex'],
                'avatar' => $data['logo'],
                'email' => $data['email'],
                'created_time'=>time(),
                'num'=>$admin['admin_id'],
                'group'=>1
            ];

            $res = db('personal')->insertGetId($user);
            if ($res !== false) {
                $this->log('添加了合伙人',$data['username']);
                return json_data([], 200, '添加成功');
            }
            return json_data([], 300, '添加失败');
        }
        return $this->fetch('add');

    }
    /**
     * 删除用户
     */
    public function del(){
        $personal_id=Request::instance()->post('personal_id');
        $res=false;

        if($personal_id){
            $res=db('personal')->where(['personal_id'=>$personal_id])->delete();
        }
        if($res){
            return json_data([],200,'删除成功');

        }
        return json_data([],300,'删除失败');
    }
    /**
     * 启用 禁用
     */
    public function disable(){
        $personal_id=Request::instance()->post('personal_id');
        $status=Request::instance()->post('status');

        if($personal_id && ($status==0 || $status==1)){
            $res=db('personal')->where(['personal_id'=>$personal_id])->update(['status'=>$status]);
            if($res!==false){
                $this->result([],200,'操作成功');
            }
        }
        $this->result([],300,'请刷新后重试');
    }



    private  function log($content,$ord_id)
    {
        $user = $this->check_authority();
        $user=[
            'admin_id'=>$user['admin_id'],
            'created_at'=>time(),
            'content'=>$content,
            'ord_id'=>$ord_id,

        ];

        db('log')->insertGetId($user);

    }



}