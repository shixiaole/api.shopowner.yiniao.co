<?php
/**
 * Created by muniao.
 * User: muniao
 * Date: 2018/8/9
 * Time: 11:22
 */

namespace app\index\controller;


use think\Request;
use think\Cache;
use think\Db;

use phpcvs\Csv;

class Cancel extends Backend
{
    /**
     * 订单列表
     */
    public function index()
    {
        $data=Request::instance()->get();
        $model = db('cancel');
        if (isset($data['start_time']) && $data['start_time'] != '') {
            $start = strtotime(date('Y-m-d 00:00:00',strtotime($data['start_time'])));
            $end   = strtotime(date('Y-m-d 23:59:59',strtotime($data['start_time'])));
            $model->where(['time' => ['between', [$start, $end]]]);
        }

        $list = $model->order('time desc')
            ->paginate(30);



        return $this->fetch('index', ['list' => $list]);
    }

    }