<?php
/**
 * Created by muniao.
 * User: muniao
 * Date: 2018/8/9
 * Time: 11:22
 */

namespace app\index\controller;


use think\Request;
use think\Cache;


use think\paginator\driver\Bootstrap;


class Master extends Backend
{
    /**
     * 订单列表
     */
    public function index()
    {
        $data=Request::instance()->get();
        $curpage = input('page') ? input('page') : 1;//当前第x页，\
        $type=3;
        if(!empty($data['type'])){
            if($data['type']==1){
                $data['start_time']=strtotime($data['start_time']);
                $data['end_time']=$data['start_time'];
                $starttime=$data['start_time'];
                $enttime=$data['end_time'];
                $type=3;
            }elseif ($data['type']==2){

                $data['start_time']=strtotime($data['start_time']);
                $data['end_time']=strtotime($data['end_time']);
                $starttime=$data['start_time'];
                $enttime=$data['end_time'];
                $type=2;
            }
        }else{
            $starttime=time();
            $enttime=time();
        }

        $data['page']=$curpage;

        $results = send_post("http://www.yiniaoo2o.com/shopowner/count/index", $data);

        $results = json_decode($results,true);

        if($results['code']==20000){
            $count=$results['count'];
            $results=$results['data'];

        }else{
            $results=[];
            $count=0;
        }
        $data = $results;   //要分页的数组
        $rows = 1;//每页显示几条记录
        $dataTo = array();
        $dataTo = array_chunk($data,$rows);

        $showdata = array();
//var_dump($dataTo);die;
//        if($dataTo){
//            $showdata = $dataTo[$curpage-1];
//        }else{
//            $showdata = null;
//        }

        $p = Bootstrap::make($showdata, $rows, $curpage,$count/10, false, [
            'var_page' => 'page',
            'path'     => url('index'),//这里根据需要修改url
            'query'    => $results,
            'fragment' => '',
        ]);
        $p->appends($_GET);
//var_dump($starttime);die;

        return $this->fetch('index', ['list' => $results,'plist'=>$p,'plistpage'=>$p->render(),'type'=>$type,'starttime'=>$starttime,'enttime'=>$enttime]);
    }




    public function cha()
    {
        $user_id=Request::instance()->post('user_id');
        if(!$user_id){
            $this->result([],300,'请刷新后重试');
        }
        $data['user_id']=$user_id;
        $results = send_post(UIP_SRC ."/shopowner/count/location", $data);
        $results = json_decode($results,true);
        if($results['code']==20000){

            return json_data(['id'=>$user_id],200);
        }
        return json_data([],300,'暂无数据');
    }
    public function map($user_id){
        $data['user_id']=$user_id;
        $results = send_post(UIP_SRC ."/shopowner/count/location", $data);
        $results = json_decode($results,true);
        foreach ($results['data'] as $k=>$v){
            $re[$k]['name']='';
            $re[$k]['xy']=[$v['lng'],$v['lat']];
        }

        return $this->fetch('map',['admin'=>$re]);

    }

}
