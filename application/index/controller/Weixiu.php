<?php
/**
 * Created by muniao.
 * User: muniao
 * Date: 2018/7/27
 * Time: 9:20
 */

namespace app\index\controller;


use app\common\model\Version;
use think\Exception;
use think\Request;

class Weixiu extends Backend
{
    /**
     * 维修劵列表
     */
    public function index(){
        $data=Request::instance()->get();

        $model=db('weixiu');
        $list=$model->order('weixiu.weixiu_id desc')
            ->paginate(20);


        return $this->fetch('index',['list'=>$list,'data'=>$data]);
    }
    /**
     * 修改
     */
    public function edit($weixiu_id=0)
    {
        if ($data = Request::instance()->post()) {
            $user = [
                'jine' => $data['jine'],
                'leibie'=>$data['leibie'],
                'tiaojan'=>$data['tiaojan'],
                'lalst_time' => strtotime($data['lalst_time']),
                'logo' => $data['logo'],


            ];

            $res = db('weixiu')->where(['weixiu_id' => $data['weixiu_id']])->update($user);

            if ($res) {

                return json_data([], 200, '编辑成功');
            }
            return json_data([], 300, '编辑失败');
        }else{
            if(!$weixiu_id){
                $this->error('请刷新后重试');
            }
            $data=db('weixiu')->where(['weixiu_id'=>$weixiu_id,])->find();

            if(!$data){
                $this->error('用户不存在或已删除');
            }
            return $this->fetch('edit',['data'=>$data]);
        }
    }
    /**
 * 添加
 */
    public function add()
    {
        if ($data = Request::instance()->post()) {
            $user = [
                'jine' => $data['jine'],
                'leibie'=>$data['leibie'],
                'tiaojan'=>$data['tiaojan'],
                'lalst_time' => strtotime($data['lalst_time']),
                'logo' => $data['logo'],
                'status' => 0,
            ];


            $res = db('weixiu')->insertGetId($user);

            if ($res !== false) {
                return json_data([], 200, '添加成功');
            }
            return json_data([], 300, '添加失败');
        }
            return $this->fetch('add');

    }
    /**
     * 删除用户
     */
    public function del(){
        $weixiu_id=Request::instance()->post('weixiu_id');
        $res=false;

        if($weixiu_id){

            $res=db('weixiu')->where(['weixiu_id'=>$weixiu_id])->update(['status'=>2]);
        }
        if($res){

            return json_data([],200,'删除成功');

        }
        return json_data([],300,'删除失败');
    }
    /**
     * 启用 禁用
     */
    public function disable(){
        $weixiu_id=Request::instance()->post('weixiu_id');
        $status=Request::instance()->post('status');

        if($weixiu_id && ($status==0 || $status==1)){
            $res=db('weixiu')->where(['weixiu_id'=>$weixiu_id])->update(['status'=>$status]);
            if($res!==false){
                $this->result([],200,'操作成功');
            }
        }
        $this->result([],300,'请刷新后重试');
    }






}