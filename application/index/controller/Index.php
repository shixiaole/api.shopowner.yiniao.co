<?php

namespace app\index\controller;


class Index extends Backend
{
    public function index()
    {
        $u=$this->check_authority();
        
        $S='';
        if (!is_array($u['ss'])) {
            if ($u['ss'] == "*") {
                $S='<dl id="menu-admin">
            <dt><i class="Hui-iconfont">&#xe687;</i> 订单管理<i class="Hui-iconfont menu_dropdown-arrow">&#xe6d5;</i></dt>
            <dd>
               <ul>
                  <li><a data-href="' . url('index/Statistics/index') . '?type=10" data-title="订单筛选" href="javascript:void(0)">订单筛选</a></li>
                    <li><a data-href="' . url('index/order/add') . '" data-title="订单列表"  href="javascript:void(0)">订单录入</a></li>
                    <li><a data-href="' . url('index/order/index') . '?type=0" data-title="待指派" href="javascript:void(0)">待指派</a></li>
                    <li><a data-href="' . url('index/order/index') . '?type=1" data-title="待接单" href="javascript:void(0)">待接单</a></li>
                    <li><a data-href="' . url('index/order/index') . '?type=2" data-title="待上门" href="javascript:void(0)">待上门</a></li>
                    <li><a data-href="' . url('index/order/index') . '?type=3" data-title="待签约" href="javascript:void(0)">待签约</a></li>
                   <li><a data-href="' . url('index/order/index') . '?type=5" data-title="施工中" href="javascript:void(0)">施工中</a></li>
                    <li><a data-href="' . url('index/order/index') . '?type=7" data-title="已完工" href="javascript:void(0)">已完工</a></li>
                    <li><a data-href="' . url('index/order/index') . '?type=6" data-title="已完成" href="javascript:void(0)">已完成</a></li>
                    <li><a data-href="' . url('index/order/index') . '?type=8" data-title="已取消" href="javascript:void(0)">已取消</a></li>
                        <li><a data-href="' . url('index/order/index') . '?type=9" data-title="退单" href="javascript:void(0)">退单</a></li>
                    <li><a data-href="' . url('index/order/index') . '?type=10" data-title="无效订单" href="javascript:void(0)">无效订单</a></li>
                  
                </ul>
            </dd>
        </dl>
        <dl id="menu-rward">
            <dt><i class="glyphicon glyphicon-indent-left">&#xe687;</i> 店铺管理<i class="Hui-iconfont menu_dropdown-arrow">&#xe6d5;</i></dt>
            <dd>
                <ul>
                    <li><a data-href="' . url('index/need/index') . '" data-title="店铺列表" href="javascript:void(0)">店铺列表</a></li>
                </ul>
            </dd>
        </dl>
        <dl id="menu-tongji">
            <dt><i class="Hui-iconfont">&#xe62c;</i> 店长管理<i class="Hui-iconfont menu_dropdown-arrow">&#xe6d5;</i></dt>
            <dd>
                <ul>
                    <li><a data-href="' . url('index/user/index') . '" data-title="店长列表" href="javascript:void(0)">店长列表</a></li>
                      <li><a data-href="' . url('index/StoreManager/index') . '" data-title="店长时长" href="javascript:void(0)">店长时长</a></li>
                </ul>
            </dd>
        </dl>
         <dl id="menu-tongji">
            <dt><i class="Hui-iconfont">&#xe62c;</i> 消息管理<i class="Hui-iconfont menu_dropdown-arrow">&#xe6d5;</i></dt>
            <dd>
                <ul>
                    <li><a data-href="' . url('index/message/index') . '" data-title="消息推送" href="javascript:void(0)">消息推送</a></li>
                  </ul>
            </dd>
        </dl>
        <dl id="menu-yeji">
            <dt><i class="glyphicon glyphicon-th"></i>业绩报表<i class="Hui-iconfont menu_dropdown-arrow">&#xe6d5;</i></dt>
            <dd>
                <ul>
                    <li><a data-href="' . url('index/achievement/index') . '" data-title="本月业绩" href="javascript:void(0)">本月业绩</a></li>
                    <li><a data-href="' . url('index/Amount/index') . '" data-title="收入统计" href="javascript:void(0)">收入统计</a></li>

                </ul>
            </dd>
        </dl>
        <dl id="menu-question">
            <dt><i class="Hui-iconfont">&#xe692;</i> 报价管理<i class="Hui-iconfont menu_dropdown-arrow">&#xe6d5;</i></dt>
            <dd>
                <ul>
                        <li><a data-href="' . url('index/product/index') . '" data-title="套餐列表" href="javascript:void(0)">套餐列表</a></li>
                    <li><a data-href="' . url('index/detailed/index') . '" data-title="报价列表" href="javascript:void(0)">报价列表</a></li>
                    <li><a data-href="' . url('index/stock/index') . '" data-title="常规材料目录表" href="javascript:void(0)">常规材料目录表</a></li>
                    <li><a data-href="' . url('index/scheme/index') . '" data-title="方案大师列表" href="javascript:void(0)">方案大师列表</a></li>
                </ul>
            </dd>
        </dl>
      
<dl id="menu-config">
            <dt><i class="Hui-iconfont">&#xe62e;</i> 设置<i class="Hui-iconfont menu_dropdown-arrow">&#xe6d5;</i></dt>
            <dd>
                <ul>
                    <li><a data-href="' . url('index/config/tel') . '" data-title="标签设置" href="javascript:void(0)">标签设置</a></li>
                    <li><a data-href="' . url('index/Warranty/index') . '" data-title="标签设置" href="javascript:void(0)">质保设置</a></li>
		     <li><a data-href="' . url('index/Residential/index') . '" data-title="物业管理" href="javascript:void(0)">物业管理</a></li>
                    <li><a data-href="' . url('index/channel/index') . '" data-title="渠道设置" href="javascript:void(0)">渠道设置</a></li>    
		    <li><a data-href="' . url('index/config/category') . '" data-title="问题修改" href="javascript:void(0)">问题修改</a></li>
                    <li><a data-href="' . url('index/config/index') . '" data-title="单位修改" href="javascript:void(0)">单位修改</a></li>
                    <li><a data-href="' . url('index/config/remote') . '" data-title="沟通记录" href="javascript:void(0)">沟通记录</a></li>
                    <li><a data-href="' . url('index/Agency/index') . '" data-title="主材代购公司" href="javascript:void(0)">主材代购公司</a></li>
                    <li><a data-href="' . url('index/about/index') . '" data-title="关于我们" href="javascript:void(0)">关于我们</a></li>

                </ul>
            </dd>
        </dl>
        <dl id="menu-cak">
            <dt><i class="glyphicon glyphicon-wrench"></i>工具管理<i class="Hui-iconfont menu_dropdown-arrow">&#xe6d5;</i></dt>
            <dd>
                <ul>
                    <li><a data-href="' . url('index/article/tel') . '" data-title="工具列表" href="javascript:void(0)">工具列表</a></li>
                    <li><a data-href="' . url('index/article/index') . '?type=0" data-title="内容列表" href="javascript:void(0)">内容列表</a></li>
                 
                    </ul>
            </dd>
        </dl>
        <dl id="menu-cak">
            <dt><i class="glyphicon glyphicon-th-list"></i>海报管理<i class="Hui-iconfont menu_dropdown-arrow">&#xe6d5;</i></dt>
            <dd>
                <ul>
                    <li><a data-href="' . url('index/article/index') . '?type=1" data-title="海报列表" href="javascript:void(0)">海报列表</a></li>
                 
                    </ul>
            </dd>
        </dl>
                <dl id="menu-cak">
            <dt><i class="glyphicon glyphicon-phone-alt"></i>合伙人端管理<i class="Hui-iconfont menu_dropdown-arrow">&#xe6d5;</i></dt>
            <dd>
                <ul>
                    <li><a data-href="' . url('index/group/index') . '" data-title="集团合伙人" href="javascript:void(0)">集团合伙人</a></li>
                    <li><a data-href="' . url('index/personal/index') . '" data-title="个人合伙人" href="javascript:void(0)">个人合伙人</a></li>
                    <li><a data-href="' . url('index/recommend/index') . '" data-title="推荐订单" href="javascript:void(0)">推荐订单</a></li>
                     <li><a data-href="' . url('index/income/ti') . '" data-title="提成比例" href="javascript:void(0)">提成比例</a></li>
                    <li><a data-href="' . url('index/income/index') . '" data-title="明细" href="javascript:void(0)">明细</a></li>
                    <li><a data-href="' . url('index/income/xian') . '" data-title="提现申请" href="javascript:void(0)">提现申请</a></li>
                     <li><a data-href="' . url('index/customer/index') . '" data-title="客户预约列表" href="javascript:void(0)">客户预约列表</a></li>
                     <li><a data-href="' . url('index/chou/index') . '" data-title="客户预约列表" href="javascript:void(0)">抽奖列表</a></li>
                    </ul>
            </dd>
        </dl>
         
        <dl id="menu-cak">
            <dt><i class="glyphicon glyphicon-equalizer"></i>客户端管理<i class="Hui-iconfont menu_dropdown-arrow">&#xe6d5;</i></dt>
            <dd>
                <ul>
                  
                   <li><a data-href="' . url('index/customers/index') . '" data-title="客户列表" href="javascript:void(0)">客户列表</a></li>
                   <li><a data-href="' . url('index/kehu/index') . '" data-title="下单列表" href="javascript:void(0)">下单列表</a></li>
                    <li><a data-href="' . url('index/youhui/index') . '" data-title="优惠券" href="javascript:void(0)">代金卷</a></li>
                   <li><a data-href="' . url('index/Discount/index') . '" data-title="优惠券领取列表" href="javascript:void(0)">优惠券领取列表</a></li>
                    <li><a data-href="' . url('index/planting/index') . '?type=0" data-title="轮播" href="javascript:void(0)">轮播</a></li>
                    <li><a data-href="' . url('index/planting/index') . '?type=1" data-title="工具" href="javascript:void(0)">工具</a></li>
                     <li><a data-href="' . url('index/lie/index') . '?type=1" data-title="工具列表" href="javascript:void(0)">工具列表</a></li>
                     
                     <li><a data-href="' . url('index/planting/index') . '?type=3" data-title="首页菜单" href="javascript:void(0)">热门推荐</a></li>
                    <li><a data-href="' . url('index/lie/index') . '?type=3 " data-title="菜单列表" href="javascript:void(0)">热门推荐列表</a></li>
                <li><a data-href="' . url('index/coupon/index') . '" data-title="地址管理" href="javascript:void(0)">地址管理</a></li>
                    
                    </ul>
            </dd>
        </dl>
	      <dl id="menu-question">
            <dt><i class="Hui-iconfont">&#xe692;</i> 双十一活动<i class="Hui-iconfont menu_dropdown-arrow">&#xe6d5;</i></dt>
            <dd>
                <ul>
                        <li><a data-href="' . url('index/Activity/export') . '" data-title="用户数据汇总" href="javascript:void(0)">用户数据汇总</a></li>
			 <li><a data-href="' . url('index/Activity/detail') . '" data-title="单用户数据" href="javascript:void(0)">单用户数据</a></li>
                  
                </ul>
            </dd>
        </dl>
         <dl id="menu-cak">
            <dt><i class="glyphicon glyphicon-barcode"></i>充值管理<i class="Hui-iconfont menu_dropdown-arrow">&#xe6d5;</i></dt>
            <dd>
                <ul>
                   <li><a data-href="' . url('index/jine/index') . '" data-title="优惠券列表" href="javascript:void(0)">充值金额</a></li>
                   <li><a data-href="' . url('index/recharge/index') . '?type=1" data-title="充值记录" href="javascript:void(0)">充值记录</a></li>
                       <li><a data-href="' . url('index/recharge/index') . '?type=2" data-title="支付记录" href="javascript:void(0)">支付记录</a></li>
                   <li><a data-href="' . url('index/recharge/index') . '?type=3" data-title="提现记录" href="javascript:void(0)">提现记录</a></li>
                    </ul>
            </dd>
        </dl>
        <dl id="menu-cak">
            <dt><i class="glyphicon glyphicon-pencil"></i>意见与建议<i class="Hui-iconfont menu_dropdown-arrow">&#xe6d5;</i></dt>
            <dd>
                <ul>
                  
                   <li><a data-href="' . url('index/opinion/index') . '" data-title="意见" href="javascript:void(0)">意见</a></li>
                    </ul>
            </dd>
        </dl>
       <dl id="menu-cak">
            <dt><i class="glyphicon glyphicon-list-alt"></i>统计管理<i class="Hui-iconfont menu_dropdown-arrow">&#xe6d5;</i></dt>
            <dd>
                <ul>
                 <li><a data-href="' . url('index/Statistics/order_list') . '" data-title="签约原始报表" href="javascript:void(0)">签约原始报表</a></li>
                 <li><a data-href="' . url('index/Statistics/orderOriginal') . '" data-title="订单原始数据表" href="javascript:void(0)">订单原始数据表</a></li>
                 <li><a data-href="' . url('index/Statistics/associatedCommunities') . '" data-title="社区关联情况分析" href="javascript:void(0)">社区关联情况分析</a></li>
                 <li><a data-href="' . url('index/Statistics/cityStoreManagerReceivesOrders') . '" data-title="城市店长接单转化" href="javascript:void(0)">城市店长接单转化</a></li>
                    <li><a data-href="' . url('index/Statistics/housekeeperSettlement') . '" data-title="管家结算数据统计" href="javascript:void(0)">管家结算数据统计</a></li>
                  
                <li><a data-href="' . url('index/Cancel/index') . '" data-title="店长行为统计" href="javascript:void(0)">店长个人飞单原因</a></li>
               

               <li><a data-href="' . url('index/Statistics/zhuan') . '" data-title="订单转化数据统计" href="javascript:void(0)">订单转化数据统计</a></li>
                  
                    <li><a data-href="' . url('index/Statistics/qu') . '" data-title="渠道上门转化报表" href="javascript:void(0)">渠道上门转化报表</a></li>
                    <li><a data-href="' . url('index/Statistics/sku') . '" data-title="渠道SKU数据统计" href="javascript:void(0)">渠道SKU数据统计</a></li>
                    <li><a data-href="' . url('index/Statistics/xiao') . '" data-title="取消原因数据统计" href="javascript:void(0)">取消原因数据统计</a></li>
                 
                    </ul>
            </dd>
        </dl>
          <dl id="menu-rule">
            <dt><i class="glyphicon glyphicon-lock"></i>权限管理<i class="Hui-iconfont menu_dropdown-arrow">&#xe6d5;</i></dt>
            <dd>
                <ul>

                   <li><a data-href="' . url('index/role/category') . '" data-title="角色组" href="javascript:void(0)">角色组</a></li>
                    <li><a data-href="' . url('index/role/remote_index') . '" data-title="菜单规则" href="javascript:void(0)">菜单规则</a></li>

                </ul>
            </dd>
        </dl>
        <dl id="menu-log">
            <dt><i class="glyphicon glyphicon-list-alt"></i>操作日志<i class="Hui-iconfont menu_dropdown-arrow">&#xe6d5;</i></dt>
            <dd>
                <ul>
                    <li><a data-href=" ' . url('index/config/log') . '" data-title="日志列表" href="javascript:void(0)">日志列表</a></li>
                </ul>
            </dd>
        </dl>';
            }
        } else{
            
            $pl=[];
            foreach ($u['ks'] as $item=>$value) {
                $i=[];
                foreach ($u['ss'] as $k=>$v) {
                    if ($value['uid'] == $v['pId'] && $v['none'] == 0) {
                        $i[$k]['co']       =$v['co'];
                        $i[$k]['type_id']  =$v['type_id'];
                        $i[$k]['node_name']=$v['node_name'];
                        $i[$k]['pId']      =$v['pId'];
                        unset($u['ss'][$k]);
                    }
                    
                }
                $pl[$item]['title']=$value['node_name'];
                $pl[$item]['data'] =$i;
                unset($i);
            }
            
            
            foreach ($pl as $k) {
                $p='';
                foreach ($k['data'] as $k1) {
                    
                    
                    $p.=' <li><a data-href="' . url('index/' . $k1['co'] . '') . '?type=' . $k1['type_id'] . '" data-title="' . $k1['node_name'] . '" href="javascript:void(0)">' . $k1['node_name'] . '</a></li>';
                    
                }
                $S.=' <dl id="menu-admin">
            <dt><i class="Hui-iconfont">&#xe687;</i>' . $k['title'] . '<i class="Hui-iconfont menu_dropdown-arrow">&#xe6d5;</i></dt>
            <dd>
                <ul>
                  ' . $p . ' 
                 </ul>
                  </dd>
                </dl>';
                
                
            }
            
        }
        
        
        return $this->fetch('index', ['v'=>$S,'loge'=>$this->admin['logo'],'username'=>$this->admin['username']]);
    }
    
    public function welcome()
    {
        return $this->fetch('', ['time'=>date('Y-m-d H:i:s')]);
    }
    
    
}
