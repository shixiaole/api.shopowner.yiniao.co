<?php
/**
 * Created by PhpStorm.
 * User: Raytine
 * Date: 2022/3/11
 * Time: 17:39
 */


use think\queue\Job;

class Hello
{

    public function fire(Job $job,$data)
    {
        $config = \Kafka\ConsumerConfig::getInstance();

        $config->setMetadataRefreshIntervalMs(10000);

        $config->setMetadataBrokerList(config('kafka_server.host'));

        $config->setGroupId('defaultConsumerGroup');

        $config->setBrokerVersion('3.0.1');

        $config->setTopics(array('topic11'));

        $config->setOffsetReset('earliest');

        $consumer = new \Kafka\Consumer();

        $consumer->start(function ($topic, $part, $message) {

            var_dump($message);

        });
    }
}